package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.RegionGB;
import org.hi.framework.service.Manager;

public interface RegionGBManager extends Manager{
    
    public void saveRegionGB(RegionGB regionGB);

    public void removeRegionGBById(Integer id);

    public RegionGB getRegionGBById(Integer id);

    public List<RegionGB> getRegionGBList(PageInfo pageInfo);
    
    public void saveSecurityRegionGB(RegionGB regionGB);

    public void removeSecurityRegionGBById(Integer id);

    public RegionGB getSecurityRegionGBById(Integer id);

    public List<RegionGB> getSecurityRegionGBList(PageInfo pageInfo);    
}
