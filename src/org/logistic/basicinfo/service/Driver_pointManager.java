package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_point;
import org.hi.framework.service.Manager;

public interface Driver_pointManager extends Manager{
    
    public void saveDriver_point(Driver_point driver_point);

    public void removeDriver_pointById(Integer id);

    public Driver_point getDriver_pointById(Integer id);

    public List<Driver_point> getDriver_pointList(PageInfo pageInfo);
    
    public void saveSecurityDriver_point(Driver_point driver_point);

    public void removeSecurityDriver_pointById(Integer id);

    public Driver_point getSecurityDriver_pointById(Integer id);

    public List<Driver_point> getSecurityDriver_pointList(PageInfo pageInfo);    
}
