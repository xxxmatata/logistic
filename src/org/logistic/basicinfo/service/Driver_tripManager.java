package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_trip;
import org.hi.framework.service.Manager;

public interface Driver_tripManager extends Manager{
    
    public void saveDriver_trip(Driver_trip driver_trip);

    public void removeDriver_tripById(Integer id);

    public Driver_trip getDriver_tripById(Integer id);

    public List<Driver_trip> getDriver_tripList(PageInfo pageInfo);
    
    public void saveSecurityDriver_trip(Driver_trip driver_trip);

    public void removeSecurityDriver_tripById(Integer id);

    public Driver_trip getSecurityDriver_tripById(Integer id);

    public List<Driver_trip> getSecurityDriver_tripList(PageInfo pageInfo);    
}
