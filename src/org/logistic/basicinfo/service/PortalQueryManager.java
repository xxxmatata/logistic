package org.logistic.basicinfo.service;

import java.util.List;
import java.util.Map;

import org.hi.framework.web.PageInfoView;
import org.logistic.constant.SqlText;

public class PortalQueryManager extends CommQueryManager {
	public List<Map> loadDriverInfo(PageInfoView pageInfo){
		return this.query(SqlText.LOAD_DRIVER_INFO_ON_PORTAL, null, pageInfo);
	}
}
