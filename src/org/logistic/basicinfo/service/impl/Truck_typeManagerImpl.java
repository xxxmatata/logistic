package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Truck_type;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Truck_typeManager;

public class Truck_typeManagerImpl extends ManagerImpl implements Truck_typeManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveTruck_type(Truck_type truck_type){
    	saveObject(truck_type);
    
    }

    public void removeTruck_typeById(Integer id){
    	removeObjectById(id);
    	
    }

    public Truck_type getTruck_typeById(Integer id){
   		return (Truck_type) getObjectById(id);
    }

    public List<Truck_type> getTruck_typeList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityTruck_type(Truck_type truck_type){
    	saveObject(truck_type);
    
    }

    public void removeSecurityTruck_typeById(Integer id){
    	removeObjectById(id);
    	
    }

    public Truck_type getSecurityTruck_typeById(Integer id){
   		return (Truck_type) getObjectById(id);
    }

    public List<Truck_type> getSecurityTruck_typeList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
