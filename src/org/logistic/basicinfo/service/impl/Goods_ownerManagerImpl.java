package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Goods_owner;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Goods_ownerManager;

public class Goods_ownerManagerImpl extends ManagerImpl implements Goods_ownerManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveGoods_owner(Goods_owner goods_owner){
    	saveObject(goods_owner);
    
    }

    public void removeGoods_ownerById(Integer id){
    	removeObjectById(id);
    	
    }

    public Goods_owner getGoods_ownerById(Integer id){
   		return (Goods_owner) getObjectById(id);
    }

    public List<Goods_owner> getGoods_ownerList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityGoods_owner(Goods_owner goods_owner){
    	saveObject(goods_owner);
    
    }

    public void removeSecurityGoods_ownerById(Integer id){
    	removeObjectById(id);
    	
    }

    public Goods_owner getSecurityGoods_ownerById(Integer id){
   		return (Goods_owner) getObjectById(id);
    }

    public List<Goods_owner> getSecurityGoods_ownerList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
