package org.logistic.basicinfo.service.impl;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.security.context.UserContextHelper;
import org.logistic.basicinfo.model.Driver2goods;
import org.logistic.basicinfo.model.Driver2goods_log;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Goods_info;
import org.logistic.basicinfo.model.Truck_info;
import org.logistic.basicinfo.service.Driver2goodsManager;
import org.logistic.basicinfo.service.Driver2goods_logManager;
import org.logistic.basicinfo.service.Driver_infoManager;
import org.logistic.basicinfo.service.ExGoods_infoManager;
import org.logistic.basicinfo.service.Goods_infoManager;
import org.logistic.basicinfo.service.Truck_infoManager;

public class ExGoods_infoManagerImpl extends Goods_infoManagerImpl implements
		ExGoods_infoManager {

	/*
	 * 增加一条承运成交记录
	 * 
	 * @see
	 * org.logistic.basicinfo.service.ExGoods_infoManager#saveAddOrderGoods_info
	 * (org.logistic.basicinfo.model.Goods_info)
	 */
	public String saveAddOrderGoods_info(Goods_info goods_info) {

		Date date = new Date();
		Timestamp orderTime = new Timestamp(date.getTime());
		Driver_info driver = goods_info.getDriver();
		Truck_info truck = driver.getTruck_info();

		//增加承运成交记录
		Driver2goods driver2goods = new Driver2goods();
		

		
		driver2goods.setCreator(UserContextHelper.getUserContext().getUser());
		driver2goods.setD2gType(101601);
		driver2goods.setDriver(driver);
		driver2goods.setGoods_info(goods_info);
		driver2goods.setOpTime(orderTime);
		driver2goods.setVersion(0);
		

		List<Driver2goods> driver2goodss = goods_info.getDriver2goodss();

		
		driver2goodss.add(driver2goods);

		//记录承运成交日志
		Driver2goods_log log = new Driver2goods_log();
		org.springframework.beans.BeanUtils.copyProperties(driver2goods, log);
		log.setRemark(goods_info.getRemark());
		log.setId(null);

		List<Driver2goods_log> logs = goods_info.getDriver2goods_logs();
		logs.add(log);

		goods_info.setDriver2goodss(driver2goodss);
		goods_info.setDriver2goods_logs(logs);
		goods_info.setOrderTime(orderTime);

		//修改货物状态
		if (0 == goods_info.getNeedNum()) {
			goods_info.setInfo_state(101201);
		}
		saveGoods_info(goods_info);
		
		//修改车辆状态
		Truck_infoManager truckMgr = (Truck_infoManager) SpringContextHolder
				.getBean(Truck_info.class);

		truck.setTruck_state(100401);
		truckMgr.saveTruck_info(truck);
		
		return null;
		
	}

	/**
	 * 解除一条承运成交记录
	 */
	public String removeOrderGoods_info(Driver2goods driver2goods) {
		
		
		//解除关联关系（否则hibernate的remove会报错）
		Goods_info goods_info = driver2goods.getGoods_info();
		goods_info.getDriver2goodss().remove(driver2goods);
		

		// 删除成交记录
		Driver2goodsManager mgr = (Driver2goodsManager) SpringContextHolder
				.getBean(Driver2goods.class);
		mgr.removeDriver2goodsById(driver2goods.getId());

		// 记录解约操作
		Driver2goods_log log = new Driver2goods_log();
		org.springframework.beans.BeanUtils.copyProperties(driver2goods, log);
		Driver2goods_logManager mgrLog = (Driver2goods_logManager) SpringContextHolder
				.getBean(Driver2goods_log.class);

		Date date = new Date();
		Timestamp time = new Timestamp(date.getTime());
		log.setOpTime(time);
		log.setD2gType(101602);
		log.setId(null);
		mgrLog.saveDriver2goods_log(log);


		// 修改货物状态
		
		if (goods_info.getNeedNum() > 0
				&& goods_info.getInfo_state().intValue() != 101200) {
			goods_info.setInfo_state(101200);
			Goods_infoManager goods_infoMgr = (Goods_infoManager) SpringContextHolder
					.getBean(Goods_info.class);
			goods_infoMgr.saveGoods_info(goods_info);
		}
		
		// 修改车辆状态
		Truck_info truck = driver2goods.getGoods_info().getDriver()
				.getTruck_info();
		truck.setTruck_state(100400);
		Truck_infoManager truckMgr = (Truck_infoManager) SpringContextHolder
				.getBean(Truck_info.class);
		truckMgr.saveTruck_info(truck);
		return null;
	}
	
	/*
	 * 记录运送评价
	 * @see org.logistic.basicinfo.service.ExGoods_infoManager#saveEvaluateDriver2goods(org.logistic.basicinfo.model.Driver2goods)
	 */
	public String saveEvaluateDriver2goods(Driver2goods driver2goods){
		
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager)SpringContextHolder.getBean(Driver2goods.class);
		driver2goodsMgr.saveDriver2goods(driver2goods);
		
		Driver2goods_logManager driver2goods_logMgr = (Driver2goods_logManager)SpringContextHolder.getBean(Driver2goods_log.class);
		Driver2goods_log log = new Driver2goods_log();
		org.springframework.beans.BeanUtils.copyProperties(driver2goods, log);
		log.setId(null);
		Date date = new Date();
		Timestamp time = new Timestamp(date.getTime());
		log.setOpTime(time);
		driver2goods_logMgr.saveDriver2goods_log(log);
		
		Driver_info driver_info = driver2goods.getDriver();
		int driverRank = driver_info.getDriverRank().intValue();

		if (driver2goods.getTaskRank().intValue() == 101700) {
			driverRank++;
		} else if (driver2goods.getTaskRank().intValue() == 101702) {
			driverRank--;
		}
		
		driver_info.setDriverRank(driverRank);
		Driver_infoManager dm = (Driver_infoManager)SpringContextHolder.getBean(Driver_info.class);
		dm.saveDriver_info(driver_info);
		return null;
		
		
	}
}
