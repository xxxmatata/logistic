package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Logistic_corp;
import org.hi.base.organization.service.impl.HiOrgManagerImpl;
import org.logistic.basicinfo.service.Logistic_corpManager;

public class Logistic_corpManagerImpl extends HiOrgManagerImpl implements Logistic_corpManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveLogistic_corp(Logistic_corp logistic_corp){
    	saveObject(logistic_corp);
    
    }

    public void removeLogistic_corpById(Integer id){
    	removeObjectById(id);
    	
    }

    public Logistic_corp getLogistic_corpById(Integer id){
   		return (Logistic_corp) getObjectById(id);
    }

    public List<Logistic_corp> getLogistic_corpList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityLogistic_corp(Logistic_corp logistic_corp){
    	saveObject(logistic_corp);
    
    }

    public void removeSecurityLogistic_corpById(Integer id){
    	removeObjectById(id);
    	
    }

    public Logistic_corp getSecurityLogistic_corpById(Integer id){
   		return (Logistic_corp) getObjectById(id);
    }

    public List<Logistic_corp> getSecurityLogistic_corpList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
