package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver2goods_log;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Driver2goods_logManager;

public class Driver2goods_logManagerImpl extends ManagerImpl implements Driver2goods_logManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveDriver2goods_log(Driver2goods_log driver2goods_log){
    	saveObject(driver2goods_log);
    
    }

    public void removeDriver2goods_logById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver2goods_log getDriver2goods_logById(Integer id){
   		return (Driver2goods_log) getObjectById(id);
    }

    public List<Driver2goods_log> getDriver2goods_logList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityDriver2goods_log(Driver2goods_log driver2goods_log){
    	saveObject(driver2goods_log);
    
    }

    public void removeSecurityDriver2goods_logById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver2goods_log getSecurityDriver2goods_logById(Integer id){
   		return (Driver2goods_log) getObjectById(id);
    }

    public List<Driver2goods_log> getSecurityDriver2goods_logList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
