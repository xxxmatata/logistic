package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_info;
import org.hi.base.organization.service.impl.HiUserManagerImpl;
import org.logistic.basicinfo.service.Driver_infoManager;

public class Driver_infoManagerImpl extends HiUserManagerImpl implements Driver_infoManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveDriver_info(Driver_info driver_info){
    	saveObject(driver_info);
    
    }

    public void removeDriver_infoById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_info getDriver_infoById(Integer id){
   		return (Driver_info) getObjectById(id);
    }

    public List<Driver_info> getDriver_infoList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityDriver_info(Driver_info driver_info){
    	saveObject(driver_info);
    
    }

    public void removeSecurityDriver_infoById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_info getSecurityDriver_infoById(Integer id){
   		return (Driver_info) getObjectById(id);
    }

    public List<Driver_info> getSecurityDriver_infoList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
