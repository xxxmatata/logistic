package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Goods2owner;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Goods2ownerManager;

public class Goods2ownerManagerImpl extends ManagerImpl implements Goods2ownerManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveGoods2owner(Goods2owner goods2owner){
    	saveObject(goods2owner);
    
    }

    public void removeGoods2ownerById(Integer id){
    	removeObjectById(id);
    	
    }

    public Goods2owner getGoods2ownerById(Integer id){
   		return (Goods2owner) getObjectById(id);
    }

    public List<Goods2owner> getGoods2ownerList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityGoods2owner(Goods2owner goods2owner){
    	saveObject(goods2owner);
    
    }

    public void removeSecurityGoods2ownerById(Integer id){
    	removeObjectById(id);
    	
    }

    public Goods2owner getSecurityGoods2ownerById(Integer id){
   		return (Goods2owner) getObjectById(id);
    }

    public List<Goods2owner> getSecurityGoods2ownerList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
