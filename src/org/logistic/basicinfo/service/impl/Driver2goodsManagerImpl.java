package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver2goods;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Driver2goodsManager;

public class Driver2goodsManagerImpl extends ManagerImpl implements Driver2goodsManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveDriver2goods(Driver2goods driver2goods){
    	saveObject(driver2goods);
    
    }

    public void removeDriver2goodsById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver2goods getDriver2goodsById(Integer id){
   		return (Driver2goods) getObjectById(id);
    }

    public List<Driver2goods> getDriver2goodsList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityDriver2goods(Driver2goods driver2goods){
    	saveObject(driver2goods);
    
    }

    public void removeSecurityDriver2goodsById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver2goods getSecurityDriver2goodsById(Integer id){
   		return (Driver2goods) getObjectById(id);
    }

    public List<Driver2goods> getSecurityDriver2goodsList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
