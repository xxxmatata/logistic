package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.RegionGB;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.RegionGBManager;

public class RegionGBManagerImpl extends ManagerImpl implements RegionGBManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveRegionGB(RegionGB regionGB){
    	saveObject(regionGB);
    
    }

    public void removeRegionGBById(Integer id){
    	removeObjectById(id);
    	
    }

    public RegionGB getRegionGBById(Integer id){
   		return (RegionGB) getObjectById(id);
    }

    public List<RegionGB> getRegionGBList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityRegionGB(RegionGB regionGB){
    	saveObject(regionGB);
    
    }

    public void removeSecurityRegionGBById(Integer id){
    	removeObjectById(id);
    	
    }

    public RegionGB getSecurityRegionGBById(Integer id){
   		return (RegionGB) getObjectById(id);
    }

    public List<RegionGB> getSecurityRegionGBList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
