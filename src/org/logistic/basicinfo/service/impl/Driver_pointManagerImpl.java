package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_point;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Driver_pointManager;

public class Driver_pointManagerImpl extends ManagerImpl implements Driver_pointManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveDriver_point(Driver_point driver_point){
    	saveObject(driver_point);
    
    }

    public void removeDriver_pointById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_point getDriver_pointById(Integer id){
   		return (Driver_point) getObjectById(id);
    }

    public List<Driver_point> getDriver_pointList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityDriver_point(Driver_point driver_point){
    	saveObject(driver_point);
    
    }

    public void removeSecurityDriver_pointById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_point getSecurityDriver_pointById(Integer id){
   		return (Driver_point) getObjectById(id);
    }

    public List<Driver_point> getSecurityDriver_pointList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
