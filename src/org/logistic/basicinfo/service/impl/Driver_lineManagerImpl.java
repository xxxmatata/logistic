package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_line;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Driver_lineManager;

public class Driver_lineManagerImpl extends ManagerImpl implements Driver_lineManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveDriver_line(Driver_line driver_line){
    	saveObject(driver_line);
    
    }

    public void removeDriver_lineById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_line getDriver_lineById(Integer id){
   		return (Driver_line) getObjectById(id);
    }

    public List<Driver_line> getDriver_lineList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityDriver_line(Driver_line driver_line){
    	saveObject(driver_line);
    
    }

    public void removeSecurityDriver_lineById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_line getSecurityDriver_lineById(Integer id){
   		return (Driver_line) getObjectById(id);
    }

    public List<Driver_line> getSecurityDriver_lineList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
