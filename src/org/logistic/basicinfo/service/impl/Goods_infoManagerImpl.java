package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Goods_info;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Goods_infoManager;

public class Goods_infoManagerImpl extends ManagerImpl implements Goods_infoManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveGoods_info(Goods_info goods_info){
    	saveObject(goods_info);
    
    }

    public void removeGoods_infoById(Integer id){
    	removeObjectById(id);
    	
    }

    public Goods_info getGoods_infoById(Integer id){
   		return (Goods_info) getObjectById(id);
    }

    public List<Goods_info> getGoods_infoList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityGoods_info(Goods_info goods_info){
    	saveObject(goods_info);
    
    }

    public void removeSecurityGoods_infoById(Integer id){
    	removeObjectById(id);
    	
    }

    public Goods_info getSecurityGoods_infoById(Integer id){
   		return (Goods_info) getObjectById(id);
    }

    public List<Goods_info> getSecurityGoods_infoList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
