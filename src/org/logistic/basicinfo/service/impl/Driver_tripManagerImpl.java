package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_trip;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Driver_tripManager;

public class Driver_tripManagerImpl extends ManagerImpl implements Driver_tripManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveDriver_trip(Driver_trip driver_trip){
    	saveObject(driver_trip);
    
    }

    public void removeDriver_tripById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_trip getDriver_tripById(Integer id){
   		return (Driver_trip) getObjectById(id);
    }

    public List<Driver_trip> getDriver_tripList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityDriver_trip(Driver_trip driver_trip){
    	saveObject(driver_trip);
    
    }

    public void removeSecurityDriver_tripById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_trip getSecurityDriver_tripById(Integer id){
   		return (Driver_trip) getObjectById(id);
    }

    public List<Driver_trip> getSecurityDriver_tripList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
