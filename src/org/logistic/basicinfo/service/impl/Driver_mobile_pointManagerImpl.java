package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_mobile_point;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Driver_mobile_pointManager;

public class Driver_mobile_pointManagerImpl extends ManagerImpl implements Driver_mobile_pointManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveDriver_mobile_point(Driver_mobile_point driver_mobile_point){
    	saveObject(driver_mobile_point);
    
    }

    public void removeDriver_mobile_pointById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_mobile_point getDriver_mobile_pointById(Integer id){
   		return (Driver_mobile_point) getObjectById(id);
    }

    public List<Driver_mobile_point> getDriver_mobile_pointList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityDriver_mobile_point(Driver_mobile_point driver_mobile_point){
    	saveObject(driver_mobile_point);
    
    }

    public void removeSecurityDriver_mobile_pointById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_mobile_point getSecurityDriver_mobile_pointById(Integer id){
   		return (Driver_mobile_point) getObjectById(id);
    }

    public List<Driver_mobile_point> getSecurityDriver_mobile_pointList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
