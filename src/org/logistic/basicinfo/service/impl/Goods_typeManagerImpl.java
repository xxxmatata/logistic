package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Goods_type;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Goods_typeManager;

public class Goods_typeManagerImpl extends ManagerImpl implements Goods_typeManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveGoods_type(Goods_type goods_type){
    	saveObject(goods_type);
    
    }

    public void removeGoods_typeById(Integer id){
    	removeObjectById(id);
    	
    }

    public Goods_type getGoods_typeById(Integer id){
   		return (Goods_type) getObjectById(id);
    }

    public List<Goods_type> getGoods_typeList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityGoods_type(Goods_type goods_type){
    	saveObject(goods_type);
    
    }

    public void removeSecurityGoods_typeById(Integer id){
    	removeObjectById(id);
    	
    }

    public Goods_type getSecurityGoods_typeById(Integer id){
   		return (Goods_type) getObjectById(id);
    }

    public List<Goods_type> getSecurityGoods_typeList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
