package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_mobile_trace;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Driver_mobile_traceManager;

public class Driver_mobile_traceManagerImpl extends ManagerImpl implements Driver_mobile_traceManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveDriver_mobile_trace(Driver_mobile_trace driver_mobile_trace){
    	saveObject(driver_mobile_trace);
    
    }

    public void removeDriver_mobile_traceById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_mobile_trace getDriver_mobile_traceById(Integer id){
   		return (Driver_mobile_trace) getObjectById(id);
    }

    public List<Driver_mobile_trace> getDriver_mobile_traceList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityDriver_mobile_trace(Driver_mobile_trace driver_mobile_trace){
    	saveObject(driver_mobile_trace);
    
    }

    public void removeSecurityDriver_mobile_traceById(Integer id){
    	removeObjectById(id);
    	
    }

    public Driver_mobile_trace getSecurityDriver_mobile_traceById(Integer id){
   		return (Driver_mobile_trace) getObjectById(id);
    }

    public List<Driver_mobile_trace> getSecurityDriver_mobile_traceList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
