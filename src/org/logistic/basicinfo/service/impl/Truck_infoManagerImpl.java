package org.logistic.basicinfo.service.impl;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Truck_info;
import org.hi.framework.service.impl.ManagerImpl;
import org.logistic.basicinfo.service.Truck_infoManager;

public class Truck_infoManagerImpl extends ManagerImpl implements Truck_infoManager{
    
    protected void preSaveObject(Object obj) {
        super.preSaveObject(obj);

    }

    protected void postSaveObject(Object obj) {
        super.postSaveObject(obj);

    }

    protected void preRemoveObject(Object obj) {
        super.preRemoveObject(obj);
        
    }

    protected void postRemoveObject(Object obj) {
        super.postRemoveObject(obj);
        
    }
    
    public void saveTruck_info(Truck_info truck_info){
    	saveObject(truck_info);
    
    }

    public void removeTruck_infoById(Integer id){
    	removeObjectById(id);
    	
    }

    public Truck_info getTruck_infoById(Integer id){
   		return (Truck_info) getObjectById(id);
    }

    public List<Truck_info> getTruck_infoList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }

    public void saveSecurityTruck_info(Truck_info truck_info){
    	saveObject(truck_info);
    
    }

    public void removeSecurityTruck_infoById(Integer id){
    	removeObjectById(id);
    	
    }

    public Truck_info getSecurityTruck_infoById(Integer id){
   		return (Truck_info) getObjectById(id);
    }

    public List<Truck_info> getSecurityTruck_infoList(PageInfo pageInfo){
    	return super.getList(pageInfo);
    }    
}
