package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Truck_info;
import org.hi.framework.service.Manager;

public interface Truck_infoManager extends Manager{
    
    public void saveTruck_info(Truck_info truck_info);

    public void removeTruck_infoById(Integer id);

    public Truck_info getTruck_infoById(Integer id);

    public List<Truck_info> getTruck_infoList(PageInfo pageInfo);
    
    public void saveSecurityTruck_info(Truck_info truck_info);

    public void removeSecurityTruck_infoById(Integer id);

    public Truck_info getSecurityTruck_infoById(Integer id);

    public List<Truck_info> getSecurityTruck_infoList(PageInfo pageInfo);    
}
