package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Goods_owner;
import org.hi.framework.service.Manager;

public interface Goods_ownerManager extends Manager{
    
    public void saveGoods_owner(Goods_owner goods_owner);

    public void removeGoods_ownerById(Integer id);

    public Goods_owner getGoods_ownerById(Integer id);

    public List<Goods_owner> getGoods_ownerList(PageInfo pageInfo);
    
    public void saveSecurityGoods_owner(Goods_owner goods_owner);

    public void removeSecurityGoods_ownerById(Integer id);

    public Goods_owner getSecurityGoods_ownerById(Integer id);

    public List<Goods_owner> getSecurityGoods_ownerList(PageInfo pageInfo);    
}
