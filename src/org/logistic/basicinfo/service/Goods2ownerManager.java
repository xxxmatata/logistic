package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Goods2owner;
import org.hi.framework.service.Manager;

public interface Goods2ownerManager extends Manager{
    
    public void saveGoods2owner(Goods2owner goods2owner);

    public void removeGoods2ownerById(Integer id);

    public Goods2owner getGoods2ownerById(Integer id);

    public List<Goods2owner> getGoods2ownerList(PageInfo pageInfo);
    
    public void saveSecurityGoods2owner(Goods2owner goods2owner);

    public void removeSecurityGoods2ownerById(Integer id);

    public Goods2owner getSecurityGoods2ownerById(Integer id);

    public List<Goods2owner> getSecurityGoods2ownerList(PageInfo pageInfo);    
}
