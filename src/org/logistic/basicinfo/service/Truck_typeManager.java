package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Truck_type;
import org.hi.framework.service.Manager;

public interface Truck_typeManager extends Manager{
    
    public void saveTruck_type(Truck_type truck_type);

    public void removeTruck_typeById(Integer id);

    public Truck_type getTruck_typeById(Integer id);

    public List<Truck_type> getTruck_typeList(PageInfo pageInfo);
    
    public void saveSecurityTruck_type(Truck_type truck_type);

    public void removeSecurityTruck_typeById(Integer id);

    public Truck_type getSecurityTruck_typeById(Integer id);

    public List<Truck_type> getSecurityTruck_typeList(PageInfo pageInfo);    
}
