package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Goods_info;
import org.hi.framework.service.Manager;

public interface Goods_infoManager extends Manager{
    
    public void saveGoods_info(Goods_info goods_info);

    public void removeGoods_infoById(Integer id);

    public Goods_info getGoods_infoById(Integer id);

    public List<Goods_info> getGoods_infoList(PageInfo pageInfo);
    
    public void saveSecurityGoods_info(Goods_info goods_info);

    public void removeSecurityGoods_infoById(Integer id);

    public Goods_info getSecurityGoods_infoById(Integer id);

    public List<Goods_info> getSecurityGoods_infoList(PageInfo pageInfo);    
}
