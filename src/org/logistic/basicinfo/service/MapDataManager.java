package org.logistic.basicinfo.service;

import java.util.List;
import java.util.Map;

import org.hi.framework.web.PageInfoView;
import org.logistic.constant.SqlText;

public class MapDataManager extends CommQueryManager {
	public List<Map> loadDriverOnMap(PageInfoView pageInfo){
		return this.query(SqlText.LOAD_DRIVER_ON_MAP, null, pageInfo);
	}
	
	public List<Map> loadDriverOnMap(String sql,String countSql,PageInfoView pageInfo){
		return this.query(sql,countSql, pageInfo);
	}	
	
	public List<Map> loadDriverTraceOnMap(String sql,String countSql,PageInfoView pageInfo){
		return this.query(sql,countSql, pageInfo);
	}	
	
}
