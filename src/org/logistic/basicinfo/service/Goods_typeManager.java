package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Goods_type;
import org.hi.framework.service.Manager;

public interface Goods_typeManager extends Manager{
    
    public void saveGoods_type(Goods_type goods_type);

    public void removeGoods_typeById(Integer id);

    public Goods_type getGoods_typeById(Integer id);

    public List<Goods_type> getGoods_typeList(PageInfo pageInfo);
    
    public void saveSecurityGoods_type(Goods_type goods_type);

    public void removeSecurityGoods_typeById(Integer id);

    public Goods_type getSecurityGoods_typeById(Integer id);

    public List<Goods_type> getSecurityGoods_typeList(PageInfo pageInfo);    
}
