package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_info;
import org.hi.base.organization.service.HiUserManager;

public interface Driver_infoManager extends HiUserManager{
    
    public void saveDriver_info(Driver_info driver_info);

    public void removeDriver_infoById(Integer id);

    public Driver_info getDriver_infoById(Integer id);

    public List<Driver_info> getDriver_infoList(PageInfo pageInfo);
    
    public void saveSecurityDriver_info(Driver_info driver_info);

    public void removeSecurityDriver_infoById(Integer id);

    public Driver_info getSecurityDriver_infoById(Integer id);

    public List<Driver_info> getSecurityDriver_infoList(PageInfo pageInfo);    
}
