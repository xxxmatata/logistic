package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_mobile_trace;
import org.hi.framework.service.Manager;

public interface Driver_mobile_traceManager extends Manager{
    
    public void saveDriver_mobile_trace(Driver_mobile_trace driver_mobile_trace);

    public void removeDriver_mobile_traceById(Integer id);

    public Driver_mobile_trace getDriver_mobile_traceById(Integer id);

    public List<Driver_mobile_trace> getDriver_mobile_traceList(PageInfo pageInfo);
    
    public void saveSecurityDriver_mobile_trace(Driver_mobile_trace driver_mobile_trace);

    public void removeSecurityDriver_mobile_traceById(Integer id);

    public Driver_mobile_trace getSecurityDriver_mobile_traceById(Integer id);

    public List<Driver_mobile_trace> getSecurityDriver_mobile_traceList(PageInfo pageInfo);    
}
