package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_line;
import org.hi.framework.service.Manager;

public interface Driver_lineManager extends Manager{
    
    public void saveDriver_line(Driver_line driver_line);

    public void removeDriver_lineById(Integer id);

    public Driver_line getDriver_lineById(Integer id);

    public List<Driver_line> getDriver_lineList(PageInfo pageInfo);
    
    public void saveSecurityDriver_line(Driver_line driver_line);

    public void removeSecurityDriver_lineById(Integer id);

    public Driver_line getSecurityDriver_lineById(Integer id);

    public List<Driver_line> getSecurityDriver_lineList(PageInfo pageInfo);    
}
