package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver_mobile_point;
import org.hi.framework.service.Manager;

public interface Driver_mobile_pointManager extends Manager{
    
    public void saveDriver_mobile_point(Driver_mobile_point driver_mobile_point);

    public void removeDriver_mobile_pointById(Integer id);

    public Driver_mobile_point getDriver_mobile_pointById(Integer id);

    public List<Driver_mobile_point> getDriver_mobile_pointList(PageInfo pageInfo);
    
    public void saveSecurityDriver_mobile_point(Driver_mobile_point driver_mobile_point);

    public void removeSecurityDriver_mobile_pointById(Integer id);

    public Driver_mobile_point getSecurityDriver_mobile_pointById(Integer id);

    public List<Driver_mobile_point> getSecurityDriver_mobile_pointList(PageInfo pageInfo);    
}
