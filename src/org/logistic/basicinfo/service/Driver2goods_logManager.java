package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver2goods_log;
import org.hi.framework.service.Manager;

public interface Driver2goods_logManager extends Manager{
    
    public void saveDriver2goods_log(Driver2goods_log driver2goods_log);

    public void removeDriver2goods_logById(Integer id);

    public Driver2goods_log getDriver2goods_logById(Integer id);

    public List<Driver2goods_log> getDriver2goods_logList(PageInfo pageInfo);
    
    public void saveSecurityDriver2goods_log(Driver2goods_log driver2goods_log);

    public void removeSecurityDriver2goods_logById(Integer id);

    public Driver2goods_log getSecurityDriver2goods_logById(Integer id);

    public List<Driver2goods_log> getSecurityDriver2goods_logList(PageInfo pageInfo);    
}
