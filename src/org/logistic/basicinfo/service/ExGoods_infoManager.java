package org.logistic.basicinfo.service;

import org.logistic.basicinfo.model.Driver2goods;
import org.logistic.basicinfo.model.Goods_info;

public interface ExGoods_infoManager extends Goods_infoManager {
	public String saveAddOrderGoods_info(Goods_info goods_info);
	public String removeOrderGoods_info(Driver2goods driver2goods);
	public String saveEvaluateDriver2goods(Driver2goods driver2goods);
}
