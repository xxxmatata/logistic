package org.logistic.basicinfo.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hi.SpringContextHolder;
import org.hi.framework.dao.hibernate.LocalSessionFactoryBean;
import org.hi.framework.web.PageInfoView;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;

public class CommQueryManager {
	
	
	private SQLQuery createCountQuery(Session session,String sqlCountTxt,
			Map condition){
		
		if (condition == null){
			return session.createSQLQuery(sqlCountTxt);
		}
		
		Iterator iterator = condition.keySet().iterator();
		
		while(iterator.hasNext()){
			String key = (String)iterator.next();
			Object value = condition.get(key);
			String s[] = key.split(" ");
			String field = s[0];
			String op = s[1];
			sqlCountTxt = sqlCountTxt + " and "+field+" "+op + " ?";
			
		}
		
		SQLQuery sqlcountQuery = session.createSQLQuery(sqlCountTxt);
		int i = 0;
		while(iterator.hasNext()){
			String key = (String)iterator.next();
			Object value = condition.get(key);
			sqlcountQuery.setParameter(i, value);
			i++;
		}
		
		
		return sqlcountQuery;
	}
	
	private SQLQuery createQuery(Session session,String sqlTxt, 
			PageInfoView pageInfo,Map condition){

		int s = pageInfo.getCurrentPage();
		s = (s - 1) * pageInfo.getPageSize();
		int e = pageInfo.getCurrentPage();
		e = e * pageInfo.getPageSize();
		
		
		if (condition == null){
			sqlTxt = sqlTxt + " limit " + s + " , " + e;
			SQLQuery sqlQuery =  session.createSQLQuery(sqlTxt);
			sqlQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
			return sqlQuery;
		}
		
		Iterator iterator = condition.keySet().iterator();
		
		while(iterator.hasNext()){
			String key = (String)iterator.next();
			Object value = condition.get(key);
			String[] sa = key.split(" ");
			String field = sa[0];
			String op = sa[1];
			sqlTxt = sqlTxt + " and "+field+" "+op + " ?";
		}
		sqlTxt = sqlTxt + " limit " + s + " , " + e;
		SQLQuery sqlQuery = session.createSQLQuery(sqlTxt);
		int i = 0;
		iterator = condition.keySet().iterator();
		while(iterator.hasNext()){
			String key = (String)iterator.next();
			Object value = condition.get(key);
			sqlQuery.setParameter(i, value);
			i++;
		}
		sqlQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		
		
		return sqlQuery;
		
	}
	
	public List<Map> query(String sqlTxt, String sqlCountTxt,
			PageInfoView pageInfo,Map condition){
		

		Session session = null;
		try {

			if (pageInfo == null){
				pageInfo = new PageInfoView();
				pageInfo.setPageSize(99999);
			}
			
			LocalSessionFactoryBean o = (LocalSessionFactoryBean) SpringContextHolder
					.getBean("&sessionFactory");
			SessionFactory sessionFactory = (SessionFactory) o.getObject();
			/**
			 * 对于没有request的Thread不能用sessionFactory.getCurrentSession()，
			 * 改用sessionFactory.openSession()；
			 */
			// Session session = sessionFactory.getCurrentSession();
			session = sessionFactory.openSession();

			int count = 0;

			if (sqlCountTxt != null) {
				SQLQuery sqlcountQuery = createCountQuery(session,sqlCountTxt,condition);
				List rsCount = sqlcountQuery.list();

				Object oCount = rsCount.get(0);
				if (oCount instanceof Integer)
					count = ((Integer) oCount).intValue();
				else if (oCount instanceof Long)
					count = ((Long) oCount).intValue();
				else if (oCount instanceof BigDecimal)
					count = ((BigDecimal) oCount).intValue();
				else if (oCount instanceof BigInteger)
					count = ((BigInteger) oCount).intValue();
				else
					count = ((Number) oCount).intValue();
			}


			SQLQuery sqlQuery = createQuery(session,sqlTxt,pageInfo,condition);

			List<Map> result = sqlQuery.list();

			if (sqlCountTxt == null) {
				count = result.size();
			}

			count = pageInfo.getMaxRecords() != 0
					&& count > pageInfo.getMaxRecords() ? pageInfo
					.getMaxRecords() : count;
			pageInfo.setTotalRecords(count); // 设置查询结果的总记录数
			int totoalPage = 0;
			totoalPage = count % pageInfo.getPageSize() > 0 ? count
					/ pageInfo.getPageSize() + 1 : count
					/ pageInfo.getPageSize();
			pageInfo.setTotalPage(totoalPage);

			if (session.isOpen()){
				session.close();
			}

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			if (session.isOpen()){
				session.close();
			}
			return null;
		} finally {
			if (session.isOpen()){
				session.close();
			}
		}
		
	
	}
	
	public List<Map> query(String sqlTxt, String sqlCountTxt,
			PageInfoView pageInfo) {
		return this.query(sqlTxt,sqlCountTxt,pageInfo,null);
	}
	
	
	/**
	 * 取当前机构及其下级机构的ID
	 * @param orgId
	 * @return
	 */
	public String getChildOrg(String orgId){
		
		List<Map> list = query("select * from (select getChildidList("+orgId+")  as id) a",null,null,null);
		
		String s = null;
		
		if (list != null){
			Map m = list.get(0);
			s = (String) m.get("id");
		}
		return s;
	}
}
