package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Driver2goods;
import org.hi.framework.service.Manager;

public interface Driver2goodsManager extends Manager{
    
    public void saveDriver2goods(Driver2goods driver2goods);

    public void removeDriver2goodsById(Integer id);

    public Driver2goods getDriver2goodsById(Integer id);

    public List<Driver2goods> getDriver2goodsList(PageInfo pageInfo);
    
    public void saveSecurityDriver2goods(Driver2goods driver2goods);

    public void removeSecurityDriver2goodsById(Integer id);

    public Driver2goods getSecurityDriver2goodsById(Integer id);

    public List<Driver2goods> getSecurityDriver2goodsList(PageInfo pageInfo);    
}
