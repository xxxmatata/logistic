package org.logistic.basicinfo.service;

import java.util.List;

import org.hi.framework.paging.PageInfo;
import org.logistic.basicinfo.model.Logistic_corp;
import org.hi.base.organization.service.HiOrgManager;

public interface Logistic_corpManager extends HiOrgManager{
    
    public void saveLogistic_corp(Logistic_corp logistic_corp);

    public void removeLogistic_corpById(Integer id);

    public Logistic_corp getLogistic_corpById(Integer id);

    public List<Logistic_corp> getLogistic_corpList(PageInfo pageInfo);
    
    public void saveSecurityLogistic_corp(Logistic_corp logistic_corp);

    public void removeSecurityLogistic_corpById(Integer id);

    public Logistic_corp getSecurityLogistic_corpById(Integer id);

    public List<Logistic_corp> getSecurityLogistic_corpList(PageInfo pageInfo);    
}
