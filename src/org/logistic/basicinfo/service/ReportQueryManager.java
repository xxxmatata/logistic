package org.logistic.basicinfo.service;

import java.util.List;
import java.util.Map;

import org.hi.framework.security.context.UserContextHelper;
import org.hi.framework.web.PageInfoView;


public class ReportQueryManager extends CommQueryManager{
	
	public List<Map> getTruck_infoList(PageInfoView pageInfo,Map condition){
		
		String userId = UserContextHelper.getUserContext().getUser().getId().toString();
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("select b.id,a.id driver_id,u.userName,b.id,c.trip_region,d.r_name_path,e.truck_type_name,c.trip_date,c.publish_time,b.truck_card,b.truck_length,b.truck_load,b.truck_width,b.truck_load,b.truck_state");
		sql.append(" from truck_info b");
		sql.append(" LEFT JOIN driver_info a on a.truck_info=b.id");
		sql.append(" LEFT JOIN hi_user u on a.id=u.id");
		sql.append(" LEFT JOIN driver_point c on a.id=c.driver_info");
		sql.append(" LEFT JOIN regiongb d on c.trip_region=d.id");
		sql.append(" LEFT JOIN truck_type e on b.truck_type=e.id");
		sql.append(" where b.deleted<>1");
		sql.append(" and b.creator in (select u1.id from hi_user u1 LEFT JOIN hi_user u2 ON u1.org=u2.org where u2.id = "+userId+")");
		return this.query(sql.toString(), null, pageInfo,condition);
	}
	
}
