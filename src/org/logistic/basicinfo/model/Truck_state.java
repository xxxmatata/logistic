package org.logistic.basicinfo.model;

import java.io.Serializable;

public class Truck_state implements Serializable{

	/**
	 * 空车
	 */
	public static final int TRUCK_STATE_NUL = 100400;
	/**
	 * 已接货
	 */
	public static final int TRUCK_STATE_COF = 100401;
	/**
	 * 已上货
	 */
	public static final int TRUCK_STATE_LOD = 100402;

}