package org.logistic.basicinfo.model;

import java.io.Serializable;

public class Driver2goodsType implements Serializable{

	/**
	 * 已联系
	 */
	public static final int DRIVER2GOODSTYPE_C1 = 101600;
	/**
	 * 已成交
	 */
	public static final int DRIVER2GOODSTYPE_C2 = 101601;
	/**
	 * 已节约
	 */
	public static final int DRIVER2GOODSTYPE_C3 = 101602;
	/**
	 * 已送达
	 */
	public static final int DRIVER2GOODSTYPE_C4 = 101603;
	/**
	 * 已接货
	 */
	public static final int DRIVER2GOODSTYPE_C5 = 101604;

}