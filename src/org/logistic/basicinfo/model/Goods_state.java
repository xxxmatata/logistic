package org.logistic.basicinfo.model;

import java.io.Serializable;

public class Goods_state implements Serializable{

	/**
	 * 求车
	 */
	public static final int GOODS_STATE_C1 = 101200;
	/**
	 * 已定车
	 */
	public static final int GOODS_STATE_C2 = 101201;
	/**
	 * 已装车
	 */
	public static final int GOODS_STATE_C3 = 101202;
	/**
	 * 已送达
	 */
	public static final int GOODS_STATE_C4 = 101203;

}