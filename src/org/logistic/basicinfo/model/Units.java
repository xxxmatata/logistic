package org.logistic.basicinfo.model;

import java.io.Serializable;

public class Units implements Serializable{

	/**
	 * 吨
	 */
	public static final int UNITS_C1 = 101400;
	/**
	 * 公斤
	 */
	public static final int UNITS_C2 = 101401;
	/**
	 * 公里
	 */
	public static final int UNITS_C3 = 101402;
	/**
	 * 立方
	 */
	public static final int UNITS_C4 = 101403;
	/**
	 * 台
	 */
	public static final int UNITS_C5 = 101404;
	/**
	 * 部
	 */
	public static final int UNITS_C6 = 101405;
	/**
	 * 件
	 */
	public static final int UNITS_C7 = 101406;
	/**
	 * 车
	 */
	public static final int UNITS_C8 = 101407;
	/**
	 * 米
	 */
	public static final int UNITS_C9 = 101408;
	/**
	 * 张
	 */
	public static final int UNITS_C10 = 101409;
	/**
	 * 箱
	 */
	public static final int UNITS_C11 = 101410;

}