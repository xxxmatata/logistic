package org.logistic.basicinfo.model;

import org.hi.common.util.CalendarUtil;
import org.logistic.basicinfo.model.original.Driver_infoAbstract;


public class Driver_info extends Driver_infoAbstract{
	private String nextPoint;

	public String getNextPoint() {
		if (super.getDriver_points() != null && super.getDriver_points().size()>0){
			Driver_point p = super.getDriver_points().get(0);
			
			nextPoint = CalendarUtil.date2String(p.getTrip_date(),"yyyy-MM-dd") + "��" + p.getTrip_region().getR_name_path();
		}else{
			nextPoint = "";
		}
		return nextPoint;
	}

	public void setNextPoint(String nextPoint) {
		this.nextPoint = nextPoint;
	}


}