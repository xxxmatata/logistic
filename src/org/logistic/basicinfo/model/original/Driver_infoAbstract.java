package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.logistic.basicinfo.model.Driver_line;
import org.hi.common.attachment.model.Attachment;
import org.logistic.basicinfo.model.Driver_point;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.RegionGB;
import org.logistic.basicinfo.model.Truck_info;
import org.hi.base.organization.model.HiUser;

public abstract class Driver_infoAbstract extends HiUser implements Serializable{

 	 /**
	 * id
	 */	
 	protected  Integer id;

 	 /**
	 * 附件
	 */	
 	protected  Attachment drive_lic_file_attachment;

 	 /**
	 * truck_info
	 */	
 	protected  Truck_info truck_info;

 	 /**
	 * 信息状态
	 */	
 	protected  Integer info_state;

 	 /**
	 * 经度
	 */	
 	protected  Double lng = 0.0;

 	 /**
	 * 纬度
	 */	
 	protected  Double lat = 0.0;

 	 /**
	 * 位置ID
	 */	
 	protected  RegionGB region_id;

 	 /**
	 * 位置描述
	 */	
 	protected  String localtion_desc;

 	 /**
	 * 评价得分
	 */	
 	protected  Integer driverRank = 0;

	private  List<Driver_line> driver_lines;
	private  List<Driver_point> driver_points;

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
    public Attachment getDrive_lic_file_attachment() {
        return this.drive_lic_file_attachment;
    }
    
    public void setDrive_lic_file_attachment(Attachment drive_lic_file_attachment) {
    		if((drive_lic_file_attachment != null && this.drive_lic_file_attachment == null) || 
				this.drive_lic_file_attachment != null && (!this.drive_lic_file_attachment.equals(drive_lic_file_attachment) || drive_lic_file_attachment == null)){
        		this.setDirty(true);
        		this.oldValues.put("drive_lic_file_attachment", this.drive_lic_file_attachment);
        	}
        this.drive_lic_file_attachment = drive_lic_file_attachment;
    }
    
    public Truck_info getTruck_info() {
        return this.truck_info;
    }
    
    public void setTruck_info(Truck_info truck_info) {
    		if((truck_info != null && this.truck_info == null) || 
				this.truck_info != null && (!this.truck_info.equals(truck_info) || truck_info == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_info", this.truck_info);
        	}
        this.truck_info = truck_info;
    }
    
   public BaseObject getParentEntity(){
	   return this.truck_info;
   }
   
   public void setParentEntity(BaseObject parent){
	   this.truck_info = (Truck_info)parent;
   }
   
    public Integer getInfo_state() {
        return this.info_state;
    }
    
    public void setInfo_state(Integer info_state) {
    		if((info_state != null && this.info_state == null) || 
				this.info_state != null && (!this.info_state.equals(info_state) || info_state == null)){
        		this.setDirty(true);
        		this.oldValues.put("info_state", this.info_state);
        	}
        this.info_state = info_state;
    }
    
    public Double getLng() {
        return this.lng;
    }
    
    public void setLng(Double lng) {
    		if((lng != null && this.lng == null) || 
				this.lng != null && (!this.lng.equals(lng) || lng == null)){
        		this.setDirty(true);
        		this.oldValues.put("lng", this.lng);
        	}
        this.lng = lng;
    }
    
    public Double getLat() {
        return this.lat;
    }
    
    public void setLat(Double lat) {
    		if((lat != null && this.lat == null) || 
				this.lat != null && (!this.lat.equals(lat) || lat == null)){
        		this.setDirty(true);
        		this.oldValues.put("lat", this.lat);
        	}
        this.lat = lat;
    }
    
    public RegionGB getRegion_id() {
        return this.region_id;
    }
    
    public void setRegion_id(RegionGB region_id) {
    		if((region_id != null && this.region_id == null) || 
				this.region_id != null && (!this.region_id.equals(region_id) || region_id == null)){
        		this.setDirty(true);
        		this.oldValues.put("region_id", this.region_id);
        	}
        this.region_id = region_id;
    }
    
    public String getLocaltion_desc() {
        return this.localtion_desc;
    }
    
    public void setLocaltion_desc(String localtion_desc) {
    		if((localtion_desc != null && this.localtion_desc == null) || 
				this.localtion_desc != null && (!this.localtion_desc.equals(localtion_desc) || localtion_desc == null)){
        		this.setDirty(true);
        		this.oldValues.put("localtion_desc", this.localtion_desc);
        	}
        this.localtion_desc = localtion_desc;
    }
    
    public Integer getDriverRank() {
        return this.driverRank;
    }
    
    public void setDriverRank(Integer driverRank) {
    		if((driverRank != null && this.driverRank == null) || 
				this.driverRank != null && (!this.driverRank.equals(driverRank) || driverRank == null)){
        		this.setDirty(true);
        		this.oldValues.put("driverRank", this.driverRank);
        	}
        this.driverRank = driverRank;
    }
    

    public void setDriver_lines(List<Driver_line> driver_lines) {
        this.driver_lines = driver_lines;
    }

    public List<Driver_line> getDriver_lines() {
        return this.driver_lines;
    }
    public void setDriver_points(List<Driver_point> driver_points) {
        this.driver_points = driver_points;
    }

    public List<Driver_point> getDriver_points() {
        return this.driver_points;
    }

   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Driver_info) ) return false;
		 Driver_info castOther = ( Driver_info ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.appendSuper(super.hashCode());
		hcb.append("Driver_info".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id)
		.append("lng", this.lng)
		.append("lat", this.lat)
		.append("localtion_desc", this.localtion_desc)
		.append("driverRank", this.driverRank);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}