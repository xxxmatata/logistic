package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.hi.common.attachment.model.Attachment;
import org.logistic.basicinfo.model.Logistic_corp;
import org.hi.base.organization.model.HiOrg;
import org.hi.base.organization.model.HiUser;

public abstract class Logistic_corpAbstract extends HiOrg implements Serializable{

 	 /**
	 * id
	 */	
 	protected  Integer id;

 	 /**
	 * 公司地址
	 */	
 	protected  String address;

 	 /**
	 * 营业执照编号
	 */	
 	protected  String op_lic;

 	 /**
	 * 附件
	 */	
 	protected  Attachment op_lic_file_attachment;

 	 /**
	 * 税务登记号
	 */	
 	protected  String tax_lic;

 	 /**
	 * 附件
	 */	
 	protected  Attachment tax_lic_file_attachment;

 	 /**
	 * 法人
	 */	
 	protected  String corp_name;

 	 /**
	 * 公司电话
	 */	
 	protected  String corp_phone;

 	 /**
	 * 手机
	 */	
 	protected  String corp_mobile;

 	 /**
	 * 信息状态
	 */	
 	protected  Integer info_state;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();


    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
    		if((address != null && this.address == null) || 
				this.address != null && (!this.address.equals(address) || address == null)){
        		this.setDirty(true);
        		this.oldValues.put("address", this.address);
        	}
        this.address = address;
    }
    
    public String getOp_lic() {
        return this.op_lic;
    }
    
    public void setOp_lic(String op_lic) {
    		if((op_lic != null && this.op_lic == null) || 
				this.op_lic != null && (!this.op_lic.equals(op_lic) || op_lic == null)){
        		this.setDirty(true);
        		this.oldValues.put("op_lic", this.op_lic);
        	}
        this.op_lic = op_lic;
    }
    
    public Attachment getOp_lic_file_attachment() {
        return this.op_lic_file_attachment;
    }
    
    public void setOp_lic_file_attachment(Attachment op_lic_file_attachment) {
    		if((op_lic_file_attachment != null && this.op_lic_file_attachment == null) || 
				this.op_lic_file_attachment != null && (!this.op_lic_file_attachment.equals(op_lic_file_attachment) || op_lic_file_attachment == null)){
        		this.setDirty(true);
        		this.oldValues.put("op_lic_file_attachment", this.op_lic_file_attachment);
        	}
        this.op_lic_file_attachment = op_lic_file_attachment;
    }
    
    public String getTax_lic() {
        return this.tax_lic;
    }
    
    public void setTax_lic(String tax_lic) {
    		if((tax_lic != null && this.tax_lic == null) || 
				this.tax_lic != null && (!this.tax_lic.equals(tax_lic) || tax_lic == null)){
        		this.setDirty(true);
        		this.oldValues.put("tax_lic", this.tax_lic);
        	}
        this.tax_lic = tax_lic;
    }
    
    public Attachment getTax_lic_file_attachment() {
        return this.tax_lic_file_attachment;
    }
    
    public void setTax_lic_file_attachment(Attachment tax_lic_file_attachment) {
    		if((tax_lic_file_attachment != null && this.tax_lic_file_attachment == null) || 
				this.tax_lic_file_attachment != null && (!this.tax_lic_file_attachment.equals(tax_lic_file_attachment) || tax_lic_file_attachment == null)){
        		this.setDirty(true);
        		this.oldValues.put("tax_lic_file_attachment", this.tax_lic_file_attachment);
        	}
        this.tax_lic_file_attachment = tax_lic_file_attachment;
    }
    
    public String getCorp_name() {
        return this.corp_name;
    }
    
    public void setCorp_name(String corp_name) {
    		if((corp_name != null && this.corp_name == null) || 
				this.corp_name != null && (!this.corp_name.equals(corp_name) || corp_name == null)){
        		this.setDirty(true);
        		this.oldValues.put("corp_name", this.corp_name);
        	}
        this.corp_name = corp_name;
    }
    
    public String getCorp_phone() {
        return this.corp_phone;
    }
    
    public void setCorp_phone(String corp_phone) {
    		if((corp_phone != null && this.corp_phone == null) || 
				this.corp_phone != null && (!this.corp_phone.equals(corp_phone) || corp_phone == null)){
        		this.setDirty(true);
        		this.oldValues.put("corp_phone", this.corp_phone);
        	}
        this.corp_phone = corp_phone;
    }
    
    public String getCorp_mobile() {
        return this.corp_mobile;
    }
    
    public void setCorp_mobile(String corp_mobile) {
    		if((corp_mobile != null && this.corp_mobile == null) || 
				this.corp_mobile != null && (!this.corp_mobile.equals(corp_mobile) || corp_mobile == null)){
        		this.setDirty(true);
        		this.oldValues.put("corp_mobile", this.corp_mobile);
        	}
        this.corp_mobile = corp_mobile;
    }
    
    public Integer getInfo_state() {
        return this.info_state;
    }
    
    public void setInfo_state(Integer info_state) {
    		if((info_state != null && this.info_state == null) || 
				this.info_state != null && (!this.info_state.equals(info_state) || info_state == null)){
        		this.setDirty(true);
        		this.oldValues.put("info_state", this.info_state);
        	}
        this.info_state = info_state;
    }
    
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Logistic_corp) ) return false;
		 Logistic_corp castOther = ( Logistic_corp ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.appendSuper(super.hashCode());
		hcb.append("Logistic_corp".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id)
		.append("address", this.address)
		.append("op_lic", this.op_lic)
		.append("tax_lic", this.tax_lic)
		.append("corp_name", this.corp_name)
		.append("corp_phone", this.corp_phone)
		.append("corp_mobile", this.corp_mobile);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}