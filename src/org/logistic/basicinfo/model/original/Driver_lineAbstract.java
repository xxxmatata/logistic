package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.logistic.basicinfo.model.Driver_line;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.RegionGB;
import org.hi.base.organization.model.HiUser;

public abstract class Driver_lineAbstract extends BaseObject implements Serializable{

 	
 	/**
	 * 主键id
	 */	
	protected  Integer id;

	/**
	 * 版本控制version
	 */	
 	protected  Integer version;

 	 /**
	 * driver_info
	 */	
 	protected  Driver_info driver_info;

 	 /**
	 * 起点1
	 */	
 	protected  RegionGB origin;

 	 /**
	 * 终点1
	 */	
 	protected  RegionGB destination;

 	 /**
	 * 报价
	 */	
 	protected  Double price;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();

 	 /**
	 * 删除标识
	 */	
 	protected  Integer deleted = 0;


    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
     public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
    		if((version != null && this.version == null) || 
				this.version != null && (!this.version.equals(version) || version == null)){
        		this.setDirty(true);
        		this.oldValues.put("version", this.version);
        	}
        this.version = version;
    }
    
    public Driver_info getDriver_info() {
        return this.driver_info;
    }
    
    public void setDriver_info(Driver_info driver_info) {
    		if((driver_info != null && this.driver_info == null) || 
				this.driver_info != null && (!this.driver_info.equals(driver_info) || driver_info == null)){
        		this.setDirty(true);
        		this.oldValues.put("driver_info", this.driver_info);
        	}
        this.driver_info = driver_info;
    }
    
   public BaseObject getParentEntity(){
	   return this.driver_info;
   }
   
   public void setParentEntity(BaseObject parent){
	   this.driver_info = (Driver_info)parent;
   }
   
    public RegionGB getOrigin() {
        return this.origin;
    }
    
    public void setOrigin(RegionGB origin) {
    		if((origin != null && this.origin == null) || 
				this.origin != null && (!this.origin.equals(origin) || origin == null)){
        		this.setDirty(true);
        		this.oldValues.put("origin", this.origin);
        	}
        this.origin = origin;
    }
    
    public RegionGB getDestination() {
        return this.destination;
    }
    
    public void setDestination(RegionGB destination) {
    		if((destination != null && this.destination == null) || 
				this.destination != null && (!this.destination.equals(destination) || destination == null)){
        		this.setDirty(true);
        		this.oldValues.put("destination", this.destination);
        	}
        this.destination = destination;
    }
    
    public Double getPrice() {
        return this.price;
    }
    
    public void setPrice(Double price) {
    		if((price != null && this.price == null) || 
				this.price != null && (!this.price.equals(price) || price == null)){
        		this.setDirty(true);
        		this.oldValues.put("price", this.price);
        	}
        this.price = price;
    }
    
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    
    public Integer getDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(Integer deleted) {
    		if((deleted != null && this.deleted == null) || 
				this.deleted != null && (!this.deleted.equals(deleted) || deleted == null)){
        		this.setDirty(true);
        		this.oldValues.put("deleted", this.deleted);
        	}
        this.deleted = deleted;
    }
    


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Driver_line) ) return false;
		 Driver_line castOther = ( Driver_line ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.append(getId());
		hcb.append("Driver_line".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id)
		.append("price", this.price)
		.append("deleted", this.deleted);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}