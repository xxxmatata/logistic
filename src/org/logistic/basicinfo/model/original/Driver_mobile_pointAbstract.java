package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.logistic.basicinfo.model.Driver_mobile_point;
import org.hi.base.organization.model.HiUser;

public abstract class Driver_mobile_pointAbstract extends BaseObject implements Serializable{

 	
 	/**
	 * 主键id
	 */	
	protected  Integer id;

	/**
	 * 版本控制version
	 */	
 	protected  Integer version;

 	 /**
	 * 国际移动用户识别码
	 */	
 	protected  String imsi;

 	 /**
	 * 移动用户号码簿号码
	 */	
 	protected  String mdn;

 	 /**
	 * 设备操作系统版本
	 */	
 	protected  String os_version;

 	 /**
	 * 经度
	 */	
 	protected  Double lng;

 	 /**
	 * 纬度
	 */	
 	protected  Double lat;

 	 /**
	 * 高度
	 */	
 	protected  Double alt;

 	 /**
	 * 速度
	 */	
 	protected  Double speed;

 	 /**
	 * 上报时间
	 */	
 	protected  Timestamp upd_time;

 	 /**
	 * 定位类型
	 */	
 	protected  String loc_type;

 	 /**
	 * 客户端IP地址
	 */	
 	protected  String client_ip;

 	 /**
	 * 位置描述
	 */	
 	protected  String loc_desc;

 	 /**
	 * 手机串号
	 */	
 	protected  String imei;

 	 /**
	 * mcc
	 */	
 	protected  String mcc;

 	 /**
	 * mnc
	 */	
 	protected  String mnc;

 	 /**
	 * lac
	 */	
 	protected  String lac;

 	 /**
	 * sid
	 */	
 	protected  String sid;

 	 /**
	 * nid
	 */	
 	protected  String nid;

 	 /**
	 * bid
	 */	
 	protected  String bid;

 	 /**
	 * radius
	 */	
 	protected  Double radius;

 	 /**
	 * error_code
	 */	
 	protected  String error_code;

 	 /**
	 * 定位时间
	 */	
 	protected  Timestamp loc_time;

 	 /**
	 * 卫星数量
	 */	
 	protected  Integer satellite_num;

 	 /**
	 * uuid
	 */	
 	protected  String uuid;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();


    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
     public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
    		if((version != null && this.version == null) || 
				this.version != null && (!this.version.equals(version) || version == null)){
        		this.setDirty(true);
        		this.oldValues.put("version", this.version);
        	}
        this.version = version;
    }
    
    public String getImsi() {
        return this.imsi;
    }
    
    public void setImsi(String imsi) {
    		if((imsi != null && this.imsi == null) || 
				this.imsi != null && (!this.imsi.equals(imsi) || imsi == null)){
        		this.setDirty(true);
        		this.oldValues.put("imsi", this.imsi);
        	}
        this.imsi = imsi;
    }
    
    public String getMdn() {
        return this.mdn;
    }
    
    public void setMdn(String mdn) {
    		if((mdn != null && this.mdn == null) || 
				this.mdn != null && (!this.mdn.equals(mdn) || mdn == null)){
        		this.setDirty(true);
        		this.oldValues.put("mdn", this.mdn);
        	}
        this.mdn = mdn;
    }
    
    public String getOs_version() {
        return this.os_version;
    }
    
    public void setOs_version(String os_version) {
    		if((os_version != null && this.os_version == null) || 
				this.os_version != null && (!this.os_version.equals(os_version) || os_version == null)){
        		this.setDirty(true);
        		this.oldValues.put("os_version", this.os_version);
        	}
        this.os_version = os_version;
    }
    
    public Double getLng() {
        return this.lng;
    }
    
    public void setLng(Double lng) {
    		if((lng != null && this.lng == null) || 
				this.lng != null && (!this.lng.equals(lng) || lng == null)){
        		this.setDirty(true);
        		this.oldValues.put("lng", this.lng);
        	}
        this.lng = lng;
    }
    
    public Double getLat() {
        return this.lat;
    }
    
    public void setLat(Double lat) {
    		if((lat != null && this.lat == null) || 
				this.lat != null && (!this.lat.equals(lat) || lat == null)){
        		this.setDirty(true);
        		this.oldValues.put("lat", this.lat);
        	}
        this.lat = lat;
    }
    
    public Double getAlt() {
        return this.alt;
    }
    
    public void setAlt(Double alt) {
    		if((alt != null && this.alt == null) || 
				this.alt != null && (!this.alt.equals(alt) || alt == null)){
        		this.setDirty(true);
        		this.oldValues.put("alt", this.alt);
        	}
        this.alt = alt;
    }
    
    public Double getSpeed() {
        return this.speed;
    }
    
    public void setSpeed(Double speed) {
    		if((speed != null && this.speed == null) || 
				this.speed != null && (!this.speed.equals(speed) || speed == null)){
        		this.setDirty(true);
        		this.oldValues.put("speed", this.speed);
        	}
        this.speed = speed;
    }
    
    public Timestamp getUpd_time() {
        return this.upd_time;
    }
    
    public void setUpd_time(Timestamp upd_time) {
    		if((upd_time != null && this.upd_time == null) || 
				this.upd_time != null && (!this.upd_time.equals(upd_time) || upd_time == null)){
        		this.setDirty(true);
        		this.oldValues.put("upd_time", this.upd_time);
        	}
        this.upd_time = upd_time;
    }
    
    public String getLoc_type() {
        return this.loc_type;
    }
    
    public void setLoc_type(String loc_type) {
    		if((loc_type != null && this.loc_type == null) || 
				this.loc_type != null && (!this.loc_type.equals(loc_type) || loc_type == null)){
        		this.setDirty(true);
        		this.oldValues.put("loc_type", this.loc_type);
        	}
        this.loc_type = loc_type;
    }
    
    public String getClient_ip() {
        return this.client_ip;
    }
    
    public void setClient_ip(String client_ip) {
    		if((client_ip != null && this.client_ip == null) || 
				this.client_ip != null && (!this.client_ip.equals(client_ip) || client_ip == null)){
        		this.setDirty(true);
        		this.oldValues.put("client_ip", this.client_ip);
        	}
        this.client_ip = client_ip;
    }
    
    public String getLoc_desc() {
        return this.loc_desc;
    }
    
    public void setLoc_desc(String loc_desc) {
    		if((loc_desc != null && this.loc_desc == null) || 
				this.loc_desc != null && (!this.loc_desc.equals(loc_desc) || loc_desc == null)){
        		this.setDirty(true);
        		this.oldValues.put("loc_desc", this.loc_desc);
        	}
        this.loc_desc = loc_desc;
    }
    
    public String getImei() {
        return this.imei;
    }
    
    public void setImei(String imei) {
    		if((imei != null && this.imei == null) || 
				this.imei != null && (!this.imei.equals(imei) || imei == null)){
        		this.setDirty(true);
        		this.oldValues.put("imei", this.imei);
        	}
        this.imei = imei;
    }
    
    public String getMcc() {
        return this.mcc;
    }
    
    public void setMcc(String mcc) {
    		if((mcc != null && this.mcc == null) || 
				this.mcc != null && (!this.mcc.equals(mcc) || mcc == null)){
        		this.setDirty(true);
        		this.oldValues.put("mcc", this.mcc);
        	}
        this.mcc = mcc;
    }
    
    public String getMnc() {
        return this.mnc;
    }
    
    public void setMnc(String mnc) {
    		if((mnc != null && this.mnc == null) || 
				this.mnc != null && (!this.mnc.equals(mnc) || mnc == null)){
        		this.setDirty(true);
        		this.oldValues.put("mnc", this.mnc);
        	}
        this.mnc = mnc;
    }
    
    public String getLac() {
        return this.lac;
    }
    
    public void setLac(String lac) {
    		if((lac != null && this.lac == null) || 
				this.lac != null && (!this.lac.equals(lac) || lac == null)){
        		this.setDirty(true);
        		this.oldValues.put("lac", this.lac);
        	}
        this.lac = lac;
    }
    
    public String getSid() {
        return this.sid;
    }
    
    public void setSid(String sid) {
    		if((sid != null && this.sid == null) || 
				this.sid != null && (!this.sid.equals(sid) || sid == null)){
        		this.setDirty(true);
        		this.oldValues.put("sid", this.sid);
        	}
        this.sid = sid;
    }
    
    public String getNid() {
        return this.nid;
    }
    
    public void setNid(String nid) {
    		if((nid != null && this.nid == null) || 
				this.nid != null && (!this.nid.equals(nid) || nid == null)){
        		this.setDirty(true);
        		this.oldValues.put("nid", this.nid);
        	}
        this.nid = nid;
    }
    
    public String getBid() {
        return this.bid;
    }
    
    public void setBid(String bid) {
    		if((bid != null && this.bid == null) || 
				this.bid != null && (!this.bid.equals(bid) || bid == null)){
        		this.setDirty(true);
        		this.oldValues.put("bid", this.bid);
        	}
        this.bid = bid;
    }
    
    public Double getRadius() {
        return this.radius;
    }
    
    public void setRadius(Double radius) {
    		if((radius != null && this.radius == null) || 
				this.radius != null && (!this.radius.equals(radius) || radius == null)){
        		this.setDirty(true);
        		this.oldValues.put("radius", this.radius);
        	}
        this.radius = radius;
    }
    
    public String getError_code() {
        return this.error_code;
    }
    
    public void setError_code(String error_code) {
    		if((error_code != null && this.error_code == null) || 
				this.error_code != null && (!this.error_code.equals(error_code) || error_code == null)){
        		this.setDirty(true);
        		this.oldValues.put("error_code", this.error_code);
        	}
        this.error_code = error_code;
    }
    
    public Timestamp getLoc_time() {
        return this.loc_time;
    }
    
    public void setLoc_time(Timestamp loc_time) {
    		if((loc_time != null && this.loc_time == null) || 
				this.loc_time != null && (!this.loc_time.equals(loc_time) || loc_time == null)){
        		this.setDirty(true);
        		this.oldValues.put("loc_time", this.loc_time);
        	}
        this.loc_time = loc_time;
    }
    
    public Integer getSatellite_num() {
        return this.satellite_num;
    }
    
    public void setSatellite_num(Integer satellite_num) {
    		if((satellite_num != null && this.satellite_num == null) || 
				this.satellite_num != null && (!this.satellite_num.equals(satellite_num) || satellite_num == null)){
        		this.setDirty(true);
        		this.oldValues.put("satellite_num", this.satellite_num);
        	}
        this.satellite_num = satellite_num;
    }
    
    public String getUuid() {
        return this.uuid;
    }
    
    public void setUuid(String uuid) {
    		if((uuid != null && this.uuid == null) || 
				this.uuid != null && (!this.uuid.equals(uuid) || uuid == null)){
        		this.setDirty(true);
        		this.oldValues.put("uuid", this.uuid);
        	}
        this.uuid = uuid;
    }
    
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Driver_mobile_point) ) return false;
		 Driver_mobile_point castOther = ( Driver_mobile_point ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.append(getId());
		hcb.append("Driver_mobile_point".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id)
		.append("imsi", this.imsi)
		.append("mdn", this.mdn)
		.append("os_version", this.os_version)
		.append("lng", this.lng)
		.append("lat", this.lat)
		.append("alt", this.alt)
		.append("speed", this.speed)
		.append("loc_type", this.loc_type)
		.append("client_ip", this.client_ip)
		.append("loc_desc", this.loc_desc)
		.append("imei", this.imei)
		.append("mcc", this.mcc)
		.append("mnc", this.mnc)
		.append("lac", this.lac)
		.append("sid", this.sid)
		.append("nid", this.nid)
		.append("bid", this.bid)
		.append("radius", this.radius)
		.append("error_code", this.error_code)
		.append("satellite_num", this.satellite_num)
		.append("uuid", this.uuid);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}