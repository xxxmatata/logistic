package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.hi.common.attachment.model.Attachment;
import org.logistic.basicinfo.model.Truck_type;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Truck_info;
import org.hi.base.organization.model.HiUser;

public abstract class Truck_infoAbstract extends BaseObject implements Serializable{

 	
 	/**
	 * 主键id
	 */	
	protected  Integer id;

	/**
	 * 版本控制version
	 */	
 	protected  Integer version;

 	 /**
	 * 车牌
	 */	
 	protected  String truck_card;

 	 /**
	 * 货车类型
	 */	
 	protected  Truck_type truck_type;

 	 /**
	 * 车长
	 */	
 	protected  Double truck_length;

 	 /**
	 * 车宽
	 */	
 	protected  Double truck_width;

 	 /**
	 * 载重
	 */	
 	protected  String truck_load;

 	 /**
	 * 保单号
	 */	
 	protected  String insure_id;

 	 /**
	 * 附件
	 */	
 	protected  Attachment insure_file_attachment;

 	 /**
	 * 货车状态
	 */	
 	protected  Integer truck_state;

 	 /**
	 * 信息状态
	 */	
 	protected  Integer info_state;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();

 	 /**
	 * 删除标识
	 */	
 	protected  Integer deleted = 0;

	private  List<Driver_info> driver_infos;

    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
     public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
    		if((version != null && this.version == null) || 
				this.version != null && (!this.version.equals(version) || version == null)){
        		this.setDirty(true);
        		this.oldValues.put("version", this.version);
        	}
        this.version = version;
    }
    
    public String getTruck_card() {
        return this.truck_card;
    }
    
    public void setTruck_card(String truck_card) {
    		if((truck_card != null && this.truck_card == null) || 
				this.truck_card != null && (!this.truck_card.equals(truck_card) || truck_card == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_card", this.truck_card);
        	}
        this.truck_card = truck_card;
    }
    
    public Truck_type getTruck_type() {
        return this.truck_type;
    }
    
    public void setTruck_type(Truck_type truck_type) {
    		if((truck_type != null && this.truck_type == null) || 
				this.truck_type != null && (!this.truck_type.equals(truck_type) || truck_type == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_type", this.truck_type);
        	}
        this.truck_type = truck_type;
    }
    
    public Double getTruck_length() {
        return this.truck_length;
    }
    
    public void setTruck_length(Double truck_length) {
    		if((truck_length != null && this.truck_length == null) || 
				this.truck_length != null && (!this.truck_length.equals(truck_length) || truck_length == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_length", this.truck_length);
        	}
        this.truck_length = truck_length;
    }
    
    public Double getTruck_width() {
        return this.truck_width;
    }
    
    public void setTruck_width(Double truck_width) {
    		if((truck_width != null && this.truck_width == null) || 
				this.truck_width != null && (!this.truck_width.equals(truck_width) || truck_width == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_width", this.truck_width);
        	}
        this.truck_width = truck_width;
    }
    
    public String getTruck_load() {
        return this.truck_load;
    }
    
    public void setTruck_load(String truck_load) {
    		if((truck_load != null && this.truck_load == null) || 
				this.truck_load != null && (!this.truck_load.equals(truck_load) || truck_load == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_load", this.truck_load);
        	}
        this.truck_load = truck_load;
    }
    
    public String getInsure_id() {
        return this.insure_id;
    }
    
    public void setInsure_id(String insure_id) {
    		if((insure_id != null && this.insure_id == null) || 
				this.insure_id != null && (!this.insure_id.equals(insure_id) || insure_id == null)){
        		this.setDirty(true);
        		this.oldValues.put("insure_id", this.insure_id);
        	}
        this.insure_id = insure_id;
    }
    
    public Attachment getInsure_file_attachment() {
        return this.insure_file_attachment;
    }
    
    public void setInsure_file_attachment(Attachment insure_file_attachment) {
    		if((insure_file_attachment != null && this.insure_file_attachment == null) || 
				this.insure_file_attachment != null && (!this.insure_file_attachment.equals(insure_file_attachment) || insure_file_attachment == null)){
        		this.setDirty(true);
        		this.oldValues.put("insure_file_attachment", this.insure_file_attachment);
        	}
        this.insure_file_attachment = insure_file_attachment;
    }
    
    public Integer getTruck_state() {
        return this.truck_state;
    }
    
    public void setTruck_state(Integer truck_state) {
    		if((truck_state != null && this.truck_state == null) || 
				this.truck_state != null && (!this.truck_state.equals(truck_state) || truck_state == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_state", this.truck_state);
        	}
        this.truck_state = truck_state;
    }
    
    public Integer getInfo_state() {
        return this.info_state;
    }
    
    public void setInfo_state(Integer info_state) {
    		if((info_state != null && this.info_state == null) || 
				this.info_state != null && (!this.info_state.equals(info_state) || info_state == null)){
        		this.setDirty(true);
        		this.oldValues.put("info_state", this.info_state);
        	}
        this.info_state = info_state;
    }
    
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    
    public Integer getDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(Integer deleted) {
    		if((deleted != null && this.deleted == null) || 
				this.deleted != null && (!this.deleted.equals(deleted) || deleted == null)){
        		this.setDirty(true);
        		this.oldValues.put("deleted", this.deleted);
        	}
        this.deleted = deleted;
    }
    

    public void setDriver_infos(List<Driver_info> driver_infos) {
        this.driver_infos = driver_infos;
    }

    public List<Driver_info> getDriver_infos() {
        return this.driver_infos;
    }

   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Truck_info) ) return false;
		 Truck_info castOther = ( Truck_info ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.append(getId());
		hcb.append("Truck_info".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id)
		.append("truck_card", this.truck_card)
		.append("truck_length", this.truck_length)
		.append("truck_width", this.truck_width)
		.append("truck_load", this.truck_load)
		.append("insure_id", this.insure_id)
		.append("deleted", this.deleted);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}