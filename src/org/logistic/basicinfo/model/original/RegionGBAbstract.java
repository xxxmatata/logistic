package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.logistic.basicinfo.model.RegionGB;
import org.hi.base.organization.model.HiUser;

public abstract class RegionGBAbstract extends BaseObject implements Serializable{

 	
 	/**
	 * 主键id
	 */	
	protected  Integer id;

	/**
	 * 版本控制version
	 */	
 	protected  Integer version;

 	 /**
	 * 区域编码
	 */	
 	protected  String r_code;

 	 /**
	 * 上级地区编码
	 */	
 	protected  String parent_r_code;

 	 /**
	 * 地区类型
	 */	
 	protected  Integer r_type;

 	 /**
	 * 地区名称
	 */	
 	protected  String r_name;

 	 /**
	 * 地区简称
	 */	
 	protected  String r_shortName;

 	 /**
	 * 英文名称
	 */	
 	protected  String r_EnName;

 	 /**
	 * 拼音简拼
	 */	
 	protected  String r_PY;

 	 /**
	 * 编码路径
	 */	
 	protected  String r_code_path;

 	 /**
	 * 名称路径
	 */	
 	protected  String r_name_path;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();

 	 /**
	 * 删除标识
	 */	
 	protected  Integer deleted = 0;


    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
     public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
    		if((version != null && this.version == null) || 
				this.version != null && (!this.version.equals(version) || version == null)){
        		this.setDirty(true);
        		this.oldValues.put("version", this.version);
        	}
        this.version = version;
    }
    
    public String getR_code() {
        return this.r_code;
    }
    
    public void setR_code(String r_code) {
    		if((r_code != null && this.r_code == null) || 
				this.r_code != null && (!this.r_code.equals(r_code) || r_code == null)){
        		this.setDirty(true);
        		this.oldValues.put("r_code", this.r_code);
        	}
        this.r_code = r_code;
    }
    
    public String getParent_r_code() {
        return this.parent_r_code;
    }
    
    public void setParent_r_code(String parent_r_code) {
    		if((parent_r_code != null && this.parent_r_code == null) || 
				this.parent_r_code != null && (!this.parent_r_code.equals(parent_r_code) || parent_r_code == null)){
        		this.setDirty(true);
        		this.oldValues.put("parent_r_code", this.parent_r_code);
        	}
        this.parent_r_code = parent_r_code;
    }
    
    public Integer getR_type() {
        return this.r_type;
    }
    
    public void setR_type(Integer r_type) {
    		if((r_type != null && this.r_type == null) || 
				this.r_type != null && (!this.r_type.equals(r_type) || r_type == null)){
        		this.setDirty(true);
        		this.oldValues.put("r_type", this.r_type);
        	}
        this.r_type = r_type;
    }
    
    public String getR_name() {
        return this.r_name;
    }
    
    public void setR_name(String r_name) {
    		if((r_name != null && this.r_name == null) || 
				this.r_name != null && (!this.r_name.equals(r_name) || r_name == null)){
        		this.setDirty(true);
        		this.oldValues.put("r_name", this.r_name);
        	}
        this.r_name = r_name;
    }
    
    public String getR_shortName() {
        return this.r_shortName;
    }
    
    public void setR_shortName(String r_shortName) {
    		if((r_shortName != null && this.r_shortName == null) || 
				this.r_shortName != null && (!this.r_shortName.equals(r_shortName) || r_shortName == null)){
        		this.setDirty(true);
        		this.oldValues.put("r_shortName", this.r_shortName);
        	}
        this.r_shortName = r_shortName;
    }
    
    public String getR_EnName() {
        return this.r_EnName;
    }
    
    public void setR_EnName(String r_EnName) {
    		if((r_EnName != null && this.r_EnName == null) || 
				this.r_EnName != null && (!this.r_EnName.equals(r_EnName) || r_EnName == null)){
        		this.setDirty(true);
        		this.oldValues.put("r_EnName", this.r_EnName);
        	}
        this.r_EnName = r_EnName;
    }
    
    public String getR_PY() {
        return this.r_PY;
    }
    
    public void setR_PY(String r_PY) {
    		if((r_PY != null && this.r_PY == null) || 
				this.r_PY != null && (!this.r_PY.equals(r_PY) || r_PY == null)){
        		this.setDirty(true);
        		this.oldValues.put("r_PY", this.r_PY);
        	}
        this.r_PY = r_PY;
    }
    
    public String getR_code_path() {
        return this.r_code_path;
    }
    
    public void setR_code_path(String r_code_path) {
    		if((r_code_path != null && this.r_code_path == null) || 
				this.r_code_path != null && (!this.r_code_path.equals(r_code_path) || r_code_path == null)){
        		this.setDirty(true);
        		this.oldValues.put("r_code_path", this.r_code_path);
        	}
        this.r_code_path = r_code_path;
    }
    
    public String getR_name_path() {
        return this.r_name_path;
    }
    
    public void setR_name_path(String r_name_path) {
    		if((r_name_path != null && this.r_name_path == null) || 
				this.r_name_path != null && (!this.r_name_path.equals(r_name_path) || r_name_path == null)){
        		this.setDirty(true);
        		this.oldValues.put("r_name_path", this.r_name_path);
        	}
        this.r_name_path = r_name_path;
    }
    
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    
    public Integer getDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(Integer deleted) {
    		if((deleted != null && this.deleted == null) || 
				this.deleted != null && (!this.deleted.equals(deleted) || deleted == null)){
        		this.setDirty(true);
        		this.oldValues.put("deleted", this.deleted);
        	}
        this.deleted = deleted;
    }
    


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof RegionGB) ) return false;
		 RegionGB castOther = ( RegionGB ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.append(getId());
		hcb.append("RegionGB".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id)
		.append("r_code", this.r_code)
		.append("parent_r_code", this.parent_r_code)
		.append("r_name", this.r_name)
		.append("r_shortName", this.r_shortName)
		.append("r_EnName", this.r_EnName)
		.append("r_PY", this.r_PY)
		.append("r_code_path", this.r_code_path)
		.append("r_name_path", this.r_name_path)
		.append("deleted", this.deleted);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}