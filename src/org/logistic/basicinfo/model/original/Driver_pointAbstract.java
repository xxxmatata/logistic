package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.logistic.basicinfo.model.Driver_point;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.RegionGB;
import org.hi.base.organization.model.HiUser;

public abstract class Driver_pointAbstract extends BaseObject implements Serializable{

 	
 	/**
	 * 主键id
	 */	
	protected  Integer id;

	/**
	 * 版本控制version
	 */	
 	protected  Integer version;

 	 /**
	 * 时间
	 */	
 	protected  Timestamp trip_date;

 	 /**
	 * 地点
	 */	
 	protected  RegionGB trip_region;

 	 /**
	 * 发布时间
	 */	
 	protected  Timestamp publish_time;

 	 /**
	 * driver_info
	 */	
 	protected  Driver_info driver_info;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();

 	 /**
	 * 删除标识
	 */	
 	protected  Integer deleted = 0;


    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
     public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
    		if((version != null && this.version == null) || 
				this.version != null && (!this.version.equals(version) || version == null)){
        		this.setDirty(true);
        		this.oldValues.put("version", this.version);
        	}
        this.version = version;
    }
    
    public Timestamp getTrip_date() {
        return this.trip_date;
    }
    
    public void setTrip_date(Timestamp trip_date) {
    		if((trip_date != null && this.trip_date == null) || 
				this.trip_date != null && (!this.trip_date.equals(trip_date) || trip_date == null)){
        		this.setDirty(true);
        		this.oldValues.put("trip_date", this.trip_date);
        	}
        this.trip_date = trip_date;
    }
    
    public RegionGB getTrip_region() {
        return this.trip_region;
    }
    
    public void setTrip_region(RegionGB trip_region) {
    		if((trip_region != null && this.trip_region == null) || 
				this.trip_region != null && (!this.trip_region.equals(trip_region) || trip_region == null)){
        		this.setDirty(true);
        		this.oldValues.put("trip_region", this.trip_region);
        	}
        this.trip_region = trip_region;
    }
    
    public Timestamp getPublish_time() {
        return this.publish_time;
    }
    
    public void setPublish_time(Timestamp publish_time) {
    		if((publish_time != null && this.publish_time == null) || 
				this.publish_time != null && (!this.publish_time.equals(publish_time) || publish_time == null)){
        		this.setDirty(true);
        		this.oldValues.put("publish_time", this.publish_time);
        	}
        this.publish_time = publish_time;
    }
    
    public Driver_info getDriver_info() {
        return this.driver_info;
    }
    
    public void setDriver_info(Driver_info driver_info) {
    		if((driver_info != null && this.driver_info == null) || 
				this.driver_info != null && (!this.driver_info.equals(driver_info) || driver_info == null)){
        		this.setDirty(true);
        		this.oldValues.put("driver_info", this.driver_info);
        	}
        this.driver_info = driver_info;
    }
    
   public BaseObject getParentEntity(){
	   return this.driver_info;
   }
   
   public void setParentEntity(BaseObject parent){
	   this.driver_info = (Driver_info)parent;
   }
   
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    
    public Integer getDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(Integer deleted) {
    		if((deleted != null && this.deleted == null) || 
				this.deleted != null && (!this.deleted.equals(deleted) || deleted == null)){
        		this.setDirty(true);
        		this.oldValues.put("deleted", this.deleted);
        	}
        this.deleted = deleted;
    }
    


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Driver_point) ) return false;
		 Driver_point castOther = ( Driver_point ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.append(getId());
		hcb.append("Driver_point".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id)
		.append("deleted", this.deleted);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}