package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.logistic.basicinfo.model.Goods_info;
import org.logistic.basicinfo.model.Goods2owner;
import org.hi.base.organization.model.HiUser;
import org.logistic.basicinfo.model.Goods_owner;

public abstract class Goods2ownerAbstract extends BaseObject implements Serializable{

 	
 	/**
	 * 主键id
	 */	
	protected  Integer id;

	/**
	 * 版本控制version
	 */	
 	protected  Integer version;

 	 /**
	 * goods_info
	 */	
 	protected  Goods_info goods_info;

 	 /**
	 * 货主
	 */	
 	protected  Goods_owner owner;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();


    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
     public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
    		if((version != null && this.version == null) || 
				this.version != null && (!this.version.equals(version) || version == null)){
        		this.setDirty(true);
        		this.oldValues.put("version", this.version);
        	}
        this.version = version;
    }
    
    public Goods_info getGoods_info() {
        return this.goods_info;
    }
    
    public void setGoods_info(Goods_info goods_info) {
    		if((goods_info != null && this.goods_info == null) || 
				this.goods_info != null && (!this.goods_info.equals(goods_info) || goods_info == null)){
        		this.setDirty(true);
        		this.oldValues.put("goods_info", this.goods_info);
        	}
        this.goods_info = goods_info;
    }
    
   public BaseObject getParentEntity(){
	   return this.goods_info;
   }
   
   public void setParentEntity(BaseObject parent){
	   this.goods_info = (Goods_info)parent;
   }
   
    public Goods_owner getOwner() {
        return this.owner;
    }
    
    public void setOwner(Goods_owner owner) {
    		if((owner != null && this.owner == null) || 
				this.owner != null && (!this.owner.equals(owner) || owner == null)){
        		this.setDirty(true);
        		this.oldValues.put("owner", this.owner);
        	}
        this.owner = owner;
    }
    
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Goods2owner) ) return false;
		 Goods2owner castOther = ( Goods2owner ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.append(getId());
		hcb.append("Goods2owner".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}