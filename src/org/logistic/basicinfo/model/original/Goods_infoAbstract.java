package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.logistic.basicinfo.model.Goods_type;
import org.logistic.basicinfo.model.Truck_type;
import org.logistic.basicinfo.model.Logistic_corp;
import org.logistic.basicinfo.model.Driver2goods_log;
import org.logistic.basicinfo.model.Driver2goods;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Goods_info;
import org.logistic.basicinfo.model.RegionGB;
import org.logistic.basicinfo.model.Goods2owner;
import org.hi.base.organization.model.HiUser;

public abstract class Goods_infoAbstract extends BaseObject implements Serializable{

 	
 	/**
	 * 主键id
	 */	
	protected  Integer id;

	/**
	 * 版本控制version
	 */	
 	protected  Integer version;

 	 /**
	 * 发布时间
	 */	
 	protected  Timestamp publishTime;

 	 /**
	 * 起点1
	 */	
 	protected  RegionGB origin;

 	 /**
	 * 终点1
	 */	
 	protected  RegionGB destination;

 	 /**
	 * 物流公司
	 */	
 	protected  Logistic_corp logistic_corp;

 	 /**
	 * 运价
	 */	
 	protected  Double freightRate;

 	 /**
	 * 运价单位
	 */	
 	protected  Integer freightUnits;

 	 /**
	 * 装货时间
	 */	
 	protected  Timestamp loadTime;

 	 /**
	 * 装货地点
	 */	
 	protected  String loadPlace;

 	 /**
	 * 求车数量
	 */	
 	protected  Integer truckNumber;

 	 /**
	 * 车型
	 */	
 	protected  Truck_type truck_type;

 	 /**
	 * 车长
	 */	
 	protected  Double truck_length = 0.0;

 	 /**
	 * 车宽
	 */	
 	protected  Double truck_width = 0.0;

 	 /**
	 * 载重
	 */	
 	protected  Double truck_load = 0.0;

 	 /**
	 * 状态
	 */	
 	protected  Integer info_state;

 	 /**
	 * 承运司机
	 */	
 	protected  Driver_info driver;

 	 /**
	 * 成交时间
	 */	
 	protected  Timestamp orderTime;

 	 /**
	 * 装货完成时间
	 */	
 	protected  Timestamp realLoadTime;

 	 /**
	 * 卸货时间
	 */	
 	protected  Timestamp unloadTime;

 	 /**
	 * 货物类型
	 */	
 	protected  Goods_type goodsType;

 	 /**
	 * 备注
	 */	
 	protected  String remark;

 	 /**
	 * 评价
	 */	
 	protected  Integer taskRank;

 	 /**
	 * 数量单位
	 */	
 	protected  Integer goodsUnits;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();

 	 /**
	 * 删除标识
	 */	
 	protected  Integer deleted = 0;

	private  List<Goods2owner> goods2owners;
	private  List<Driver2goods> driver2goodss;
	private  List<Driver2goods_log> driver2goods_logs;
	
	private int needNum;
	

	/**
	 * 还需多少车
	 * @return
	 */
    public int getNeedNum() {
		return this.truckNumber.intValue() - this.getDriver2goodss().size();
	}

	public void setNeedNum(int needNum) {
		this.needNum = needNum;
	}

	public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
     public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
    		if((version != null && this.version == null) || 
				this.version != null && (!this.version.equals(version) || version == null)){
        		this.setDirty(true);
        		this.oldValues.put("version", this.version);
        	}
        this.version = version;
    }
    
    public Timestamp getPublishTime() {
        return this.publishTime;
    }
    
    public void setPublishTime(Timestamp publishTime) {
    		if((publishTime != null && this.publishTime == null) || 
				this.publishTime != null && (!this.publishTime.equals(publishTime) || publishTime == null)){
        		this.setDirty(true);
        		this.oldValues.put("publishTime", this.publishTime);
        	}
        this.publishTime = publishTime;
    }
    
    public RegionGB getOrigin() {
        return this.origin;
    }
    
    public void setOrigin(RegionGB origin) {
    		if((origin != null && this.origin == null) || 
				this.origin != null && (!this.origin.equals(origin) || origin == null)){
        		this.setDirty(true);
        		this.oldValues.put("origin", this.origin);
        	}
        this.origin = origin;
    }
    
    public RegionGB getDestination() {
        return this.destination;
    }
    
    public void setDestination(RegionGB destination) {
    		if((destination != null && this.destination == null) || 
				this.destination != null && (!this.destination.equals(destination) || destination == null)){
        		this.setDirty(true);
        		this.oldValues.put("destination", this.destination);
        	}
        this.destination = destination;
    }
    
    public Logistic_corp getLogistic_corp() {
        return this.logistic_corp;
    }
    
    public void setLogistic_corp(Logistic_corp logistic_corp) {
    		if((logistic_corp != null && this.logistic_corp == null) || 
				this.logistic_corp != null && (!this.logistic_corp.equals(logistic_corp) || logistic_corp == null)){
        		this.setDirty(true);
        		this.oldValues.put("logistic_corp", this.logistic_corp);
        	}
        this.logistic_corp = logistic_corp;
    }
    
    public Double getFreightRate() {
        return this.freightRate;
    }
    
    public void setFreightRate(Double freightRate) {
    		if((freightRate != null && this.freightRate == null) || 
				this.freightRate != null && (!this.freightRate.equals(freightRate) || freightRate == null)){
        		this.setDirty(true);
        		this.oldValues.put("freightRate", this.freightRate);
        	}
        this.freightRate = freightRate;
    }
    
    public Integer getFreightUnits() {
        return this.freightUnits;
    }
    
    public void setFreightUnits(Integer freightUnits) {
    		if((freightUnits != null && this.freightUnits == null) || 
				this.freightUnits != null && (!this.freightUnits.equals(freightUnits) || freightUnits == null)){
        		this.setDirty(true);
        		this.oldValues.put("freightUnits", this.freightUnits);
        	}
        this.freightUnits = freightUnits;
    }
    
    public Timestamp getLoadTime() {
        return this.loadTime;
    }
    
    public void setLoadTime(Timestamp loadTime) {
    		if((loadTime != null && this.loadTime == null) || 
				this.loadTime != null && (!this.loadTime.equals(loadTime) || loadTime == null)){
        		this.setDirty(true);
        		this.oldValues.put("loadTime", this.loadTime);
        	}
        this.loadTime = loadTime;
    }
    
    public String getLoadPlace() {
        return this.loadPlace;
    }
    
    public void setLoadPlace(String loadPlace) {
    		if((loadPlace != null && this.loadPlace == null) || 
				this.loadPlace != null && (!this.loadPlace.equals(loadPlace) || loadPlace == null)){
        		this.setDirty(true);
        		this.oldValues.put("loadPlace", this.loadPlace);
        	}
        this.loadPlace = loadPlace;
    }
    
    public Integer getTruckNumber() {
        return this.truckNumber;
    }
    
    public void setTruckNumber(Integer truckNumber) {
    		if((truckNumber != null && this.truckNumber == null) || 
				this.truckNumber != null && (!this.truckNumber.equals(truckNumber) || truckNumber == null)){
        		this.setDirty(true);
        		this.oldValues.put("truckNumber", this.truckNumber);
        	}
        this.truckNumber = truckNumber;
    }
    
    public Truck_type getTruck_type() {
        return this.truck_type;
    }
    
    public void setTruck_type(Truck_type truck_type) {
    		if((truck_type != null && this.truck_type == null) || 
				this.truck_type != null && (!this.truck_type.equals(truck_type) || truck_type == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_type", this.truck_type);
        	}
        this.truck_type = truck_type;
    }
    
    public Double getTruck_length() {
        return this.truck_length;
    }
    
    public void setTruck_length(Double truck_length) {
    		if((truck_length != null && this.truck_length == null) || 
				this.truck_length != null && (!this.truck_length.equals(truck_length) || truck_length == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_length", this.truck_length);
        	}
        this.truck_length = truck_length;
    }
    
    public Double getTruck_width() {
        return this.truck_width;
    }
    
    public void setTruck_width(Double truck_width) {
    		if((truck_width != null && this.truck_width == null) || 
				this.truck_width != null && (!this.truck_width.equals(truck_width) || truck_width == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_width", this.truck_width);
        	}
        this.truck_width = truck_width;
    }
    
    public Double getTruck_load() {
        return this.truck_load;
    }
    
    public void setTruck_load(Double truck_load) {
    		if((truck_load != null && this.truck_load == null) || 
				this.truck_load != null && (!this.truck_load.equals(truck_load) || truck_load == null)){
        		this.setDirty(true);
        		this.oldValues.put("truck_load", this.truck_load);
        	}
        this.truck_load = truck_load;
    }
    
    public Integer getInfo_state() {
        return this.info_state;
    }
    
    public void setInfo_state(Integer info_state) {
    		if((info_state != null && this.info_state == null) || 
				this.info_state != null && (!this.info_state.equals(info_state) || info_state == null)){
        		this.setDirty(true);
        		this.oldValues.put("info_state", this.info_state);
        	}
        this.info_state = info_state;
    }
    
    public Driver_info getDriver() {
        return this.driver;
    }
    
    public void setDriver(Driver_info driver) {
    		if((driver != null && this.driver == null) || 
				this.driver != null && (!this.driver.equals(driver) || driver == null)){
        		this.setDirty(true);
        		this.oldValues.put("driver", this.driver);
        	}
        this.driver = driver;
    }
    
    public Timestamp getOrderTime() {
        return this.orderTime;
    }
    
    public void setOrderTime(Timestamp orderTime) {
    		if((orderTime != null && this.orderTime == null) || 
				this.orderTime != null && (!this.orderTime.equals(orderTime) || orderTime == null)){
        		this.setDirty(true);
        		this.oldValues.put("orderTime", this.orderTime);
        	}
        this.orderTime = orderTime;
    }
    
    public Timestamp getRealLoadTime() {
        return this.realLoadTime;
    }
    
    public void setRealLoadTime(Timestamp realLoadTime) {
    		if((realLoadTime != null && this.realLoadTime == null) || 
				this.realLoadTime != null && (!this.realLoadTime.equals(realLoadTime) || realLoadTime == null)){
        		this.setDirty(true);
        		this.oldValues.put("realLoadTime", this.realLoadTime);
        	}
        this.realLoadTime = realLoadTime;
    }
    
    public Timestamp getUnloadTime() {
        return this.unloadTime;
    }
    
    public void setUnloadTime(Timestamp unloadTime) {
    		if((unloadTime != null && this.unloadTime == null) || 
				this.unloadTime != null && (!this.unloadTime.equals(unloadTime) || unloadTime == null)){
        		this.setDirty(true);
        		this.oldValues.put("unloadTime", this.unloadTime);
        	}
        this.unloadTime = unloadTime;
    }
    
    public Goods_type getGoodsType() {
        return this.goodsType;
    }
    
    public void setGoodsType(Goods_type goodsType) {
    		if((goodsType != null && this.goodsType == null) || 
				this.goodsType != null && (!this.goodsType.equals(goodsType) || goodsType == null)){
        		this.setDirty(true);
        		this.oldValues.put("goodsType", this.goodsType);
        	}
        this.goodsType = goodsType;
    }
    
    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
    		if((remark != null && this.remark == null) || 
				this.remark != null && (!this.remark.equals(remark) || remark == null)){
        		this.setDirty(true);
        		this.oldValues.put("remark", this.remark);
        	}
        this.remark = remark;
    }
    
    public Integer getTaskRank() {
        return this.taskRank;
    }
    
    public void setTaskRank(Integer taskRank) {
    		if((taskRank != null && this.taskRank == null) || 
				this.taskRank != null && (!this.taskRank.equals(taskRank) || taskRank == null)){
        		this.setDirty(true);
        		this.oldValues.put("taskRank", this.taskRank);
        	}
        this.taskRank = taskRank;
    }
    
    public Integer getGoodsUnits() {
        return this.goodsUnits;
    }
    
    public void setGoodsUnits(Integer goodsUnits) {
    		if((goodsUnits != null && this.goodsUnits == null) || 
				this.goodsUnits != null && (!this.goodsUnits.equals(goodsUnits) || goodsUnits == null)){
        		this.setDirty(true);
        		this.oldValues.put("goodsUnits", this.goodsUnits);
        	}
        this.goodsUnits = goodsUnits;
    }
    
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    
    public Integer getDeleted() {
        return this.deleted;
    }
    
    public void setDeleted(Integer deleted) {
    		if((deleted != null && this.deleted == null) || 
				this.deleted != null && (!this.deleted.equals(deleted) || deleted == null)){
        		this.setDirty(true);
        		this.oldValues.put("deleted", this.deleted);
        	}
        this.deleted = deleted;
    }
    

    public void setGoods2owners(List<Goods2owner> goods2owners) {
        this.goods2owners = goods2owners;
    }

    public List<Goods2owner> getGoods2owners() {
        return this.goods2owners;
    }
    public void setDriver2goodss(List<Driver2goods> driver2goodss) {
        this.driver2goodss = driver2goodss;
    }

    public List<Driver2goods> getDriver2goodss() {
        return this.driver2goodss;
    }
    public void setDriver2goods_logs(List<Driver2goods_log> driver2goods_logs) {
        this.driver2goods_logs = driver2goods_logs;
    }

    public List<Driver2goods_log> getDriver2goods_logs() {
        return this.driver2goods_logs;
    }

   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Goods_info) ) return false;
		 Goods_info castOther = ( Goods_info ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.append(getId());
		hcb.append("Goods_info".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id)
		.append("freightRate", this.freightRate)
		.append("loadPlace", this.loadPlace)
		.append("truckNumber", this.truckNumber)
		.append("truck_length", this.truck_length)
		.append("truck_width", this.truck_width)
		.append("truck_load", this.truck_load)
		.append("remark", this.remark)
		.append("deleted", this.deleted);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}