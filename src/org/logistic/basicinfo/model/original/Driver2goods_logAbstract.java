package org.logistic.basicinfo.model.original;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.framework.model.BaseObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.logistic.basicinfo.model.Driver2goods_log;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Goods_info;
import org.hi.base.organization.model.HiUser;

public abstract class Driver2goods_logAbstract extends BaseObject implements Serializable{

 	
 	/**
	 * 主键id
	 */	
	protected  Integer id;

	/**
	 * 版本控制version
	 */	
 	protected  Integer version;

 	 /**
	 * 司机
	 */	
 	protected  Driver_info driver;

 	 /**
	 * 关系类型
	 */	
 	protected  Integer d2gType;

 	 /**
	 * 发生时间
	 */	
 	protected  Timestamp opTime;

 	 /**
	 * 评价
	 */	
 	protected  Integer taskRank;

 	 /**
	 * 备注
	 */	
 	protected  String remark;

 	 /**
	 * goods_info
	 */	
 	protected  Goods_info goods_info;

 	 /**
	 * 创建人
	 */	
 	protected  HiUser creator = org.hi.framework.security.context.UserContextHelper.getUser();


    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
    		if((id != null && this.id == null) || 
				this.id != null && (!this.id.equals(id) || id == null)){
        		this.setDirty(true);
        		this.oldValues.put("id", this.id);
        	}
        this.id = id;
    }
    
     public Integer getVersion() {
        return this.version;
    }
    
    public void setVersion(Integer version) {
    		if((version != null && this.version == null) || 
				this.version != null && (!this.version.equals(version) || version == null)){
        		this.setDirty(true);
        		this.oldValues.put("version", this.version);
        	}
        this.version = version;
    }
    
    public Driver_info getDriver() {
        return this.driver;
    }
    
    public void setDriver(Driver_info driver) {
    		if((driver != null && this.driver == null) || 
				this.driver != null && (!this.driver.equals(driver) || driver == null)){
        		this.setDirty(true);
        		this.oldValues.put("driver", this.driver);
        	}
        this.driver = driver;
    }
    
    public Integer getD2gType() {
        return this.d2gType;
    }
    
    public void setD2gType(Integer d2gType) {
    		if((d2gType != null && this.d2gType == null) || 
				this.d2gType != null && (!this.d2gType.equals(d2gType) || d2gType == null)){
        		this.setDirty(true);
        		this.oldValues.put("d2gType", this.d2gType);
        	}
        this.d2gType = d2gType;
    }
    
    public Timestamp getOpTime() {
        return this.opTime;
    }
    
    public void setOpTime(Timestamp opTime) {
    		if((opTime != null && this.opTime == null) || 
				this.opTime != null && (!this.opTime.equals(opTime) || opTime == null)){
        		this.setDirty(true);
        		this.oldValues.put("opTime", this.opTime);
        	}
        this.opTime = opTime;
    }
    
    public Integer getTaskRank() {
        return this.taskRank;
    }
    
    public void setTaskRank(Integer taskRank) {
    		if((taskRank != null && this.taskRank == null) || 
				this.taskRank != null && (!this.taskRank.equals(taskRank) || taskRank == null)){
        		this.setDirty(true);
        		this.oldValues.put("taskRank", this.taskRank);
        	}
        this.taskRank = taskRank;
    }
    
    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
    		if((remark != null && this.remark == null) || 
				this.remark != null && (!this.remark.equals(remark) || remark == null)){
        		this.setDirty(true);
        		this.oldValues.put("remark", this.remark);
        	}
        this.remark = remark;
    }
    
    public Goods_info getGoods_info() {
        return this.goods_info;
    }
    
    public void setGoods_info(Goods_info goods_info) {
    		if((goods_info != null && this.goods_info == null) || 
				this.goods_info != null && (!this.goods_info.equals(goods_info) || goods_info == null)){
        		this.setDirty(true);
        		this.oldValues.put("goods_info", this.goods_info);
        	}
        this.goods_info = goods_info;
    }
    
   public BaseObject getParentEntity(){
	   return this.goods_info;
   }
   
   public void setParentEntity(BaseObject parent){
	   this.goods_info = (Goods_info)parent;
   }
   
    public HiUser getCreator() {
        return this.creator;
    }
    
    public void setCreator(HiUser creator) {
    		if((creator != null && this.creator == null) || 
				this.creator != null && (!this.creator.equals(creator) || creator == null)){
        		this.setDirty(true);
        		this.oldValues.put("creator", this.creator);
        	}
        this.creator = creator;
    }
    


   public boolean equals(Object other) {
         if ( (this == other ) ) return true;
		 if ( (other == null ) ) return false;
		 if ( !(other instanceof Driver2goods_log) ) return false;
		 Driver2goods_log castOther = ( Driver2goods_log ) other; 
		 
		 return  ( (this.getId()==castOther.getId()) || ( this.getId()!=null && castOther.getId()!=null && this.getId().equals(castOther.getId()) ) );
   }
   
   public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
        hcb.append(getId());
		hcb.append("Driver2goods_log".hashCode());
        return hcb.toHashCode();
    }

   public String toString() {
       ToStringBuilder sb = new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE);
       sb.append("id", this.id);
      
        return sb.toString();
   }

   public Serializable getPrimarykey(){
   		return id;
   }



}