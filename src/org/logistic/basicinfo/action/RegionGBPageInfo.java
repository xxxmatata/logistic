package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.hi.base.organization.action.HiUserPageInfo;

public class RegionGBPageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  String  f_r_code;
 	protected  String  f_r_code_op;
	protected  String  f_parent_r_code;
 	protected  String  f_parent_r_code_op;
	protected  Integer  f_r_type;
 	protected  String  f_r_type_op;
	protected  String  f_r_name;
 	protected  String  f_r_name_op;
	protected  String  f_r_shortName;
 	protected  String  f_r_shortName_op;
	protected  String  f_r_EnName;
 	protected  String  f_r_EnName_op;
	protected  String  f_r_PY;
 	protected  String  f_r_PY_op;
	protected  String  f_r_code_path;
 	protected  String  f_r_code_path_op;
	protected  String  f_r_name_path;
 	protected  String  f_r_name_path_op;
	protected  Integer  f_deleted;
 	protected  String  f_deleted_op;

 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public String getF_r_code() {
        return this.f_r_code;
    }
    
    public void setF_r_code(String f_r_code) {
        this.f_r_code = f_r_code;
    }
    
    public String getF_r_code_op() {
        return this.f_r_code_op;
    }
    
    public void setF_r_code_op(String f_r_code_op) {
        this.f_r_code_op = f_r_code_op;
    }
   
    public String getF_parent_r_code() {
        return this.f_parent_r_code;
    }
    
    public void setF_parent_r_code(String f_parent_r_code) {
        this.f_parent_r_code = f_parent_r_code;
    }
    
    public String getF_parent_r_code_op() {
        return this.f_parent_r_code_op;
    }
    
    public void setF_parent_r_code_op(String f_parent_r_code_op) {
        this.f_parent_r_code_op = f_parent_r_code_op;
    }
   
    public Integer getF_r_type() {
        return this.f_r_type;
    }
    
    public void setF_r_type(Integer f_r_type) {
        this.f_r_type = f_r_type;
    }
    
    public String getF_r_type_op() {
        return this.f_r_type_op;
    }
    
    public void setF_r_type_op(String f_r_type_op) {
        this.f_r_type_op = f_r_type_op;
    }
   
    public String getF_r_name() {
        return this.f_r_name;
    }
    
    public void setF_r_name(String f_r_name) {
        this.f_r_name = f_r_name;
    }
    
    public String getF_r_name_op() {
        return this.f_r_name_op;
    }
    
    public void setF_r_name_op(String f_r_name_op) {
        this.f_r_name_op = f_r_name_op;
    }
   
    public String getF_r_shortName() {
        return this.f_r_shortName;
    }
    
    public void setF_r_shortName(String f_r_shortName) {
        this.f_r_shortName = f_r_shortName;
    }
    
    public String getF_r_shortName_op() {
        return this.f_r_shortName_op;
    }
    
    public void setF_r_shortName_op(String f_r_shortName_op) {
        this.f_r_shortName_op = f_r_shortName_op;
    }
   
    public String getF_r_EnName() {
        return this.f_r_EnName;
    }
    
    public void setF_r_EnName(String f_r_EnName) {
        this.f_r_EnName = f_r_EnName;
    }
    
    public String getF_r_EnName_op() {
        return this.f_r_EnName_op;
    }
    
    public void setF_r_EnName_op(String f_r_EnName_op) {
        this.f_r_EnName_op = f_r_EnName_op;
    }
   
    public String getF_r_PY() {
        return this.f_r_PY;
    }
    
    public void setF_r_PY(String f_r_PY) {
        this.f_r_PY = f_r_PY;
    }
    
    public String getF_r_PY_op() {
        return this.f_r_PY_op;
    }
    
    public void setF_r_PY_op(String f_r_PY_op) {
        this.f_r_PY_op = f_r_PY_op;
    }
   
    public String getF_r_code_path() {
        return this.f_r_code_path;
    }
    
    public void setF_r_code_path(String f_r_code_path) {
        this.f_r_code_path = f_r_code_path;
    }
    
    public String getF_r_code_path_op() {
        return this.f_r_code_path_op;
    }
    
    public void setF_r_code_path_op(String f_r_code_path_op) {
        this.f_r_code_path_op = f_r_code_path_op;
    }
   
    public String getF_r_name_path() {
        return this.f_r_name_path;
    }
    
    public void setF_r_name_path(String f_r_name_path) {
        this.f_r_name_path = f_r_name_path;
    }
    
    public String getF_r_name_path_op() {
        return this.f_r_name_path_op;
    }
    
    public void setF_r_name_path_op(String f_r_name_path_op) {
        this.f_r_name_path_op = f_r_name_path_op;
    }
   
    public Integer getF_deleted() {
        return this.f_deleted;
    }
    
    public void setF_deleted(Integer f_deleted) {
        this.f_deleted = f_deleted;
    }
    
    public String getF_deleted_op() {
        return this.f_deleted_op;
    }
    
    public void setF_deleted_op(String f_deleted_op) {
        this.f_deleted_op = f_deleted_op;
    }
   
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
