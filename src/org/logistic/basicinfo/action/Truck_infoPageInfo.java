package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.logistic.basicinfo.action.Truck_typePageInfo;
import org.hi.common.attachment.action.AttachmentPageInfo;
import org.hi.base.organization.action.HiUserPageInfo;

public class Truck_infoPageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  String  f_truck_card;
 	protected  String  f_truck_card_op;
	protected  Double  f_truck_length;
 	protected  String  f_truck_length_op;
	protected  Double  f_truck_width;
 	protected  String  f_truck_width_op;
	protected  String  f_truck_load;
 	protected  String  f_truck_load_op;
	protected  String  f_insure_id;
 	protected  String  f_insure_id_op;
	protected  Integer  f_truck_state;
 	protected  String  f_truck_state_op;
	protected  Integer  f_info_state;
 	protected  String  f_info_state_op;
	protected  Integer  f_deleted;
 	protected  String  f_deleted_op;

 	protected  Truck_typePageInfo truck_type;
 	protected  AttachmentPageInfo insure_file_attachment;
 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public String getF_truck_card() {
        return this.f_truck_card;
    }
    
    public void setF_truck_card(String f_truck_card) {
        this.f_truck_card = f_truck_card;
    }
    
    public String getF_truck_card_op() {
        return this.f_truck_card_op;
    }
    
    public void setF_truck_card_op(String f_truck_card_op) {
        this.f_truck_card_op = f_truck_card_op;
    }
   
    public Double getF_truck_length() {
        return this.f_truck_length;
    }
    
    public void setF_truck_length(Double f_truck_length) {
        this.f_truck_length = f_truck_length;
    }
    
    public String getF_truck_length_op() {
        return this.f_truck_length_op;
    }
    
    public void setF_truck_length_op(String f_truck_length_op) {
        this.f_truck_length_op = f_truck_length_op;
    }
   
    public Double getF_truck_width() {
        return this.f_truck_width;
    }
    
    public void setF_truck_width(Double f_truck_width) {
        this.f_truck_width = f_truck_width;
    }
    
    public String getF_truck_width_op() {
        return this.f_truck_width_op;
    }
    
    public void setF_truck_width_op(String f_truck_width_op) {
        this.f_truck_width_op = f_truck_width_op;
    }
   
    public String getF_truck_load() {
        return this.f_truck_load;
    }
    
    public void setF_truck_load(String f_truck_load) {
        this.f_truck_load = f_truck_load;
    }
    
    public String getF_truck_load_op() {
        return this.f_truck_load_op;
    }
    
    public void setF_truck_load_op(String f_truck_load_op) {
        this.f_truck_load_op = f_truck_load_op;
    }
   
    public String getF_insure_id() {
        return this.f_insure_id;
    }
    
    public void setF_insure_id(String f_insure_id) {
        this.f_insure_id = f_insure_id;
    }
    
    public String getF_insure_id_op() {
        return this.f_insure_id_op;
    }
    
    public void setF_insure_id_op(String f_insure_id_op) {
        this.f_insure_id_op = f_insure_id_op;
    }
   
    public Integer getF_truck_state() {
        return this.f_truck_state;
    }
    
    public void setF_truck_state(Integer f_truck_state) {
        this.f_truck_state = f_truck_state;
    }
    
    public String getF_truck_state_op() {
        return this.f_truck_state_op;
    }
    
    public void setF_truck_state_op(String f_truck_state_op) {
        this.f_truck_state_op = f_truck_state_op;
    }
   
    public Integer getF_info_state() {
        return this.f_info_state;
    }
    
    public void setF_info_state(Integer f_info_state) {
        this.f_info_state = f_info_state;
    }
    
    public String getF_info_state_op() {
        return this.f_info_state_op;
    }
    
    public void setF_info_state_op(String f_info_state_op) {
        this.f_info_state_op = f_info_state_op;
    }
   
    public Integer getF_deleted() {
        return this.f_deleted;
    }
    
    public void setF_deleted(Integer f_deleted) {
        this.f_deleted = f_deleted;
    }
    
    public String getF_deleted_op() {
        return this.f_deleted_op;
    }
    
    public void setF_deleted_op(String f_deleted_op) {
        this.f_deleted_op = f_deleted_op;
    }
   
	public Truck_typePageInfo getTruck_type() {
		return truck_type;
	}

	public void setTruck_type(Truck_typePageInfo truck_type) {
		this.truck_type = truck_type;
	}
	public AttachmentPageInfo getInsure_file_attachment() {
		return insure_file_attachment;
	}

	public void setInsure_file_attachment(AttachmentPageInfo insure_file_attachment) {
		this.insure_file_attachment = insure_file_attachment;
	}
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
