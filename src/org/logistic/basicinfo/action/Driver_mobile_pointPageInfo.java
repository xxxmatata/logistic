package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.hi.base.organization.action.HiUserPageInfo;

public class Driver_mobile_pointPageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  String  f_imsi;
 	protected  String  f_imsi_op;
	protected  String  f_mdn;
 	protected  String  f_mdn_op;
	protected  String  f_os_version;
 	protected  String  f_os_version_op;
	protected  Double  f_lng;
 	protected  String  f_lng_op;
	protected  Double  f_lat;
 	protected  String  f_lat_op;
	protected  Double  f_alt;
 	protected  String  f_alt_op;
	protected  Double  f_speed;
 	protected  String  f_speed_op;
	protected  Timestamp  f_upd_time;
 	protected  String  f_upd_time_op;
	protected  Timestamp  f_upd_time01;
	protected  String  f_upd_time01_op;
	protected  String  f_loc_type;
 	protected  String  f_loc_type_op;
	protected  String  f_client_ip;
 	protected  String  f_client_ip_op;
	protected  String  f_loc_desc;
 	protected  String  f_loc_desc_op;
	protected  String  f_imei;
 	protected  String  f_imei_op;
	protected  String  f_mcc;
 	protected  String  f_mcc_op;
	protected  String  f_mnc;
 	protected  String  f_mnc_op;
	protected  String  f_lac;
 	protected  String  f_lac_op;
	protected  String  f_sid;
 	protected  String  f_sid_op;
	protected  String  f_nid;
 	protected  String  f_nid_op;
	protected  String  f_bid;
 	protected  String  f_bid_op;
	protected  Double  f_radius;
 	protected  String  f_radius_op;
	protected  String  f_error_code;
 	protected  String  f_error_code_op;
	protected  Timestamp  f_loc_time;
 	protected  String  f_loc_time_op;
	protected  Timestamp  f_loc_time01;
	protected  String  f_loc_time01_op;
	protected  Integer  f_satellite_num;
 	protected  String  f_satellite_num_op;
	protected  String  f_uuid;
 	protected  String  f_uuid_op;

 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public String getF_imsi() {
        return this.f_imsi;
    }
    
    public void setF_imsi(String f_imsi) {
        this.f_imsi = f_imsi;
    }
    
    public String getF_imsi_op() {
        return this.f_imsi_op;
    }
    
    public void setF_imsi_op(String f_imsi_op) {
        this.f_imsi_op = f_imsi_op;
    }
   
    public String getF_mdn() {
        return this.f_mdn;
    }
    
    public void setF_mdn(String f_mdn) {
        this.f_mdn = f_mdn;
    }
    
    public String getF_mdn_op() {
        return this.f_mdn_op;
    }
    
    public void setF_mdn_op(String f_mdn_op) {
        this.f_mdn_op = f_mdn_op;
    }
   
    public String getF_os_version() {
        return this.f_os_version;
    }
    
    public void setF_os_version(String f_os_version) {
        this.f_os_version = f_os_version;
    }
    
    public String getF_os_version_op() {
        return this.f_os_version_op;
    }
    
    public void setF_os_version_op(String f_os_version_op) {
        this.f_os_version_op = f_os_version_op;
    }
   
    public Double getF_lng() {
        return this.f_lng;
    }
    
    public void setF_lng(Double f_lng) {
        this.f_lng = f_lng;
    }
    
    public String getF_lng_op() {
        return this.f_lng_op;
    }
    
    public void setF_lng_op(String f_lng_op) {
        this.f_lng_op = f_lng_op;
    }
   
    public Double getF_lat() {
        return this.f_lat;
    }
    
    public void setF_lat(Double f_lat) {
        this.f_lat = f_lat;
    }
    
    public String getF_lat_op() {
        return this.f_lat_op;
    }
    
    public void setF_lat_op(String f_lat_op) {
        this.f_lat_op = f_lat_op;
    }
   
    public Double getF_alt() {
        return this.f_alt;
    }
    
    public void setF_alt(Double f_alt) {
        this.f_alt = f_alt;
    }
    
    public String getF_alt_op() {
        return this.f_alt_op;
    }
    
    public void setF_alt_op(String f_alt_op) {
        this.f_alt_op = f_alt_op;
    }
   
    public Double getF_speed() {
        return this.f_speed;
    }
    
    public void setF_speed(Double f_speed) {
        this.f_speed = f_speed;
    }
    
    public String getF_speed_op() {
        return this.f_speed_op;
    }
    
    public void setF_speed_op(String f_speed_op) {
        this.f_speed_op = f_speed_op;
    }
   
    public Timestamp getF_upd_time() {
        return this.f_upd_time;
    }
    
    public void setF_upd_time(Timestamp f_upd_time) {
        this.f_upd_time = f_upd_time;
    }
    
    public String getF_upd_time_op() {
        return this.f_upd_time_op;
    }
    
    public void setF_upd_time_op(String f_upd_time_op) {
        this.f_upd_time_op = f_upd_time_op;
    }
    public Timestamp getF_upd_time01() {
        return this.f_upd_time01;
    }
    
    public void setF_upd_time01(Timestamp f_upd_time01) {
        this.f_upd_time01 = f_upd_time01;
    }
    
    public String getF_upd_time01_op() {
        return this.f_upd_time01_op;
    }
    
    public void setF_upd_time01_op(String f_upd_time01_op) {
        this.f_upd_time01_op = f_upd_time01_op;
    }
   
    public String getF_loc_type() {
        return this.f_loc_type;
    }
    
    public void setF_loc_type(String f_loc_type) {
        this.f_loc_type = f_loc_type;
    }
    
    public String getF_loc_type_op() {
        return this.f_loc_type_op;
    }
    
    public void setF_loc_type_op(String f_loc_type_op) {
        this.f_loc_type_op = f_loc_type_op;
    }
   
    public String getF_client_ip() {
        return this.f_client_ip;
    }
    
    public void setF_client_ip(String f_client_ip) {
        this.f_client_ip = f_client_ip;
    }
    
    public String getF_client_ip_op() {
        return this.f_client_ip_op;
    }
    
    public void setF_client_ip_op(String f_client_ip_op) {
        this.f_client_ip_op = f_client_ip_op;
    }
   
    public String getF_loc_desc() {
        return this.f_loc_desc;
    }
    
    public void setF_loc_desc(String f_loc_desc) {
        this.f_loc_desc = f_loc_desc;
    }
    
    public String getF_loc_desc_op() {
        return this.f_loc_desc_op;
    }
    
    public void setF_loc_desc_op(String f_loc_desc_op) {
        this.f_loc_desc_op = f_loc_desc_op;
    }
   
    public String getF_imei() {
        return this.f_imei;
    }
    
    public void setF_imei(String f_imei) {
        this.f_imei = f_imei;
    }
    
    public String getF_imei_op() {
        return this.f_imei_op;
    }
    
    public void setF_imei_op(String f_imei_op) {
        this.f_imei_op = f_imei_op;
    }
   
    public String getF_mcc() {
        return this.f_mcc;
    }
    
    public void setF_mcc(String f_mcc) {
        this.f_mcc = f_mcc;
    }
    
    public String getF_mcc_op() {
        return this.f_mcc_op;
    }
    
    public void setF_mcc_op(String f_mcc_op) {
        this.f_mcc_op = f_mcc_op;
    }
   
    public String getF_mnc() {
        return this.f_mnc;
    }
    
    public void setF_mnc(String f_mnc) {
        this.f_mnc = f_mnc;
    }
    
    public String getF_mnc_op() {
        return this.f_mnc_op;
    }
    
    public void setF_mnc_op(String f_mnc_op) {
        this.f_mnc_op = f_mnc_op;
    }
   
    public String getF_lac() {
        return this.f_lac;
    }
    
    public void setF_lac(String f_lac) {
        this.f_lac = f_lac;
    }
    
    public String getF_lac_op() {
        return this.f_lac_op;
    }
    
    public void setF_lac_op(String f_lac_op) {
        this.f_lac_op = f_lac_op;
    }
   
    public String getF_sid() {
        return this.f_sid;
    }
    
    public void setF_sid(String f_sid) {
        this.f_sid = f_sid;
    }
    
    public String getF_sid_op() {
        return this.f_sid_op;
    }
    
    public void setF_sid_op(String f_sid_op) {
        this.f_sid_op = f_sid_op;
    }
   
    public String getF_nid() {
        return this.f_nid;
    }
    
    public void setF_nid(String f_nid) {
        this.f_nid = f_nid;
    }
    
    public String getF_nid_op() {
        return this.f_nid_op;
    }
    
    public void setF_nid_op(String f_nid_op) {
        this.f_nid_op = f_nid_op;
    }
   
    public String getF_bid() {
        return this.f_bid;
    }
    
    public void setF_bid(String f_bid) {
        this.f_bid = f_bid;
    }
    
    public String getF_bid_op() {
        return this.f_bid_op;
    }
    
    public void setF_bid_op(String f_bid_op) {
        this.f_bid_op = f_bid_op;
    }
   
    public Double getF_radius() {
        return this.f_radius;
    }
    
    public void setF_radius(Double f_radius) {
        this.f_radius = f_radius;
    }
    
    public String getF_radius_op() {
        return this.f_radius_op;
    }
    
    public void setF_radius_op(String f_radius_op) {
        this.f_radius_op = f_radius_op;
    }
   
    public String getF_error_code() {
        return this.f_error_code;
    }
    
    public void setF_error_code(String f_error_code) {
        this.f_error_code = f_error_code;
    }
    
    public String getF_error_code_op() {
        return this.f_error_code_op;
    }
    
    public void setF_error_code_op(String f_error_code_op) {
        this.f_error_code_op = f_error_code_op;
    }
   
    public Timestamp getF_loc_time() {
        return this.f_loc_time;
    }
    
    public void setF_loc_time(Timestamp f_loc_time) {
        this.f_loc_time = f_loc_time;
    }
    
    public String getF_loc_time_op() {
        return this.f_loc_time_op;
    }
    
    public void setF_loc_time_op(String f_loc_time_op) {
        this.f_loc_time_op = f_loc_time_op;
    }
    public Timestamp getF_loc_time01() {
        return this.f_loc_time01;
    }
    
    public void setF_loc_time01(Timestamp f_loc_time01) {
        this.f_loc_time01 = f_loc_time01;
    }
    
    public String getF_loc_time01_op() {
        return this.f_loc_time01_op;
    }
    
    public void setF_loc_time01_op(String f_loc_time01_op) {
        this.f_loc_time01_op = f_loc_time01_op;
    }
   
    public Integer getF_satellite_num() {
        return this.f_satellite_num;
    }
    
    public void setF_satellite_num(Integer f_satellite_num) {
        this.f_satellite_num = f_satellite_num;
    }
    
    public String getF_satellite_num_op() {
        return this.f_satellite_num_op;
    }
    
    public void setF_satellite_num_op(String f_satellite_num_op) {
        this.f_satellite_num_op = f_satellite_num_op;
    }
   
    public String getF_uuid() {
        return this.f_uuid;
    }
    
    public void setF_uuid(String f_uuid) {
        this.f_uuid = f_uuid;
    }
    
    public String getF_uuid_op() {
        return this.f_uuid_op;
    }
    
    public void setF_uuid_op(String f_uuid_op) {
        this.f_uuid_op = f_uuid_op;
    }
   
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
