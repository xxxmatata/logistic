package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.logistic.basicinfo.action.RegionGBPageInfo;
import org.logistic.basicinfo.action.Logistic_corpPageInfo;
import org.logistic.basicinfo.action.Truck_typePageInfo;
import org.logistic.basicinfo.action.Driver_infoPageInfo;
import org.logistic.basicinfo.action.Goods_typePageInfo;
import org.hi.base.organization.action.HiUserPageInfo;

public class Goods_infoPageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  Timestamp  f_publishTime;
 	protected  String  f_publishTime_op;
	protected  Timestamp  f_publishTime01;
	protected  String  f_publishTime01_op;
	protected  Double  f_freightRate;
 	protected  String  f_freightRate_op;
	protected  Integer  f_freightUnits;
 	protected  String  f_freightUnits_op;
	protected  Timestamp  f_loadTime;
 	protected  String  f_loadTime_op;
	protected  Timestamp  f_loadTime01;
	protected  String  f_loadTime01_op;
	protected  String  f_loadPlace;
 	protected  String  f_loadPlace_op;
	protected  Integer  f_truckNumber;
 	protected  String  f_truckNumber_op;
	protected  Double  f_truck_length;
 	protected  String  f_truck_length_op;
	protected  Double  f_truck_width;
 	protected  String  f_truck_width_op;
	protected  Double  f_truck_load;
 	protected  String  f_truck_load_op;
	protected  Integer  f_info_state;
 	protected  String  f_info_state_op;
	protected  Timestamp  f_orderTime;
 	protected  String  f_orderTime_op;
	protected  Timestamp  f_orderTime01;
	protected  String  f_orderTime01_op;
	protected  Timestamp  f_realLoadTime;
 	protected  String  f_realLoadTime_op;
	protected  Timestamp  f_realLoadTime01;
	protected  String  f_realLoadTime01_op;
	protected  Timestamp  f_unloadTime;
 	protected  String  f_unloadTime_op;
	protected  Timestamp  f_unloadTime01;
	protected  String  f_unloadTime01_op;
	protected  String  f_remark;
 	protected  String  f_remark_op;
	protected  Integer  f_taskRank;
 	protected  String  f_taskRank_op;
	protected  Integer  f_goodsUnits;
 	protected  String  f_goodsUnits_op;
	protected  Integer  f_deleted;
 	protected  String  f_deleted_op;

 	protected  RegionGBPageInfo origin;
 	protected  RegionGBPageInfo destination;
 	protected  Logistic_corpPageInfo logistic_corp;
 	protected  Truck_typePageInfo truck_type;
 	protected  Driver_infoPageInfo driver;
 	protected  Goods_typePageInfo goodsType;
 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public Timestamp getF_publishTime() {
        return this.f_publishTime;
    }
    
    public void setF_publishTime(Timestamp f_publishTime) {
        this.f_publishTime = f_publishTime;
    }
    
    public String getF_publishTime_op() {
        return this.f_publishTime_op;
    }
    
    public void setF_publishTime_op(String f_publishTime_op) {
        this.f_publishTime_op = f_publishTime_op;
    }
    public Timestamp getF_publishTime01() {
        return this.f_publishTime01;
    }
    
    public void setF_publishTime01(Timestamp f_publishTime01) {
        this.f_publishTime01 = f_publishTime01;
    }
    
    public String getF_publishTime01_op() {
        return this.f_publishTime01_op;
    }
    
    public void setF_publishTime01_op(String f_publishTime01_op) {
        this.f_publishTime01_op = f_publishTime01_op;
    }
   
    public Double getF_freightRate() {
        return this.f_freightRate;
    }
    
    public void setF_freightRate(Double f_freightRate) {
        this.f_freightRate = f_freightRate;
    }
    
    public String getF_freightRate_op() {
        return this.f_freightRate_op;
    }
    
    public void setF_freightRate_op(String f_freightRate_op) {
        this.f_freightRate_op = f_freightRate_op;
    }
   
    public Integer getF_freightUnits() {
        return this.f_freightUnits;
    }
    
    public void setF_freightUnits(Integer f_freightUnits) {
        this.f_freightUnits = f_freightUnits;
    }
    
    public String getF_freightUnits_op() {
        return this.f_freightUnits_op;
    }
    
    public void setF_freightUnits_op(String f_freightUnits_op) {
        this.f_freightUnits_op = f_freightUnits_op;
    }
   
    public Timestamp getF_loadTime() {
        return this.f_loadTime;
    }
    
    public void setF_loadTime(Timestamp f_loadTime) {
        this.f_loadTime = f_loadTime;
    }
    
    public String getF_loadTime_op() {
        return this.f_loadTime_op;
    }
    
    public void setF_loadTime_op(String f_loadTime_op) {
        this.f_loadTime_op = f_loadTime_op;
    }
    public Timestamp getF_loadTime01() {
        return this.f_loadTime01;
    }
    
    public void setF_loadTime01(Timestamp f_loadTime01) {
        this.f_loadTime01 = f_loadTime01;
    }
    
    public String getF_loadTime01_op() {
        return this.f_loadTime01_op;
    }
    
    public void setF_loadTime01_op(String f_loadTime01_op) {
        this.f_loadTime01_op = f_loadTime01_op;
    }
   
    public String getF_loadPlace() {
        return this.f_loadPlace;
    }
    
    public void setF_loadPlace(String f_loadPlace) {
        this.f_loadPlace = f_loadPlace;
    }
    
    public String getF_loadPlace_op() {
        return this.f_loadPlace_op;
    }
    
    public void setF_loadPlace_op(String f_loadPlace_op) {
        this.f_loadPlace_op = f_loadPlace_op;
    }
   
    public Integer getF_truckNumber() {
        return this.f_truckNumber;
    }
    
    public void setF_truckNumber(Integer f_truckNumber) {
        this.f_truckNumber = f_truckNumber;
    }
    
    public String getF_truckNumber_op() {
        return this.f_truckNumber_op;
    }
    
    public void setF_truckNumber_op(String f_truckNumber_op) {
        this.f_truckNumber_op = f_truckNumber_op;
    }
   
    public Double getF_truck_length() {
        return this.f_truck_length;
    }
    
    public void setF_truck_length(Double f_truck_length) {
        this.f_truck_length = f_truck_length;
    }
    
    public String getF_truck_length_op() {
        return this.f_truck_length_op;
    }
    
    public void setF_truck_length_op(String f_truck_length_op) {
        this.f_truck_length_op = f_truck_length_op;
    }
   
    public Double getF_truck_width() {
        return this.f_truck_width;
    }
    
    public void setF_truck_width(Double f_truck_width) {
        this.f_truck_width = f_truck_width;
    }
    
    public String getF_truck_width_op() {
        return this.f_truck_width_op;
    }
    
    public void setF_truck_width_op(String f_truck_width_op) {
        this.f_truck_width_op = f_truck_width_op;
    }
   
    public Double getF_truck_load() {
        return this.f_truck_load;
    }
    
    public void setF_truck_load(Double f_truck_load) {
        this.f_truck_load = f_truck_load;
    }
    
    public String getF_truck_load_op() {
        return this.f_truck_load_op;
    }
    
    public void setF_truck_load_op(String f_truck_load_op) {
        this.f_truck_load_op = f_truck_load_op;
    }
   
    public Integer getF_info_state() {
        return this.f_info_state;
    }
    
    public void setF_info_state(Integer f_info_state) {
        this.f_info_state = f_info_state;
    }
    
    public String getF_info_state_op() {
        return this.f_info_state_op;
    }
    
    public void setF_info_state_op(String f_info_state_op) {
        this.f_info_state_op = f_info_state_op;
    }
   
    public Timestamp getF_orderTime() {
        return this.f_orderTime;
    }
    
    public void setF_orderTime(Timestamp f_orderTime) {
        this.f_orderTime = f_orderTime;
    }
    
    public String getF_orderTime_op() {
        return this.f_orderTime_op;
    }
    
    public void setF_orderTime_op(String f_orderTime_op) {
        this.f_orderTime_op = f_orderTime_op;
    }
    public Timestamp getF_orderTime01() {
        return this.f_orderTime01;
    }
    
    public void setF_orderTime01(Timestamp f_orderTime01) {
        this.f_orderTime01 = f_orderTime01;
    }
    
    public String getF_orderTime01_op() {
        return this.f_orderTime01_op;
    }
    
    public void setF_orderTime01_op(String f_orderTime01_op) {
        this.f_orderTime01_op = f_orderTime01_op;
    }
   
    public Timestamp getF_realLoadTime() {
        return this.f_realLoadTime;
    }
    
    public void setF_realLoadTime(Timestamp f_realLoadTime) {
        this.f_realLoadTime = f_realLoadTime;
    }
    
    public String getF_realLoadTime_op() {
        return this.f_realLoadTime_op;
    }
    
    public void setF_realLoadTime_op(String f_realLoadTime_op) {
        this.f_realLoadTime_op = f_realLoadTime_op;
    }
    public Timestamp getF_realLoadTime01() {
        return this.f_realLoadTime01;
    }
    
    public void setF_realLoadTime01(Timestamp f_realLoadTime01) {
        this.f_realLoadTime01 = f_realLoadTime01;
    }
    
    public String getF_realLoadTime01_op() {
        return this.f_realLoadTime01_op;
    }
    
    public void setF_realLoadTime01_op(String f_realLoadTime01_op) {
        this.f_realLoadTime01_op = f_realLoadTime01_op;
    }
   
    public Timestamp getF_unloadTime() {
        return this.f_unloadTime;
    }
    
    public void setF_unloadTime(Timestamp f_unloadTime) {
        this.f_unloadTime = f_unloadTime;
    }
    
    public String getF_unloadTime_op() {
        return this.f_unloadTime_op;
    }
    
    public void setF_unloadTime_op(String f_unloadTime_op) {
        this.f_unloadTime_op = f_unloadTime_op;
    }
    public Timestamp getF_unloadTime01() {
        return this.f_unloadTime01;
    }
    
    public void setF_unloadTime01(Timestamp f_unloadTime01) {
        this.f_unloadTime01 = f_unloadTime01;
    }
    
    public String getF_unloadTime01_op() {
        return this.f_unloadTime01_op;
    }
    
    public void setF_unloadTime01_op(String f_unloadTime01_op) {
        this.f_unloadTime01_op = f_unloadTime01_op;
    }
   
    public String getF_remark() {
        return this.f_remark;
    }
    
    public void setF_remark(String f_remark) {
        this.f_remark = f_remark;
    }
    
    public String getF_remark_op() {
        return this.f_remark_op;
    }
    
    public void setF_remark_op(String f_remark_op) {
        this.f_remark_op = f_remark_op;
    }
   
    public Integer getF_taskRank() {
        return this.f_taskRank;
    }
    
    public void setF_taskRank(Integer f_taskRank) {
        this.f_taskRank = f_taskRank;
    }
    
    public String getF_taskRank_op() {
        return this.f_taskRank_op;
    }
    
    public void setF_taskRank_op(String f_taskRank_op) {
        this.f_taskRank_op = f_taskRank_op;
    }
   
    public Integer getF_goodsUnits() {
        return this.f_goodsUnits;
    }
    
    public void setF_goodsUnits(Integer f_goodsUnits) {
        this.f_goodsUnits = f_goodsUnits;
    }
    
    public String getF_goodsUnits_op() {
        return this.f_goodsUnits_op;
    }
    
    public void setF_goodsUnits_op(String f_goodsUnits_op) {
        this.f_goodsUnits_op = f_goodsUnits_op;
    }
   
    public Integer getF_deleted() {
        return this.f_deleted;
    }
    
    public void setF_deleted(Integer f_deleted) {
        this.f_deleted = f_deleted;
    }
    
    public String getF_deleted_op() {
        return this.f_deleted_op;
    }
    
    public void setF_deleted_op(String f_deleted_op) {
        this.f_deleted_op = f_deleted_op;
    }
   
	public RegionGBPageInfo getOrigin() {
		return origin;
	}

	public void setOrigin(RegionGBPageInfo origin) {
		this.origin = origin;
	}
	public RegionGBPageInfo getDestination() {
		return destination;
	}

	public void setDestination(RegionGBPageInfo destination) {
		this.destination = destination;
	}
	public Logistic_corpPageInfo getLogistic_corp() {
		return logistic_corp;
	}

	public void setLogistic_corp(Logistic_corpPageInfo logistic_corp) {
		this.logistic_corp = logistic_corp;
	}
	public Truck_typePageInfo getTruck_type() {
		return truck_type;
	}

	public void setTruck_type(Truck_typePageInfo truck_type) {
		this.truck_type = truck_type;
	}
	public Driver_infoPageInfo getDriver() {
		return driver;
	}

	public void setDriver(Driver_infoPageInfo driver) {
		this.driver = driver;
	}
	public Goods_typePageInfo getGoodsType() {
		return goodsType;
	}

	public void setGoodsType(Goods_typePageInfo goodsType) {
		this.goodsType = goodsType;
	}
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
