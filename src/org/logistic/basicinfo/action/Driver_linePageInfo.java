package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.logistic.basicinfo.action.Driver_infoPageInfo;
import org.logistic.basicinfo.action.RegionGBPageInfo;
import org.hi.base.organization.action.HiUserPageInfo;

public class Driver_linePageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  Double  f_price;
 	protected  String  f_price_op;
	protected  Integer  f_deleted;
 	protected  String  f_deleted_op;

 	protected  Driver_infoPageInfo driver_info;
 	protected  RegionGBPageInfo origin;
 	protected  RegionGBPageInfo destination;
 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public Double getF_price() {
        return this.f_price;
    }
    
    public void setF_price(Double f_price) {
        this.f_price = f_price;
    }
    
    public String getF_price_op() {
        return this.f_price_op;
    }
    
    public void setF_price_op(String f_price_op) {
        this.f_price_op = f_price_op;
    }
   
    public Integer getF_deleted() {
        return this.f_deleted;
    }
    
    public void setF_deleted(Integer f_deleted) {
        this.f_deleted = f_deleted;
    }
    
    public String getF_deleted_op() {
        return this.f_deleted_op;
    }
    
    public void setF_deleted_op(String f_deleted_op) {
        this.f_deleted_op = f_deleted_op;
    }
   
	public Driver_infoPageInfo getDriver_info() {
		return driver_info;
	}

	public void setDriver_info(Driver_infoPageInfo driver_info) {
		this.driver_info = driver_info;
	}
	public RegionGBPageInfo getOrigin() {
		return origin;
	}

	public void setOrigin(RegionGBPageInfo origin) {
		this.origin = origin;
	}
	public RegionGBPageInfo getDestination() {
		return destination;
	}

	public void setDestination(RegionGBPageInfo destination) {
		this.destination = destination;
	}
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
