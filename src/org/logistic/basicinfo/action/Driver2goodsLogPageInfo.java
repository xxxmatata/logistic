package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.logistic.basicinfo.action.Driver_infoPageInfo;
import org.logistic.basicinfo.action.Goods_infoPageInfo;
import org.hi.base.organization.action.HiUserPageInfo;

public class Driver2goodsLogPageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  Integer  f_d2gType;
 	protected  String  f_d2gType_op;
	protected  Timestamp  f_opTime;
 	protected  String  f_opTime_op;
	protected  Timestamp  f_opTime01;
	protected  String  f_opTime01_op;
	protected  String  f_taskRank;
 	protected  String  f_taskRank_op;
	protected  String  f_remark;
 	protected  String  f_remark_op;

 	protected  Driver_infoPageInfo driver;
 	protected  Goods_infoPageInfo goods_info;
 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public Integer getF_d2gType() {
        return this.f_d2gType;
    }
    
    public void setF_d2gType(Integer f_d2gType) {
        this.f_d2gType = f_d2gType;
    }
    
    public String getF_d2gType_op() {
        return this.f_d2gType_op;
    }
    
    public void setF_d2gType_op(String f_d2gType_op) {
        this.f_d2gType_op = f_d2gType_op;
    }
   
    public Timestamp getF_opTime() {
        return this.f_opTime;
    }
    
    public void setF_opTime(Timestamp f_opTime) {
        this.f_opTime = f_opTime;
    }
    
    public String getF_opTime_op() {
        return this.f_opTime_op;
    }
    
    public void setF_opTime_op(String f_opTime_op) {
        this.f_opTime_op = f_opTime_op;
    }
    public Timestamp getF_opTime01() {
        return this.f_opTime01;
    }
    
    public void setF_opTime01(Timestamp f_opTime01) {
        this.f_opTime01 = f_opTime01;
    }
    
    public String getF_opTime01_op() {
        return this.f_opTime01_op;
    }
    
    public void setF_opTime01_op(String f_opTime01_op) {
        this.f_opTime01_op = f_opTime01_op;
    }
   
    public String getF_taskRank() {
        return this.f_taskRank;
    }
    
    public void setF_taskRank(String f_taskRank) {
        this.f_taskRank = f_taskRank;
    }
    
    public String getF_taskRank_op() {
        return this.f_taskRank_op;
    }
    
    public void setF_taskRank_op(String f_taskRank_op) {
        this.f_taskRank_op = f_taskRank_op;
    }
   
    public String getF_remark() {
        return this.f_remark;
    }
    
    public void setF_remark(String f_remark) {
        this.f_remark = f_remark;
    }
    
    public String getF_remark_op() {
        return this.f_remark_op;
    }
    
    public void setF_remark_op(String f_remark_op) {
        this.f_remark_op = f_remark_op;
    }
   
	public Driver_infoPageInfo getDriver() {
		return driver;
	}

	public void setDriver(Driver_infoPageInfo driver) {
		this.driver = driver;
	}
	public Goods_infoPageInfo getGoods_info() {
		return goods_info;
	}

	public void setGoods_info(Goods_infoPageInfo goods_info) {
		this.goods_info = goods_info;
	}
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
