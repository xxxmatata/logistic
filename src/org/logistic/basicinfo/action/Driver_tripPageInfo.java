package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.logistic.basicinfo.action.Driver_infoPageInfo;
import org.logistic.basicinfo.action.RegionGBPageInfo;
import org.hi.base.organization.action.HiUserPageInfo;

public class Driver_tripPageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  Timestamp  f_trip_date;
 	protected  String  f_trip_date_op;
	protected  Timestamp  f_trip_date01;
	protected  String  f_trip_date01_op;
	protected  Timestamp  f_publish_time;
 	protected  String  f_publish_time_op;
	protected  Timestamp  f_publish_time01;
	protected  String  f_publish_time01_op;
	protected  Integer  f_deleted;
 	protected  String  f_deleted_op;

 	protected  Driver_infoPageInfo driver_id;
 	protected  RegionGBPageInfo trip_region;
 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public Timestamp getF_trip_date() {
        return this.f_trip_date;
    }
    
    public void setF_trip_date(Timestamp f_trip_date) {
        this.f_trip_date = f_trip_date;
    }
    
    public String getF_trip_date_op() {
        return this.f_trip_date_op;
    }
    
    public void setF_trip_date_op(String f_trip_date_op) {
        this.f_trip_date_op = f_trip_date_op;
    }
    public Timestamp getF_trip_date01() {
        return this.f_trip_date01;
    }
    
    public void setF_trip_date01(Timestamp f_trip_date01) {
        this.f_trip_date01 = f_trip_date01;
    }
    
    public String getF_trip_date01_op() {
        return this.f_trip_date01_op;
    }
    
    public void setF_trip_date01_op(String f_trip_date01_op) {
        this.f_trip_date01_op = f_trip_date01_op;
    }
   
    public Timestamp getF_publish_time() {
        return this.f_publish_time;
    }
    
    public void setF_publish_time(Timestamp f_publish_time) {
        this.f_publish_time = f_publish_time;
    }
    
    public String getF_publish_time_op() {
        return this.f_publish_time_op;
    }
    
    public void setF_publish_time_op(String f_publish_time_op) {
        this.f_publish_time_op = f_publish_time_op;
    }
    public Timestamp getF_publish_time01() {
        return this.f_publish_time01;
    }
    
    public void setF_publish_time01(Timestamp f_publish_time01) {
        this.f_publish_time01 = f_publish_time01;
    }
    
    public String getF_publish_time01_op() {
        return this.f_publish_time01_op;
    }
    
    public void setF_publish_time01_op(String f_publish_time01_op) {
        this.f_publish_time01_op = f_publish_time01_op;
    }
   
    public Integer getF_deleted() {
        return this.f_deleted;
    }
    
    public void setF_deleted(Integer f_deleted) {
        this.f_deleted = f_deleted;
    }
    
    public String getF_deleted_op() {
        return this.f_deleted_op;
    }
    
    public void setF_deleted_op(String f_deleted_op) {
        this.f_deleted_op = f_deleted_op;
    }
   
	public Driver_infoPageInfo getDriver_id() {
		return driver_id;
	}

	public void setDriver_id(Driver_infoPageInfo driver_id) {
		this.driver_id = driver_id;
	}
	public RegionGBPageInfo getTrip_region() {
		return trip_region;
	}

	public void setTrip_region(RegionGBPageInfo trip_region) {
		this.trip_region = trip_region;
	}
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
