package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.base.organization.action.HiUserPageInfo;
import org.hi.common.attachment.action.AttachmentPageInfo;
import org.logistic.basicinfo.action.Truck_infoPageInfo;
import org.logistic.basicinfo.action.RegionGBPageInfo;

public class Driver_infoPageInfo extends HiUserPageInfo{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  Integer  f_info_state;
 	protected  String  f_info_state_op;
	protected  Double  f_lng;
 	protected  String  f_lng_op;
	protected  Double  f_lat;
 	protected  String  f_lat_op;
	protected  String  f_localtion_desc;
 	protected  String  f_localtion_desc_op;
	protected  Integer  f_driverRank;
 	protected  String  f_driverRank_op;

 	protected  AttachmentPageInfo drive_lic_file_attachment;
 	protected  Truck_infoPageInfo truck_info;
 	protected  RegionGBPageInfo region_id;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public Integer getF_info_state() {
        return this.f_info_state;
    }
    
    public void setF_info_state(Integer f_info_state) {
        this.f_info_state = f_info_state;
    }
    
    public String getF_info_state_op() {
        return this.f_info_state_op;
    }
    
    public void setF_info_state_op(String f_info_state_op) {
        this.f_info_state_op = f_info_state_op;
    }
   
    public Double getF_lng() {
        return this.f_lng;
    }
    
    public void setF_lng(Double f_lng) {
        this.f_lng = f_lng;
    }
    
    public String getF_lng_op() {
        return this.f_lng_op;
    }
    
    public void setF_lng_op(String f_lng_op) {
        this.f_lng_op = f_lng_op;
    }
   
    public Double getF_lat() {
        return this.f_lat;
    }
    
    public void setF_lat(Double f_lat) {
        this.f_lat = f_lat;
    }
    
    public String getF_lat_op() {
        return this.f_lat_op;
    }
    
    public void setF_lat_op(String f_lat_op) {
        this.f_lat_op = f_lat_op;
    }
   
    public String getF_localtion_desc() {
        return this.f_localtion_desc;
    }
    
    public void setF_localtion_desc(String f_localtion_desc) {
        this.f_localtion_desc = f_localtion_desc;
    }
    
    public String getF_localtion_desc_op() {
        return this.f_localtion_desc_op;
    }
    
    public void setF_localtion_desc_op(String f_localtion_desc_op) {
        this.f_localtion_desc_op = f_localtion_desc_op;
    }
   
    public Integer getF_driverRank() {
        return this.f_driverRank;
    }
    
    public void setF_driverRank(Integer f_driverRank) {
        this.f_driverRank = f_driverRank;
    }
    
    public String getF_driverRank_op() {
        return this.f_driverRank_op;
    }
    
    public void setF_driverRank_op(String f_driverRank_op) {
        this.f_driverRank_op = f_driverRank_op;
    }
   
	public AttachmentPageInfo getDrive_lic_file_attachment() {
		return drive_lic_file_attachment;
	}

	public void setDrive_lic_file_attachment(AttachmentPageInfo drive_lic_file_attachment) {
		this.drive_lic_file_attachment = drive_lic_file_attachment;
	}
	public Truck_infoPageInfo getTruck_info() {
		return truck_info;
	}

	public void setTruck_info(Truck_infoPageInfo truck_info) {
		this.truck_info = truck_info;
	}
	public RegionGBPageInfo getRegion_id() {
		return region_id;
	}

	public void setRegion_id(RegionGBPageInfo region_id) {
		this.region_id = region_id;
	}

}
