package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.hi.base.organization.action.HiUserPageInfo;

public class Goods_ownerPageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  String  f_ownerName;
 	protected  String  f_ownerName_op;
	protected  String  f_mobileNumber;
 	protected  String  f_mobileNumber_op;
	protected  String  f_description;
 	protected  String  f_description_op;
	protected  Integer  f_deleted;
 	protected  String  f_deleted_op;

 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public String getF_ownerName() {
        return this.f_ownerName;
    }
    
    public void setF_ownerName(String f_ownerName) {
        this.f_ownerName = f_ownerName;
    }
    
    public String getF_ownerName_op() {
        return this.f_ownerName_op;
    }
    
    public void setF_ownerName_op(String f_ownerName_op) {
        this.f_ownerName_op = f_ownerName_op;
    }
   
    public String getF_mobileNumber() {
        return this.f_mobileNumber;
    }
    
    public void setF_mobileNumber(String f_mobileNumber) {
        this.f_mobileNumber = f_mobileNumber;
    }
    
    public String getF_mobileNumber_op() {
        return this.f_mobileNumber_op;
    }
    
    public void setF_mobileNumber_op(String f_mobileNumber_op) {
        this.f_mobileNumber_op = f_mobileNumber_op;
    }
   
    public String getF_description() {
        return this.f_description;
    }
    
    public void setF_description(String f_description) {
        this.f_description = f_description;
    }
    
    public String getF_description_op() {
        return this.f_description_op;
    }
    
    public void setF_description_op(String f_description_op) {
        this.f_description_op = f_description_op;
    }
   
    public Integer getF_deleted() {
        return this.f_deleted;
    }
    
    public void setF_deleted(Integer f_deleted) {
        this.f_deleted = f_deleted;
    }
    
    public String getF_deleted_op() {
        return this.f_deleted_op;
    }
    
    public void setF_deleted_op(String f_deleted_op) {
        this.f_deleted_op = f_deleted_op;
    }
   
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
