package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.framework.web.PageInfoView;
import org.logistic.basicinfo.action.Goods_infoPageInfo;
import org.logistic.basicinfo.action.Goods_ownerPageInfo;
import org.hi.base.organization.action.HiUserPageInfo;

public class Goods2ownerPageInfo extends PageInfoView{

	protected  Integer  f_id;
 	protected  String  f_id_op;

 	protected  Goods_infoPageInfo goods_info;
 	protected  Goods_ownerPageInfo owner;
 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
	public Goods_infoPageInfo getGoods_info() {
		return goods_info;
	}

	public void setGoods_info(Goods_infoPageInfo goods_info) {
		this.goods_info = goods_info;
	}
	public Goods_ownerPageInfo getOwner() {
		return owner;
	}

	public void setOwner(Goods_ownerPageInfo owner) {
		this.owner = owner;
	}
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
