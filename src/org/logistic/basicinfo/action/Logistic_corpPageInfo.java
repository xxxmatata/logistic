package org.logistic.basicinfo.action;

import java.sql.Timestamp;
import java.util.Date;

import org.hi.base.organization.action.HiOrgPageInfo;
import org.hi.common.attachment.action.AttachmentPageInfo;
import org.hi.base.organization.action.HiUserPageInfo;

public class Logistic_corpPageInfo extends HiOrgPageInfo{

	protected  Integer  f_id;
 	protected  String  f_id_op;
	protected  String  f_address;
 	protected  String  f_address_op;
	protected  String  f_op_lic;
 	protected  String  f_op_lic_op;
	protected  String  f_tax_lic;
 	protected  String  f_tax_lic_op;
	protected  String  f_corp_name;
 	protected  String  f_corp_name_op;
	protected  String  f_corp_phone;
 	protected  String  f_corp_phone_op;
	protected  String  f_corp_mobile;
 	protected  String  f_corp_mobile_op;
	protected  Integer  f_info_state;
 	protected  String  f_info_state_op;

 	protected  AttachmentPageInfo op_lic_file_attachment;
 	protected  AttachmentPageInfo tax_lic_file_attachment;
 	protected  HiUserPageInfo creator;

    public Integer getF_id() {
        return this.f_id;
    }
    
    public void setF_id(Integer f_id) {
        this.f_id = f_id;
    }
    
    public String getF_id_op() {
        return this.f_id_op;
    }
    
    public void setF_id_op(String f_id_op) {
        this.f_id_op = f_id_op;
    }
   
    public String getF_address() {
        return this.f_address;
    }
    
    public void setF_address(String f_address) {
        this.f_address = f_address;
    }
    
    public String getF_address_op() {
        return this.f_address_op;
    }
    
    public void setF_address_op(String f_address_op) {
        this.f_address_op = f_address_op;
    }
   
    public String getF_op_lic() {
        return this.f_op_lic;
    }
    
    public void setF_op_lic(String f_op_lic) {
        this.f_op_lic = f_op_lic;
    }
    
    public String getF_op_lic_op() {
        return this.f_op_lic_op;
    }
    
    public void setF_op_lic_op(String f_op_lic_op) {
        this.f_op_lic_op = f_op_lic_op;
    }
   
    public String getF_tax_lic() {
        return this.f_tax_lic;
    }
    
    public void setF_tax_lic(String f_tax_lic) {
        this.f_tax_lic = f_tax_lic;
    }
    
    public String getF_tax_lic_op() {
        return this.f_tax_lic_op;
    }
    
    public void setF_tax_lic_op(String f_tax_lic_op) {
        this.f_tax_lic_op = f_tax_lic_op;
    }
   
    public String getF_corp_name() {
        return this.f_corp_name;
    }
    
    public void setF_corp_name(String f_corp_name) {
        this.f_corp_name = f_corp_name;
    }
    
    public String getF_corp_name_op() {
        return this.f_corp_name_op;
    }
    
    public void setF_corp_name_op(String f_corp_name_op) {
        this.f_corp_name_op = f_corp_name_op;
    }
   
    public String getF_corp_phone() {
        return this.f_corp_phone;
    }
    
    public void setF_corp_phone(String f_corp_phone) {
        this.f_corp_phone = f_corp_phone;
    }
    
    public String getF_corp_phone_op() {
        return this.f_corp_phone_op;
    }
    
    public void setF_corp_phone_op(String f_corp_phone_op) {
        this.f_corp_phone_op = f_corp_phone_op;
    }
   
    public String getF_corp_mobile() {
        return this.f_corp_mobile;
    }
    
    public void setF_corp_mobile(String f_corp_mobile) {
        this.f_corp_mobile = f_corp_mobile;
    }
    
    public String getF_corp_mobile_op() {
        return this.f_corp_mobile_op;
    }
    
    public void setF_corp_mobile_op(String f_corp_mobile_op) {
        this.f_corp_mobile_op = f_corp_mobile_op;
    }
   
    public Integer getF_info_state() {
        return this.f_info_state;
    }
    
    public void setF_info_state(Integer f_info_state) {
        this.f_info_state = f_info_state;
    }
    
    public String getF_info_state_op() {
        return this.f_info_state_op;
    }
    
    public void setF_info_state_op(String f_info_state_op) {
        this.f_info_state_op = f_info_state_op;
    }
   
	public AttachmentPageInfo getOp_lic_file_attachment() {
		return op_lic_file_attachment;
	}

	public void setOp_lic_file_attachment(AttachmentPageInfo op_lic_file_attachment) {
		this.op_lic_file_attachment = op_lic_file_attachment;
	}
	public AttachmentPageInfo getTax_lic_file_attachment() {
		return tax_lic_file_attachment;
	}

	public void setTax_lic_file_attachment(AttachmentPageInfo tax_lic_file_attachment) {
		this.tax_lic_file_attachment = tax_lic_file_attachment;
	}
	public HiUserPageInfo getCreator() {
		return creator;
	}

	public void setCreator(HiUserPageInfo creator) {
		this.creator = creator;
	}

}
