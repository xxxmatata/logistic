package org.logistic.basicinfo.action.struts;

//http://localhost:8080/logistic/updateLocation.action?driver_mobile_trace.error_code=161&driver_mobile_trace.loc_time=2013-12-13 10:36:40&driver_mobile_trace.loc_type=161&driver_mobile_trace.lat=40.652528&driver_mobile_trace.lng=124.207068&driver_mobile_trace.radius=2000.0&driver_mobile_trace.loc_desc=%E6%B9%96%E5%8C%97%E7%9C%81%E6%AD%A6%E6%B1%89%E5%B8%82%E4%B8%9C%E8%A5%BF%E6%B9%96%E5%8C%BA%E9%87%91%E9%93%B6%E6%B9%96%E8%B7%AF&driver_mobile_trace.sid=13884&driver_mobile_trace.nid=27&driver_mobile_trace.bid=4473&driver_mobile_trace.imsi=460030497685449&driver_mobile_trace.imei=A1000012AF8E698&driver_mobile_trace.uuid=5e5988534e9f090&driver_mobile_trace.mdn=18907181594&driver_mobile_trace.os_version=4.1.2


import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.dao.Filter;
import org.hi.framework.dao.impl.SimpleFilter;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.paging.impl.PageInfoImpl;
import org.hi.framework.web.PageInfoUtil;
import org.logistic.basicinfo.action.Driver_mobile_pointPageInfo;
import org.logistic.basicinfo.model.Driver_mobile_point;
import org.logistic.basicinfo.model.Driver_mobile_trace;
import org.logistic.basicinfo.service.Driver_mobile_pointManager;
import org.logistic.basicinfo.service.Driver_mobile_traceManager;
import org.springframework.beans.BeanUtils;

public class ExDriver_mobile_traceAction extends Driver_mobile_traceAction{

	/**
	 * 新增/修改保存手机轨迹
	 */
	public String updateLocation() throws Exception {
		Driver_mobile_traceManager driver_mobile_traceMgr = (Driver_mobile_traceManager)SpringContextHolder.getBean(Driver_mobile_trace.class);
		if(super.perExecute(driver_mobile_trace)!= null) return returnCommand();
		
		if (driver_mobile_trace.getLoc_desc() != null){
			String loc_desc = URLDecoder.decode(driver_mobile_trace.getLoc_desc(), "utf-8");
			driver_mobile_trace.setLoc_desc(loc_desc);
		}
		Date curr = new Date();
		Timestamp opTime = new Timestamp(curr.getTime());
		
		String ip = this.getRequest().getRemoteAddr();
		
		driver_mobile_trace.setUpd_time(opTime);
		driver_mobile_trace.setClient_ip(ip);
		
		driver_mobile_traceMgr.saveDriver_mobile_trace(driver_mobile_trace);
		
		Driver_mobile_pointManager driver_mobile_pointMgr = (Driver_mobile_pointManager)SpringContextHolder.getBean(Driver_mobile_point.class);
		Driver_mobile_pointPageInfo pageInfo = new Driver_mobile_pointPageInfo();
		List<Driver_mobile_point> driver_mobile_points;


		PageInfo sarchPageInfo = new PageInfoImpl();
		Filter filter = new SimpleFilter();
		filter.addCondition("mdn", driver_mobile_trace.getMdn(),Filter.OPERATOR_EQ);
		sarchPageInfo.setFilter(filter);
		
		driver_mobile_points = driver_mobile_pointMgr.getDriver_mobile_pointList(sarchPageInfo);
		
		if (driver_mobile_points!=null && driver_mobile_points.size()>0){
			Driver_mobile_point point = driver_mobile_points.get(0);
			Integer id = point.getId();
			BeanUtils.copyProperties(driver_mobile_trace, point);
			point.setId(id);
			driver_mobile_pointMgr.saveDriver_mobile_point(point);
		}else{
			Driver_mobile_point point = new Driver_mobile_point();
			BeanUtils.copyProperties(driver_mobile_trace, point);
			point.setId(null);
			driver_mobile_pointMgr.saveDriver_mobile_point(point);
			
		}
		
		super.postExecute(driver_mobile_trace);
		return returnCommand();
	}
	
}
