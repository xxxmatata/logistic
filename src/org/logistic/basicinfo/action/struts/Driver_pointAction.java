package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Driver_pointPageInfo;
import org.logistic.basicinfo.model.Driver_point;
import org.logistic.basicinfo.service.Driver_pointManager;

public class Driver_pointAction extends BaseAction{
	private Driver_point driver_point;
	private Driver_pointPageInfo pageInfo;
	private List<Driver_point> driver_points;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存司机位置发布
	 */
	public String saveDriver_point() throws Exception {
		Driver_pointManager driver_pointMgr = (Driver_pointManager)SpringContextHolder.getBean(Driver_point.class);
		if(super.perExecute(driver_point)!= null) return returnCommand();
		driver_pointMgr.saveDriver_point(driver_point);
		super.postExecute(driver_point);
		return returnCommand();
	}
	
	
	/**
	 * 删除司机位置发布
	 */
	public String removeDriver_point() throws Exception {
		Driver_pointManager driver_pointMgr = (Driver_pointManager)SpringContextHolder.getBean(Driver_point.class);
		driver_pointMgr.removeDriver_pointById(driver_point.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些司机位置发布
	 */
	public String removeAllDriver_point() throws Exception {
		Driver_pointManager driver_pointMgr = (Driver_pointManager)SpringContextHolder.getBean(Driver_point.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer driver_pointid = new Integer( ids[i] );
				driver_pointMgr.removeDriver_pointById(driver_pointid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看司机位置发布
	 */
	public String viewDriver_point() throws Exception {
		Driver_pointManager driver_pointMgr = (Driver_pointManager)SpringContextHolder.getBean(Driver_point.class);
		driver_point = driver_pointMgr.getDriver_pointById(driver_point.getId());
		return returnCommand();
	}
	
	/**
	 * 司机位置发布列表
	 */
	public String driver_pointList() throws Exception {
		Driver_pointManager driver_pointMgr = (Driver_pointManager)SpringContextHolder.getBean(Driver_point.class);
		pageInfo = pageInfo == null ? new Driver_pointPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		driver_points = driver_pointMgr.getSecurityDriver_pointList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Driver_point getDriver_point() {
		return driver_point;
	}

	public void setDriver_point(Driver_point driver_point) {
		this.driver_point = driver_point;
	}
	
	public List<Driver_point> getDriver_points() {
		return driver_points;
	}

	public void setDriver_points(List<Driver_point> driver_points) {
		this.driver_points = driver_points;
	}

	public Driver_pointPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Driver_pointPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
