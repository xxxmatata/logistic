package org.logistic.basicinfo.action.struts;

import java.util.List;
import java.util.Map;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;
import org.logistic.basicinfo.action.Driver_infoPageInfo;
import org.logistic.basicinfo.action.Goods_infoPageInfo;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Goods_info;
import org.logistic.basicinfo.service.Goods_infoManager;
import org.logistic.basicinfo.service.PortalQueryManager;

public class PortalIndexAction extends BaseAction {
	protected Driver_infoPageInfo driver_infoPageInfo;
	protected List<Map> driver_infos;
	
	protected Goods_infoPageInfo goods_infoPageInfo;
	protected List<Goods_info> goods_infos;
	
	public String index() throws Exception {
		PortalQueryManager portalQueryManager = new PortalQueryManager();
		driver_infos = portalQueryManager.loadDriverInfo(null);
		
		Goods_infoManager goods_infoMgr = (Goods_infoManager)SpringContextHolder.getBean(Goods_info.class);
		goods_infoPageInfo = goods_infoPageInfo == null ? new Goods_infoPageInfo() : goods_infoPageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(goods_infoPageInfo, this);
		goods_infos = goods_infoMgr.getSecurityGoods_infoList(sarchPageInfo);
		
		return returnCommand();
	}

	public Driver_infoPageInfo getDriver_infoPageInfo() {
		return driver_infoPageInfo;
	}

	public void setDriver_infoPageInfo(Driver_infoPageInfo driver_infoPageInfo) {
		this.driver_infoPageInfo = driver_infoPageInfo;
	}

	public List<Map> getDriver_infos() {
		return driver_infos;
	}

	public void setDriver_infos(List<Map> driver_infos) {
		this.driver_infos = driver_infos;
	}

	public Goods_infoPageInfo getGoods_infoPageInfo() {
		return goods_infoPageInfo;
	}

	public void setGoods_infoPageInfo(Goods_infoPageInfo goods_infoPageInfo) {
		this.goods_infoPageInfo = goods_infoPageInfo;
	}

	public List<Goods_info> getGoods_infos() {
		return goods_infos;
	}

	public void setGoods_infos(List<Goods_info> goods_infos) {
		this.goods_infos = goods_infos;
	}

}
