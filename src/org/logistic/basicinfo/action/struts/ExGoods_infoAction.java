package org.logistic.basicinfo.action.struts;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.security.context.UserContextHelper;
import org.logistic.basicinfo.model.Driver2goods;
import org.logistic.basicinfo.model.Driver2goods_log;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Goods_info;
import org.logistic.basicinfo.model.Logistic_corp;
import org.logistic.basicinfo.model.Truck_info;
import org.logistic.basicinfo.service.Driver2goodsManager;
import org.logistic.basicinfo.service.ExGoods_infoManager;
import org.logistic.basicinfo.service.Goods_infoManager;
import org.logistic.basicinfo.service.Logistic_corpManager;

import com.opensymphony.util.BeanUtils;

public class ExGoods_infoAction extends Goods_infoAction {
	
	private Driver2goods driver2goods;

	public Driver2goods getDriver2goods() {
		return driver2goods;
	}

	public void setDriver2goods(Driver2goods driver2goods) {
		this.driver2goods = driver2goods;
	}

	/**
	 * 新增/修改保存货物信息
	 */
	public String saveGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager) SpringContextHolder
				.getBean(Goods_info.class);
		if (super.perExecute(goods_info) != null)
			return returnCommand();

		Logistic_corpManager logistic_corpMgr = (Logistic_corpManager) SpringContextHolder
				.getBean(Logistic_corp.class);
		Logistic_corp logistic_corp = logistic_corpMgr
				.getLogistic_corpById(UserContextHelper.getUserContext()
						.getOrg().getId());

		goods_info.setLogistic_corp(logistic_corp);

		goods_infoMgr.saveGoods_info(goods_info);
		super.postExecute(goods_info);
		return returnCommand();
	}

	/**
	 * 发布货物信息
	 */
	public String publishGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager) SpringContextHolder
				.getBean(Goods_info.class);
		if (super.perExecute(goods_info) != null)
			return returnCommand();

		Logistic_corpManager logistic_corpMgr = (Logistic_corpManager) SpringContextHolder
				.getBean(Logistic_corp.class);
		Logistic_corp logistic_corp = logistic_corpMgr
				.getLogistic_corpById(UserContextHelper.getUserContext()
						.getOrg().getId());

		goods_info.setLogistic_corp(logistic_corp);

		goods_info.setInfo_state(101200);

		goods_infoMgr.saveGoods_info(goods_info);
		super.postExecute(goods_info);
		return returnCommand();
	}

	/**
	 * 承运确认(货-人：一对一)
	 */
	public String orderGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager) SpringContextHolder
				.getBean(Goods_info.class);
		if (super.perExecute(goods_info) != null)
			return returnCommand();

		goods_info.setInfo_state(101201);
		Date date = new Date();

		Timestamp orderTime = new Timestamp(date.getTime());
		goods_info.setOrderTime(orderTime);
		List<Driver2goods> driver2goodss = goods_info.getDriver2goodss();

		Driver_info oldDriver = (Driver_info) goods_info.getOldValue("driver");

		boolean first = true;
		for (Driver2goods dg : driver2goodss) {
			if (dg.getGoods_info().getId().equals(goods_info.getId())
					&& dg.getDriver().getId().equals(
							goods_info.getDriver().getId())) {
				dg.setD2gType(101601);
				dg.setOpTime(orderTime);
				first = false;
			}
			if (dg.getGoods_info().getId().equals(goods_info.getId())
					&& dg.getDriver().getId().equals(oldDriver.getId())) {
				dg.setD2gType(101602);
				dg.setOpTime(orderTime);
			}
		}

		if (first) {
			Driver2goods driver2goods = new Driver2goods();
			driver2goods.setCreator(UserContextHelper.getUserContext()
					.getUser());
			driver2goods.setD2gType(101601);
			driver2goods.setDriver(goods_info.getDriver());
			driver2goods.setGoods_info(goods_info);
			driver2goods.setOpTime(orderTime);
			driver2goods.setVersion(0);
			driver2goodss.add(driver2goods);
		}

		goods_info.setDriver2goodss(driver2goodss);
		goods_infoMgr.saveGoods_info(goods_info);
		super.postExecute(goods_info);
		return returnCommand();
	}

	/**
	 * 承运确认(货-人：一对多)
	 */
	public String addOrderGoods_info() throws Exception {
		ExGoods_infoManager goods_infoMgr = (ExGoods_infoManager) SpringContextHolder
				.getBean(ExGoods_infoManager.class);

		if(UserContextHelper.getUserContext() == null || UserContextHelper.getUserContext().getUser() == null){
			return returnCommand("您未登录或者已经超时，请重新登录！");
		}
		
		if (super.perExecute(goods_info) != null)
			return returnCommand();


		Driver_info driver = goods_info.getDriver();
		
		Truck_info truck = driver.getTruck_info();
		if (truck == null){
			return returnCommand(driver.getFullName()+" 尚无对应车辆记录。");
		}
		
		int truckNum = goods_info.getTruckNumber();
		List<Driver2goods> driver2goodss = goods_info.getDriver2goodss();
		if (driver2goodss.size() >= truckNum){
			return returnCommand("已经确认的车辆数量超过所需车辆。");
		}
		
		for (Driver2goods dd:driver2goodss){
			//避免二次增加
			if (dd.getDriver().getId().equals(goods_info.getDriver().getId())){
				return returnCommand(driver.getFullName()+" 已经确认。");
			}
		}		
		
		
		goods_infoMgr.saveAddOrderGoods_info(goods_info);
		super.postExecute(goods_info);
		return returnCommand();
	}

	/**
	 * 取消成交确认
	 * @return
	 * @throws Exception
	 */
	public String removeDriver2goods() throws Exception {
		
		ExGoods_infoManager goods_infoMgr = (ExGoods_infoManager) SpringContextHolder
		.getBean(ExGoods_infoManager.class);
		Driver2goodsManager mgr = (Driver2goodsManager)SpringContextHolder.getBean(Driver2goods.class);
		driver2goods = mgr.getDriver2goodsById(driver2goods.getId());
		goods_infoMgr.removeOrderGoods_info(driver2goods);
		return returnCommand();
	}	
	

	
	/**
	 * 装货确认
	 */
	public String loadSaveGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager) SpringContextHolder
				.getBean(Goods_info.class);
		if (super.perExecute(goods_info) != null)
			return returnCommand();

		Integer oldState = goods_info.getInfo_state();
		if (oldState.intValue() != 101201) {
			return returnCommand("货物尚未承运确认！");
		}

		goods_info.setInfo_state(101202);

		List<Driver2goods> driver2goodss = goods_info.getDriver2goodss();

		for (Driver2goods dg : driver2goodss) {
			if (dg.getGoods_info().getId().equals(goods_info.getId())
					&& dg.getDriver().getId().equals(
							goods_info.getDriver().getId())) {
				dg.setD2gType(101604);
				dg.setOpTime(goods_info.getRealLoadTime());
			}
		}
		goods_infoMgr.saveGoods_info(goods_info);
		super.postExecute(goods_info);
		return returnCommand();
	}

	/**
	 * 司机装货确认
	 */
	public String driverLoad() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager) SpringContextHolder
				.getBean(Goods_info.class);
		if (super.perExecute(goods_info) != null)
			return returnCommand();

		Integer oldState = goods_info.getInfo_state();
		if (oldState.intValue() != 101201) {
			return returnCommand("货物尚未承运确认！");
		}

		goods_info.setInfo_state(101202);

		List<Driver2goods> driver2goodss = goods_info.getDriver2goodss();

		for (Driver2goods dg : driver2goodss) {
			if (dg.getGoods_info().getId().equals(goods_info.getId())
					&& dg.getDriver().getId().equals(
							goods_info.getDriver().getId())) {
				dg.setD2gType(101604);
				dg.setOpTime(goods_info.getRealLoadTime());
			}
		}
		goods_infoMgr.saveGoods_info(goods_info);
		super.postExecute(goods_info);
		return returnCommand();
	}
	
	
	/**
	 * 到达确认
	 */
	public String unloadSaveGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager) SpringContextHolder
				.getBean(Goods_info.class);
		if (super.perExecute(goods_info) != null)
			return returnCommand();

		Integer oldState = goods_info.getInfo_state();
		if (oldState.intValue() != 101202) {
			return returnCommand("货物尚未装车确认！");
		}

		goods_info.setInfo_state(101203);

		List<Driver2goods> driver2goodss = goods_info.getDriver2goodss();

		for (Driver2goods dg : driver2goodss) {
			if (dg.getGoods_info().getId().equals(goods_info.getId())
					&& dg.getDriver().getId().equals(
							goods_info.getDriver().getId())) {
				dg.setD2gType(101603);
				dg.setOpTime(goods_info.getRealLoadTime());
			}
		}

		int driverRank = goods_info.getDriver().getDriverRank().intValue();

		if (goods_info.getTaskRank().intValue() == 101700) {
			driverRank++;
		} else if (goods_info.getTaskRank().intValue() == 101702) {
			driverRank--;
		}
		goods_info.getDriver().setDriverRank(driverRank);
		goods_infoMgr.saveGoods_info(goods_info);
		super.postExecute(goods_info);
		return returnCommand();
	}

}
