package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.dao.Filter;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.logistic.basicinfo.action.RegionGBPageInfo;
import org.logistic.basicinfo.model.RegionGB;
import org.logistic.basicinfo.service.RegionGBManager;



public class ExRegionGBAction extends RegionGBAction {
	/**
	 * 国标区域编码列表
	 */
	public String regionGBList() throws Exception {
		RegionGBManager regionGBMgr = (RegionGBManager)SpringContextHolder.getBean(RegionGB.class);
		pageInfo = pageInfo == null ? new RegionGBPageInfo() : pageInfo;
		String pCode = this.getRequest().getParameter("pCode");
		
		if (pCode == null || pCode.length()==0){
			pCode = "000000";
		}
		pageInfo.setF_parent_r_code(pCode);
		pageInfo.setF_parent_r_code_op(Filter.OPERATOR_EQ);
		pageInfo.setSorterName("r_code");
		
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		regionGBs = regionGBMgr.getRegionGBList(sarchPageInfo);
		
		pageInfo.setF_r_code(pCode);
		pageInfo.setF_r_code_op(Filter.OPERATOR_EQ);
		pageInfo.setF_parent_r_code(null);
		sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		List<RegionGB> regionGBs = regionGBMgr.getRegionGBList(sarchPageInfo);
		
		if (regionGBs.size()>0){
			this.regionGB = regionGBs.get(0);
		}
		
		return returnCommand();	
	}

}
