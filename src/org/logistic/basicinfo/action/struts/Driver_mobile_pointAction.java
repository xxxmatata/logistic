package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Driver_mobile_pointPageInfo;
import org.logistic.basicinfo.model.Driver_mobile_point;
import org.logistic.basicinfo.service.Driver_mobile_pointManager;

public class Driver_mobile_pointAction extends BaseAction{
	private Driver_mobile_point driver_mobile_point;
	private Driver_mobile_pointPageInfo pageInfo;
	private List<Driver_mobile_point> driver_mobile_points;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存手机最新位置
	 */
	public String saveDriver_mobile_point() throws Exception {
		Driver_mobile_pointManager driver_mobile_pointMgr = (Driver_mobile_pointManager)SpringContextHolder.getBean(Driver_mobile_point.class);
		if(super.perExecute(driver_mobile_point)!= null) return returnCommand();
		driver_mobile_pointMgr.saveDriver_mobile_point(driver_mobile_point);
		super.postExecute(driver_mobile_point);
		return returnCommand();
	}
	
	
	/**
	 * 删除手机最新位置
	 */
	public String removeDriver_mobile_point() throws Exception {
		Driver_mobile_pointManager driver_mobile_pointMgr = (Driver_mobile_pointManager)SpringContextHolder.getBean(Driver_mobile_point.class);
		driver_mobile_pointMgr.removeDriver_mobile_pointById(driver_mobile_point.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些手机最新位置
	 */
	public String removeAllDriver_mobile_point() throws Exception {
		Driver_mobile_pointManager driver_mobile_pointMgr = (Driver_mobile_pointManager)SpringContextHolder.getBean(Driver_mobile_point.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer driver_mobile_pointid = new Integer( ids[i] );
				driver_mobile_pointMgr.removeDriver_mobile_pointById(driver_mobile_pointid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看手机最新位置
	 */
	public String viewDriver_mobile_point() throws Exception {
		Driver_mobile_pointManager driver_mobile_pointMgr = (Driver_mobile_pointManager)SpringContextHolder.getBean(Driver_mobile_point.class);
		driver_mobile_point = driver_mobile_pointMgr.getDriver_mobile_pointById(driver_mobile_point.getId());
		return returnCommand();
	}
	
	/**
	 * 手机最新位置列表
	 */
	public String driver_mobile_pointList() throws Exception {
		Driver_mobile_pointManager driver_mobile_pointMgr = (Driver_mobile_pointManager)SpringContextHolder.getBean(Driver_mobile_point.class);
		pageInfo = pageInfo == null ? new Driver_mobile_pointPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		driver_mobile_points = driver_mobile_pointMgr.getSecurityDriver_mobile_pointList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Driver_mobile_point getDriver_mobile_point() {
		return driver_mobile_point;
	}

	public void setDriver_mobile_point(Driver_mobile_point driver_mobile_point) {
		this.driver_mobile_point = driver_mobile_point;
	}
	
	public List<Driver_mobile_point> getDriver_mobile_points() {
		return driver_mobile_points;
	}

	public void setDriver_mobile_points(List<Driver_mobile_point> driver_mobile_points) {
		this.driver_mobile_points = driver_mobile_points;
	}

	public Driver_mobile_pointPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Driver_mobile_pointPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
