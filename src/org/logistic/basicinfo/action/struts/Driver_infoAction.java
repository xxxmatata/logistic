package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;
import org.logistic.basicinfo.action.Driver_infoPageInfo;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.ExDriver_info;
import org.logistic.basicinfo.service.Driver_infoManager;

public class Driver_infoAction extends BaseAction{
	protected Driver_info driver_info;
	protected Driver_infoPageInfo pageInfo;
	protected List<Driver_info> driver_infos;
	protected String orderIndexs;
	
	
	/**
	 * 新增/修改保存司机信息
	 */
	public String saveDriver_info() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager)SpringContextHolder.getBean(Driver_info.class);
		if(super.perExecute(driver_info)!= null) return returnCommand();
		driver_infoMgr.saveDriver_info(driver_info);
		super.postExecute(driver_info);
		return returnCommand();
	}
	
	
	/**
	 * 删除司机信息
	 */
	public String removeDriver_info() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager)SpringContextHolder.getBean(Driver_info.class);
		driver_infoMgr.removeDriver_infoById(driver_info.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些司机信息
	 */
	public String removeAllDriver_info() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager)SpringContextHolder.getBean(Driver_info.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer driver_infoid = new Integer( ids[i] );
				driver_infoMgr.removeDriver_infoById(driver_infoid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看司机信息
	 */
	public String viewDriver_info() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager)SpringContextHolder.getBean(Driver_info.class);
		driver_info = driver_infoMgr.getDriver_infoById(driver_info.getId());
		return returnCommand();
	}
	
	/**
	 * 司机信息列表
	 */
	public String driver_infoList() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager)SpringContextHolder.getBean(Driver_info.class);
		pageInfo = pageInfo == null ? new Driver_infoPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		driver_infos = driver_infoMgr.getSecurityDriver_infoList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Driver_info getDriver_info() {
		return driver_info;
	}

	public void setDriver_info(Driver_info driver_info) {
		this.driver_info = driver_info;
	}
	
	public List<Driver_info> getDriver_infos() {
		return driver_infos;
	}

	public void setDriver_infos(List<Driver_info> driver_infos) {
		this.driver_infos = driver_infos;
	}

	public Driver_infoPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Driver_infoPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
