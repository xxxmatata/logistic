package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.security.context.UserContextHelper;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Logistic_corpPageInfo;
import org.logistic.basicinfo.model.Logistic_corp;
import org.logistic.basicinfo.service.Logistic_corpManager;

public class Logistic_corpAction extends BaseAction{
	private Logistic_corp logistic_corp;
	private Logistic_corpPageInfo pageInfo;
	private List<Logistic_corp> logistic_corps;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存物流公司
	 */
	public String saveLogistic_corp() throws Exception {
		Logistic_corpManager logistic_corpMgr = (Logistic_corpManager)SpringContextHolder.getBean(Logistic_corp.class);
		if(super.perExecute(logistic_corp)!= null) return returnCommand();
		logistic_corpMgr.saveLogistic_corp(logistic_corp);
		super.postExecute(logistic_corp);
		return returnCommand();
	}
	
	
	/**
	 * 删除物流公司
	 */
	public String removeLogistic_corp() throws Exception {
		Logistic_corpManager logistic_corpMgr = (Logistic_corpManager)SpringContextHolder.getBean(Logistic_corp.class);
		logistic_corpMgr.removeLogistic_corpById(logistic_corp.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些物流公司
	 */
	public String removeAllLogistic_corp() throws Exception {
		Logistic_corpManager logistic_corpMgr = (Logistic_corpManager)SpringContextHolder.getBean(Logistic_corp.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer logistic_corpid = new Integer( ids[i] );
				logistic_corpMgr.removeLogistic_corpById(logistic_corpid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看物流公司
	 */
	public String viewLogistic_corp() throws Exception {
		Logistic_corpManager logistic_corpMgr = (Logistic_corpManager)SpringContextHolder.getBean(Logistic_corp.class);
		logistic_corp = logistic_corpMgr.getLogistic_corpById(logistic_corp.getId());
		return returnCommand();
	}

	/**
	 *查看所属的物流公司
	 */
	public String viewOwnsLogistic_corp() throws Exception {
		Logistic_corpManager logistic_corpMgr = (Logistic_corpManager)SpringContextHolder.getBean(Logistic_corp.class);
		
		
		logistic_corp = logistic_corpMgr.getLogistic_corpById(UserContextHelper.getUserContext().getOrgId());
		return returnCommand();
	}
	
	
	/**
	 * 物流公司列表
	 */
	public String logistic_corpList() throws Exception {
		Logistic_corpManager logistic_corpMgr = (Logistic_corpManager)SpringContextHolder.getBean(Logistic_corp.class);
		pageInfo = pageInfo == null ? new Logistic_corpPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		logistic_corps = logistic_corpMgr.getSecurityLogistic_corpList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Logistic_corp getLogistic_corp() {
		return logistic_corp;
	}

	public void setLogistic_corp(Logistic_corp logistic_corp) {
		this.logistic_corp = logistic_corp;
	}
	
	public List<Logistic_corp> getLogistic_corps() {
		return logistic_corps;
	}

	public void setLogistic_corps(List<Logistic_corp> logistic_corps) {
		this.logistic_corps = logistic_corps;
	}

	public Logistic_corpPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Logistic_corpPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
