package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Goods_infoPageInfo;
import org.logistic.basicinfo.model.Goods_info;
import org.logistic.basicinfo.service.Goods_infoManager;

public class Goods_infoAction extends BaseAction{
	protected Goods_info goods_info;
	protected Goods_infoPageInfo pageInfo;
	protected List<Goods_info> goods_infos;
	protected String orderIndexs;
	
	
	/**
	 * 新增/修改保存货物信息
	 */
	public String saveGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager)SpringContextHolder.getBean(Goods_info.class);
		if(super.perExecute(goods_info)!= null) return returnCommand();
		goods_infoMgr.saveGoods_info(goods_info);
		super.postExecute(goods_info);
		return returnCommand();
	}
	
	
	/**
	 * 删除货物信息
	 */
	public String removeGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager)SpringContextHolder.getBean(Goods_info.class);
		goods_infoMgr.removeGoods_infoById(goods_info.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些货物信息
	 */
	public String removeAllGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager)SpringContextHolder.getBean(Goods_info.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer goods_infoid = new Integer( ids[i] );
				goods_infoMgr.removeGoods_infoById(goods_infoid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看货物信息
	 */
	public String viewGoods_info() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager)SpringContextHolder.getBean(Goods_info.class);
		goods_info = goods_infoMgr.getGoods_infoById(goods_info.getId());
		return returnCommand();
	}
	
	/**
	 * 货物信息列表
	 */
	public String goods_infoList() throws Exception {
		Goods_infoManager goods_infoMgr = (Goods_infoManager)SpringContextHolder.getBean(Goods_info.class);
		pageInfo = pageInfo == null ? new Goods_infoPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		goods_infos = goods_infoMgr.getSecurityGoods_infoList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Goods_info getGoods_info() {
		return goods_info;
	}

	public void setGoods_info(Goods_info goods_info) {
		this.goods_info = goods_info;
	}
	
	public List<Goods_info> getGoods_infos() {
		return goods_infos;
	}

	public void setGoods_infos(List<Goods_info> goods_infos) {
		this.goods_infos = goods_infos;
	}

	public Goods_infoPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Goods_infoPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
