package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Driver2goodsPageInfo;
import org.logistic.basicinfo.model.Driver2goods;
import org.logistic.basicinfo.service.Driver2goodsManager;

public class Driver2goodsAction extends BaseAction{
	protected Driver2goods driver2goods;
	protected Driver2goodsPageInfo pageInfo;
	protected List<Driver2goods> driver2goodss;
	protected String orderIndexs;
	
	
	/**
	 * 新增/修改保存司机与货的关系
	 */
	public String saveDriver2goods() throws Exception {
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager)SpringContextHolder.getBean(Driver2goods.class);
		if(super.perExecute(driver2goods)!= null) return returnCommand();
		driver2goodsMgr.saveDriver2goods(driver2goods);
		super.postExecute(driver2goods);
		return returnCommand();
	}
	
	
	/**
	 * 删除司机与货的关系
	 */
	public String removeDriver2goods() throws Exception {
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager)SpringContextHolder.getBean(Driver2goods.class);
		driver2goodsMgr.removeDriver2goodsById(driver2goods.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些司机与货的关系
	 */
	public String removeAllDriver2goods() throws Exception {
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager)SpringContextHolder.getBean(Driver2goods.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer driver2goodsid = new Integer( ids[i] );
				driver2goodsMgr.removeDriver2goodsById(driver2goodsid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看司机与货的关系
	 */
	public String viewDriver2goods() throws Exception {
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager)SpringContextHolder.getBean(Driver2goods.class);
		driver2goods = driver2goodsMgr.getDriver2goodsById(driver2goods.getId());
		return returnCommand();
	}
	
	/**
	 * 司机与货的关系列表
	 */
	public String driver2goodsList() throws Exception {
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager)SpringContextHolder.getBean(Driver2goods.class);
		pageInfo = pageInfo == null ? new Driver2goodsPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		driver2goodss = driver2goodsMgr.getSecurityDriver2goodsList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Driver2goods getDriver2goods() {
		return driver2goods;
	}

	public void setDriver2goods(Driver2goods driver2goods) {
		this.driver2goods = driver2goods;
	}
	
	public List<Driver2goods> getDriver2goodss() {
		return driver2goodss;
	}

	public void setDriver2goodss(List<Driver2goods> driver2goodss) {
		this.driver2goodss = driver2goodss;
	}

	public Driver2goodsPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Driver2goodsPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
