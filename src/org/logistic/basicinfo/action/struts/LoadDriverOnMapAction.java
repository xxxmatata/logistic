package org.logistic.basicinfo.action.struts;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hi.base.organization.model.HiOrg;
import org.hi.common.util.CalendarUtil;
import org.hi.common.util.StringUtils;
import org.hi.framework.security.context.UserContextHelper;
import org.hi.framework.web.struts.BaseAction;
import org.logistic.basicinfo.service.CommQueryManager;
import org.logistic.basicinfo.service.MapDataManager;
import org.logistic.constant.SqlText;

public class LoadDriverOnMapAction extends BaseAction {
	
	//bssw_lng="+bssw.lng+"&bssw_lat="+bssw.lat+"&bsne_lng"+bsne.lng+"&bsne_lat"+bsne.lat
	
	private String bssw_lng;
	private String bssw_lat;
	private String bsne_lng;
	private String bsne_lat;
	private String recentTime;
	
	private List<Map> drivers;
	
	public String loadDriverOnMap() throws Exception {
		
		HiOrg opOrg = UserContextHelper.getUserContext().getOrg();
		if (opOrg == null){
			returnCommand("��¼��ʱ�������µ�¼��");
		}
		
		CommQueryManager m = new CommQueryManager();
		
		String sql = SqlText.LOAD_DRIVER_ON_MAP;
		MapDataManager mapData = new MapDataManager();
		
//���ݷ�Χ����		
//		String id = m.getChildOrg(opOrg.getId().toString());
//		if (id != null){
//			sql = sql 
//			+ " and b.org in ( " + id + " ) ";
//		}

		if (bssw_lng != null){
			sql = sql 
			+" and a.lng >= " + bssw_lng
			+" and a.lng <= " + bsne_lng
			+" and a.lat >= " + bssw_lat
			+" and a.lat <= " + bsne_lat;
		}
		
		if (!StringUtils.isNull(recentTime)){
			long r = Long.valueOf(recentTime).longValue();
			r = r*60000;
			long c = System.currentTimeMillis();
			c = c - r;
			Date date = new Date(c);
			
			sql = sql 
			+" and a.upd_time >= " + CalendarUtil.getDateForMysql(date);
		}
		
		drivers = mapData.loadDriverOnMap(sql,null,null);
		
		return returnCommand();
	}	

	public List<Map> getDrivers() {
		return drivers;
	}

	public void setDrivers(List<Map> drivers) {
		this.drivers = drivers;
	}

	public String getBssw_lng() {
		return bssw_lng;
	}

	public void setBssw_lng(String bssw_lng) {
		this.bssw_lng = bssw_lng;
	}

	public String getBssw_lat() {
		return bssw_lat;
	}

	public void setBssw_lat(String bssw_lat) {
		this.bssw_lat = bssw_lat;
	}

	public String getBsne_lng() {
		return bsne_lng;
	}

	public void setBsne_lng(String bsne_lng) {
		this.bsne_lng = bsne_lng;
	}

	public String getBsne_lat() {
		return bsne_lat;
	}

	public void setBsne_lat(String bsne_lat) {
		this.bsne_lat = bsne_lat;
	}

	public String getRecentTime() {
		return recentTime;
	}

	public void setRecentTime(String recentTime) {
		this.recentTime = recentTime;
	}


}
