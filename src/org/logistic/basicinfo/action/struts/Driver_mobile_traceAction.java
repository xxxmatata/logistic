package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Driver_mobile_tracePageInfo;
import org.logistic.basicinfo.model.Driver_mobile_trace;
import org.logistic.basicinfo.service.Driver_mobile_traceManager;

public class Driver_mobile_traceAction extends BaseAction{
	protected Driver_mobile_trace driver_mobile_trace;
	protected Driver_mobile_tracePageInfo pageInfo;
	protected List<Driver_mobile_trace> driver_mobile_traces;
	protected String orderIndexs;
	
	
	/**
	 * 新增/修改保存手机轨迹
	 */
	public String saveDriver_mobile_trace() throws Exception {
		Driver_mobile_traceManager driver_mobile_traceMgr = (Driver_mobile_traceManager)SpringContextHolder.getBean(Driver_mobile_trace.class);
		if(super.perExecute(driver_mobile_trace)!= null) return returnCommand();
		driver_mobile_traceMgr.saveDriver_mobile_trace(driver_mobile_trace);
		super.postExecute(driver_mobile_trace);
		return returnCommand();
	}
	
	
	/**
	 * 删除手机轨迹
	 */
	public String removeDriver_mobile_trace() throws Exception {
		Driver_mobile_traceManager driver_mobile_traceMgr = (Driver_mobile_traceManager)SpringContextHolder.getBean(Driver_mobile_trace.class);
		driver_mobile_traceMgr.removeDriver_mobile_traceById(driver_mobile_trace.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些手机轨迹
	 */
	public String removeAllDriver_mobile_trace() throws Exception {
		Driver_mobile_traceManager driver_mobile_traceMgr = (Driver_mobile_traceManager)SpringContextHolder.getBean(Driver_mobile_trace.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer driver_mobile_traceid = new Integer( ids[i] );
				driver_mobile_traceMgr.removeDriver_mobile_traceById(driver_mobile_traceid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看手机轨迹
	 */
	public String viewDriver_mobile_trace() throws Exception {
		Driver_mobile_traceManager driver_mobile_traceMgr = (Driver_mobile_traceManager)SpringContextHolder.getBean(Driver_mobile_trace.class);
		driver_mobile_trace = driver_mobile_traceMgr.getDriver_mobile_traceById(driver_mobile_trace.getId());
		return returnCommand();
	}
	
	/**
	 * 手机轨迹列表
	 */
	public String driver_mobile_traceList() throws Exception {
		Driver_mobile_traceManager driver_mobile_traceMgr = (Driver_mobile_traceManager)SpringContextHolder.getBean(Driver_mobile_trace.class);
		pageInfo = pageInfo == null ? new Driver_mobile_tracePageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		driver_mobile_traces = driver_mobile_traceMgr.getSecurityDriver_mobile_traceList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Driver_mobile_trace getDriver_mobile_trace() {
		return driver_mobile_trace;
	}

	public void setDriver_mobile_trace(Driver_mobile_trace driver_mobile_trace) {
		this.driver_mobile_trace = driver_mobile_trace;
	}
	
	public List<Driver_mobile_trace> getDriver_mobile_traces() {
		return driver_mobile_traces;
	}

	public void setDriver_mobile_traces(List<Driver_mobile_trace> driver_mobile_traces) {
		this.driver_mobile_traces = driver_mobile_traces;
	}

	public Driver_mobile_tracePageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Driver_mobile_tracePageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
