package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Goods_ownerPageInfo;
import org.logistic.basicinfo.model.Goods_owner;
import org.logistic.basicinfo.service.Goods_ownerManager;

public class Goods_ownerAction extends BaseAction{
	private Goods_owner goods_owner;
	private Goods_ownerPageInfo pageInfo;
	private List<Goods_owner> goods_owners;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存货主信息
	 */
	public String saveGoods_owner() throws Exception {
		Goods_ownerManager goods_ownerMgr = (Goods_ownerManager)SpringContextHolder.getBean(Goods_owner.class);
		if(super.perExecute(goods_owner)!= null) return returnCommand();
		goods_ownerMgr.saveGoods_owner(goods_owner);
		super.postExecute(goods_owner);
		return returnCommand();
	}
	
	
	/**
	 * 删除货主信息
	 */
	public String removeGoods_owner() throws Exception {
		Goods_ownerManager goods_ownerMgr = (Goods_ownerManager)SpringContextHolder.getBean(Goods_owner.class);
		goods_ownerMgr.removeGoods_ownerById(goods_owner.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些货主信息
	 */
	public String removeAllGoods_owner() throws Exception {
		Goods_ownerManager goods_ownerMgr = (Goods_ownerManager)SpringContextHolder.getBean(Goods_owner.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer goods_ownerid = new Integer( ids[i] );
				goods_ownerMgr.removeGoods_ownerById(goods_ownerid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看货主信息
	 */
	public String viewGoods_owner() throws Exception {
		Goods_ownerManager goods_ownerMgr = (Goods_ownerManager)SpringContextHolder.getBean(Goods_owner.class);
		goods_owner = goods_ownerMgr.getGoods_ownerById(goods_owner.getId());
		return returnCommand();
	}
	
	/**
	 * 货主信息列表
	 */
	public String goods_ownerList() throws Exception {
		Goods_ownerManager goods_ownerMgr = (Goods_ownerManager)SpringContextHolder.getBean(Goods_owner.class);
		pageInfo = pageInfo == null ? new Goods_ownerPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		goods_owners = goods_ownerMgr.getSecurityGoods_ownerList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Goods_owner getGoods_owner() {
		return goods_owner;
	}

	public void setGoods_owner(Goods_owner goods_owner) {
		this.goods_owner = goods_owner;
	}
	
	public List<Goods_owner> getGoods_owners() {
		return goods_owners;
	}

	public void setGoods_owners(List<Goods_owner> goods_owners) {
		this.goods_owners = goods_owners;
	}

	public Goods_ownerPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Goods_ownerPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
