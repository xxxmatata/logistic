package org.logistic.basicinfo.action.struts;

import org.hi.SpringContextHolder;
import org.hi.base.organization.model.HiOrg;
import org.hi.framework.security.context.UserContextHelper;
import org.hi.framework.web.BusinessException;
import org.hi.i18n.util.I18NUtil;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Truck_info;
import org.logistic.basicinfo.service.Driver_infoManager;
import org.logistic.basicinfo.service.Truck_infoManager;

public class ExDriver_infoAction extends Driver_infoAction {
	
	private boolean editable;
	
	
	public boolean isEditable() {
		return editable;
	}


	public void setEditable(boolean editable) {
		this.editable = editable;
	}


	/**
	 *查看司机信息（物流公司用，不能修改本公司信息）
	 */
	public String viewDriver_info_logistic() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager)SpringContextHolder.getBean(Driver_info.class);
		driver_info = driver_infoMgr.getDriver_infoById(driver_info.getId());
		HiOrg opOrg = UserContextHelper.getUserContext().getOrg();
		if (opOrg == null){
			returnCommand("登录超时，请重新登录。");
		}
		HiOrg driverOrg = driver_info.getOrg();
		if (driver_info.getId()!=null && !driver_info.getId().equals(-1) && !opOrg.getId().equals(driver_info.getOrg().getId())){
			editable = false;
		}else{
			editable = true;
		}
		return returnCommand();
	}
	
	
	/**
	 * 新增/修改保存司机信息
	 */
	public String saveDriver_info() throws Exception {
		try {

			if (driver_info.getOldValue("info_state") != null
					&& driver_info.getOldValue("info_state").toString().equals(
							"100601"))
				return returnCommand("已审核，不能修改！");

			Driver_infoManager driver_infoMgr = (Driver_infoManager) SpringContextHolder
					.getBean(Driver_info.class);
			if (super.perExecute(driver_info) != null)
				return returnCommand();

			driver_info.setOrg(UserContextHelper.getUserContext().getOrg());

			String driver_info_truck_info_id = this
					.getParameter("driver_info.truck_info.id");

			String old_driver_info_truck_info_id = "";
			if (driver_info.getTruck_info() != null
					&& driver_info.getTruck_info().getId() != null) {
				old_driver_info_truck_info_id = driver_info.getTruck_info()
						.getId().toString();
			}

			if (!old_driver_info_truck_info_id
					.equals(driver_info_truck_info_id)
					&& driver_info_truck_info_id != null) {
				Truck_infoManager truck_infoMgr = (Truck_infoManager) SpringContextHolder
						.getBean(Truck_info.class);
				Truck_info truck_info = truck_infoMgr.getTruck_infoById(new Integer(
						driver_info_truck_info_id));
				
				if (truck_info != null){
					driver_info.setTruck_info(truck_info);
				}
			}

			driver_infoMgr.saveDriver_info(driver_info);
			super.postExecute(driver_info);
		} catch (Exception ex) {
			throw new BusinessException(I18NUtil
					.getString("信息保存失败，可能是司机手机号码重复")
					+ "!");
		}
		return returnCommand();
	}

	
	/**
	 * 平台管理员修改，司机信息
	 */
	public String saveDriver_info_sys() throws Exception {
		try {


			Driver_infoManager driver_infoMgr = (Driver_infoManager) SpringContextHolder
					.getBean(Driver_info.class);
			if (super.perExecute(driver_info) != null)
				return returnCommand();

			String driver_info_truck_info_id = this
					.getParameter("driver_info.truck_info.id");

			String old_driver_info_truck_info_id = "";
			if (driver_info.getTruck_info() != null
					&& driver_info.getTruck_info().getId() != null) {
				old_driver_info_truck_info_id = driver_info.getTruck_info()
						.getId().toString();
			}

			if (!old_driver_info_truck_info_id
					.equals(driver_info_truck_info_id)
					&& driver_info_truck_info_id != null) {
				Truck_infoManager truck_infoMgr = (Truck_infoManager) SpringContextHolder
						.getBean(Truck_info.class);
				Truck_info truck_info = truck_infoMgr.getTruck_infoById(new Integer(
						driver_info_truck_info_id));
				
				if (truck_info != null){
					driver_info.setTruck_info(truck_info);
				}
			}

			driver_infoMgr.saveDriver_info(driver_info);
			super.postExecute(driver_info);
		} catch (Exception ex) {
			throw new BusinessException(I18NUtil
					.getString("信息保存失败，可能是司机手机号码重复")
					+ "!");
		}
		return returnCommand();
	}
	
	/**
	 * 删除司机信息
	 */
	public String removeDriver_info() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager) SpringContextHolder
				.getBean(Driver_info.class);
		driver_info = driver_infoMgr.getDriver_infoById(driver_info.getId());
		
		HiOrg opOrg = UserContextHelper.getUserContext().getOrg();
		if (opOrg == null){
			returnCommand("登录超时，请重新登录。");
		}
		HiOrg driverOrg = driver_info.getOrg();
		if (!opOrg.getId().equals(driver_info.getOrg().getId())){
			return returnCommand("非本单位司机，不能删除！");
		}
		
		if (driver_info.getInfo_state() != null
				&& driver_info.getInfo_state().intValue() == 100601) {
			return returnCommand("司机：" + driver_info.getFullName() + "-"
					+ driver_info.getUserName() + " 已审核，不能删除！");
		}
		driver_infoMgr.removeDriver_infoById(driver_info.getId());
		return returnCommand();
	}

	
	/**
	 * 解除司机和车辆的关系
	 */
	public String releaseDriver_info() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager) SpringContextHolder
				.getBean(Driver_info.class);
		driver_info = driver_infoMgr.getDriver_infoById(driver_info.getId());
		if (driver_info.getInfo_state() != null
				&& driver_info.getInfo_state().intValue() == 100601) {
			return returnCommand("司机：" + driver_info.getFullName() + "-"
					+ driver_info.getUserName() + " 已审核，不能修改！");
		}
		Truck_info truck_info = driver_info.getTruck_info();
		if (truck_info != null && truck_info.getInfo_state() != null && truck_info.getInfo_state().intValue() == 100601){
			return returnCommand("车辆：" + truck_info.getTruck_card() + " 已审核，不能修改！");			
		}
		driver_info.setTruck_info(null);
		driver_infoMgr.saveDriver_info(driver_info);
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些司机信息
	 */
	public String removeAllDriver_info() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager) SpringContextHolder
				.getBean(Driver_info.class);
		if (orderIndexs != null && orderIndexs.length() > 0) {
			String[] ids = orderIndexs.split(",");
			for (int i = 0; i < ids.length; i++) {
				if (ids[i].length() > 0) {
					Integer driver_infoid = new Integer(ids[i]);
					driver_info = driver_infoMgr
							.getDriver_infoById(driver_infoid);
					if (driver_info.getInfo_state() != null
							&& driver_info.getInfo_state().intValue() == 100601) {
						return returnCommand("司机：" + driver_info.getFullName()
								+ "-" + driver_info.getUserName()
								+ " 已审核，不能删除！");
					}
					driver_infoMgr.removeDriver_infoById(driver_infoid);
				}
			}
		}

		return returnCommand();
	}

}
