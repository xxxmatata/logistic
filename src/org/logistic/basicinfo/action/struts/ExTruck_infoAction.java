package org.logistic.basicinfo.action.struts;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hi.SpringContextHolder;
import org.hi.base.organization.model.HiOrg;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.security.context.UserContextHelper;
import org.hi.framework.service.QueryManager;
import org.hi.framework.web.BusinessException;
import org.hi.framework.web.PageInfoUtil;
import org.hi.i18n.util.I18NUtil;
import org.logistic.basicinfo.action.Truck_infoPageInfo;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Truck_info;
import org.logistic.basicinfo.service.ReportQueryManager;
import org.logistic.basicinfo.service.Truck_infoManager;

public class ExTruck_infoAction extends Truck_infoAction {
	
	protected List<Map> result;
	
	protected Timestamp trip_date_01;
	protected Timestamp trip_date_02;
	
	protected String truck_card;
	protected String r_name_path;
	protected Integer truck_state;
	protected String truck_type_name;

	public String getTruck_type_name() {
		return truck_type_name;
	}

	public void setTruck_type_name(String truck_type_name) {
		this.truck_type_name = truck_type_name;
	}

	public List<Map> getResult() {
		return result;
	}

	public void setResult(List<Map> result) {
		this.result = result;
	}

	public Timestamp getTrip_date_01() {
		return trip_date_01;
	}

	public void setTrip_date_01(Timestamp trip_date_01) {
		this.trip_date_01 = trip_date_01;
	}

	public Timestamp getTrip_date_02() {
		return trip_date_02;
	}

	public void setTrip_date_02(Timestamp trip_date_02) {
		this.trip_date_02 = trip_date_02;
	}

	public String getTruck_card() {
		return truck_card;
	}

	public void setTruck_card(String truck_card) {
		this.truck_card = truck_card;
	}

	public String getR_name_path() {
		return r_name_path;
	}

	public void setR_name_path(String r_name_path) {
		this.r_name_path = r_name_path;
	}

	public Integer getTruck_state() {
		return truck_state;
	}

	public void setTruck_state(Integer truck_state) {
		this.truck_state = truck_state;
	}

	/**
	 * 新增/修改保存货车信息表
	 */
	public String saveTruck_info() throws Exception {
		if (truck_info.getOldValue("info_state") != null
				&& truck_info.getOldValue("info_state").toString().equals(
						"100601")) {
			return returnCommand("已审核，不能修改！");
		}

		if (truck_info.getDriver_infos() != null
				&& truck_info.getDriver_infos().size() > 1) {
			return returnCommand("一台车辆只能对于一个注册用户！");
		}
		Truck_infoManager truck_infoMgr = (Truck_infoManager) SpringContextHolder
				.getBean(Truck_info.class);

		if (super.perExecute(truck_info) != null)
			return returnCommand();

		List<Driver_info> drivers = truck_info.getDriver_infos();
		if (drivers != null && drivers.size() > 0) {
			HiOrg org = UserContextHelper.getUserContext().getOrg();
			for (Driver_info driver : drivers) {
				driver.setOrg(org);
				driver.setInfo_state(100600);
				// 如果遗漏creator会导致受数据权限约束的用户不能查不出来。
				driver.setCreator(UserContextHelper.getUserContext().getUser());
				Integer type = new Integer(1403);
				driver.setUserMgrType(type);
			}
		}
		try {
			truck_infoMgr.saveTruck_info(truck_info);
		} catch (Exception ex) {
			String msg = ex.getMessage();
			msg = msg == null ? "" : msg;
			if (msg.contains("could not insert") && msg.contains("Driver_info")) {
				throw new BusinessException(I18NUtil
						.getString("信息保存失败，可能是司机手机号码重复")
						+ "!");
			}
		}
		super.postExecute(truck_info);
		return returnCommand();

	}

	/**
	 * 由平台管理员修改信息（不能用于新增）
	 */
	public String saveTruck_info_sys() throws Exception {

		if (truck_info.getId() == null) {
			return returnCommand("只能修改，不能新增！");
		}

		Truck_infoManager truck_infoMgr = (Truck_infoManager) SpringContextHolder
				.getBean(Truck_info.class);

		if (super.perExecute(truck_info) != null)
			return returnCommand();
		try {
			truck_infoMgr.saveTruck_info(truck_info);
		} catch (Exception ex) {
			String msg = ex.getMessage();
			msg = msg == null ? "" : msg;
			if (msg.contains("could not insert") && msg.contains("Driver_info")) {
				throw new BusinessException(I18NUtil
						.getString("信息保存失败，可能是司机手机号码重复")
						+ "!");
			}
		}
		super.postExecute(truck_info);
		return returnCommand();
	}

	/**
	 * 删除指定的某些货车信息表
	 */
	public String removeAllTruck_info() throws Exception {
		Truck_infoManager truck_infoMgr = (Truck_infoManager) SpringContextHolder
				.getBean(Truck_info.class);
		if (orderIndexs != null && orderIndexs.length() > 0) {
			String[] ids = orderIndexs.split(",");
			for (int i = 0; i < ids.length; i++) {
				if (ids[i].length() > 0) {
					Integer truck_infoid = new Integer(ids[i]);
					truck_info = truck_infoMgr.getTruck_infoById(truck_infoid);
					if (truck_info.getInfo_state() != null
							&& truck_info.getInfo_state().intValue() == 100601)
						return returnCommand("车辆：" + truck_info.getTruck_card()
								+ " 已审核，不能删除！");
					truck_infoMgr.removeTruck_infoById(truck_infoid);
				}
			}
		}

		return returnCommand();
	}

	/**
	 * 删除货车信息表
	 */
	public String removeTruck_info() throws Exception {
		Truck_infoManager truck_infoMgr = (Truck_infoManager) SpringContextHolder
				.getBean(Truck_info.class);

		truck_info = truck_infoMgr.getTruck_infoById(truck_info.getId());
		if (truck_info.getInfo_state() != null
				&& truck_info.getInfo_state().intValue() == 100601)
			return returnCommand("车辆：" + truck_info.getTruck_card()
					+ " 已审核，不能删除！");

		truck_infoMgr.removeTruck_infoById(truck_info.getId());
		return returnCommand();
	}

	/**
	 * 没有匹配司机的货车信息表列表
	 */
	public String noDriverTruck_infoList() throws Exception {
		QueryManager queryManager = (QueryManager) SpringContextHolder
				.getBean(QueryManager.class);
		pageInfo = pageInfo == null ? new Truck_infoPageInfo() : pageInfo;

		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		String alias = "truck";//Truck_info.class.getName(); 别名不能用className，否则createQuery抛异常。

		String hql = "from Truck_info as " + alias;

		hql = hql + " where not exists (";

		hql = hql
				+ " from Driver_info as driver where driver.truck_info = "+alias+")";

		
		truck_infos = queryManager.getSecurityHQLObjectsUndeleteList(hql,
				sarchPageInfo, Truck_info.class,alias);

//		 hql =
//		 "from Truck_info as truck where not exists ( from Driver_info as driver where driver.truck_info = truck) and ( truck.deleted <> 1 OR truck.deleted IS NULL ) AND ( truck.creator.org.id = 20 )";
//		 truck_infos = queryManager.getHQLObjects(hql, sarchPageInfo);

		return returnCommand();
	}
	
	/**
	 * 货车信息表列表
	 */
	public String getTruck_infoList() throws Exception {
		
		if(UserContextHelper.getUserContext() == null || UserContextHelper.getUserContext().getUser() == null){
			return returnCommand("您未登录或者已经超时，请重新登录！");
		}
		
		
		Map m = new HashMap();
		
		if (this.r_name_path != null && this.r_name_path.length()>0){
			m.put("r_name_path like", "%"+r_name_path+"%");
		}
		if (this.trip_date_01 != null){
			m.put("trip_date >=", trip_date_01);
		}
		if (this.trip_date_02 != null){
			m.put("trip_date <=", trip_date_02);
		}
		if (this.truck_card != null && this.truck_card.length()>0){
			m.put("truck_card like", "%"+truck_card+"%");
		}
		if (this.truck_type_name != null && this.truck_type_name.length()>0){
			m.put("truck_type_name like",  "%"+truck_type_name+"%");
		}
		if (this.truck_state != null){
			m.put("truck_state =", truck_state);
		}
		
		
		pageInfo = pageInfo==null?new Truck_infoPageInfo():pageInfo;
		ReportQueryManager query = new ReportQueryManager();
		result = query.getTruck_infoList(pageInfo,m);
		return returnCommand();

	}
	

}
