package org.logistic.basicinfo.action.struts;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;

import org.acegisecurity.providers.encoding.MessageDigestPasswordEncoder;
import org.hi.SpringContextHolder;
import org.hi.framework.dao.Filter;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.service.QueryManager;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;
import org.logistic.basicinfo.action.Driver2goodsPageInfo;
import org.logistic.basicinfo.action.Driver_infoPageInfo;
import org.logistic.basicinfo.action.Driver_linePageInfo;
import org.logistic.basicinfo.action.Driver_pointPageInfo;
import org.logistic.basicinfo.action.Goods_infoPageInfo;
import org.logistic.basicinfo.model.Driver2goods;
import org.logistic.basicinfo.model.Driver_info;
import org.logistic.basicinfo.model.Driver_line;
import org.logistic.basicinfo.model.Driver_point;
import org.logistic.basicinfo.model.Goods_info;
import org.logistic.basicinfo.service.Driver2goodsManager;
import org.logistic.basicinfo.service.Driver_infoManager;
import org.logistic.basicinfo.service.Driver_lineManager;
import org.logistic.basicinfo.service.Driver_pointManager;
import org.logistic.basicinfo.service.Goods_infoManager;

public class DriverAppAction extends BaseAction {

	protected Driver_info driver_info;
	protected Driver_infoPageInfo driverPageInfo;
	protected Goods_infoPageInfo goodsPageInfo;
	protected String mima;

	protected Driver2goods currDriver2goods;
	protected Driver2goods orderDriver2goods;
	protected Driver_point driver_point;
	protected Driver_line driver_line;

	protected Driver_pointPageInfo driverPointPageInfo;

	protected List<Goods_info> goods_infos;
	protected List<Driver_line> driver_lines;

	public String d_login() throws Exception {
		Cookie[] cookies = this.getRequest().getCookies();
		if (cookies == null) {
			return SUCCESS;
		}

		String userName = null;
		String ps = null;
		for (Cookie cookie : cookies) {
			if ("userName".equals(cookie.getName())) {
				userName = cookie.getValue();
			}
			if ("password".equals(cookie.getName())) {
				ps = cookie.getValue();
			}
		}

		if (userName == null) {
			return SUCCESS;
		}
		if (ps == null) {
			return SUCCESS;
		}
		Driver_infoManager driver_infoMgr = (Driver_infoManager) SpringContextHolder
				.getBean(Driver_info.class);
		driverPageInfo = driverPageInfo == null ? new Driver_infoPageInfo()
				: driverPageInfo;

		driverPageInfo.setF_userName(userName);
		driverPageInfo.setF_userName_op(Filter.OPERATOR_EQ);

		if (driverPageInfo.getF_userName() == null
				|| driverPageInfo.getF_userName().trim().length() == 0) {
			super.setErrorMsg("用户名密码不正确");
			return SUCCESS;
		}
		driverPageInfo.setF_deleted(0);
		driverPageInfo.setF_deleted_op(Filter.OPERATOR_EQ);

		PageInfo sarchPageInfo = PageInfoUtil.populate(driverPageInfo, this);

		List<Driver_info> list = driver_infoMgr
				.getDriver_infoList(sarchPageInfo);

		if (list != null && list.size() > 0) {
			Driver_info driver = list.get(0);

			if (!ps.equals(driver.getPassword())) {
				return SUCCESS;
			}

			this.getSession().setAttribute(DRIVER_APP_DRIVER_INFO, driver);
			return "cookie_success";
		} else {
			return SUCCESS;
		}
	}

	public String d_logout() throws Exception {
		this.getSession().removeAttribute(DRIVER_APP_DRIVER_INFO);
		return SUCCESS;
	}

	/**
	 * 司机登录
	 */
	public String driverLogin() throws Exception {
		Driver_infoManager driver_infoMgr = (Driver_infoManager) SpringContextHolder
				.getBean(Driver_info.class);
		driverPageInfo = driverPageInfo == null ? new Driver_infoPageInfo()
				: driverPageInfo;

		if (driverPageInfo.getF_userName() == null
				|| driverPageInfo.getF_userName().trim().length() == 0) {
			super.setErrorMsg("用户名密码不正确");
			return ERROR;
		}
		driverPageInfo.setF_deleted(0);
		driverPageInfo.setF_deleted_op(Filter.OPERATOR_EQ);

		PageInfo sarchPageInfo = PageInfoUtil.populate(driverPageInfo, this);

		List<Driver_info> list = driver_infoMgr
				.getDriver_infoList(sarchPageInfo);

		if (list != null && list.size() > 0) {

			Driver_info driver = list.get(0);

			String password = driver.getPassword();

			if (password == null || password.length() == 0) {
				this.getSession().setAttribute(DRIVER_APP_DRIVER_INFO, driver);
				return "modifyPassword";
			}
			MessageDigestPasswordEncoder passwordEncoder = (MessageDigestPasswordEncoder) SpringContextHolder
					.getBean("passwordEncoder");
			String mima2md5 = passwordEncoder.encodePassword(mima, null);

			if (password != null && password.equals(mima2md5)) {

				Cookie cookie1 = new Cookie("userName", driver.getUserName());
				Cookie cookie2 = new Cookie("password", driver.getPassword());
				this.getResponse().addCookie(cookie1);
				this.getResponse().addCookie(cookie2);

				this.getSession().setAttribute(DRIVER_APP_DRIVER_INFO, driver);

				return SUCCESS;
			} else {
				super.setErrorMsg("用户名密码不正确");
				return ERROR;
			}
		} else {
			return ERROR;// error
		}

	}

	/**
	 * 进入司机登录后的首页
	 */
	public String indexView() throws Exception {

		Driver_info s_driver = (Driver_info) this.getSession().getAttribute(
				DRIVER_APP_DRIVER_INFO);

		if (s_driver == null) {
			return "nologin";
		}

		this.driver_info = s_driver;

		QueryManager queryManager = (QueryManager) SpringContextHolder
				.getBean(QueryManager.class);
		String hql = "from Driver2goods a where a.driver.id = "
				+ driver_info.getId();

		Driver2goodsPageInfo dgPageInfo = new Driver2goodsPageInfo();
		PageInfo dgSearche = PageInfoUtil.populate(dgPageInfo, this);
		List<Driver2goods> list = queryManager.getHQLObjectsUndelete(hql,
				dgSearche, Driver2goods.class);

		for (Driver2goods dg : list) {
			if (dg.getD2gType() == 101601) {// 已经成交的记录
				orderDriver2goods = dg;
			}
			if (dg.getD2gType() == 101604) {// 已经接货的记录
				currDriver2goods = dg;
			}
		}

		Goods_infoManager goods_infoMgr = (Goods_infoManager) SpringContextHolder
				.getBean(Goods_info.class);

		goodsPageInfo = goodsPageInfo == null ? new Goods_infoPageInfo()
				: goodsPageInfo;
		
		goodsPageInfo.setSorterName("publishTime");
		goodsPageInfo.setSorterDirection("desc");
		goodsPageInfo.setPageSize(1);

		goodsPageInfo.setF_info_state(101200);
		goodsPageInfo.setF_info_state_op(Filter.OPERATOR_EQ);
		
		long l = 1000*60*60*24;
		
		Date date = new Date();
		
		long ago = date.getTime() - 3*l;
		
		goodsPageInfo.setF_publishTime(new Timestamp(ago));
		goodsPageInfo.setF_publishTime_op(Filter.OPERATOR_GREATER_EQ);

		goodsPageInfo.setSorterName("publishTime");
		goodsPageInfo.setSorterDirection("desc");
		PageInfo gSearche = PageInfoUtil.populate(goodsPageInfo, this);

		goods_infos = goods_infoMgr.getGoods_infoList(gSearche);

		Driver_pointManager driver_pointMgr = (Driver_pointManager) SpringContextHolder
				.getBean(Driver_point.class);
		driverPageInfo = new Driver_infoPageInfo();
		driverPageInfo.setF_id(driver_info.getId());
		driverPageInfo.setF_id_op(Filter.OPERATOR_EQ);
		driverPointPageInfo = new Driver_pointPageInfo();
		driverPointPageInfo.setDriver_info(driverPageInfo);
		PageInfo dtPageInfo = PageInfoUtil.populate(driverPointPageInfo, this);
		List<Driver_point> dtList = driver_pointMgr
				.getDriver_pointList(dtPageInfo);
		if (dtList != null && dtList.size() > 0) {
			driver_point = dtList.get(0);
		} else {
			driver_point = null;
		}

		return returnCommand();
	}

	/**
	 * 司机装货确认
	 */
	public String driverLoad() throws Exception {
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager) SpringContextHolder
				.getBean(Driver2goods.class);
		if (super.perExecute(orderDriver2goods) != null)
			return returnCommand();
		orderDriver2goods = driver2goodsMgr
				.getDriver2goodsById(orderDriver2goods.getId());
		Integer oldState = orderDriver2goods.getD2gType();
		if (oldState.intValue() != 101601) {
			return returnCommand("货物尚未承运确认！");
		}

		Date date = new Date();
		Timestamp opTime = new Timestamp(date.getTime());

		orderDriver2goods.setD2gType(101604);
		orderDriver2goods.setOpTime(opTime);

		driver2goodsMgr.saveDriver2goods(orderDriver2goods);
		super.postExecute(orderDriver2goods);

		return returnCommand();
	}

	/**
	 * 司机货物送到确认
	 */
	public String driverUnload() throws Exception {
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager) SpringContextHolder
				.getBean(Driver2goods.class);
		if (super.perExecute(currDriver2goods) != null)
			return returnCommand();
		currDriver2goods = driver2goodsMgr.getDriver2goodsById(currDriver2goods
				.getId());

		Integer oldState = currDriver2goods.getD2gType();
		if (oldState.intValue() != 101604) {
			return returnCommand("货物尚未装车！");
		}

		Date date = new Date();
		Timestamp opTime = new Timestamp(date.getTime());

		currDriver2goods.setD2gType(101603);
		currDriver2goods.setOpTime(opTime);

		driver2goodsMgr.saveDriver2goods(currDriver2goods);
		super.postExecute(currDriver2goods);

		return returnCommand();
	}

	/**
	 * 新增/修改保存司机行程
	 */
	public String saveDriver_trip() throws Exception {
		Driver_pointManager driver_pointMgr = (Driver_pointManager) SpringContextHolder
				.getBean(Driver_point.class);
		if (super.perExecute(driver_point) != null)
			return returnCommand();

		Driver_info s_driver = (Driver_info) this.getSession().getAttribute(
				DRIVER_APP_DRIVER_INFO);

		if (s_driver == null) {
			return "nologin";
		}

		Date date = new Date();
		Timestamp opTime = new Timestamp(date.getTime());

		driver_point.setDriver_info(s_driver);
		driver_point.setPublish_time(opTime);

		driver_pointMgr.saveDriver_point(driver_point);
		super.postExecute(driver_point);
		return returnCommand();
	}

	/**
	 * 删除司机行程
	 */
	public String removeDriver_trip() throws Exception {
		Driver_pointManager driver_pointMgr = (Driver_pointManager) SpringContextHolder
				.getBean(Driver_point.class);
		driver_pointMgr.removeDriver_pointById(driver_point.getId());
		return returnCommand();
	}

	/**
	 *查看司机行程
	 */
	public String viewDriver_trip() throws Exception {
		if (super.perExecute(driver_info) != null)
			return returnCommand();
		Driver_info s_driver = (Driver_info) this.getSession().getAttribute(
				DRIVER_APP_DRIVER_INFO);

		if (s_driver == null) {
			return "nologin";
		}

		Driver_pointManager driver_pointMgr = (Driver_pointManager) SpringContextHolder
				.getBean(Driver_point.class);
		driver_point = driver_pointMgr
				.getDriver_pointById(driver_point.getId());
		return returnCommand();
	}

	/**
	 * 修改密码
	 * 
	 * @return
	 * @throws Exception
	 */
	public String driverModifyPassword() throws Exception {
		if (super.perExecute(driver_info) != null)
			return returnCommand();
		Driver_info s_driver = (Driver_info) this.getSession().getAttribute(
				DRIVER_APP_DRIVER_INFO);

		if (s_driver == null) {
			return "nologin";
		}

		MessageDigestPasswordEncoder passwordEncoder = (MessageDigestPasswordEncoder) SpringContextHolder
				.getBean("passwordEncoder");
		String newPassword = getRequest().getParameter(
				"driver_info.newPassword");
		String password = passwordEncoder.encodePassword(newPassword, null);

		if (newPassword != null && !newPassword.equals("")
				&& !newPassword.equals(s_driver.getPassword())) {
			s_driver.setPassword(password);
		}

		this.driver_info = s_driver;
		Driver_infoManager driver_infoMgr = (Driver_infoManager) SpringContextHolder
				.getBean(Driver_info.class);

		driver_infoMgr.saveDriver_info(driver_info);
		super.postExecute(driver_info);

		return returnCommand();
	}

	/**
	 * 打开修改密码的页面
	 * 
	 * @return
	 * @throws Exception
	 */
	public String driverModifyPasswordView() throws Exception {
		Driver_info s_driver = (Driver_info) this.getSession().getAttribute(
				DRIVER_APP_DRIVER_INFO);

		if (s_driver == null) {
			return "nologin";
		}

		this.driver_info = s_driver;

		return returnCommand();
	}

	/**
	 * 设置司机长跑线路-列表
	 * 
	 * @return
	 * @throws Exception
	 */
	public String driverLineList() throws Exception {

		Driver_info s_driver = (Driver_info) this.getSession().getAttribute(
				DRIVER_APP_DRIVER_INFO);

		if (s_driver == null) {
			return "nologin";
		}

		this.driver_info = s_driver;
		Driver_linePageInfo l_pageInfo = null;

		Driver_lineManager driver_lineMgr = (Driver_lineManager) SpringContextHolder
				.getBean(Driver_line.class);
		l_pageInfo = l_pageInfo == null ? new Driver_linePageInfo()
				: l_pageInfo;
		driverPageInfo = new Driver_infoPageInfo();
		driverPageInfo.setF_id(driver_info.getId());
		driverPageInfo.setF_id_op(Filter.OPERATOR_EQ);
		l_pageInfo.setDriver_info(driverPageInfo);

		PageInfo sarchPageInfo = PageInfoUtil.populate(l_pageInfo, this);

		driver_lines = driver_lineMgr.getDriver_lineList(sarchPageInfo);

		return returnCommand();
	}

	/**
	 * 设置司机长跑线路-编辑
	 * 
	 * @return
	 * @throws Exception
	 */
	public String driverLineView() throws Exception {

		Driver_info s_driver = (Driver_info) this.getSession().getAttribute(
				DRIVER_APP_DRIVER_INFO);

		if (s_driver == null) {
			return "nologin";
		}

		this.driver_info = s_driver;

		Driver_lineManager driver_lineMgr = (Driver_lineManager) SpringContextHolder
				.getBean(Driver_line.class);
		driver_line = driver_lineMgr.getDriver_lineById(driver_line.getId());

		return returnCommand();
	}

	/**
	 * 设置司机长跑线路-保存
	 * 
	 * @return
	 * @throws Exception
	 */
	public String driverLineSave() throws Exception {

		Driver_info s_driver = (Driver_info) this.getSession().getAttribute(
				DRIVER_APP_DRIVER_INFO);

		if (s_driver == null) {
			return "nologin";
		}

		this.driver_info = s_driver;

		Driver_lineManager driver_lineMgr = (Driver_lineManager) SpringContextHolder
				.getBean(Driver_line.class);
		if (super.perExecute(driver_line) != null)
			return returnCommand();

		driver_line.setDriver_info(driver_info);

		driver_lineMgr.saveDriver_line(driver_line);
		super.postExecute(driver_line);

		return returnCommand();
	}

	/**
	 * 设置司机长跑线路-删除
	 * 
	 * @return
	 * @throws Exception
	 */
	public String driverLineDelete() throws Exception {

		Driver_lineManager driver_lineMgr = (Driver_lineManager) SpringContextHolder
				.getBean(Driver_line.class);
		driver_lineMgr.removeDriver_lineById(driver_line.getId());

		return returnCommand();
	}

	public Driver_info getDriver_info() {
		return driver_info;
	}

	public void setDriver_info(Driver_info driver_info) {
		this.driver_info = driver_info;
	}

	public String getMima() {
		return mima;
	}

	public void setMima(String mima) {
		this.mima = mima;
	}

	public List<Goods_info> getGoods_infos() {
		return goods_infos;
	}

	public void setGoods_infos(List<Goods_info> goods_infos) {
		this.goods_infos = goods_infos;
	}

	public Driver2goods getCurrDriver2goods() {
		return currDriver2goods;
	}

	public void setCurrDriver2goods(Driver2goods currDriver2goods) {
		this.currDriver2goods = currDriver2goods;
	}

	public Driver2goods getOrderDriver2goods() {
		return orderDriver2goods;
	}

	public void setOrderDriver2goods(Driver2goods orderDriver2goods) {
		this.orderDriver2goods = orderDriver2goods;
	}

	public Driver_infoPageInfo getDriverPageInfo() {
		return driverPageInfo;
	}

	public void setDriverPageInfo(Driver_infoPageInfo driverPageInfo) {
		this.driverPageInfo = driverPageInfo;
	}

	public Goods_infoPageInfo getGoodsPageInfo() {
		return goodsPageInfo;
	}

	public void setGoodsPageInfo(Goods_infoPageInfo goodsPageInfo) {
		this.goodsPageInfo = goodsPageInfo;
	}

	public Driver_point getDriver_point() {
		return driver_point;
	}

	public void setDriver_point(Driver_point driver_point) {
		this.driver_point = driver_point;
	}

	public Driver_pointPageInfo getDriverTripPageInfo() {
		return driverPointPageInfo;
	}

	public void setDriverTripPageInfo(Driver_pointPageInfo driverPointPageInfo) {
		this.driverPointPageInfo = driverPointPageInfo;
	}

	public Driver_line getDriver_line() {
		return driver_line;
	}

	public void setDriver_line(Driver_line driver_line) {
		this.driver_line = driver_line;
	}

	public Driver_pointPageInfo getDriverPointPageInfo() {
		return driverPointPageInfo;
	}

	public void setDriverPointPageInfo(Driver_pointPageInfo driverPointPageInfo) {
		this.driverPointPageInfo = driverPointPageInfo;
	}

	public List<Driver_line> getDriver_lines() {
		return driver_lines;
	}

	public void setDriver_lines(List<Driver_line> driver_lines) {
		this.driver_lines = driver_lines;
	}

}
