package org.logistic.basicinfo.action.struts;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hi.SpringContextHolder;
import org.hi.common.util.CalendarUtil;
import org.hi.common.util.StringUtils;
import org.hi.framework.dao.Filter;
import org.hi.framework.dao.impl.SimpleFilter;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.paging.impl.PageInfoImpl;
import org.hi.framework.web.struts.BaseAction;
import org.logistic.basicinfo.action.Driver_mobile_pointPageInfo;
import org.logistic.basicinfo.model.Driver_mobile_point;
import org.logistic.basicinfo.service.Driver_mobile_pointManager;
import org.logistic.basicinfo.service.MapDataManager;
import org.logistic.constant.SqlText;

public class BaiduMapLbsAction extends BaseAction {

	private String lat;

	private String lng;

	private String mdn;
	
	private Timestamp begin;
	
	private Timestamp end;
	
	private List<Map> trace;
	
	private String getTopPoint() throws Exception {
		Driver_mobile_pointManager driver_mobile_pointMgr = (Driver_mobile_pointManager)SpringContextHolder.getBean(Driver_mobile_point.class);
		Driver_mobile_pointPageInfo pageInfo = new Driver_mobile_pointPageInfo();
		List<Driver_mobile_point> driver_mobile_points;
		
		PageInfo sarchPageInfo = new PageInfoImpl();
		Filter filter = new SimpleFilter();
		filter.addCondition("mdn", mdn,Filter.OPERATOR_EQ);
		sarchPageInfo.setFilter(filter);
		
		driver_mobile_points = driver_mobile_pointMgr.getDriver_mobile_pointList(sarchPageInfo);
		
		if (driver_mobile_points!=null && driver_mobile_points.size()>0){
			Driver_mobile_point point = driver_mobile_points.get(0);
			lat = point.getLat().toString();
			lng = point.getLng().toString();
			return returnCommand();
		}else{
			return returnCommand("号码 "+mdn+" 暂时不能定位！");
		}
		
	}

	public String localteMobile() throws Exception {

//		LbsInf l = new LbsInf();
//		String latLng = null;
//
//		for (int i = 0; i < 20; i++) {
//			try {
//				latLng = l.getLbsFromthb(mdn);
//			} catch (Exception e) {
//				return returnCommand("号码 "+mdn+" 暂时不能定位！");
//			}
//			if (latLng != null && latLng.trim().length()>0 && !latLng.equals("0") && !latLng.equals("-1")) {
//				String[] s = latLng.split(",");
//				lat = s[0];
//				lng = s[1];
//				return returnCommand();
//			}
//		}
//		return returnCommand("号码 "+mdn+" 暂时不能定位！");

		return getTopPoint();

	}

	public String driverTrace() throws Exception {
		
		getTopPoint();

		String sql = SqlText.LOAD_DRIVER_TRACE_ON_MAP;
		MapDataManager mapData = new MapDataManager();

		if (mdn == null){
			return returnCommand("没有查询对象。");
		}
		
		if (begin == null){
			begin = new Timestamp(System.currentTimeMillis());
		}
		
		if (end == null){
			end = new Timestamp(System.currentTimeMillis());
		}
		
		sql = sql 
		+ " where mdn = "
		+ mdn
		+ " and upd_time >= " + CalendarUtil.getDateForMysql(begin)
		+ " and upd_time <= " + CalendarUtil.getDateForMysql(end)
		+ " order by upd_time";
		

		
		trace = mapData.loadDriverOnMap(sql,null,null);
		
		return returnCommand();

	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getMdn() {
		return mdn;
	}

	public void setMdn(String mdn) {
		this.mdn = mdn;
	}

	public Timestamp getBegin() {
		return begin;
	}

	public void setBegin(Timestamp begin) {
		this.begin = begin;
	}

	public Timestamp getEnd() {
		return end;
	}

	public void setEnd(Timestamp end) {
		this.end = end;
	}

	public List<Map> getTrace() {
		return trace;
	}

	public void setTrace(List<Map> trace) {
		this.trace = trace;
	}

}
