package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Truck_typePageInfo;
import org.logistic.basicinfo.model.Truck_type;
import org.logistic.basicinfo.service.Truck_typeManager;

public class Truck_typeAction extends BaseAction{
	private Truck_type truck_type;
	private Truck_typePageInfo pageInfo;
	private List<Truck_type> truck_types;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存货车类型
	 */
	public String saveTruck_type() throws Exception {
		Truck_typeManager truck_typeMgr = (Truck_typeManager)SpringContextHolder.getBean(Truck_type.class);
		if(super.perExecute(truck_type)!= null) return returnCommand();
		truck_typeMgr.saveTruck_type(truck_type);
		super.postExecute(truck_type);
		return returnCommand();
	}
	
	
	/**
	 * 删除货车类型
	 */
	public String removeTruck_type() throws Exception {
		Truck_typeManager truck_typeMgr = (Truck_typeManager)SpringContextHolder.getBean(Truck_type.class);
		truck_typeMgr.removeTruck_typeById(truck_type.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些货车类型
	 */
	public String removeAllTruck_type() throws Exception {
		Truck_typeManager truck_typeMgr = (Truck_typeManager)SpringContextHolder.getBean(Truck_type.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer truck_typeid = new Integer( ids[i] );
				truck_typeMgr.removeTruck_typeById(truck_typeid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看货车类型
	 */
	public String viewTruck_type() throws Exception {
		Truck_typeManager truck_typeMgr = (Truck_typeManager)SpringContextHolder.getBean(Truck_type.class);
		truck_type = truck_typeMgr.getTruck_typeById(truck_type.getId());
		return returnCommand();
	}
	
	/**
	 * 货车类型列表
	 */
	public String truck_typeList() throws Exception {
		Truck_typeManager truck_typeMgr = (Truck_typeManager)SpringContextHolder.getBean(Truck_type.class);
		pageInfo = pageInfo == null ? new Truck_typePageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		truck_types = truck_typeMgr.getSecurityTruck_typeList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Truck_type getTruck_type() {
		return truck_type;
	}

	public void setTruck_type(Truck_type truck_type) {
		this.truck_type = truck_type;
	}
	
	public List<Truck_type> getTruck_types() {
		return truck_types;
	}

	public void setTruck_types(List<Truck_type> truck_types) {
		this.truck_types = truck_types;
	}

	public Truck_typePageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Truck_typePageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
