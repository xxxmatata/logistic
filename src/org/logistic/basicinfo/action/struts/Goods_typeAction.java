package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Goods_typePageInfo;
import org.logistic.basicinfo.model.Goods_type;
import org.logistic.basicinfo.service.Goods_typeManager;

public class Goods_typeAction extends BaseAction{
	private Goods_type goods_type;
	private Goods_typePageInfo pageInfo;
	private List<Goods_type> goods_types;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存货物类型
	 */
	public String saveGoods_type() throws Exception {
		Goods_typeManager goods_typeMgr = (Goods_typeManager)SpringContextHolder.getBean(Goods_type.class);
		if(super.perExecute(goods_type)!= null) return returnCommand();
		goods_typeMgr.saveGoods_type(goods_type);
		super.postExecute(goods_type);
		return returnCommand();
	}
	
	
	/**
	 * 删除货物类型
	 */
	public String removeGoods_type() throws Exception {
		Goods_typeManager goods_typeMgr = (Goods_typeManager)SpringContextHolder.getBean(Goods_type.class);
		goods_typeMgr.removeGoods_typeById(goods_type.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些货物类型
	 */
	public String removeAllGoods_type() throws Exception {
		Goods_typeManager goods_typeMgr = (Goods_typeManager)SpringContextHolder.getBean(Goods_type.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer goods_typeid = new Integer( ids[i] );
				goods_typeMgr.removeGoods_typeById(goods_typeid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看货物类型
	 */
	public String viewGoods_type() throws Exception {
		Goods_typeManager goods_typeMgr = (Goods_typeManager)SpringContextHolder.getBean(Goods_type.class);
		goods_type = goods_typeMgr.getGoods_typeById(goods_type.getId());
		return returnCommand();
	}
	
	/**
	 * 货物类型列表
	 */
	public String goods_typeList() throws Exception {
		Goods_typeManager goods_typeMgr = (Goods_typeManager)SpringContextHolder.getBean(Goods_type.class);
		pageInfo = pageInfo == null ? new Goods_typePageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		goods_types = goods_typeMgr.getSecurityGoods_typeList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Goods_type getGoods_type() {
		return goods_type;
	}

	public void setGoods_type(Goods_type goods_type) {
		this.goods_type = goods_type;
	}
	
	public List<Goods_type> getGoods_types() {
		return goods_types;
	}

	public void setGoods_types(List<Goods_type> goods_types) {
		this.goods_types = goods_types;
	}

	public Goods_typePageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Goods_typePageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
