package org.logistic.basicinfo.action.struts;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.logistic.basicinfo.action.Driver_pointPageInfo;
import org.logistic.basicinfo.model.Driver_trip;
import org.logistic.basicinfo.service.Driver_pointManager;

public class ExDriver_tripAction extends Driver_tripAction {
	/**
	 * 司机行程列表
	 */
	public String driver_tripList() throws Exception {
		Driver_pointManager driver_tripMgr = (Driver_pointManager)SpringContextHolder.getBean(Driver_trip.class);
		pageInfo = pageInfo == null ? new Driver_pointPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		driver_points = driver_tripMgr.getDriver_pointList(sarchPageInfo);
		return returnCommand();	
	}

}
