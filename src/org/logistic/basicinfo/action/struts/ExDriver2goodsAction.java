package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.dao.Filter;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.logistic.basicinfo.action.Driver2goods_logPageInfo;
import org.logistic.basicinfo.action.Driver_infoPageInfo;
import org.logistic.basicinfo.action.Goods_infoPageInfo;
import org.logistic.basicinfo.model.Driver2goods;
import org.logistic.basicinfo.model.Driver2goods_log;
import org.logistic.basicinfo.service.Driver2goodsManager;
import org.logistic.basicinfo.service.Driver2goods_logManager;
import org.logistic.basicinfo.service.ExGoods_infoManager;

public class ExDriver2goodsAction extends Driver2goodsAction {
	protected List<Driver2goods_log> driver2goodss_log;

	
	/**
	 *查看司机与货的关系
	 */
	public String viewDriver2goods() throws Exception {
		Driver2goodsManager driver2goodsMgr = (Driver2goodsManager)SpringContextHolder.getBean(Driver2goods.class);
		driver2goods = driver2goodsMgr.getDriver2goodsById(driver2goods.getId());
		
		Driver2goods_logManager driver2goods_logMgr = (Driver2goods_logManager)SpringContextHolder.getBean(Driver2goods_log.class);
		Driver2goods_logPageInfo pageInfoLog  = new Driver2goods_logPageInfo() ;

		Goods_infoPageInfo goods_infoPageInfo = new Goods_infoPageInfo();
		goods_infoPageInfo.setF_id(driver2goods.getGoods_info().getId());
		goods_infoPageInfo.setF_id_op(Filter.OPERATOR_EQ);
		pageInfoLog.setGoods_info(goods_infoPageInfo);
		
		Driver_infoPageInfo driverPageInfo = new Driver_infoPageInfo();
		driverPageInfo.setF_id(driver2goods.getDriver().getId());
		driverPageInfo.setF_id_op(Filter.OPERATOR_EQ);
		pageInfoLog.setDriver(driverPageInfo);
		pageInfoLog.setSorterName("opTime");
		pageInfoLog.setSorterDirection("desc");
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfoLog, this);

		driver2goodss_log = driver2goods_logMgr.getDriver2goods_logList(sarchPageInfo);
		
		return returnCommand();
	}
	
	/**
	 * 记录运送评价
	 */
	public String saveEvaluateDriver2goods() throws Exception {
		
		ExGoods_infoManager goods_infoMgr = (ExGoods_infoManager) SpringContextHolder
		.getBean(ExGoods_infoManager.class);		
		
		if(super.perExecute(driver2goods)!= null) return returnCommand();
		
		if (driver2goods.getD2gType()!=null && driver2goods.getD2gType().intValue()==101600){
			return returnCommand("本次送货尚未成交！");
		}
		if (driver2goods.getD2gType()!=null && driver2goods.getD2gType().intValue()==101602){
			return returnCommand("本次送货确认已经解约！");
		}

		if (driver2goods.getD2gType()!=null && driver2goods.getD2gType().intValue()==101605){
			return returnCommand("本次送货已经评价！");
		}
		
		driver2goods.setD2gType(101605);
		goods_infoMgr.saveEvaluateDriver2goods(driver2goods);
		super.postExecute(driver2goods);
		return returnCommand();
	}	
	
	public List<Driver2goods_log> getDriver2goodss_log() {
		return driver2goodss_log;
	}
	public void setDriver2goodss_log(List<Driver2goods_log> driver2goodss_log) {
		this.driver2goodss_log = driver2goodss_log;
	}
	


}
