package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Goods2ownerPageInfo;
import org.logistic.basicinfo.model.Goods2owner;
import org.logistic.basicinfo.service.Goods2ownerManager;

public class Goods2ownerAction extends BaseAction{
	private Goods2owner goods2owner;
	private Goods2ownerPageInfo pageInfo;
	private List<Goods2owner> goods2owners;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存Entity6
	 */
	public String saveGoods2owner() throws Exception {
		Goods2ownerManager goods2ownerMgr = (Goods2ownerManager)SpringContextHolder.getBean(Goods2owner.class);
		if(super.perExecute(goods2owner)!= null) return returnCommand();
		goods2ownerMgr.saveGoods2owner(goods2owner);
		super.postExecute(goods2owner);
		return returnCommand();
	}
	
	
	/**
	 * 删除Entity6
	 */
	public String removeGoods2owner() throws Exception {
		Goods2ownerManager goods2ownerMgr = (Goods2ownerManager)SpringContextHolder.getBean(Goods2owner.class);
		goods2ownerMgr.removeGoods2ownerById(goods2owner.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些Entity6
	 */
	public String removeAllGoods2owner() throws Exception {
		Goods2ownerManager goods2ownerMgr = (Goods2ownerManager)SpringContextHolder.getBean(Goods2owner.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer goods2ownerid = new Integer( ids[i] );
				goods2ownerMgr.removeGoods2ownerById(goods2ownerid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看Entity6
	 */
	public String viewGoods2owner() throws Exception {
		Goods2ownerManager goods2ownerMgr = (Goods2ownerManager)SpringContextHolder.getBean(Goods2owner.class);
		goods2owner = goods2ownerMgr.getGoods2ownerById(goods2owner.getId());
		return returnCommand();
	}
	
	/**
	 * Entity6列表
	 */
	public String goods2ownerList() throws Exception {
		Goods2ownerManager goods2ownerMgr = (Goods2ownerManager)SpringContextHolder.getBean(Goods2owner.class);
		pageInfo = pageInfo == null ? new Goods2ownerPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		goods2owners = goods2ownerMgr.getSecurityGoods2ownerList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Goods2owner getGoods2owner() {
		return goods2owner;
	}

	public void setGoods2owner(Goods2owner goods2owner) {
		this.goods2owner = goods2owner;
	}
	
	public List<Goods2owner> getGoods2owners() {
		return goods2owners;
	}

	public void setGoods2owners(List<Goods2owner> goods2owners) {
		this.goods2owners = goods2owners;
	}

	public Goods2ownerPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Goods2ownerPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
