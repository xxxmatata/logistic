package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Driver_linePageInfo;
import org.logistic.basicinfo.model.Driver_line;
import org.logistic.basicinfo.service.Driver_lineManager;

public class Driver_lineAction extends BaseAction{
	private Driver_line driver_line;
	private Driver_linePageInfo pageInfo;
	private List<Driver_line> driver_lines;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存司机长跑线路
	 */
	public String saveDriver_line() throws Exception {
		Driver_lineManager driver_lineMgr = (Driver_lineManager)SpringContextHolder.getBean(Driver_line.class);
		if(super.perExecute(driver_line)!= null) return returnCommand();
		driver_lineMgr.saveDriver_line(driver_line);
		super.postExecute(driver_line);
		return returnCommand();
	}
	
	
	/**
	 * 删除司机长跑线路
	 */
	public String removeDriver_line() throws Exception {
		Driver_lineManager driver_lineMgr = (Driver_lineManager)SpringContextHolder.getBean(Driver_line.class);
		driver_lineMgr.removeDriver_lineById(driver_line.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些司机长跑线路
	 */
	public String removeAllDriver_line() throws Exception {
		Driver_lineManager driver_lineMgr = (Driver_lineManager)SpringContextHolder.getBean(Driver_line.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer driver_lineid = new Integer( ids[i] );
				driver_lineMgr.removeDriver_lineById(driver_lineid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看司机长跑线路
	 */
	public String viewDriver_line() throws Exception {
		Driver_lineManager driver_lineMgr = (Driver_lineManager)SpringContextHolder.getBean(Driver_line.class);
		driver_line = driver_lineMgr.getDriver_lineById(driver_line.getId());
		return returnCommand();
	}
	
	/**
	 * 司机长跑线路列表
	 */
	public String driver_lineList() throws Exception {
		Driver_lineManager driver_lineMgr = (Driver_lineManager)SpringContextHolder.getBean(Driver_line.class);
		pageInfo = pageInfo == null ? new Driver_linePageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		driver_lines = driver_lineMgr.getSecurityDriver_lineList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Driver_line getDriver_line() {
		return driver_line;
	}

	public void setDriver_line(Driver_line driver_line) {
		this.driver_line = driver_line;
	}
	
	public List<Driver_line> getDriver_lines() {
		return driver_lines;
	}

	public void setDriver_lines(List<Driver_line> driver_lines) {
		this.driver_lines = driver_lines;
	}

	public Driver_linePageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Driver_linePageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
