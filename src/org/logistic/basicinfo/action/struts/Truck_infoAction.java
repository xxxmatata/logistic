package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Truck_infoPageInfo;
import org.logistic.basicinfo.model.Truck_info;
import org.logistic.basicinfo.service.Truck_infoManager;

public class Truck_infoAction extends BaseAction{
	protected Truck_info truck_info;
	protected Truck_infoPageInfo pageInfo;
	protected List<Truck_info> truck_infos;
	protected String orderIndexs;
	
	
	/**
	 * 新增/修改保存货车信息表
	 */
	public String saveTruck_info() throws Exception {
		Truck_infoManager truck_infoMgr = (Truck_infoManager)SpringContextHolder.getBean(Truck_info.class);
		if(super.perExecute(truck_info)!= null) return returnCommand();
		truck_infoMgr.saveTruck_info(truck_info);
		super.postExecute(truck_info);
		return returnCommand();
	}
	
	
	/**
	 * 删除货车信息表
	 */
	public String removeTruck_info() throws Exception {
		Truck_infoManager truck_infoMgr = (Truck_infoManager)SpringContextHolder.getBean(Truck_info.class);
		truck_infoMgr.removeTruck_infoById(truck_info.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些货车信息表
	 */
	public String removeAllTruck_info() throws Exception {
		Truck_infoManager truck_infoMgr = (Truck_infoManager)SpringContextHolder.getBean(Truck_info.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer truck_infoid = new Integer( ids[i] );
				truck_infoMgr.removeTruck_infoById(truck_infoid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看货车信息表
	 */
	public String viewTruck_info() throws Exception {
		Truck_infoManager truck_infoMgr = (Truck_infoManager)SpringContextHolder.getBean(Truck_info.class);
		truck_info = truck_infoMgr.getTruck_infoById(truck_info.getId());
		return returnCommand();
	}
	
	/**
	 * 货车信息表列表
	 */
	public String truck_infoList() throws Exception {
		Truck_infoManager truck_infoMgr = (Truck_infoManager)SpringContextHolder.getBean(Truck_info.class);
		pageInfo = pageInfo == null ? new Truck_infoPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		truck_infos = truck_infoMgr.getSecurityTruck_infoList(sarchPageInfo);
		//truck_infos = truck_infoMgr.getTruck_infoList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Truck_info getTruck_info() {
		return truck_info;
	}

	public void setTruck_info(Truck_info truck_info) {
		this.truck_info = truck_info;
	}
	
	public List<Truck_info> getTruck_infos() {
		return truck_infos;
	}

	public void setTruck_infos(List<Truck_info> truck_infos) {
		this.truck_infos = truck_infos;
	}

	public Truck_infoPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Truck_infoPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
