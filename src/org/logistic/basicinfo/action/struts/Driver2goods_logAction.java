package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.Driver2goods_logPageInfo;
import org.logistic.basicinfo.model.Driver2goods_log;
import org.logistic.basicinfo.service.Driver2goods_logManager;

public class Driver2goods_logAction extends BaseAction{
	private Driver2goods_log driver2goods_log;
	private Driver2goods_logPageInfo pageInfo;
	private List<Driver2goods_log> driver2goods_logs;
	private String orderIndexs;
	
	
	/**
	 * 新增/修改保存司机与货的关系历史记录
	 */
	public String saveDriver2goods_log() throws Exception {
		Driver2goods_logManager driver2goods_logMgr = (Driver2goods_logManager)SpringContextHolder.getBean(Driver2goods_log.class);
		if(super.perExecute(driver2goods_log)!= null) return returnCommand();
		driver2goods_logMgr.saveDriver2goods_log(driver2goods_log);
		super.postExecute(driver2goods_log);
		return returnCommand();
	}
	
	
	/**
	 * 删除司机与货的关系历史记录
	 */
	public String removeDriver2goods_log() throws Exception {
		Driver2goods_logManager driver2goods_logMgr = (Driver2goods_logManager)SpringContextHolder.getBean(Driver2goods_log.class);
		driver2goods_logMgr.removeDriver2goods_logById(driver2goods_log.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些司机与货的关系历史记录
	 */
	public String removeAllDriver2goods_log() throws Exception {
		Driver2goods_logManager driver2goods_logMgr = (Driver2goods_logManager)SpringContextHolder.getBean(Driver2goods_log.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer driver2goods_logid = new Integer( ids[i] );
				driver2goods_logMgr.removeDriver2goods_logById(driver2goods_logid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看司机与货的关系历史记录
	 */
	public String viewDriver2goods_log() throws Exception {
		Driver2goods_logManager driver2goods_logMgr = (Driver2goods_logManager)SpringContextHolder.getBean(Driver2goods_log.class);
		driver2goods_log = driver2goods_logMgr.getDriver2goods_logById(driver2goods_log.getId());
		return returnCommand();
	}
	
	/**
	 * 司机与货的关系历史记录列表
	 */
	public String driver2goods_logList() throws Exception {
		Driver2goods_logManager driver2goods_logMgr = (Driver2goods_logManager)SpringContextHolder.getBean(Driver2goods_log.class);
		pageInfo = pageInfo == null ? new Driver2goods_logPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		driver2goods_logs = driver2goods_logMgr.getSecurityDriver2goods_logList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public Driver2goods_log getDriver2goods_log() {
		return driver2goods_log;
	}

	public void setDriver2goods_log(Driver2goods_log driver2goods_log) {
		this.driver2goods_log = driver2goods_log;
	}
	
	public List<Driver2goods_log> getDriver2goods_logs() {
		return driver2goods_logs;
	}

	public void setDriver2goods_logs(List<Driver2goods_log> driver2goods_logs) {
		this.driver2goods_logs = driver2goods_logs;
	}

	public Driver2goods_logPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(Driver2goods_logPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
