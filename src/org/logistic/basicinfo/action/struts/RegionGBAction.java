package org.logistic.basicinfo.action.struts;

import java.util.List;

import org.hi.SpringContextHolder;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.web.PageInfoUtil;
import org.hi.framework.web.struts.BaseAction;

import org.logistic.basicinfo.action.RegionGBPageInfo;
import org.logistic.basicinfo.model.RegionGB;
import org.logistic.basicinfo.service.RegionGBManager;

public class RegionGBAction extends BaseAction{
	protected RegionGB regionGB;
	protected RegionGBPageInfo pageInfo;
	protected List<RegionGB> regionGBs;
	protected String orderIndexs;
	
	
	/**
	 * 新增/修改保存国标区域编码
	 */
	public String saveRegionGB() throws Exception {
		RegionGBManager regionGBMgr = (RegionGBManager)SpringContextHolder.getBean(RegionGB.class);
		if(super.perExecute(regionGB)!= null) return returnCommand();
		regionGBMgr.saveRegionGB(regionGB);
		super.postExecute(regionGB);
		return returnCommand();
	}
	
	
	/**
	 * 删除国标区域编码
	 */
	public String removeRegionGB() throws Exception {
		RegionGBManager regionGBMgr = (RegionGBManager)SpringContextHolder.getBean(RegionGB.class);
		regionGBMgr.removeRegionGBById(regionGB.getId());
		return returnCommand();
	}
	
	/**
	 * 删除指定的某些国标区域编码
	 */
	public String removeAllRegionGB() throws Exception {
		RegionGBManager regionGBMgr = (RegionGBManager)SpringContextHolder.getBean(RegionGB.class);
		if (orderIndexs != null && orderIndexs.length()> 0 )
		{
			String[] ids= orderIndexs.split(",");
			for( int i=0; i<ids.length; i++)
			{
				if (ids[i].length()>0)
				{
				Integer regionGBid = new Integer( ids[i] );
				regionGBMgr.removeRegionGBById(regionGBid);
				}
			}
		}
		
		return returnCommand();
	}
	
	/**
	 *查看国标区域编码
	 */
	public String viewRegionGB() throws Exception {
		RegionGBManager regionGBMgr = (RegionGBManager)SpringContextHolder.getBean(RegionGB.class);
		regionGB = regionGBMgr.getRegionGBById(regionGB.getId());
		return returnCommand();
	}
	
	/**
	 * 国标区域编码列表
	 */
	public String regionGBList() throws Exception {
		RegionGBManager regionGBMgr = (RegionGBManager)SpringContextHolder.getBean(RegionGB.class);
		pageInfo = pageInfo == null ? new RegionGBPageInfo() : pageInfo;
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		regionGBs = regionGBMgr.getSecurityRegionGBList(sarchPageInfo);
		
		return returnCommand();	
	}
	
	
	
	
	public RegionGB getRegionGB() {
		return regionGB;
	}

	public void setRegionGB(RegionGB regionGB) {
		this.regionGB = regionGB;
	}
	
	public List<RegionGB> getRegionGBs() {
		return regionGBs;
	}

	public void setRegionGBs(List<RegionGB> regionGBs) {
		this.regionGBs = regionGBs;
	}

	public RegionGBPageInfo getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(RegionGBPageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}	
	
	public String getOrderIndexs() {
		return orderIndexs;
	}

	public void setOrderIndexs(String orderIndexs) {
		this.orderIndexs = orderIndexs;
	}
	
}
