package org.logistic.constant;

public class SqlText {
	
	//在首页地图上加载司机列表和位置
	public static final String LOAD_DRIVER_ON_MAP = "select b.fullName,b.userName,a.*,d.truck_card from driver_mobile_point a"
		+" join hi_user b on a.mdn=b.userName "
		+" join driver_info c on c.id = b.id "
		+" join truck_info d on c.truck_info = d.id "
		+" where 1=1 ";

	public static final String LOAD_DRIVER_TRACE_ON_MAP = "select * from driver_mobile_trace";

	public static final String LOAD_DRIVER_INFO_ON_PORTAL = "select a.id,fullName,truck_card,truck_type_name,truck_length,truck_width,truck_load from driver_info a,hi_user b,truck_info c ,truck_type d where a.id = b.id and a.truck_info=c.id and c.truck_type=d.id ";
	
}
