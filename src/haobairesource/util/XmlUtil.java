package haobairesource.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class XmlUtil {
	public static String testXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request><Ctrl/><Head/><Body><Asxml/></Body></Request>";

	/**
	 * @param args
	 * @throws DocumentException
	 * @throws IOException
	 */
	public static void main(String[] args) throws DocumentException,
			IOException {
		Document xmlDoc = DocumentHelper.parseText(testXML);
		Element newNode = DocumentHelper.createElement("testName");
		newNode.addText("child1");
		xmlDoc.getRootElement().element("Head").add(newNode);
		xmlDoc.getRootElement().element("Body").element("Asxml").setText("一些值");

		String xmlString = toXML(xmlDoc, "GBK");

		System.out.println(xmlString);

	}

	/**
	 * 将Docment对象转成XML字符串
	 * @param xmlDoc
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	public static String toXML(org.dom4j.Document xmlDoc, String encoding)
			throws IOException {
		ByteArrayOutputStream byteRep = new ByteArrayOutputStream();
		OutputFormat format = OutputFormat.createPrettyPrint();// 缩减型格式
		// OutputFormat format = OutputFormat.createCompactFormat();//紧凑型格式
		format.setEncoding(encoding);// 设置编码
		// format.setTrimText(false);//设置text中是否要删除其中多余的空格
		XMLWriter xw;
		xw = new XMLWriter(byteRep, format);
		xw.write(xmlDoc);
		return byteRep.toString();
	}
}
