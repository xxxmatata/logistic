package haobairesource.util;

import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class HttpUtils {

	private static final Logger logger = Logger.getLogger(HttpUtils.class);

	// public static InputStream getHtml4Url(String urls) {
	// HttpClient http = new DefaultHttpClient();
	//
	// try {
	// URL url = new URL(urls);
	// URI uri = new URI(url.getProtocol(), url.getUserInfo(), url
	// .getHost(), url.getPort(), url.getPath(), url.getQuery(),
	// null);
	//		
	// HttpGet httpget = new HttpGet(uri);
	//
	// HttpEntity entity = null;
	//
	// HttpResponse response = http.execute(httpget);
	// entity = response.getEntity();
	// return entity.getContent();
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// return null;
	// }
	//
	// }

	public static String getHtml4Url(String urls) throws Exception {
		URL url;

		HttpClient http = new DefaultHttpClient();

		url = new URL(urls);
		URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(),
				url.getPort(), url.getPath(), url.getQuery(), null);

		HttpGet httpget = new HttpGet(uri);

		HttpEntity entity = null;

		HttpResponse response = http.execute(httpget);
		entity = response.getEntity();
		return EntityUtils.toString(entity);

	}

	public static String postHtml4Url(String urls, Map<String, String> params)
			throws Exception {
		URL url;

		HttpClient http = new DefaultHttpClient();

		url = new URL(urls);
		URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(),
				url.getPort(), url.getPath(), url.getQuery(), null);

		HttpPost httpPost = new HttpPost(uri);

		List<NameValuePair> list = new ArrayList<NameValuePair>();

		if (params != null) {
			Iterator<String> iterator = params.keySet().iterator();
			while (iterator.hasNext()) {
				String key = iterator.next();
				String value = params.get(key);
				list.add(new BasicNameValuePair(key, value));

			}
		}

		httpPost.setEntity(new UrlEncodedFormEntity(list, HTTP.UTF_8));

		HttpEntity entity = null;

		HttpResponse response = http.execute(httpPost);
		entity = response.getEntity();
		return EntityUtils.toString(entity);

	}

	public static void main(String[] args) throws Exception {
		// getTimestamp();
		Map<String, String> m = new HashMap<String, String>();
		m
				.put(
						"jsonStr",
						"{\"userId\":1,\"mdn\":null,\"locMode\":'GPS��λ',\"pos\":{\"lng\":null,\"lat\":null}}");
		String resp = postHtml4Url(
				"http://localhost:8080/jhi_demo/updatePosition_updatePosition.action",
				m);
		System.out.println(resp);
	}

}
