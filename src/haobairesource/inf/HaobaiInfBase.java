package haobairesource.inf;

import haobairesource.util.AuthUtils;

import haobairesource.util.HttpUtils;
import haobairesource.util.XmlUtil;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

public class HaobaiInfBase {





	protected String parseToXmlStr(InputStream xmlStream) throws Exception {
		SAXReader reader = new SAXReader();
		Document doc = reader.read(xmlStream);
		return XmlUtil.toXML(doc, "GBK");
	}
	
	
	/**
	 * ��ȡʱ���
	 * @return
	 */
	private String getTimestamp() {
		TimeZone tz = TimeZone.getTimeZone("ETC/GMT-8");
		TimeZone.setDefault(tz);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timestamp = sdf.format(new Date());

		return timestamp;
	}	
}
