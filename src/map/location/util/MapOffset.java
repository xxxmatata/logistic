package map.location.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import net.sf.json.JSONObject;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

/**
 * GPS坐标转地图坐标
 * @author Administrator
 *
 */
public class MapOffset {


	public static String gps2baidu(String lng, String lat) throws IOException {

		URL url = new URL(
				"http://api.map.baidu.com/ag/coord/convert?from=0&to=4&mode=1&x="
						+ lng + "&y=" + lat);
		URLConnection connection = url.openConnection();

		/**
		 * 然后把连接设为输出模式。URLConnection通常作为输入来使用，比如下载一个Web页。
		 * 通过把URLConnection设为输出，你可以把数据向你个Web页传送。下面是如何做：
		 */
		connection.setDoOutput(true);
		OutputStreamWriter out = new OutputStreamWriter(connection
				.getOutputStream(), "utf-8");
		// remember to clean up
		out.flush();
		out.close();
		// 一旦发送成功，用以下方法就可以得到服务器的回应：

		InputStream l_urlStream;
		l_urlStream = connection.getInputStream();
		BufferedReader l_reader = new BufferedReader(new InputStreamReader(
				l_urlStream));

		StringBuffer jb = new StringBuffer();
		String line = null;
		while ((line = l_reader.readLine()) != null) {
			jb.append(line);
		}
		String s = jb.toString();
		s = s.substring(1, s.length() - 1);
		// System.out.println(s);
		JSONObject jsonObject = JSONObject.fromObject(s);
		String x1 = jsonObject.get("x").toString();
		String y1 = jsonObject.get("y").toString();

		x1 = new String(Base64.decode(x1));
		y1 = new String(Base64.decode(y1));
		String desc = y1 + "," + x1;
		return desc;
	}

	
	public static void main(String[] args) throws Exception{
		
		System.out.println(gps2baidu("114.3934974","30.4823911"));
	}
}
