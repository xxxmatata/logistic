package map.location.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
/**
 * 加载google地图纠偏库
 * @author Administrator
 *
 */
public class GoogleOffset {
	
	private static Map m = new HashMap();
	
	static {
		try{
			String filepath = "map_local/offset_google.txt";// 注意filepath的内容；
			File file = new File(filepath);
			//System.out.println(file);

			FileReader reader = new FileReader("map_local/offset_google.txt");
			BufferedReader br = new BufferedReader(reader);
			String s1 = null;
			while ((s1 = br.readLine()) != null) {
				String[] s2 = s1.split(",");
				//System.out.println(s2[0]);
				String offsetlng = s2[4];
				String offsetlat = s2[5];
				String lng,lat = null;
				
				if (s2[0].indexOf(".") == -1){
					lng = s2[0]+".0";
				}else{
					lng = s2[0];
				}
				
				if (s2[1].indexOf(".") == -1){
					lat = s2[1]+".0";
				}else{
					lat = s2[1];
				}
				
				m.put(lng+","+lat, offsetlng+","+offsetlat);
				
				
			}
			//30.501361,114.394458
			br.close();
			reader.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * 采取截位查找0.1纠偏数据库
	 * @param lng
	 * @param lat
	 * @return
	 */
	public static String gps2google(String lng,String lat){
		int i = lng.indexOf(".");
		String kLng = lng.substring(0,i+2);
		
		 i = lat.indexOf(".");
		String kLat = lat.substring(0,i+2);
		
		String result = (String)m.get(kLng+","+kLat);

		
		double dLng = Double.parseDouble(lng);
		double dLat = Double.parseDouble(lat);
		
		String[] offset = result.split(",");
		double dOffsetLng = Double.parseDouble(offset[0]);
		double dOffsetLat = Double.parseDouble(offset[1]);
		
		String xLng = String.valueOf(dLng+dOffsetLng);
		String xLat = String.valueOf(dLat+dOffsetLat);
		
		result = xLat+","+xLng;
		
		return result;
	}
	
	public static void main(String[] args) throws Exception {
		String rs = gps2google("116.396584","39.912706");
		System.out.println(rs);
	}
	
	
}
