package org.hi.framework.service;

import java.util.List;

import org.hi.framework.dao.Filter;
import org.hi.framework.dao.hibernate.BaseDAOHibernate;
import org.hi.framework.dao.impl.FilterFactory;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.service.impl.ManagerImpl;
import org.hibernate.Query;
import org.hibernate.Session;

//public class QueryManager extends ManagerImpl implements Manager { 不实现manager，手工实现各种filter加载。
public class QueryManager extends ManagerImpl implements Manager {	
	
	/**
	 * 查找对象列表
	 * @param hql
	 * @param pageInfo
	 * @param clazz
	 * @return
	 */
	public List getHQLObjects(String hql, PageInfo pageInfo){
		return dao.getHQLObjects(hql, pageInfo);
	}

	
	/**
	 * 查找未删除的对象
	 * @param hql
	 * @param pageInfo
	 * @param clazz
	 * @return
	 */
	public List getHQLObjectsUndelete(String hql, PageInfo pageInfo,Class clazz){
		Filter filter = deletedFilter(clazz);
		pageInfo.setFilter(filter);
		return dao.getHQLObjects(hql, pageInfo);
	}

	/**
	 * 按数据权限，查找未删除的对象getSecurity(xxx)List(MethodConfigAttributeDefHolder 不接受参数String hql,Class clazz)，故单独添加securityFilter。
	 * @param hql
	 * @param pageInfo
	 * @param clazz
	 * @return
	 */
	public List getSecurityHQLObjectsUndeleteList(String hql, PageInfo pageInfo,Class clazz){
		return getSecurityHQLObjectsUndeleteList(hql,pageInfo,null);
	}
	public List getSecurityHQLObjectsUndeleteList(String hql, PageInfo pageInfo,Class clazz,String alias){
		Filter deleteFilter = FilterFactory.deletedFilter(clazz,alias);
		Filter securityFilter = FilterFactory.getSecurityFilter(alias); 
		pageInfo.setFilter(deleteFilter);
		pageInfo.setFilter(securityFilter);
		return dao.getHQLObjects(hql, pageInfo);
	}	
	
	public List getSqlQueryResult(String sqlStr, Class clazz){
		Query query =  dao.getSqlQuery(sqlStr).addEntity(clazz);
		return query.list();
	}

}
