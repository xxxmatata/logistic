package org.hi.framework.web;

import java.util.Map;

public class CommenPageInfoView extends PageInfoView {
	private Map<String,Object> param;

	public Map<String,Object> getParam() {
		return param;
	}

	public void setParam(Map<String,Object> param) {
		this.param = param;
	}
}
