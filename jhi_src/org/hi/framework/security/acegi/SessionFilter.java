package org.hi.framework.security.acegi;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.acegisecurity.context.HttpSessionContextIntegrationFilter;
import org.acegisecurity.context.SecurityContext;

public class SessionFilter extends HttpSessionContextIntegrationFilter {

	public SessionFilter() throws ServletException {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		super.afterPropertiesSet();
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest req = (HttpServletRequest)arg0;

		String path = req.getServletPath();
		
		if (!"/j_security_check".equals(path)){
			super.doFilter(req, arg1, arg2);
			return;
		}
		
		Enumeration enu = req.getParameterNames();
		
		while (enu.hasMoreElements()){
			String key = enu.nextElement().toString();
			String value = req.getParameter(key);
			req.getSession().setAttribute(key, value);
		}
		super.doFilter(req, arg1, arg2);
	}

	@Override
	public SecurityContext generateNewContext() throws ServletException {
		// TODO Auto-generated method stub
		return super.generateNewContext();
	}

	@Override
	public Class getContext() {
		// TODO Auto-generated method stub
		return super.getContext();
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		super.init(filterConfig);
	}

	@Override
	public boolean isAllowSessionCreation() {
		// TODO Auto-generated method stub
		return super.isAllowSessionCreation();
	}

	@Override
	public boolean isCloneFromHttpSession() {
		// TODO Auto-generated method stub
		return super.isCloneFromHttpSession();
	}

	@Override
	public boolean isForceEagerSessionCreation() {
		// TODO Auto-generated method stub
		return super.isForceEagerSessionCreation();
	}

	@Override
	public void setAllowSessionCreation(boolean allowSessionCreation) {
		// TODO Auto-generated method stub
		super.setAllowSessionCreation(allowSessionCreation);
	}

	@Override
	public void setCloneFromHttpSession(boolean cloneFromHttpSession) {
		// TODO Auto-generated method stub
		super.setCloneFromHttpSession(cloneFromHttpSession);
	}

	@Override
	public void setContext(Class secureContext) {
		// TODO Auto-generated method stub
		super.setContext(secureContext);
	}

	@Override
	public void setForceEagerSessionCreation(boolean forceEagerSessionCreation) {
		// TODO Auto-generated method stub
		super.setForceEagerSessionCreation(forceEagerSessionCreation);
	}

}
