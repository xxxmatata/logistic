/**
 * @{#} CalendarUtil.java Create on May 28, 2010
 *
 * Copyright (c) 2010 by xinfeng
 */
package org.hi.common.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/** 
 * @author xinfeng <br>
 *         email: bkyangxinfeng@hotmail.com 
 * @version 1.0
 * @description 
 */
public class CalendarUtil {
	
	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	public static Timestamp getDateFor00(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return new Timestamp(calendar.getTimeInMillis());
	}
	public static Timestamp getDateFor23(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return new Timestamp(calendar.getTimeInMillis());
	}
	
	public static Timestamp getDateFor00(Date date) {
		if(date == null)
			return null;
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return getDateFor00(calendar);
	}
	
	public static Timestamp getDateFor23(Date date) {
		if(date == null)
			return null;
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		return getDateFor23(calendar);
	}
	
	public static Timestamp getDateFor00(Timestamp timestamp) {
		if(timestamp == null)
			return null;
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(new Date(timestamp.getTime()));
		return getDateFor00(calendar);
	}
	
	public static Timestamp getDateFor23(Timestamp timestamp) {
		if(timestamp == null)
			return null;
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(new Date(timestamp.getTime()));
		return getDateFor23(calendar);
	}
	
	
	public static String date2String(Timestamp timestamp,String format){
		if (format == null){
			format = DATE_FORMAT;
		}
		DateFormat sdf = new SimpleDateFormat(format); 
		return sdf.format(timestamp);
	}

	public static String getDateForMysql(Date date) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		int y = calendar.get(Calendar.YEAR);
		String _y = String.valueOf(y);
		int mo = calendar.get(Calendar.MONTH);
		String _mo = String.valueOf(mo+1);
		if (_mo.length()==1)
			_mo = "0"+_mo;
		int d = calendar.get(Calendar.DAY_OF_MONTH);
		String _d = String.valueOf(d);
		if (_d.length()==1)
			_d = "0"+_d;
		int h = calendar.get(Calendar.HOUR_OF_DAY);
		String _h = String.valueOf(h);
		if (_h.length()==1)
			_h = "0"+_h;
		int mi = calendar.get(Calendar.MINUTE);
		String _mi = String.valueOf(mi);
		if (_mi.length()==1)
			_mi = "0"+_mi;
		int s = calendar.get(Calendar.SECOND);
		String _s = String.valueOf(s);
		if (_s.length()==1)
			_s = "0"+_s;

		return _y+_mo+_d+_h+_mi+_s;
	}
	
	
	public static void main(String[] args){
		System.out.println(System.currentTimeMillis());
		Date date = new Date(System.currentTimeMillis()-60000);
		System.out.println(getDateForMysql(date));
	}
	
}
