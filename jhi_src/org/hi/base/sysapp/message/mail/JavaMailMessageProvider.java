package org.hi.base.sysapp.message.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.hi.SpringContextHolder;
import org.hi.base.organization.model.HiUser;
import org.hi.base.organization.service.HiUserManager;
import org.hi.base.sysapp.message.AbstractMessageProvider;
import org.hi.base.sysapp.model.Message;
import org.hi.base.sysapp.model.MessageParameter;
import org.hi.framework.web.BusinessException;
import org.hi.i18n.util.I18NUtil;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;


public class JavaMailMessageProvider extends AbstractMessageProvider {
	
	protected static final String MESSAGE_PARAMETER_BCC = "BCC";
	protected static final String MESSAGE_PARAMETER_CC = "CC";
	
	protected String host;
	protected int port;
	protected String encoding;
	protected String userName;
	protected String password;
	protected String from;

	public void sendMessage(Message message) throws BusinessException {
		if(message.getReceivers() == null && this.getParameter(message.getMessageParameters(), MESSAGE_PARAMETER_ALL) == null)
			return;
		
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		
		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.auth", "true"); 
		
		sender.setJavaMailProperties(javaMailProperties);
		sender.setHost(this.getHost());
		sender.setPort(this.getPort());
		sender.setUsername(this.getUserName());
		sender.setPassword(this.getPassword());
		
		MimeMessage mimeMessage = sender.createMimeMessage();
		try{
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true,getEncoding());
			helper.setFrom(this.getFrom());
			helper.setText(message.getMessageText());
			String pValue = null;
			
			//设置接件人
			
			pValue = this.getParameter(message.getMessageParameters(), MESSAGE_PARAMETER_ALL);
			String[] receivers = null;
			if(pValue != null){							
				if(Boolean.valueOf(pValue)){			//如果有all参数,则all优先
					receivers = this.getAllReceivers();
				}
			}
			else{
				receivers = message.getReceivers().split(",");
			}
			if(receivers == null && receivers.length > 0 )	//如果处理后仍没有接件人,则返回
				return;
			
			helper.setTo(receivers);
			
			
			//设置邮件标题
			pValue = this.getParameter(message.getMessageParameters(), MESSAGE_PARAMETER_SUBJECT);
			if(pValue != null)
				helper.setSubject(pValue);
			
			//设置抄送人
			pValue = this.getParameter(message.getMessageParameters(), MESSAGE_PARAMETER_CC);
			if(pValue != null){
				String[] cc = pValue.split(",");
				helper.setCc(cc);
			}

			//设置暗送人
			pValue = this.getParameter(message.getMessageParameters(), MESSAGE_PARAMETER_BCC);
			if(pValue != null){
				String[] bcc = pValue.split(",");
				helper.setBcc(bcc);
			}
		}
		catch (MessagingException e) {
			new BusinessException(I18NUtil.getString("发送邮件失败"));
		}
		sender.send(mimeMessage);
//		this.sentProcess(message);
	}
	
	
	public String[] getAllReceivers() {
		List<String> receivers = new ArrayList<String>();
		HiUserManager userMgr = (HiUserManager)SpringContextHolder.getBean(HiUser.class);
		List<HiUser> users = userMgr.getObjects();
		String userMail = null;
		for (HiUser user : users) {
			userMail = user.getMail();
			if(userMail != null && userMail.indexOf("@") > 0)
				receivers.add(userMail);
		}
		return receivers.toArray(new String[receivers.size()]);
	}
	
	public String getReceivers(String ids) {
		HiUserManager userMgr = (HiUserManager)SpringContextHolder.getBean(HiUser.class);
		StringBuffer sb = new StringBuffer("");
		String[] users = ids.split(",");
		for (String userId : users) {
			HiUser  user = (HiUser)userMgr.getObjectById(new Integer(userId));
			String userMail = user.getMail();
			if(userMail != null && userMail.indexOf("@") > 0)
				sb.append(userMail).append(",");
		}
		return sb.toString();
	}
	
	public void setHost(String host) {
		this.host = host;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getHost(){
		return host==null?"smtp.sina.com.cn":host;
	}
	
	public String getUserName(){
		return userName==null?"xxxmatata":userName;
	}
	public String getPassword(){
		return password==null?"321765":password;
	}
	public String getFrom(){
		return from==null?"xxxmatata@sina.com":from;
	}
	
	public String getEncoding(){
		return encoding==null?"UTF-8":encoding;
	}

	public int getPort(){
		return port==0 ? 25 : port;
	}
	
	public void sendMail(String email,String title ,String Message){
		Message message = new Message();
		message.setSender("xxxmatata@sina.com");
		message.setReceivers(email);
		message.setMessageText(Message);
		List<MessageParameter> list = new ArrayList<MessageParameter>();
		
		MessageParameter subject = new MessageParameter();
		subject.setParameterKey(MESSAGE_PARAMETER_SUBJECT);
		subject.setParameterValue(title);
		list.add(subject);
		
//		MessageParameter cc = new MessageParameter();
//		cc.setParameterKey(MESSAGE_PARAMETER_CC);
//		cc.setParameterValue("blue_jiang@sina.com");
//		list.add(cc);
//		
//		MessageParameter bcc = new MessageParameter();
//		bcc.setParameterKey(MESSAGE_PARAMETER_BCC);
//		bcc.setParameterValue("10334167@qq.com");
//		list.add(bcc);
		
		message.setMessageParameters(list);
		
		sendMessage(message);
		
	}
	
	public void sendHtmlMail(String email,String title ,String message){

		//TODO		
		
	}
	
	
	public static void main(String[] args) {
		JavaMailMessageProvider provider  = new JavaMailMessageProvider();
		provider.sendMail("18907181564@189.cn", "something todo", "http://localhost:8080/apple_travel");
	}


}
