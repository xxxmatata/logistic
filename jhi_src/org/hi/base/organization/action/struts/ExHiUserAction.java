package org.hi.base.organization.action.struts;

import java.util.List;

import org.acegisecurity.providers.encoding.MessageDigestPasswordEncoder;
import org.hi.SpringContextHolder;
import org.hi.base.organization.action.HiUserPageInfo;
import org.hi.base.organization.model.HiUser;
import org.hi.base.organization.service.HiUserManager;
import org.hi.framework.dao.Filter;
import org.hi.framework.dao.impl.FilterFactory;
import org.hi.framework.paging.PageInfo;
import org.hi.framework.security.model.Role;
import org.hi.framework.security.model.UserRole;
import org.hi.framework.security.service.RoleManager;
import org.hi.framework.security.service.UserRoleManager;
import org.hi.framework.web.BusinessException;
import org.hi.framework.web.PageInfoUtil;
import org.hi.i18n.util.I18NUtil;

public class ExHiUserAction extends HiUserAction{
	/**
	 * 管理员列表
	 */
	public String hiUserList() throws Exception {
		HiUserManager hiUserMgr = (HiUserManager)SpringContextHolder.getBean(HiUser.class);
		pageInfo = pageInfo == null ? new HiUserPageInfo() : pageInfo;
		
		/**
		 * 不对司机进行管理
		 */
		pageInfo.setF_userMgrType(new Integer(1402));
		pageInfo.setF_userMgrType_op(Filter.OPERATOR_EQ);
		PageInfo sarchPageInfo = PageInfoUtil.populate(pageInfo, this);
		
		hiUsers = hiUserMgr.getSecurityHiUserList(sarchPageInfo);
		
		
		return returnCommand();	
	}
	
	/**
	 * 新增/修改保存物流公司管理员
	 */
	public String saveHiUser() throws Exception {
		HiUserManager hiUserMgr = (HiUserManager)SpringContextHolder.getBean(HiUser.class);
		Filter filter = FilterFactory.getSimpleFilter("userName", hiUser.getUserName(), Filter.OPERATOR_EQ);
		List<HiUser> users = hiUserMgr.getObjects(filter);
		if((hiUser.getId() == null && users.size() >0) || (hiUser.getId() != null && users.size() >1))
			throw new BusinessException(I18NUtil.getString("帐号重复") + "!");
		
		if(super.perExecute(hiUser)!= null) 
			return returnCommand();

		MessageDigestPasswordEncoder passwordEncoder = (MessageDigestPasswordEncoder)SpringContextHolder.getBean("passwordEncoder");
		String newPassword = getRequest().getParameter("hiUser.newPassword");
		String password = passwordEncoder.encodePassword(newPassword, null);
		
		if(!newPassword.equals("") && !newPassword.equals(hiUser.getPassword())){
			hiUser.setPassword(password);
		}
		
		hiUser.setUserMgrType(1402);
		hiUserMgr.saveHiUser(hiUser);
		
		RoleManager roleMgr = (RoleManager)SpringContextHolder.getBean(Role.class);
		Role role = roleMgr.getRoleById(2);
		roleMgr.saveUserRole(role, hiUser);

		return returnCommand();
	}
	
}
