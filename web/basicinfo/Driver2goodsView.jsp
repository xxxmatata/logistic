<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="司机与货的关系"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="司机名称" entity="Driver2goods"/>：</dt><dd><hi:select emu="info_state" name="driver2goods.driver.info_state" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="当前关系" entity="Driver2goods"/>：</dt><dd><hi:select emu="driver2goodsType" name="driver2goods.d2gType" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="发生时间" entity="Driver2goods"/>：</dt><dd><fmt:formatDate value="${driver2goods.opTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="评价" entity="Driver2goods"/>：</dt><dd><hi:select emu="taskRank" name="driver2goods.taskRank" isLabel="true"/></dd>
		</dl>

		<div class="divider"></div>
			</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
