<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="goods_infoList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="goods_infoList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
			<li class="dateRange">
				<label><hi:text key="发布时间" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.f_publishTime" class="date" readonly="readonly" value="<fmt:formatDate value='${pageInfo.f_publishTime}' pattern='yyyy-MM-dd'/>"/>
				<input type="hidden" name="pageInfo.f_publishTime_op" value="&gt;="><span class="limit">-</span>
				<input type="text" name="pageInfo.f_publishTime01" class="date" readonly="readonly" value="<fmt:formatDate value='${pageInfo.f_publishTime01}' pattern='yyyy-MM-dd'/>"/>
				<input type="hidden" name="pageInfo.f_publishTime01_op" value="&lt;=">
			</li>	  
			<li>
				<label><hi:text key="起点" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.origin.f_r_name" value="${pageInfo.origin.f_r_name}"/>
			</li>	  
			<li>
				<label><hi:text key="货车车型" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.truck_type.f_truck_type_name" value="${pageInfo.truck_type.f_truck_type_name}"/>
			</li>	  
			<li>
				<label><hi:text key="车长" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.f_truck_length" value="${pageInfo.f_truck_length}"/>
			</li>	  
			<li>
				<label><hi:text key="载重" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.f_truck_load" value="${pageInfo.f_truck_load}"/>
			</li>	  
			<li>
				<label><hi:text key="状态" entity="Goods_info"/>:</label>
				<hi:search name="pageInfo.f_info_state" emu="goods_state"/>
			</li>	  
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="GOODS_INFO_SAVE"><li><a class="add" href="<hi:url>goods_infoEdit.action?goods_info.id=-1</hi:url>" target="navTab" rel="goods_info"><span><hi:text key="新建" parameterLanguageKeys="货物信息"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="GOODS_INFO_DEL"><li><a class="delete" href="<hi:url>goods_infoRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', publishTime:'',origin_r_name:'',dest_r_name:'',info_state_1:'',freightRate:'',freightUnits:'',loadTime:'',loadPlace:'',truckNumber:'',truck_type_name:'',truck_length:'',truck_width:'',truck_load:'',info_state:'',info_state_2:'',orderTime:'',realLoadTime:'',unloadTime:'',goodsTypeName:'',goodsUnits:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="publishTime" class="${pageInfo.sorterName eq 'publishTime' ? pageInfo.sorterDirection : ''}"><hi:text key="发布时间" entity="Goods_info"/></th>
				<th orderField="origin.r_name" class="${pageInfo.sorterName eq 'origin.r_name' ? pageInfo.sorterDirection : ''}"><hi:text key="起点" entity="Goods_info"/></th>
				<th orderField="destination.r_name" class="${pageInfo.sorterName eq 'destination.r_name' ? pageInfo.sorterDirection : ''}"><hi:text key="终点" entity="Goods_info"/></th>
				<th orderField="logistic_corp.info_state" class="${pageInfo.sorterName eq 'logistic_corp.info_state' ? pageInfo.sorterDirection : ''}"><hi:text key="信息状态" entity="Goods_info"/></th>
				<th orderField="freightRate" class="${pageInfo.sorterName eq 'freightRate' ? pageInfo.sorterDirection : ''}"><hi:text key="运价" entity="Goods_info"/></th>
				<th orderField="freightUnits" class="${pageInfo.sorterName eq 'freightUnits' ? pageInfo.sorterDirection : ''}"><hi:text key="运价单位" entity="Goods_info"/></th>
				<th orderField="loadTime" class="${pageInfo.sorterName eq 'loadTime' ? pageInfo.sorterDirection : ''}"><hi:text key="装货时间" entity="Goods_info"/></th>
				<th orderField="loadPlace" class="${pageInfo.sorterName eq 'loadPlace' ? pageInfo.sorterDirection : ''}"><hi:text key="装货地点" entity="Goods_info"/></th>
				<th orderField="truckNumber" class="${pageInfo.sorterName eq 'truckNumber' ? pageInfo.sorterDirection : ''}"><hi:text key="求车数量" entity="Goods_info"/></th>
				<th orderField="truck_type.truck_type_name" class="${pageInfo.sorterName eq 'truck_type.truck_type_name' ? pageInfo.sorterDirection : ''}"><hi:text key="货车车型" entity="Goods_info"/></th>
				<th orderField="truck_length" class="${pageInfo.sorterName eq 'truck_length' ? pageInfo.sorterDirection : ''}"><hi:text key="车长" entity="Goods_info"/></th>
				<th orderField="truck_width" class="${pageInfo.sorterName eq 'truck_width' ? pageInfo.sorterDirection : ''}"><hi:text key="车宽" entity="Goods_info"/></th>
				<th orderField="truck_load" class="${pageInfo.sorterName eq 'truck_load' ? pageInfo.sorterDirection : ''}"><hi:text key="载重" entity="Goods_info"/></th>
				<th orderField="info_state" class="${pageInfo.sorterName eq 'info_state' ? pageInfo.sorterDirection : ''}"><hi:text key="状态" entity="Goods_info"/></th>
				<th orderField="driver.info_state" class="${pageInfo.sorterName eq 'driver.info_state' ? pageInfo.sorterDirection : ''}"><hi:text key="信息状态" entity="Goods_info"/></th>
				<th orderField="orderTime" class="${pageInfo.sorterName eq 'orderTime' ? pageInfo.sorterDirection : ''}"><hi:text key="成交时间" entity="Goods_info"/></th>
				<th orderField="realLoadTime" class="${pageInfo.sorterName eq 'realLoadTime' ? pageInfo.sorterDirection : ''}"><hi:text key="装货完成时间" entity="Goods_info"/></th>
				<th orderField="unloadTime" class="${pageInfo.sorterName eq 'unloadTime' ? pageInfo.sorterDirection : ''}"><hi:text key="卸货时间" entity="Goods_info"/></th>
				<th orderField="goodsType.goodsTypeName" class="${pageInfo.sorterName eq 'goodsType.goodsTypeName' ? pageInfo.sorterDirection : ''}"><hi:text key="货物类型名称" entity="Goods_info"/></th>
				<th orderField="goodsUnits" class="${pageInfo.sorterName eq 'goodsUnits' ? pageInfo.sorterDirection : ''}"><hi:text key="数量单位" entity="Goods_info"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${goods_infos}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td><fmt:formatDate value="${item.publishTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				    <td><authz:authorize ifAnyGranted="REGIONGB_VIEW"><a href="<hi:url>regionGBView.action?regionGB.id=${item.origin.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.origin.r_name}
					<authz:authorize ifAnyGranted="REGIONGB_VIEW"></a></authz:authorize>
					</td>
				    <td><authz:authorize ifAnyGranted="REGIONGB_VIEW"><a href="<hi:url>regionGBView.action?regionGB.id=${item.destination.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.destination.r_name}
					<authz:authorize ifAnyGranted="REGIONGB_VIEW"></a></authz:authorize>
					</td>
				    <td><authz:authorize ifAnyGranted="LOGISTIC_CORP_VIEW"><a href="<hi:url>logistic_corpView.action?logistic_corp.id=${item.logistic_corp.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
				    <hi:select emu="info_state" name="goods_infos[${s.index}].logistic_corp.info_state" isLabel="true"/>
					<authz:authorize ifAnyGranted="LOGISTIC_CORP_VIEW"></a></authz:authorize>
					</td>
				    <td>${item.freightRate}</td>
				    <td><hi:select emu="units" name="goods_infos[${s.index}].freightUnits" isLabel="true"/></td>
				    <td><fmt:formatDate value="${item.loadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				    <td>${item.loadPlace}</td>
				    <td>${item.truckNumber}</td>
				    <td><authz:authorize ifAnyGranted="TRUCK_TYPE_VIEW"><a href="<hi:url>truck_typeView.action?truck_type.id=${item.truck_type.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.truck_type.truck_type_name}
					<authz:authorize ifAnyGranted="TRUCK_TYPE_VIEW"></a></authz:authorize>
					</td>
				    <td>${item.truck_length}</td>
				    <td>${item.truck_width}</td>
				    <td>${item.truck_load}</td>
				    <td><hi:select emu="goods_state" name="goods_infos[${s.index}].info_state" isLabel="true"/></td>
				    <td><authz:authorize ifAnyGranted="DRIVER_INFO_VIEW"><a href="<hi:url>driver_infoView.action?driver_info.id=${item.driver.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
				    <hi:select emu="info_state" name="goods_infos[${s.index}].driver.info_state" isLabel="true"/>
					<authz:authorize ifAnyGranted="DRIVER_INFO_VIEW"></a></authz:authorize>
					</td>
				    <td><fmt:formatDate value="${item.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				    <td><fmt:formatDate value="${item.realLoadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				    <td><fmt:formatDate value="${item.unloadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				    <td><authz:authorize ifAnyGranted="GOODS_TYPE_VIEW"><a href="<hi:url>goods_typeView.action?goods_type.id=${item.goodsType.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.goodsType.goodsTypeName}
					<authz:authorize ifAnyGranted="GOODS_TYPE_VIEW"></a></authz:authorize>
					</td>
				    <td><hi:select emu="units" name="goods_infos[${s.index}].goodsUnits" isLabel="true"/></td>
				<td>
				<c:choose>
					<c:when test="${empty lookup}">
				    <authz:authorize ifAnyGranted="GOODS_INFO_DEL">
				      <a class="btnDel" href="<hi:url>goods_infoRemove.action?ajax=1&goods_info.id=${item.id}</hi:url>" target="navTabTodo" title="<hi:text key="删除" parameterLanguageKeys="货物信息"/>"><hi:text key="删除"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="GOODS_INFO_VIEW">
				      <a class="btnView" href="<hi:url>goods_infoView.action?goods_info.id=${item.id}</hi:url>" target="navTab" rel="goods_info" title="<hi:text key="查看" parameterLanguageKeys="货物信息"/>"><hi:text key="查看"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="GOODS_INFO_SAVE">
				      <a class="btnEdit" href="<hi:url>goods_infoEdit.action?goods_info.id=${item.id}</hi:url>" target="navTab" rel="goods_info" title="<hi:text key="编辑" parameterLanguageKeys="货物信息"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
					<c:otherwise>
						<a class="btnSelect" href="javascript:$.bringBack({id:'${item.id}', publishTime:'${item.publishTime}',origin_r_name:'${item.origin.r_name}',dest_r_name:'${item.destination.r_name}',info_state_1:'<hi:select emu="info_state" name="goods_infos[${s.index}].logistic_corp.info_state" isLabel="true"/>',freightRate:'${item.freightRate}',freightUnits:'<hi:select emu="units" name="goods_infos[${s.index}].freightUnits" isLabel="true"/>',loadTime:'${item.loadTime}',loadPlace:'${item.loadPlace}',truckNumber:'${item.truckNumber}',truck_type_name:'${item.truck_type.truck_type_name}',truck_length:'${item.truck_length}',truck_width:'${item.truck_width}',truck_load:'${item.truck_load}',info_state:'<hi:select emu="goods_state" name="goods_infos[${s.index}].info_state" isLabel="true"/>',info_state_2:'<hi:select emu="info_state" name="goods_infos[${s.index}].driver.info_state" isLabel="true"/>',orderTime:'${item.orderTime}',realLoadTime:'${item.realLoadTime}',unloadTime:'${item.unloadTime}',goodsTypeName:'${item.goodsType.goodsTypeName}',goodsUnits:'<hi:select emu="units" name="goods_infos[${s.index}].goodsUnits" isLabel="true"/>'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
