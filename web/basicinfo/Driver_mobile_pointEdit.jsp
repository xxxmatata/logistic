<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="手机最新位置"/></h2>
<form action="driver_mobile_pointSave.action?navTabId=driver_mobile_pointList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="国际移动用户识别码" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.imsi" class="textInput" value="${driver_mobile_point.imsi}" maxlength="20"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="移动用户号码簿号码" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.mdn" class="textInput" value="${driver_mobile_point.mdn}" maxlength="20"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="设备操作系统版本" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.os_version" class="textInput" value="${driver_mobile_point.os_version}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="经度" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.lng" class="textInput float" value="${driver_mobile_point.lng}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="纬度" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.lat" class="textInput float" value="${driver_mobile_point.lat}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="高度" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.alt" class="textInput float" value="${driver_mobile_point.alt}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="速度" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.speed" class="textInput float" value="${driver_mobile_point.speed}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="上报时间" entity="Driver_mobile_point"/>：</dt>
			<dd>
				<input type="text" name="driver_mobile_point.upd_time" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${driver_mobile_point.upd_time}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="定位类型" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.loc_type" class="textInput" value="${driver_mobile_point.loc_type}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="客户端IP地址" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.client_ip" class="textInput" value="${driver_mobile_point.client_ip}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="位置描述" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.loc_desc" class="textInput" value="${driver_mobile_point.loc_desc}" maxlength="200"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="手机串号" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.imei" class="textInput" value="${driver_mobile_point.imei}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="mcc" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.mcc" class="textInput" value="${driver_mobile_point.mcc}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="mnc" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.mnc" class="textInput" value="${driver_mobile_point.mnc}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="lac" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.lac" class="textInput" value="${driver_mobile_point.lac}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="sid" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.sid" class="textInput" value="${driver_mobile_point.sid}" maxlength="8"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="nid" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.nid" class="textInput" value="${driver_mobile_point.nid}" maxlength="8"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="bid" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.bid" class="textInput" value="${driver_mobile_point.bid}" maxlength="8"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="radius" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.radius" class="textInput float" value="${driver_mobile_point.radius}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="error_code" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.error_code" class="textInput" value="${driver_mobile_point.error_code}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="定位时间" entity="Driver_mobile_point"/>：</dt>
			<dd>
				<input type="text" name="driver_mobile_point.loc_time" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${driver_mobile_point.loc_time}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="卫星数量" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.satellite_num" class="textInput integer" value="${driver_mobile_point.satellite_num}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="uuid" entity="Driver_mobile_point"/>：</dt><dd><input type="text" name="driver_mobile_point.uuid" class="textInput" value="${driver_mobile_point.uuid}" maxlength="30"/></dd>
		</dl>
				<input type="hidden" name="driver_mobile_point.id" value="${driver_mobile_point.id}"/>
				<input type="hidden" name="driver_mobile_point.creator.id" value="${driver_mobile_point.creator.id}"/>
				<input type="hidden" name="driver_mobile_point.version" value="${driver_mobile_point.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
