<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="司机长跑线路"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="起点" entity="Driver_line"/>：</dt><dd>${driver_line.origin.r_shortName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="终点" entity="Driver_line"/>：</dt><dd>${driver_line.destination.r_shortName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="报价" entity="Driver_line"/>：</dt><dd>${driver_line.price}</dd>
		</dl>

		<div class="divider"></div>
			</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
