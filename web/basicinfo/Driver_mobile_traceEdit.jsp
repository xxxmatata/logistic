<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="手机轨迹"/></h2>
<form action="driver_mobile_traceSave.action?navTabId=driver_mobile_traceList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="国际移动用户识别码" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.imsi" class="textInput" value="${driver_mobile_trace.imsi}" maxlength="20"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="移动用户号码簿号码" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.mdn" class="textInput" value="${driver_mobile_trace.mdn}" maxlength="20"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="设备操作系统版本" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.os_version" class="textInput" value="${driver_mobile_trace.os_version}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="经度" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.lng" class="textInput float" value="${driver_mobile_trace.lng}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="纬度" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.lat" class="textInput float" value="${driver_mobile_trace.lat}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="高度" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.alt" class="textInput float" value="${driver_mobile_trace.alt}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="速度" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.speed" class="textInput float" value="${driver_mobile_trace.speed}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="上报时间" entity="Driver_mobile_trace"/>：</dt>
			<dd>
				<input type="text" name="driver_mobile_trace.upd_time" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${driver_mobile_trace.upd_time}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="定位类型" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.loc_type" class="textInput" value="${driver_mobile_trace.loc_type}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="客户端IP地址" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.client_ip" class="textInput" value="${driver_mobile_trace.client_ip}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="位置描述" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.loc_desc" class="textInput" value="${driver_mobile_trace.loc_desc}" maxlength="200"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="手机串号" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.imei" class="textInput" value="${driver_mobile_trace.imei}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="mcc" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.mcc" class="textInput" value="${driver_mobile_trace.mcc}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="mnc" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.mnc" class="textInput" value="${driver_mobile_trace.mnc}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="lac" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.lac" class="textInput" value="${driver_mobile_trace.lac}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="sid" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.sid" class="textInput" value="${driver_mobile_trace.sid}" maxlength="8"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="nid" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.nid" class="textInput" value="${driver_mobile_trace.nid}" maxlength="8"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="bid" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.bid" class="textInput" value="${driver_mobile_trace.bid}" maxlength="8"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="radius" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.radius" class="textInput float" value="${driver_mobile_trace.radius}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="error_code" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.error_code" class="textInput" value="${driver_mobile_trace.error_code}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="定位时间" entity="Driver_mobile_trace"/>：</dt>
			<dd>
				<input type="text" name="driver_mobile_trace.loc_time" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${driver_mobile_trace.loc_time}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="卫星数量" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.satellite_num" class="textInput integer" value="${driver_mobile_trace.satellite_num}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="uuid" entity="Driver_mobile_trace"/>：</dt><dd><input type="text" name="driver_mobile_trace.uuid" class="textInput" value="${driver_mobile_trace.uuid}" maxlength="30"/></dd>
		</dl>
				<input type="hidden" name="driver_mobile_trace.id" value="${driver_mobile_trace.id}"/>
				<input type="hidden" name="driver_mobile_trace.version" value="${driver_mobile_trace.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
