<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="司机与货的关系"/></h2>
<form action="driver2goodsSave.action?navTabId=driver2goodsList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="司机名称" entity="Driver2goods"/>：</dt>
			<dd>
				<input type="hidden" name="driver2goods.driver.id" value="${driver2goods.driver.id}"/>
				<input type="text" class="textInput" name="driver2goods.hi_driver.info_state" value="<hi:select emu="info_state" name="driver2goods.driver.info_state" isLabel="true"/>"
					autocomplete="off" lookupGroup="driver2goods" lookupName="driver" suggestClass="org.logistic.basicinfo.model.Driver_info" searchFields="info_state"/>
				<a class="btnLook" href="<hi:url>driver_infoLookup.action?lookup=1</hi:url>" lookupGroup="driver2goods" lookupName="driver"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="当前关系" entity="Driver2goods"/>：</dt><dd><hi:select emu="driver2goodsType" name="driver2goods.d2gType"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="发生时间" entity="Driver2goods"/>：</dt>
			<dd>
				<input type="text" name="driver2goods.opTime" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${driver2goods.opTime}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="评价" entity="Driver2goods"/>：</dt><dd><hi:select emu="taskRank" name="driver2goods.taskRank"/></dd>			
		</dl>
				<input type="hidden" name="driver2goods.id" value="${driver2goods.id}"/>
				<input type="hidden" name="driver2goods.goods_info.id" value="${driver2goods.goods_info.id}"/>
				<input type="hidden" name="driver2goods.creator.id" value="${driver2goods.creator.id}"/>
				<input type="hidden" name="driver2goods.version" value="${driver2goods.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
