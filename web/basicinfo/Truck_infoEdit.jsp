<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="货车信息表"/></h2>
<form action="truck_infoSave.action?navTabId=truck_infoList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="车牌" entity="Truck_info"/>：</dt><dd><input type="text" name="truck_info.truck_card" class="textInput required" value="${truck_info.truck_card}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="货车类型" entity="Truck_info"/>：</dt>
			<dd>
				<input type="hidden" name="truck_info.truck_type.id" value="${truck_info.truck_type.id}"/>
				<input type="text" class="textInput" name="truck_info.hi_truck_type.truck_type_name" value="${truck_info.truck_type.truck_type_name}"
					autocomplete="off" lookupGroup="truck_info" lookupName="truck_type" suggestClass="org.logistic.basicinfo.model.Truck_type" searchFields="truck_type_name"/>
				<a class="btnLook" href="<hi:url>truck_typeLookup.action?lookup=1</hi:url>" lookupGroup="truck_info" lookupName="truck_type"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="车长" entity="Truck_info"/>：</dt><dd><input type="text" name="truck_info.truck_length" class="textInput float" value="${truck_info.truck_length}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="车宽" entity="Truck_info"/>：</dt><dd><input type="text" name="truck_info.truck_width" class="textInput float" value="${truck_info.truck_width}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="载重" entity="Truck_info"/>：</dt><dd><input type="text" name="truck_info.truck_load" class="textInput" value="${truck_info.truck_load}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="保单号" entity="Truck_info"/>：</dt><dd><input type="text" name="truck_info.insure_id" class="textInput" value="${truck_info.insure_id}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="保单附件" entity="Truck_info"/>：</dt>
			<dd>
				<input type="hidden" name="truck_info.insure_file_attachment.id" value="${truck_info.insure_file_attachment.id}"/>
				<input type="text" class="textInput" name="truck_info.hi_insure_file_attachment.fileName" value="${truck_info.insure_file_attachment.fileName}" readonly="readonly"/>
				<a class="btnAttach" href="<hi:url>attachmentLookup.action?lookup=1&from=attachment&saveType=1</hi:url>" lookupGroup="truck_info" lookupName="insure_file_attachment" width="560" height="300" title="<hi:text key="附件"/>"><hi:text key="附件"/></a>
				<c:if test="${not empty truck_info.insure_file_attachment}">
				<a class="btnView" href="attachmentView.action?attachment.id=${truck_info.insure_file_attachment.id}" target="_blank">
					<hi:text key="查看"/>
				</a>
				</c:if>			
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="货车状态" entity="Truck_info"/>：</dt><dd><hi:select emu="truck_state" name="truck_info.truck_state"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Truck_info"/>：</dt><dd><hi:select emu="info_state" name="truck_info.info_state"/></dd>			
		</dl>
				<input type="hidden" name="truck_info.id" value="${truck_info.id}"/>
				<input type="hidden" name="truck_info.insure_file_attachment" value="${truck_info.insure_file_attachment}"/>
				<input type="hidden" name="truck_info.creator.id" value="${truck_info.creator.id}"/>
				<input type="hidden" name="truck_info.deleted" value="${truck_info.deleted}"/>
				<input type="hidden" name="truck_info.version" value="${truck_info.version}"/>

		<div class="divider"></div>
		
		<div class="tabs">
			<div class="tabsHeader">
				<div class="tabsHeaderContent">
					<ul>
						<li><a href="javascript:void(0)"><span><hi:text key="司机信息"/></span></a></li>
					</ul>
				</div>
			</div>
			<div class="tabsContent" style="height:150px;">
				<div>
					<table class="list nowrap" width="100%" itemDetail="truck_info.driver_infos">
						<thead>
							<tr>
								<th type="attach" class="" name="fileName" lookupName="drive_lic_file_attachment" lookupUrl="<hi:url>attachmentLookup.action?lookup=1&from=attachment&saveType=1</hi:url>" size="12"><hi:text key="驾驶证附件" entity="Driver_info"/></th>
								<th type="enum" name="info_state" enumName="info_state" size="12"><hi:text key="信息状态" entity="Driver_info"/></th>
								<th type="lookup" class="" name="r_shortName" lookupName="region_id" lookupUrl="<hi:url>regionGBLookup.action?lookup=1</hi:url>" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName" size="12"><hi:text key="地区简称" entity="Driver_info"/></th>
								<th type="text" class="" name="localtion_desc" size="12" maxlength="300"><hi:text key="位置描述" entity="Driver_info"/></th>
								<th type="text" class=" integer" name="driverRank" size="12"><hi:text key="评价得分" entity="Driver_info"/></th>
								<th type="del" width="60"><hi:text key="操作"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${truck_info.driver_infos}"  varStatus="s">
							<tr>
							<input type="hidden" name="truck_info.driver_infos[${s.index}].id" value="${item.id}"/>
							<input type="hidden" name="truck_info.driver_infos[${s.index}].version" value="${item.version}"/>
								<td>
									<input type="hidden" name="truck_info.driver_infos[${s.index}].drive_lic_file_attachment.id" value="${item.drive_lic_file_attachment.id}"/>
									<input type="text" class="" name="truck_info.driver_infos[${s.index}].hi_drive_lic_file_attachment.fileName" value="${item.drive_lic_file_attachment.fileName}" size="12" readonly="readonly"/>
									<a class="btnAttach" href="<hi:url>attachmentLookup.action?lookup=1&from=attachment&saveType=1</hi:url>" lookupGroup="truck_info.driver_infos" lookupName="drive_lic_file_attachment" index="${s.index}" width="560" height="300" title="<hi:text key="附件"/>"><hi:text key="附件"/></a>
									<c:if test="${not empty item.drive_lic_file_attachment}">
									<a class="btnView" href="attachmentView.action?attachment.id=${item.drive_lic_file_attachment.id}" target="_blank">
										<hi:text key="查看"/>
									</a>
									</c:if>	
								</td>
								<td>
									<hi:select emu="info_state" name="truck_info.driver_infos[${s.index}].info_state" />
								</td>
								<td>
									<input type="hidden" name="truck_info.driver_infos[${s.index}].region_id.id" value="${item.region_id.id}"/>
									<input type="text" class="" name="truck_info.driver_infos[${s.index}].hi_region_id.r_shortName" value="${item.region_id.r_shortName}" size="12" 
										autocomplete="off" lookupGroup="truck_info.driver_infos" lookupName="region_id" index="${s.index}" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName"/>
									<a class="btnLook" href="<hi:url>regionGBLookup.action?lookup=1</hi:url>" lookupGroup="truck_info.driver_infos" lookupName="region_id" index="${s.index}" title="<hi:text key="查找带回"/>"><hi:text key="查找带回"/></a>
								</td>
								<td>
									<input type="text" class="" name="truck_info.driver_infos[${s.index}].localtion_desc" value="${item.localtion_desc}" size="12" maxlength="300"/>
								</td>
								<td>
									<input type="text" class=" integer" name="truck_info.driver_infos[${s.index}].driverRank" value="${item.driverRank}" size="12" alt="<hi:text key="请输入整数"/>"/>
								</td>
								<td><a href="<hi:url>driver_infoRemove.action?ajax=1&driver_info.id=${item.id}</hi:url>" class="btnDel" title="<hi:text key="确定要删除吗?"/>">删除</a></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>								
				
			</div>
			<div class="tabsFooter">
				<div class="tabsFooterContent"></div>
			</div>
		</div>				
	</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
