<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="货物信息"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="发布时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.publishTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="起点" entity="Goods_info"/>：</dt><dd>${goods_info.origin.r_name}</dd>
		</dl>
		<dl>
			<dt><hi:text key="终点" entity="Goods_info"/>：</dt><dd>${goods_info.destination.r_name}</dd>
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Goods_info"/>：</dt><dd><hi:select emu="info_state" name="goods_info.logistic_corp.info_state" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="运价" entity="Goods_info"/>：</dt><dd>${goods_info.freightRate}</dd>
		</dl>
		<dl>
			<dt><hi:text key="运价单位" entity="Goods_info"/>：</dt><dd><hi:select emu="units" name="goods_info.freightUnits" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="装货时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.loadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="装货地点" entity="Goods_info"/>：</dt><dd>${goods_info.loadPlace}</dd>
		</dl>
		<dl>
			<dt><hi:text key="求车数量" entity="Goods_info"/>：</dt><dd>${goods_info.truckNumber}</dd>
		</dl>
		<dl>
			<dt><hi:text key="货车车型" entity="Goods_info"/>：</dt><dd>${goods_info.truck_type.truck_type_name}</dd>
		</dl>
		<dl>
			<dt><hi:text key="车长" entity="Goods_info"/>：</dt><dd>${goods_info.truck_length}</dd>
		</dl>
		<dl>
			<dt><hi:text key="车宽" entity="Goods_info"/>：</dt><dd>${goods_info.truck_width}</dd>
		</dl>
		<dl>
			<dt><hi:text key="载重" entity="Goods_info"/>：</dt><dd>${goods_info.truck_load}</dd>
		</dl>
		<dl>
			<dt><hi:text key="状态" entity="Goods_info"/>：</dt><dd><hi:select emu="goods_state" name="goods_info.info_state" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Goods_info"/>：</dt><dd><hi:select emu="info_state" name="goods_info.driver.info_state" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="成交时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="装货完成时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.realLoadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="卸货时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.unloadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="货物类型名称" entity="Goods_info"/>：</dt><dd>${goods_info.goodsType.goodsTypeName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="备注" entity="Goods_info"/>：</dt><dd>${goods_info.remark}</dd>
		</dl>
		<dl>
			<dt><hi:text key="评价" entity="Goods_info"/>：</dt><dd><hi:select emu="taskRank" name="goods_info.taskRank" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="数量单位" entity="Goods_info"/>：</dt><dd><hi:select emu="units" name="goods_info.goodsUnits" isLabel="true"/></dd>
		</dl>

		<div class="divider"></div>
		
		<div class="tabs">
			<div class="tabsHeader">
				<div class="tabsHeaderContent">
					<ul>
						<li><a href="javascript:void(0)"><span><hi:text key="相关货主"/></span></a></li>
						<li><a href="javascript:void(0)"><span><hi:text key="司机与货的关系"/></span></a></li>
						<li><a href="javascript:void(0)"><span><hi:text key="司机与货的关系历史记录"/></span></a></li>
					</ul>
				</div>
			</div>
			<div class="tabsContent" style="height:120px;">
				<div style="overflow: auto">
					<table class="list" width="100%">
						<thead>
							<tr>
								<th><hi:text key="货主名称" entity="Goods2owner"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${goods_info.goods2owners}">
							<tr>						
								<td>${item.owner.ownerName}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div style="overflow: auto">
					<table class="list" width="100%">
						<thead>
							<tr>
								<th><hi:text key="司机名称" entity="Driver2goods"/></th>
								<th><hi:text key="当前关系" entity="Driver2goods"/></th>
								<th><hi:text key="发生时间" entity="Driver2goods"/></th>
								<th><hi:text key="评价" entity="Driver2goods"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${goods_info.driver2goodss}">
							<tr>						
								<td>${item.driver.info_state}</td>
				        		<td><hi:select emu="driver2goodsType" name="item.d2gType" isLabel="true"/></td>
								<td><fmt:formatDate value="${item.opTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				        		<td><hi:select emu="taskRank" name="item.taskRank" isLabel="true"/></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div style="overflow: auto">
					<table class="list" width="100%">
						<thead>
							<tr>
								<th><hi:text key="司机名称" entity="Driver2goods_log"/></th>
								<th><hi:text key="关系类型" entity="Driver2goods_log"/></th>
								<th><hi:text key="发生时间" entity="Driver2goods_log"/></th>
								<th><hi:text key="评价" entity="Driver2goods_log"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${goods_info.driver2goods_logs}">
							<tr>						
								<td>${item.driver.info_state}</td>
				        		<td><hi:select emu="driver2goodsType" name="item.d2gType" isLabel="true"/></td>
								<td><fmt:formatDate value="${item.opTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				        		<td><hi:select emu="taskRank" name="item.taskRank" isLabel="true"/></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
			</div>
			<div class="tabsFooter">
				<div class="tabsFooterContent"></div>
			</div>
		</div>				
	</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
