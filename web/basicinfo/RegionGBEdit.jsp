<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="国标区域编码"/></h2>
<form action="regionGBSave.action?navTabId=regionGBList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="区域编码" entity="RegionGB"/>：</dt><dd><input type="text" name="regionGB.r_code" class="textInput" value="${regionGB.r_code}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="上级地区编码" entity="RegionGB"/>：</dt><dd><input type="text" name="regionGB.parent_r_code" class="textInput" value="${regionGB.parent_r_code}" maxlength="10"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="地区类型" entity="RegionGB"/>：</dt><dd><hi:select emu="region_type" name="regionGB.r_type"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="地区名称" entity="RegionGB"/>：</dt><dd><input type="text" name="regionGB.r_name" class="textInput" value="${regionGB.r_name}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="地区简称" entity="RegionGB"/>：</dt><dd><input type="text" name="regionGB.r_shortName" class="textInput" value="${regionGB.r_shortName}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="英文名称" entity="RegionGB"/>：</dt><dd><input type="text" name="regionGB.r_EnName" class="textInput" value="${regionGB.r_EnName}" maxlength="100"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="拼音简拼" entity="RegionGB"/>：</dt><dd><input type="text" name="regionGB.r_PY" class="textInput" value="${regionGB.r_PY}" maxlength="5"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="编码路径" entity="RegionGB"/>：</dt><dd><input type="text" name="regionGB.r_code_path" class="textInput" value="${regionGB.r_code_path}" maxlength="100"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="名称路径" entity="RegionGB"/>：</dt><dd><input type="text" name="regionGB.r_name_path" class="textInput" value="${regionGB.r_name_path}" maxlength="300"/></dd>
		</dl>
				<input type="hidden" name="regionGB.id" value="${regionGB.id}"/>
				<input type="hidden" name="regionGB.creator.id" value="${regionGB.creator.id}"/>
				<input type="hidden" name="regionGB.deleted" value="${regionGB.deleted}"/>
				<input type="hidden" name="regionGB.version" value="${regionGB.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
