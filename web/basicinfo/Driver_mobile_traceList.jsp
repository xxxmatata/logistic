<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="driver_mobile_traceList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="driver_mobile_traceList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
			<li>
				<label><hi:text key="移动用户号码簿号码" entity="Driver_mobile_trace"/>:</label>
				<input type="text" name="pageInfo.f_mdn" value="${pageInfo.f_mdn}"/>
			</li>	  
			<li class="dateRange">
				<label><hi:text key="上报时间" entity="Driver_mobile_trace"/>:</label>
				<input type="text" name="pageInfo.f_upd_time" class="date" readonly="readonly" value="<fmt:formatDate value='${pageInfo.f_upd_time}' pattern='yyyy-MM-dd'/>"/>
				<input type="hidden" name="pageInfo.f_upd_time_op" value="&gt;="><span class="limit">-</span>
				<input type="text" name="pageInfo.f_upd_time01" class="date" readonly="readonly" value="<fmt:formatDate value='${pageInfo.f_upd_time01}' pattern='yyyy-MM-dd'/>"/>
				<input type="hidden" name="pageInfo.f_upd_time01_op" value="&lt;=">
			</li>	  
			<li>
				<label><hi:text key="error_code" entity="Driver_mobile_trace"/>:</label>
				<input type="text" name="pageInfo.f_error_code" value="${pageInfo.f_error_code}"/>
			</li>	  
			<li>

			</li>	  
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="DRIVER_MOBILE_TRACE_SAVE"><li><a class="add" href="<hi:url>driver_mobile_traceEdit.action?driver_mobile_trace.id=-1</hi:url>" target="navTab" rel="driver_mobile_trace"><span><hi:text key="新建" parameterLanguageKeys="手机轨迹"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="DRIVER_MOBILE_TRACE_DEL"><li><a class="delete" href="<hi:url>driver_mobile_traceRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', imsi:'',mdn:'',os_version:'',lng:'',lat:'',alt:'',speed:'',upd_time:'',loc_type:'',client_ip:'',loc_desc:'',imei:'',sid:'',nid:'',bid:'',radius:'',error_code:'',loc_time:'',satellite_num:'',uuid:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="imsi" class="${pageInfo.sorterName eq 'imsi' ? pageInfo.sorterDirection : ''}"><hi:text key="国际移动用户识别码" entity="Driver_mobile_trace"/></th>
				<th orderField="mdn" class="${pageInfo.sorterName eq 'mdn' ? pageInfo.sorterDirection : ''}"><hi:text key="移动用户号码簿号码" entity="Driver_mobile_trace"/></th>
				<th orderField="os_version" class="${pageInfo.sorterName eq 'os_version' ? pageInfo.sorterDirection : ''}"><hi:text key="设备操作系统版本" entity="Driver_mobile_trace"/></th>
				<th orderField="lng" class="${pageInfo.sorterName eq 'lng' ? pageInfo.sorterDirection : ''}"><hi:text key="经度" entity="Driver_mobile_trace"/></th>
				<th orderField="lat" class="${pageInfo.sorterName eq 'lat' ? pageInfo.sorterDirection : ''}"><hi:text key="纬度" entity="Driver_mobile_trace"/></th>
				<th orderField="alt" class="${pageInfo.sorterName eq 'alt' ? pageInfo.sorterDirection : ''}"><hi:text key="高度" entity="Driver_mobile_trace"/></th>
				<th orderField="speed" class="${pageInfo.sorterName eq 'speed' ? pageInfo.sorterDirection : ''}"><hi:text key="速度" entity="Driver_mobile_trace"/></th>
				<th orderField="upd_time" class="${pageInfo.sorterName eq 'upd_time' ? pageInfo.sorterDirection : ''}"><hi:text key="上报时间" entity="Driver_mobile_trace"/></th>
				<th orderField="loc_type" class="${pageInfo.sorterName eq 'loc_type' ? pageInfo.sorterDirection : ''}"><hi:text key="定位类型" entity="Driver_mobile_trace"/></th>
				<th orderField="client_ip" class="${pageInfo.sorterName eq 'client_ip' ? pageInfo.sorterDirection : ''}"><hi:text key="客户端IP地址" entity="Driver_mobile_trace"/></th>
				<th orderField="loc_desc" class="${pageInfo.sorterName eq 'loc_desc' ? pageInfo.sorterDirection : ''}"><hi:text key="位置描述" entity="Driver_mobile_trace"/></th>
				<th orderField="imei" class="${pageInfo.sorterName eq 'imei' ? pageInfo.sorterDirection : ''}"><hi:text key="手机串号" entity="Driver_mobile_trace"/></th>
				<th orderField="sid" class="${pageInfo.sorterName eq 'sid' ? pageInfo.sorterDirection : ''}"><hi:text key="sid" entity="Driver_mobile_trace"/></th>
				<th orderField="nid" class="${pageInfo.sorterName eq 'nid' ? pageInfo.sorterDirection : ''}"><hi:text key="nid" entity="Driver_mobile_trace"/></th>
				<th orderField="bid" class="${pageInfo.sorterName eq 'bid' ? pageInfo.sorterDirection : ''}"><hi:text key="bid" entity="Driver_mobile_trace"/></th>
				<th orderField="radius" class="${pageInfo.sorterName eq 'radius' ? pageInfo.sorterDirection : ''}"><hi:text key="radius" entity="Driver_mobile_trace"/></th>
				<th orderField="error_code" class="${pageInfo.sorterName eq 'error_code' ? pageInfo.sorterDirection : ''}"><hi:text key="error_code" entity="Driver_mobile_trace"/></th>
				<th orderField="loc_time" class="${pageInfo.sorterName eq 'loc_time' ? pageInfo.sorterDirection : ''}"><hi:text key="定位时间" entity="Driver_mobile_trace"/></th>
				<th orderField="satellite_num" class="${pageInfo.sorterName eq 'satellite_num' ? pageInfo.sorterDirection : ''}"><hi:text key="卫星数量" entity="Driver_mobile_trace"/></th>
				<th orderField="uuid" class="${pageInfo.sorterName eq 'uuid' ? pageInfo.sorterDirection : ''}"><hi:text key="uuid" entity="Driver_mobile_trace"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${driver_mobile_traces}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td>${item.imsi}</td>
				    <td>${item.mdn}</td>
				    <td>${item.os_version}</td>
				    <td>${item.lng}</td>
				    <td>${item.lat}</td>
				    <td>${item.alt}</td>
				    <td>${item.speed}</td>
				    <td><fmt:formatDate value="${item.upd_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				    <td>${item.loc_type}</td>
				    <td>${item.client_ip}</td>
				    <td>${item.loc_desc}</td>
				    <td>${item.imei}</td>
				    <td>${item.sid}</td>
				    <td>${item.nid}</td>
				    <td>${item.bid}</td>
				    <td>${item.radius}</td>
				    <td>${item.error_code}</td>
				    <td><fmt:formatDate value="${item.loc_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				    <td>${item.satellite_num}</td>
				    <td>${item.uuid}</td>
				<td>
				<c:choose>
					<c:when test="${empty lookup}">
				    <authz:authorize ifAnyGranted="DRIVER_MOBILE_TRACE_DEL">
				      <a class="btnDel" href="<hi:url>driver_mobile_traceRemove.action?ajax=1&driver_mobile_trace.id=${item.id}</hi:url>" target="navTabTodo" title="<hi:text key="删除" parameterLanguageKeys="手机轨迹"/>"><hi:text key="删除"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="DRIVER_MOBILE_TRACE_VIEW">
				      <a class="btnView" href="<hi:url>driver_mobile_traceView.action?driver_mobile_trace.id=${item.id}</hi:url>" target="navTab" rel="driver_mobile_trace" title="<hi:text key="查看" parameterLanguageKeys="手机轨迹"/>"><hi:text key="查看"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="DRIVER_MOBILE_TRACE_SAVE">
				      <a class="btnEdit" href="<hi:url>driver_mobile_traceEdit.action?driver_mobile_trace.id=${item.id}</hi:url>" target="navTab" rel="driver_mobile_trace" title="<hi:text key="编辑" parameterLanguageKeys="手机轨迹"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
					<c:otherwise>
						<a class="btnSelect" href="javascript:$.bringBack({id:'${item.id}', imsi:'${item.imsi}',mdn:'${item.mdn}',os_version:'${item.os_version}',lng:'${item.lng}',lat:'${item.lat}',alt:'${item.alt}',speed:'${item.speed}',upd_time:'${item.upd_time}',loc_type:'${item.loc_type}',client_ip:'${item.client_ip}',loc_desc:'${item.loc_desc}',imei:'${item.imei}',sid:'${item.sid}',nid:'${item.nid}',bid:'${item.bid}',radius:'${item.radius}',error_code:'${item.error_code}',loc_time:'${item.loc_time}',satellite_num:'${item.satellite_num}',uuid:'${item.uuid}'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
