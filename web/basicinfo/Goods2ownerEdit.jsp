<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="相关货主"/></h2>
<form action="goods2ownerSave.action?navTabId=goods2ownerList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="货主名称" entity="Goods2owner"/>：</dt>
			<dd>
				<input type="hidden" name="goods2owner.owner.id" value="${goods2owner.owner.id}"/>
				<input type="text" class="textInput" name="goods2owner.hi_owner.ownerName" value="${goods2owner.owner.ownerName}"
					autocomplete="off" lookupGroup="goods2owner" lookupName="owner" suggestClass="org.logistic.basicinfo.model.Goods_owner" searchFields="ownerName"/>
				<a class="btnLook" href="<hi:url>goods_ownerLookup.action?lookup=1</hi:url>" lookupGroup="goods2owner" lookupName="owner"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
				<input type="hidden" name="goods2owner.id" value="${goods2owner.id}"/>
				<input type="hidden" name="goods2owner.goods_info.id" value="${goods2owner.goods_info.id}"/>
				<input type="hidden" name="goods2owner.creator.id" value="${goods2owner.creator.id}"/>
				<input type="hidden" name="goods2owner.version" value="${goods2owner.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
