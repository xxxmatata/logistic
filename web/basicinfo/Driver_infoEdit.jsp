<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="司机信息"/></h2>
<form action="driver_infoSave.action?navTabId=driver_infoList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="驾驶证附件" entity="Driver_info"/>：</dt>
			<dd>
				<input type="hidden" name="driver_info.drive_lic_file_attachment.id" value="${driver_info.drive_lic_file_attachment.id}"/>
				<input type="text" class="textInput" name="driver_info.hi_drive_lic_file_attachment.fileName" value="${driver_info.drive_lic_file_attachment.fileName}" readonly="readonly"/>
				<a class="btnAttach" href="<hi:url>attachmentLookup.action?lookup=1&from=attachment&saveType=1</hi:url>" lookupGroup="driver_info" lookupName="drive_lic_file_attachment" width="560" height="300" title="<hi:text key="附件"/>"><hi:text key="附件"/></a>
				<c:if test="${not empty driver_info.drive_lic_file_attachment}">
				<a class="btnView" href="attachmentView.action?attachment.id=${driver_info.drive_lic_file_attachment.id}" target="_blank">
					<hi:text key="查看"/>
				</a>
				</c:if>			
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Driver_info"/>：</dt><dd><hi:select emu="info_state" name="driver_info.info_state"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="经度" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.lng" class="textInput float" value="${driver_info.lng}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="纬度" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.lat" class="textInput float" value="${driver_info.lat}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="地区简称" entity="Driver_info"/>：</dt>
			<dd>
				<input type="hidden" name="driver_info.region_id.id" value="${driver_info.region_id.id}"/>
				<input type="text" class="textInput" name="driver_info.hi_region_id.r_shortName" value="${driver_info.region_id.r_shortName}"
					autocomplete="off" lookupGroup="driver_info" lookupName="region_id" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName"/>
				<a class="btnLook" href="<hi:url>regionGBLookup.action?lookup=1</hi:url>" lookupGroup="driver_info" lookupName="region_id"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="位置描述" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.localtion_desc" class="textInput" value="${driver_info.localtion_desc}" maxlength="300"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="评价得分" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.driverRank" class="textInput integer" value="${driver_info.driverRank}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="帐号" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.userName" class="textInput required" value="${driver_info.userName}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="国家" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.country" class="textInput" value="${driver_info.country}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="时区" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.timeZone" class="textInput" value="${driver_info.timeZone}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="帐号可用" entity="Driver_info"/>：</dt><dd><hi:select emu="yesNo" name="driver_info.accountEnabled"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="加锁" entity="Driver_info"/>：</dt><dd><hi:select emu="yesNo" name="driver_info.accountLocked"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="用效期至" entity="Driver_info"/>：</dt>
			<dd>
				<input type="text" name="driver_info.expiredDate" class="textInput date" readonly="readonly"
					value="<fmt:formatDate value='${driver_info.expiredDate}' pattern='yyyy-MM-dd'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="是否过期" entity="Driver_info"/>：</dt><dd><hi:select emu="yesNo" name="driver_info.credentialsExpired"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="姓名" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.fullName" class="textInput required" value="${driver_info.fullName}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="部门" entity="Driver_info"/>：</dt>
			<dd>
				<input type="hidden" name="driver_info.org.id" value="${driver_info.org.id}"/>
				<input type="text" class="textInput" name="driver_info.hi_org.orgName" value="${driver_info.org.orgName}"
					autocomplete="off" lookupGroup="driver_info" lookupName="org" suggestClass="org.hi.base.organization.model.HiOrg" searchFields="orgName"/>
				<a class="btnLook" href="<hi:url>hiOrgLookup.action?lookup=1</hi:url>" lookupGroup="driver_info" lookupName="org"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="性别" entity="Driver_info"/>：</dt><dd><hi:select emu="gender" name="driver_info.gender"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="地址" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.address" class="textInput" value="${driver_info.address}" maxlength="200"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="电话" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.phone" class="textInput" value="${driver_info.phone}" maxlength="50"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="手机" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.mobile" class="textInput" value="${driver_info.mobile}" maxlength="50"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="邮编" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.zip" class="textInput" value="${driver_info.zip}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="身份证" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.SSN" class="textInput" value="${driver_info.SSN}" maxlength="50"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="E-Mail" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.mail" class="textInput" value="${driver_info.mail}" maxlength="100"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="用户类型" entity="Driver_info"/>：</dt><dd><hi:select emu="userType" name="driver_info.userMgrType"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="提醒方式" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.notifyMode" class="textInput" value="${driver_info.notifyMode}" maxlength="200"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="描述" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.description" class="textInput" value="${driver_info.description}" maxlength="500"/></dd>
		</dl>
				<input type="hidden" name="driver_info.id" value="${driver_info.id}"/>
				<input type="hidden" name="driver_info.drive_lic_file_attachment" value="${driver_info.drive_lic_file_attachment}"/>
				<input type="hidden" name="driver_info.truck_info.id" value="${driver_info.truck_info.id}"/>
				<input type="hidden" name="driver_info.password" value="${driver_info.password}"/>
				<input type="hidden" name="driver_info.creator.id" value="${driver_info.creator.id}"/>
				<input type="hidden" name="driver_info.deleted" value="${driver_info.deleted}"/>
				<input type="hidden" name="driver_info.version" value="${driver_info.version}"/>

		<div class="divider"></div>
		
		<div class="tabs">
			<div class="tabsHeader">
				<div class="tabsHeaderContent">
					<ul>
						<li><a href="javascript:void(0)"><span><hi:text key="司机长跑线路"/></span></a></li>
						<li><a href="javascript:void(0)"><span><hi:text key="司机位置发布"/></span></a></li>
					</ul>
				</div>
			</div>
			<div class="tabsContent" style="height:150px;">
				<div>
					<table class="list nowrap" width="100%" itemDetail="driver_info.driver_lines">
						<thead>
							<tr>
								<th type="lookup" class="" name="r_shortName" lookupName="origin" lookupUrl="<hi:url>regionGBLookup.action?lookup=1</hi:url>" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName" size="12"><hi:text key="起点" entity="Driver_line"/></th>
								<th type="lookup" class="" name="r_shortName" lookupName="destination" lookupUrl="<hi:url>regionGBLookup.action?lookup=1</hi:url>" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName" size="12"><hi:text key="终点" entity="Driver_line"/></th>
								<th type="text" class=" float" name="price" size="12"><hi:text key="报价" entity="Driver_line"/></th>
								<th type="del" width="60"><hi:text key="操作"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${driver_info.driver_lines}"  varStatus="s">
							<tr>
							<input type="hidden" name="driver_info.driver_lines[${s.index}].id" value="${item.id}"/>
							<input type="hidden" name="driver_info.driver_lines[${s.index}].version" value="${item.version}"/>
								<td>
									<input type="hidden" name="driver_info.driver_lines[${s.index}].origin.id" value="${item.origin.id}"/>
									<input type="text" class="" name="driver_info.driver_lines[${s.index}].hi_origin.r_shortName" value="${item.origin.r_shortName}" size="12" 
										autocomplete="off" lookupGroup="driver_info.driver_lines" lookupName="origin" index="${s.index}" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName"/>
									<a class="btnLook" href="<hi:url>regionGBLookup.action?lookup=1</hi:url>" lookupGroup="driver_info.driver_lines" lookupName="origin" index="${s.index}" title="<hi:text key="查找带回"/>"><hi:text key="查找带回"/></a>
								</td>
								<td>
									<input type="hidden" name="driver_info.driver_lines[${s.index}].destination.id" value="${item.destination.id}"/>
									<input type="text" class="" name="driver_info.driver_lines[${s.index}].hi_destination.r_shortName" value="${item.destination.r_shortName}" size="12" 
										autocomplete="off" lookupGroup="driver_info.driver_lines" lookupName="destination" index="${s.index}" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName"/>
									<a class="btnLook" href="<hi:url>regionGBLookup.action?lookup=1</hi:url>" lookupGroup="driver_info.driver_lines" lookupName="destination" index="${s.index}" title="<hi:text key="查找带回"/>"><hi:text key="查找带回"/></a>
								</td>
								<td>
									<input type="text" class=" float" name="driver_info.driver_lines[${s.index}].price" value="${item.price}" size="12" alt="<hi:text key="请输浮点数"/>"/>
								</td>
								<td><a href="<hi:url>driver_lineRemove.action?ajax=1&driver_line.id=${item.id}</hi:url>" class="btnDel" title="<hi:text key="确定要删除吗?"/>">删除</a></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>								
				<div>
					<table class="list nowrap" width="100%" itemDetail="driver_info.driver_points">
						<thead>
							<tr>
								<th type="datetime" class=" date" name="trip_date" size="12"><hi:text key="时间" entity="Driver_point"/></th>
								<th type="lookup" class="" name="r_shortName" lookupName="trip_region" lookupUrl="<hi:url>regionGBLookup.action?lookup=1</hi:url>" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName" size="12"><hi:text key="地区简称" entity="Driver_point"/></th>
								<th type="datetime" class=" date" name="publish_time" size="12"><hi:text key="发布时间" entity="Driver_point"/></th>
								<th type="del" width="60"><hi:text key="操作"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${driver_info.driver_points}"  varStatus="s">
							<tr>
							<input type="hidden" name="driver_info.driver_points[${s.index}].id" value="${item.id}"/>
							<input type="hidden" name="driver_info.driver_points[${s.index}].version" value="${item.version}"/>
								<td>
									<input type="text" class="date" name="driver_info.driver_points[${s.index}].trip_date" class="date" pattern='yyyy-MM-dd HH:mm:ss'
										value="<fmt:formatDate value='${item.trip_date}' pattern='yyyy-MM-dd HH:mm:ss'/>" size="12"  readonly="readonly"/>
									<a class="inputDateButton" href="javascript:void(0)"><hi:text key="选择"/></a>
								</td>
								<td>
									<input type="hidden" name="driver_info.driver_points[${s.index}].trip_region.id" value="${item.trip_region.id}"/>
									<input type="text" class="" name="driver_info.driver_points[${s.index}].hi_trip_region.r_shortName" value="${item.trip_region.r_shortName}" size="12" 
										autocomplete="off" lookupGroup="driver_info.driver_points" lookupName="trip_region" index="${s.index}" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName"/>
									<a class="btnLook" href="<hi:url>regionGBLookup.action?lookup=1</hi:url>" lookupGroup="driver_info.driver_points" lookupName="trip_region" index="${s.index}" title="<hi:text key="查找带回"/>"><hi:text key="查找带回"/></a>
								</td>
								<td>
									<input type="text" class="date" name="driver_info.driver_points[${s.index}].publish_time" class="date" pattern='yyyy-MM-dd HH:mm:ss'
										value="<fmt:formatDate value='${item.publish_time}' pattern='yyyy-MM-dd HH:mm:ss'/>" size="12"  readonly="readonly"/>
									<a class="inputDateButton" href="javascript:void(0)"><hi:text key="选择"/></a>
								</td>
								<td><a href="<hi:url>driver_pointRemove.action?ajax=1&driver_point.id=${item.id}</hi:url>" class="btnDel" title="<hi:text key="确定要删除吗?"/>">删除</a></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>								
				
			</div>
			<div class="tabsFooter">
				<div class="tabsFooterContent"></div>
			</div>
		</div>				
	</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
