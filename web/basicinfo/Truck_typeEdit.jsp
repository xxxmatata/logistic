<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="货车类型"/></h2>
<form action="truck_typeSave.action?navTabId=truck_typeList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="货车车型" entity="Truck_type"/>：</dt><dd><input type="text" name="truck_type.truck_type_name" class="textInput required" value="${truck_type.truck_type_name}" maxlength="30"/></dd>
		</dl>
				<input type="hidden" name="truck_type.id" value="${truck_type.id}"/>
				<input type="hidden" name="truck_type.creator.id" value="${truck_type.creator.id}"/>
				<input type="hidden" name="truck_type.deleted" value="${truck_type.deleted}"/>
				<input type="hidden" name="truck_type.version" value="${truck_type.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
