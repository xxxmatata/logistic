<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="司机位置发布"/></h2>
<form action="driver_pointSave.action?navTabId=driver_pointList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="时间" entity="Driver_point"/>：</dt>
			<dd>
				<input type="text" name="driver_point.trip_date" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${driver_point.trip_date}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="地区简称" entity="Driver_point"/>：</dt>
			<dd>
				<input type="hidden" name="driver_point.trip_region.id" value="${driver_point.trip_region.id}"/>
				<input type="text" class="textInput" name="driver_point.hi_trip_region.r_shortName" value="${driver_point.trip_region.r_shortName}"
					autocomplete="off" lookupGroup="driver_point" lookupName="trip_region" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName"/>
				<a class="btnLook" href="<hi:url>regionGBLookup.action?lookup=1</hi:url>" lookupGroup="driver_point" lookupName="trip_region"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="发布时间" entity="Driver_point"/>：</dt>
			<dd>
				<input type="text" name="driver_point.publish_time" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${driver_point.publish_time}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
				<input type="hidden" name="driver_point.id" value="${driver_point.id}"/>
				<input type="hidden" name="driver_point.driver_info.id" value="${driver_point.driver_info.id}"/>
				<input type="hidden" name="driver_point.creator.id" value="${driver_point.creator.id}"/>
				<input type="hidden" name="driver_point.deleted" value="${driver_point.deleted}"/>
				<input type="hidden" name="driver_point.version" value="${driver_point.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
