<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="司机长跑线路"/></h2>
<form action="driver_lineSave.action?navTabId=driver_lineList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="起点" entity="Driver_line"/>：</dt>
			<dd>
				<input type="hidden" name="driver_line.origin.id" value="${driver_line.origin.id}"/>
				<input type="text" class="textInput" name="driver_line.hi_origin.r_shortName" value="${driver_line.origin.r_shortName}"
					autocomplete="off" lookupGroup="driver_line" lookupName="origin" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName"/>
				<a class="btnLook" href="<hi:url>regionGBLookup.action?lookup=1</hi:url>" lookupGroup="driver_line" lookupName="origin"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="终点" entity="Driver_line"/>：</dt>
			<dd>
				<input type="hidden" name="driver_line.destination.id" value="${driver_line.destination.id}"/>
				<input type="text" class="textInput" name="driver_line.hi_destination.r_shortName" value="${driver_line.destination.r_shortName}"
					autocomplete="off" lookupGroup="driver_line" lookupName="destination" suggestClass="org.logistic.basicinfo.model.RegionGB" searchFields="r_shortName"/>
				<a class="btnLook" href="<hi:url>regionGBLookup.action?lookup=1</hi:url>" lookupGroup="driver_line" lookupName="destination"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="报价" entity="Driver_line"/>：</dt><dd><input type="text" name="driver_line.price" class="textInput float" value="${driver_line.price}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>
				<input type="hidden" name="driver_line.id" value="${driver_line.id}"/>
				<input type="hidden" name="driver_line.driver_info.id" value="${driver_line.driver_info.id}"/>
				<input type="hidden" name="driver_line.creator.id" value="${driver_line.creator.id}"/>
				<input type="hidden" name="driver_line.deleted" value="${driver_line.deleted}"/>
				<input type="hidden" name="driver_line.version" value="${driver_line.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
