<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="truck_infoList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="truck_infoList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
			<li>
				<label><hi:text key="车牌" entity="Truck_info"/>:</label>
				<input type="text" name="pageInfo.f_truck_card" value="${pageInfo.f_truck_card}"/>
			</li>	  
			<li>
				<label><hi:text key="货车类型" entity="Truck_info"/>:</label>
				<input type="text" name="pageInfo.truck_type.f_truck_type_name" value="${pageInfo.truck_type.f_truck_type_name}"/>
			</li>	  
			<li>
				<label><hi:text key="车长" entity="Truck_info"/>:</label>
				<input type="text" name="pageInfo.f_truck_length" value="${pageInfo.f_truck_length}"/>
			</li>	  
			<li>
				<label><hi:text key="货车状态" entity="Truck_info"/>:</label>
				<hi:search name="pageInfo.f_truck_state" emu="truck_state"/>
			</li>	  
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="TRUCK_INFO_SAVE"><li><a class="add" href="<hi:url>truck_infoEdit.action?truck_info.id=-1</hi:url>" target="navTab" rel="truck_info"><span><hi:text key="新建" parameterLanguageKeys="货车信息表"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="TRUCK_INFO_DEL"><li><a class="delete" href="<hi:url>truck_infoRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', truck_card:'',truck_type_name:'',truck_length:'',truck_width:'',truck_load:'',insure_id:'',insure_file:'',truck_state:'',info_state:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="truck_card" class="${pageInfo.sorterName eq 'truck_card' ? pageInfo.sorterDirection : ''}"><hi:text key="车牌" entity="Truck_info"/></th>
				<th orderField="truck_type.truck_type_name" class="${pageInfo.sorterName eq 'truck_type.truck_type_name' ? pageInfo.sorterDirection : ''}"><hi:text key="货车类型" entity="Truck_info"/></th>
				<th orderField="truck_length" class="${pageInfo.sorterName eq 'truck_length' ? pageInfo.sorterDirection : ''}"><hi:text key="车长" entity="Truck_info"/></th>
				<th orderField="truck_width" class="${pageInfo.sorterName eq 'truck_width' ? pageInfo.sorterDirection : ''}"><hi:text key="车宽" entity="Truck_info"/></th>
				<th orderField="truck_load" class="${pageInfo.sorterName eq 'truck_load' ? pageInfo.sorterDirection : ''}"><hi:text key="载重" entity="Truck_info"/></th>
				<th orderField="insure_id" class="${pageInfo.sorterName eq 'insure_id' ? pageInfo.sorterDirection : ''}"><hi:text key="保单号" entity="Truck_info"/></th>
				<th orderField="insure_file_attachment.fileName" class="${pageInfo.sorterName eq 'insure_file_attachment.fileName' ? pageInfo.sorterDirection : ''}"><hi:text key="保单附件" entity="Truck_info"/></th>
				<th orderField="truck_state" class="${pageInfo.sorterName eq 'truck_state' ? pageInfo.sorterDirection : ''}"><hi:text key="货车状态" entity="Truck_info"/></th>
				<th orderField="info_state" class="${pageInfo.sorterName eq 'info_state' ? pageInfo.sorterDirection : ''}"><hi:text key="信息状态" entity="Truck_info"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${truck_infos}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td>${item.truck_card}</td>
				    <td><authz:authorize ifAnyGranted="TRUCK_TYPE_VIEW"><a href="<hi:url>truck_typeView.action?truck_type.id=${item.truck_type.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.truck_type.truck_type_name}
					<authz:authorize ifAnyGranted="TRUCK_TYPE_VIEW"></a></authz:authorize>
					</td>
				    <td>${item.truck_length}</td>
				    <td>${item.truck_width}</td>
				    <td>${item.truck_load}</td>
				    <td>${item.insure_id}</td>
				    <td>
						<c:if test="${not empty item.insure_file_attachment}">
				   		<a href="<hi:url>attachmentView.action?attachment.id=${item.insure_file_attachment.id}</hi:url>" target="_blank">${item.insure_file_attachment.fileNameImage}</a>
				     	</c:if>
				   </td>		    
				    <td><hi:select emu="truck_state" name="truck_infos[${s.index}].truck_state" isLabel="true"/></td>
				    <td><hi:select emu="info_state" name="truck_infos[${s.index}].info_state" isLabel="true"/></td>
				<td>
				<c:choose>
					<c:when test="${empty lookup}">
				    <authz:authorize ifAnyGranted="TRUCK_INFO_DEL">
				      <a class="btnDel" href="<hi:url>truck_infoRemove.action?ajax=1&truck_info.id=${item.id}</hi:url>" target="navTabTodo" title="<hi:text key="删除" parameterLanguageKeys="货车信息表"/>"><hi:text key="删除"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="TRUCK_INFO_VIEW">
				      <a class="btnView" href="<hi:url>truck_infoView.action?truck_info.id=${item.id}</hi:url>" target="navTab" rel="truck_info" title="<hi:text key="查看" parameterLanguageKeys="货车信息表"/>"><hi:text key="查看"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="TRUCK_INFO_SAVE">
				      <a class="btnEdit" href="<hi:url>truck_infoEdit.action?truck_info.id=${item.id}</hi:url>" target="navTab" rel="truck_info" title="<hi:text key="编辑" parameterLanguageKeys="货车信息表"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
					<c:otherwise>
						<a class="btnSelect" href="javascript:$.bringBack({id:'${item.id}', truck_card:'${item.truck_card}',truck_type_name:'${item.truck_type.truck_type_name}',truck_length:'${item.truck_length}',truck_width:'${item.truck_width}',truck_load:'${item.truck_load}',insure_id:'${item.insure_id}',insure_file:'${item.insure_file_attachment.fileName}',truck_state:'<hi:select emu="truck_state" name="truck_infos[${s.index}].truck_state" isLabel="true"/>',info_state:'<hi:select emu="info_state" name="truck_infos[${s.index}].info_state" isLabel="true"/>'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
