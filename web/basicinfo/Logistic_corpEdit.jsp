<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="物流公司"/></h2>
<form action="logistic_corpSave.action?navTabId=logistic_corpList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="公司地址" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.address" class="textInput" value="${logistic_corp.address}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="营业执照编号" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.op_lic" class="textInput" value="${logistic_corp.op_lic}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="营业执照附件" entity="Logistic_corp"/>：</dt>
			<dd>
				<input type="hidden" name="logistic_corp.op_lic_file_attachment.id" value="${logistic_corp.op_lic_file_attachment.id}"/>
				<input type="text" class="textInput" name="logistic_corp.hi_op_lic_file_attachment.fileName" value="${logistic_corp.op_lic_file_attachment.fileName}" readonly="readonly"/>
				<a class="btnAttach" href="<hi:url>attachmentLookup.action?lookup=1&from=attachment&saveType=1</hi:url>" lookupGroup="logistic_corp" lookupName="op_lic_file_attachment" width="560" height="300" title="<hi:text key="附件"/>"><hi:text key="附件"/></a>
				<c:if test="${not empty logistic_corp.op_lic_file_attachment}">
				<a class="btnView" href="attachmentView.action?attachment.id=${logistic_corp.op_lic_file_attachment.id}" target="_blank">
					<hi:text key="查看"/>
				</a>
				</c:if>			
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="税务登记号" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.tax_lic" class="textInput" value="${logistic_corp.tax_lic}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="税务登记证附件" entity="Logistic_corp"/>：</dt>
			<dd>
				<input type="hidden" name="logistic_corp.tax_lic_file_attachment.id" value="${logistic_corp.tax_lic_file_attachment.id}"/>
				<input type="text" class="textInput" name="logistic_corp.hi_tax_lic_file_attachment.fileName" value="${logistic_corp.tax_lic_file_attachment.fileName}" readonly="readonly"/>
				<a class="btnAttach" href="<hi:url>attachmentLookup.action?lookup=1&from=attachment&saveType=1</hi:url>" lookupGroup="logistic_corp" lookupName="tax_lic_file_attachment" width="560" height="300" title="<hi:text key="附件"/>"><hi:text key="附件"/></a>
				<c:if test="${not empty logistic_corp.tax_lic_file_attachment}">
				<a class="btnView" href="attachmentView.action?attachment.id=${logistic_corp.tax_lic_file_attachment.id}" target="_blank">
					<hi:text key="查看"/>
				</a>
				</c:if>			
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="法人" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.corp_name" class="textInput" value="${logistic_corp.corp_name}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="公司电话" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.corp_phone" class="textInput phone" value="${logistic_corp.corp_phone}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="手机" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.corp_mobile" class="textInput mobile" value="${logistic_corp.corp_mobile}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Logistic_corp"/>：</dt><dd><hi:select emu="info_state" name="logistic_corp.info_state"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="部门名称" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.orgName" class="textInput required" value="${logistic_corp.orgName}" maxlength="50"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="部门编号" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.orgNum" class="textInput" value="${logistic_corp.orgNum}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="部门经理" entity="Logistic_corp"/>：</dt>
			<dd>
				<input type="hidden" name="logistic_corp.manager.id" value="${logistic_corp.manager.id}"/>
				<input type="text" class="textInput" name="logistic_corp.hi_manager.fullName" value="${logistic_corp.manager.fullName}"
					autocomplete="off" lookupGroup="logistic_corp" lookupName="manager" suggestClass="org.hi.base.organization.model.HiUser" searchFields="fullName"/>
				<a class="btnLook" href="<hi:url>hiUserLookup.action?lookup=1</hi:url>" lookupGroup="logistic_corp" lookupName="manager"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="上级部门" entity="Logistic_corp"/>：</dt>
			<dd>
				<input type="hidden" name="logistic_corp.parentOrg.id" value="${logistic_corp.parentOrg.id}"/>
				<input type="text" class="textInput" name="logistic_corp.hi_parentOrg.orgName" value="${logistic_corp.parentOrg.orgName}"
					autocomplete="off" lookupGroup="logistic_corp" lookupName="parentOrg" suggestClass="org.hi.base.organization.model.HiOrg" searchFields="orgName"/>
				<a class="btnLook" href="<hi:url>hiOrgLookup.action?lookup=1</hi:url>" lookupGroup="logistic_corp" lookupName="parentOrg"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="描述" entity="Logistic_corp"/>：</dt><dd><input type="text" name="logistic_corp.description" class="textInput" value="${logistic_corp.description}" maxlength="500"/></dd>
		</dl>
				<input type="hidden" name="logistic_corp.id" value="${logistic_corp.id}"/>
				<input type="hidden" name="logistic_corp.op_lic_file_attachment" value="${logistic_corp.op_lic_file_attachment}"/>
				<input type="hidden" name="logistic_corp.tax_lic_file_attachment" value="${logistic_corp.tax_lic_file_attachment}"/>
				<input type="hidden" name="logistic_corp.creator.id" value="${logistic_corp.creator.id}"/>
				<input type="hidden" name="logistic_corp.deleted" value="${logistic_corp.deleted}"/>
				<input type="hidden" name="logistic_corp.version" value="${logistic_corp.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
