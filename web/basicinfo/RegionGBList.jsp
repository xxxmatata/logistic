<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="regionGBList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="regionGBList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="REGIONGB_SAVE"><li><a class="add" href="<hi:url>regionGBEdit.action?regionGB.id=-1</hi:url>" target="navTab" rel="regionGB"><span><hi:text key="新建" parameterLanguageKeys="国标区域编码"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="REGIONGB_DEL"><li><a class="delete" href="<hi:url>regionGBRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', r_code:'',parent_r_code:'',r_type:'',r_name:'',r_shortName:'',r_EnName:'',r_PY:'',r_code_path:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="r_code" class="${pageInfo.sorterName eq 'r_code' ? pageInfo.sorterDirection : ''}"><hi:text key="区域编码" entity="RegionGB"/></th>
				<th orderField="parent_r_code" class="${pageInfo.sorterName eq 'parent_r_code' ? pageInfo.sorterDirection : ''}"><hi:text key="上级地区编码" entity="RegionGB"/></th>
				<th orderField="r_type" class="${pageInfo.sorterName eq 'r_type' ? pageInfo.sorterDirection : ''}"><hi:text key="地区类型" entity="RegionGB"/></th>
				<th orderField="r_name" class="${pageInfo.sorterName eq 'r_name' ? pageInfo.sorterDirection : ''}"><hi:text key="地区名称" entity="RegionGB"/></th>
				<th orderField="r_shortName" class="${pageInfo.sorterName eq 'r_shortName' ? pageInfo.sorterDirection : ''}"><hi:text key="地区简称" entity="RegionGB"/></th>
				<th orderField="r_EnName" class="${pageInfo.sorterName eq 'r_EnName' ? pageInfo.sorterDirection : ''}"><hi:text key="英文名称" entity="RegionGB"/></th>
				<th orderField="r_PY" class="${pageInfo.sorterName eq 'r_PY' ? pageInfo.sorterDirection : ''}"><hi:text key="拼音简拼" entity="RegionGB"/></th>
				<th orderField="r_code_path" class="${pageInfo.sorterName eq 'r_code_path' ? pageInfo.sorterDirection : ''}"><hi:text key="编码路径" entity="RegionGB"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${regionGBs}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td>${item.r_code}</td>
				    <td>${item.parent_r_code}</td>
				    <td><hi:select emu="region_type" name="regionGBs[${s.index}].r_type" isLabel="true"/></td>
				    <td>${item.r_name}</td>
				    <td>${item.r_shortName}</td>
				    <td>${item.r_EnName}</td>
				    <td>${item.r_PY}</td>
				    <td>${item.r_code_path}</td>
				<td>
				<c:choose>
					<c:when test="${empty lookup}">
				    <authz:authorize ifAnyGranted="REGIONGB_DEL">
				      <a class="btnDel" href="<hi:url>regionGBRemove.action?ajax=1&regionGB.id=${item.id}</hi:url>" target="navTabTodo" title="<hi:text key="删除" parameterLanguageKeys="国标区域编码"/>"><hi:text key="删除"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="REGIONGB_VIEW">
				      <a class="btnView" href="<hi:url>regionGBView.action?regionGB.id=${item.id}</hi:url>" target="navTab" rel="regionGB" title="<hi:text key="查看" parameterLanguageKeys="国标区域编码"/>"><hi:text key="查看"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="REGIONGB_SAVE">
				      <a class="btnEdit" href="<hi:url>regionGBEdit.action?regionGB.id=${item.id}</hi:url>" target="navTab" rel="regionGB" title="<hi:text key="编辑" parameterLanguageKeys="国标区域编码"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
					<c:otherwise>
						<a class="btnSelect" href="javascript:$.bringBack({id:'${item.id}', r_code:'${item.r_code}',parent_r_code:'${item.parent_r_code}',r_type:'<hi:select emu="region_type" name="regionGBs[${s.index}].r_type" isLabel="true"/>',r_name:'${item.r_name}',r_shortName:'${item.r_shortName}',r_EnName:'${item.r_EnName}',r_PY:'${item.r_PY}',r_code_path:'${item.r_code_path}'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
