<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="手机最新位置"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="国际移动用户识别码" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.imsi}</dd>
		</dl>
		<dl>
			<dt><hi:text key="移动用户号码簿号码" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.mdn}</dd>
		</dl>
		<dl>
			<dt><hi:text key="设备操作系统版本" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.os_version}</dd>
		</dl>
		<dl>
			<dt><hi:text key="经度" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.lng}</dd>
		</dl>
		<dl>
			<dt><hi:text key="纬度" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.lat}</dd>
		</dl>
		<dl>
			<dt><hi:text key="高度" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.alt}</dd>
		</dl>
		<dl>
			<dt><hi:text key="速度" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.speed}</dd>
		</dl>
		<dl>
			<dt><hi:text key="上报时间" entity="Driver_mobile_point"/>：</dt><dd><fmt:formatDate value="${driver_mobile_point.upd_time}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="定位类型" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.loc_type}</dd>
		</dl>
		<dl>
			<dt><hi:text key="客户端IP地址" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.client_ip}</dd>
		</dl>
		<dl>
			<dt><hi:text key="位置描述" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.loc_desc}</dd>
		</dl>
		<dl>
			<dt><hi:text key="手机串号" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.imei}</dd>
		</dl>
		<dl>
			<dt><hi:text key="mcc" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.mcc}</dd>
		</dl>
		<dl>
			<dt><hi:text key="mnc" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.mnc}</dd>
		</dl>
		<dl>
			<dt><hi:text key="lac" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.lac}</dd>
		</dl>
		<dl>
			<dt><hi:text key="sid" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.sid}</dd>
		</dl>
		<dl>
			<dt><hi:text key="nid" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.nid}</dd>
		</dl>
		<dl>
			<dt><hi:text key="bid" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.bid}</dd>
		</dl>
		<dl>
			<dt><hi:text key="radius" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.radius}</dd>
		</dl>
		<dl>
			<dt><hi:text key="error_code" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.error_code}</dd>
		</dl>
		<dl>
			<dt><hi:text key="定位时间" entity="Driver_mobile_point"/>：</dt><dd><fmt:formatDate value="${driver_mobile_point.loc_time}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="卫星数量" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.satellite_num}</dd>
		</dl>
		<dl>
			<dt><hi:text key="uuid" entity="Driver_mobile_point"/>：</dt><dd>${driver_mobile_point.uuid}</dd>
		</dl>

		<div class="divider"></div>
			</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
