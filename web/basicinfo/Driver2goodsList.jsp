<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="driver2goodsList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="driver2goodsList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="DRIVER2GOODS_SAVE"><li><a class="add" href="<hi:url>driver2goodsEdit.action?driver2goods.id=-1</hi:url>" target="navTab" rel="driver2goods"><span><hi:text key="新建" parameterLanguageKeys="司机与货的关系"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="DRIVER2GOODS_DEL"><li><a class="delete" href="<hi:url>driver2goodsRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', info_state:'',d2gType:'',opTime:'',taskRank:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="driver.info_state" class="${pageInfo.sorterName eq 'driver.info_state' ? pageInfo.sorterDirection : ''}"><hi:text key="司机名称" entity="Driver2goods"/></th>
				<th orderField="d2gType" class="${pageInfo.sorterName eq 'd2gType' ? pageInfo.sorterDirection : ''}"><hi:text key="当前关系" entity="Driver2goods"/></th>
				<th orderField="opTime" class="${pageInfo.sorterName eq 'opTime' ? pageInfo.sorterDirection : ''}"><hi:text key="发生时间" entity="Driver2goods"/></th>
				<th orderField="taskRank" class="${pageInfo.sorterName eq 'taskRank' ? pageInfo.sorterDirection : ''}"><hi:text key="评价" entity="Driver2goods"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${driver2goodss}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td><authz:authorize ifAnyGranted="DRIVER_INFO_VIEW"><a href="<hi:url>driver_infoView.action?driver_info.id=${item.driver.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
				    <hi:select emu="info_state" name="driver2goodss[${s.index}].driver.info_state" isLabel="true"/>
					<authz:authorize ifAnyGranted="DRIVER_INFO_VIEW"></a></authz:authorize>
					</td>
				    <td><hi:select emu="driver2goodsType" name="driver2goodss[${s.index}].d2gType" isLabel="true"/></td>
				    <td><fmt:formatDate value="${item.opTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
				    <td><hi:select emu="taskRank" name="driver2goodss[${s.index}].taskRank" isLabel="true"/></td>
				<td>
				<c:choose>
					<c:when test="${empty lookup}">
				    <authz:authorize ifAnyGranted="DRIVER2GOODS_DEL">
				      <a class="btnDel" href="<hi:url>driver2goodsRemove.action?ajax=1&driver2goods.id=${item.id}</hi:url>" target="navTabTodo" title="<hi:text key="删除" parameterLanguageKeys="司机与货的关系"/>"><hi:text key="删除"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="DRIVER2GOODS_VIEW">
				      <a class="btnView" href="<hi:url>driver2goodsView.action?driver2goods.id=${item.id}</hi:url>" target="navTab" rel="driver2goods" title="<hi:text key="查看" parameterLanguageKeys="司机与货的关系"/>"><hi:text key="查看"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="DRIVER2GOODS_SAVE">
				      <a class="btnEdit" href="<hi:url>driver2goodsEdit.action?driver2goods.id=${item.id}</hi:url>" target="navTab" rel="driver2goods" title="<hi:text key="编辑" parameterLanguageKeys="司机与货的关系"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
					<c:otherwise>
						<a class="btnSelect" href="javascript:$.bringBack({id:'${item.id}', info_state:'<hi:select emu="info_state" name="driver2goodss[${s.index}].driver.info_state" isLabel="true"/>',d2gType:'<hi:select emu="driver2goodsType" name="driver2goodss[${s.index}].d2gType" isLabel="true"/>',opTime:'${item.opTime}',taskRank:'<hi:select emu="taskRank" name="driver2goodss[${s.index}].taskRank" isLabel="true"/>'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
