<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="国标区域编码"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="区域编码" entity="RegionGB"/>：</dt><dd>${regionGB.r_code}</dd>
		</dl>
		<dl>
			<dt><hi:text key="上级地区编码" entity="RegionGB"/>：</dt><dd>${regionGB.parent_r_code}</dd>
		</dl>
		<dl>
			<dt><hi:text key="地区类型" entity="RegionGB"/>：</dt><dd><hi:select emu="region_type" name="regionGB.r_type" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="地区名称" entity="RegionGB"/>：</dt><dd>${regionGB.r_name}</dd>
		</dl>
		<dl>
			<dt><hi:text key="地区简称" entity="RegionGB"/>：</dt><dd>${regionGB.r_shortName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="英文名称" entity="RegionGB"/>：</dt><dd>${regionGB.r_EnName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="拼音简拼" entity="RegionGB"/>：</dt><dd>${regionGB.r_PY}</dd>
		</dl>
		<dl>
			<dt><hi:text key="编码路径" entity="RegionGB"/>：</dt><dd>${regionGB.r_code_path}</dd>
		</dl>
		<dl>
			<dt><hi:text key="名称路径" entity="RegionGB"/>：</dt><dd>${regionGB.r_name_path}</dd>
		</dl>

		<div class="divider"></div>
			</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
