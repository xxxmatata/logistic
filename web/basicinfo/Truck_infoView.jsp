<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="货车信息表"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="车牌" entity="Truck_info"/>：</dt><dd>${truck_info.truck_card}</dd>
		</dl>
		<dl>
			<dt><hi:text key="货车类型" entity="Truck_info"/>：</dt><dd>${truck_info.truck_type.truck_type_name}</dd>
		</dl>
		<dl>
			<dt><hi:text key="车长" entity="Truck_info"/>：</dt><dd>${truck_info.truck_length}</dd>
		</dl>
		<dl>
			<dt><hi:text key="车宽" entity="Truck_info"/>：</dt><dd>${truck_info.truck_width}</dd>
		</dl>
		<dl>
			<dt><hi:text key="载重" entity="Truck_info"/>：</dt><dd>${truck_info.truck_load}</dd>
		</dl>
		<dl>
			<dt><hi:text key="保单号" entity="Truck_info"/>：</dt><dd>${truck_info.insure_id}</dd>
		</dl>
		<dl>
			<dt><hi:text key="保单附件" entity="Truck_info"/>：</dt>
			<dd>
				<c:if test="${not empty truck_info.insure_file_attachment}">
				<a href="<hi:url>attachmentView.action?attachment.id=${truck_info.insure_file_attachment.id}</hi:url>" target="_blank">
					${truck_info.insure_file_attachment.fileNameImage}
				</a>
				</c:if>
			</dd>				  
		</dl>
		<dl>
			<dt><hi:text key="货车状态" entity="Truck_info"/>：</dt><dd><hi:select emu="truck_state" name="truck_info.truck_state" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Truck_info"/>：</dt><dd><hi:select emu="info_state" name="truck_info.info_state" isLabel="true"/></dd>
		</dl>

		<div class="divider"></div>
		
		<div class="tabs">
			<div class="tabsHeader">
				<div class="tabsHeaderContent">
					<ul>
						<li><a href="javascript:void(0)"><span><hi:text key="司机信息"/></span></a></li>
					</ul>
				</div>
			</div>
			<div class="tabsContent" style="height:120px;">
				<div style="overflow: auto">
					<table class="list" width="100%">
						<thead>
							<tr>
								<th><hi:text key="驾驶证附件" entity="Driver_info"/></th>
								<th><hi:text key="信息状态" entity="Driver_info"/></th>
								<th><hi:text key="地区简称" entity="Driver_info"/></th>
								<th><hi:text key="位置描述" entity="Driver_info"/></th>
								<th><hi:text key="评价得分" entity="Driver_info"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${truck_info.driver_infos}">
							<tr>						
								<td>
									<a href="<hi:url>attachmentView.action?attachment.id=${item.drive_lic_file_attachment.id}</hi:url>" target="blank"  >
										${item.drive_lic_file_attachment.fileNameImage}
									</a>
								</td>
				        		<td><hi:select emu="info_state" name="item.info_state" isLabel="true"/></td>
								<td>${item.region_id.r_shortName}</td>
								<td>${item.localtion_desc}</td>
								<td>${item.driverRank}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
			</div>
			<div class="tabsFooter">
				<div class="tabsFooterContent"></div>
			</div>
		</div>				
	</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
