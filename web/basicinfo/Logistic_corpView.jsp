<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="物流公司"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="公司地址" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.address}</dd>
		</dl>
		<dl>
			<dt><hi:text key="营业执照编号" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.op_lic}</dd>
		</dl>
		<dl>
			<dt><hi:text key="营业执照附件" entity="Logistic_corp"/>：</dt>
			<dd>
				<c:if test="${not empty logistic_corp.op_lic_file_attachment}">
				<a href="<hi:url>attachmentView.action?attachment.id=${logistic_corp.op_lic_file_attachment.id}</hi:url>" target="_blank">
					${logistic_corp.op_lic_file_attachment.fileNameImage}
				</a>
				</c:if>
			</dd>				  
		</dl>
		<dl>
			<dt><hi:text key="税务登记号" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.tax_lic}</dd>
		</dl>
		<dl>
			<dt><hi:text key="税务登记证附件" entity="Logistic_corp"/>：</dt>
			<dd>
				<c:if test="${not empty logistic_corp.tax_lic_file_attachment}">
				<a href="<hi:url>attachmentView.action?attachment.id=${logistic_corp.tax_lic_file_attachment.id}</hi:url>" target="_blank">
					${logistic_corp.tax_lic_file_attachment.fileNameImage}
				</a>
				</c:if>
			</dd>				  
		</dl>
		<dl>
			<dt><hi:text key="法人" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.corp_name}</dd>
		</dl>
		<dl>
			<dt><hi:text key="公司电话" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.corp_phone}</dd>
		</dl>
		<dl>
			<dt><hi:text key="手机" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.corp_mobile}</dd>
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Logistic_corp"/>：</dt><dd><hi:select emu="info_state" name="logistic_corp.info_state" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="部门名称" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.orgName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="部门编号" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.orgNum}</dd>
		</dl>
		<dl>
			<dt><hi:text key="部门经理" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.manager.fullName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="上级部门" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.parentOrg.orgName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="描述" entity="Logistic_corp"/>：</dt><dd>${logistic_corp.description}</dd>
		</dl>

		<div class="divider"></div>
			</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
