<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="driver_lineList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="driver_lineList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
			<li>
				<label><hi:text key="起点" entity="Driver_line"/>:</label>
				<input type="text" name="pageInfo.origin.f_r_shortName" value="${pageInfo.origin.f_r_shortName}"/>
			</li>	  
			<li>
				<label><hi:text key="终点" entity="Driver_line"/>:</label>
				<input type="text" name="pageInfo.destination.f_r_shortName" value="${pageInfo.destination.f_r_shortName}"/>
			</li>	  
			<li>
				<label><hi:text key="报价" entity="Driver_line"/>:</label>
				<input type="text" name="pageInfo.f_price" value="${pageInfo.f_price}"/>
			</li>	  
			<li>

			</li>	  
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="DRIVER_LINE_SAVE"><li><a class="add" href="<hi:url>driver_lineEdit.action?driver_line.id=-1</hi:url>" target="navTab" rel="driver_line"><span><hi:text key="新建" parameterLanguageKeys="司机长跑线路"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="DRIVER_LINE_DEL"><li><a class="delete" href="<hi:url>driver_lineRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', origin_r_shortName:'',dest_r_shortName:'',price:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="origin.r_shortName" class="${pageInfo.sorterName eq 'origin.r_shortName' ? pageInfo.sorterDirection : ''}"><hi:text key="起点" entity="Driver_line"/></th>
				<th orderField="destination.r_shortName" class="${pageInfo.sorterName eq 'destination.r_shortName' ? pageInfo.sorterDirection : ''}"><hi:text key="终点" entity="Driver_line"/></th>
				<th orderField="price" class="${pageInfo.sorterName eq 'price' ? pageInfo.sorterDirection : ''}"><hi:text key="报价" entity="Driver_line"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${driver_lines}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td><authz:authorize ifAnyGranted="REGIONGB_VIEW"><a href="<hi:url>regionGBView.action?regionGB.id=${item.origin.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.origin.r_shortName}
					<authz:authorize ifAnyGranted="REGIONGB_VIEW"></a></authz:authorize>
					</td>
				    <td><authz:authorize ifAnyGranted="REGIONGB_VIEW"><a href="<hi:url>regionGBView.action?regionGB.id=${item.destination.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.destination.r_shortName}
					<authz:authorize ifAnyGranted="REGIONGB_VIEW"></a></authz:authorize>
					</td>
				    <td>${item.price}</td>
				<td>
				<c:choose>
					<c:when test="${empty lookup}">
				    <authz:authorize ifAnyGranted="DRIVER_LINE_DEL">
				      <a class="btnDel" href="<hi:url>driver_lineRemove.action?ajax=1&driver_line.id=${item.id}</hi:url>" target="navTabTodo" title="<hi:text key="删除" parameterLanguageKeys="司机长跑线路"/>"><hi:text key="删除"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="DRIVER_LINE_VIEW">
				      <a class="btnView" href="<hi:url>driver_lineView.action?driver_line.id=${item.id}</hi:url>" target="navTab" rel="driver_line" title="<hi:text key="查看" parameterLanguageKeys="司机长跑线路"/>"><hi:text key="查看"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="DRIVER_LINE_SAVE">
				      <a class="btnEdit" href="<hi:url>driver_lineEdit.action?driver_line.id=${item.id}</hi:url>" target="navTab" rel="driver_line" title="<hi:text key="编辑" parameterLanguageKeys="司机长跑线路"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
					<c:otherwise>
						<a class="btnSelect" href="javascript:$.bringBack({id:'${item.id}', origin_r_shortName:'${item.origin.r_shortName}',dest_r_shortName:'${item.destination.r_shortName}',price:'${item.price}'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
