<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="司机与货的关系历史记录"/></h2>
<form action="driver2goodsLogSave.action?navTabId=driver2goodsLogList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="司机名称" entity="Driver2goodsLog"/>：</dt>
			<dd>
				<input type="hidden" name="driver2goodsLog.driver.id" value="${driver2goodsLog.driver.id}"/>
				<input type="text" class="textInput" name="driver2goodsLog.hi_driver.info_state" value="<hi:select emu="info_state" name="driver2goodsLog.driver.info_state" isLabel="true"/>"
					autocomplete="off" lookupGroup="driver2goodsLog" lookupName="driver" suggestClass="org.logistic.basicinfo.model.Driver_info" searchFields="info_state"/>
				<a class="btnLook" href="<hi:url>driver_infoLookup.action?lookup=1</hi:url>" lookupGroup="driver2goodsLog" lookupName="driver"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="关系类型" entity="Driver2goodsLog"/>：</dt><dd><hi:select emu="driver2goodsType" name="driver2goodsLog.d2gType"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="发生时间" entity="Driver2goodsLog"/>：</dt>
			<dd>
				<input type="text" name="driver2goodsLog.opTime" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${driver2goodsLog.opTime}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="评价" entity="Driver2goodsLog"/>：</dt><dd><input type="text" name="driver2goodsLog.taskRank" class="textInput" value="${driver2goodsLog.taskRank}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="货物类型名称" entity="Driver2goodsLog"/>：</dt>
			<dd>
				<input type="hidden" name="driver2goodsLog.goods_info.id" value="${driver2goodsLog.goods_info.id}"/>
				<input type="text" class="textInput" name="driver2goodsLog.hi_goods_info.goodsTypeName" value="${driver2goodsLog.goods_info.goodsTypeName}"
					autocomplete="off" lookupGroup="driver2goodsLog" lookupName="goods_info" suggestClass="org.logistic.basicinfo.model.Goods_info" searchFields="goodsTypeName"/>
				<a class="btnLook" href="<hi:url>goods_infoLookup.action?lookup=1</hi:url>" lookupGroup="driver2goodsLog" lookupName="goods_info"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl  class="nowrap">
			<dt><hi:text key="备注" entity="Driver2goodsLog"/>：</dt>
			<dd>
				<textarea class="editor" name="driver2goodsLog.remark" rows="8" cols="95"
					upLinkUrl="xhEditorUpload.action" upLinkExt="zip,rar,txt" 
					upImgUrl="xhEditorUpload.action" upImgExt="jpg,jpeg,gif,png" 
					upFlashUrl="xhEditorUpload.action" upFlashExt="swf"
					upMediaUrl="xhEditorUpload.action" upMediaExt:"avi" html5Upload="false">
				${driver2goodsLog.remark}</textarea>
			</dd>
		</dl>
				<input type="hidden" name="driver2goodsLog.id" value="${driver2goodsLog.id}"/>
				<input type="hidden" name="driver2goodsLog.creator.id" value="${driver2goodsLog.creator.id}"/>
				<input type="hidden" name="driver2goodsLog.version" value="${driver2goodsLog.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
