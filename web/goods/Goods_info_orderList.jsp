<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="logistic_goods_info_orderList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="logistic_goods_info_orderList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">
			<li class="dateRange">
				<label><hi:text key="装货时间" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.f_loadTime" class="date" readonly="readonly" value="<fmt:formatDate value='${pageInfo.f_loadTime}' pattern='yyyy-MM-dd'/>"/>
				<input type="hidden" name="pageInfo.f_loadTime_op" value="&gt;="><span class="limit">-</span>
				<input type="text" name="pageInfo.f_loadTime01" class="date" readonly="readonly" value="<fmt:formatDate value='${pageInfo.f_loadTime01}' pattern='yyyy-MM-dd'/>"/>
				<input type="hidden" name="pageInfo.f_loadTime01_op" value="&lt;=">
			</li>	  
			<li class="dateRange">
				<label><hi:text key="发布时间" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.f_publishTime" class="date" readonly="readonly" value="<fmt:formatDate value='${pageInfo.f_publishTime}' pattern='yyyy-MM-dd'/>"/>
				<input type="hidden" name="pageInfo.f_publishTime_op" value="&gt;="><span class="limit">-</span>
				<input type="text" name="pageInfo.f_publishTime01" class="date" readonly="readonly" value="<fmt:formatDate value='${pageInfo.f_publishTime01}' pattern='yyyy-MM-dd'/>"/>
				<input type="hidden" name="pageInfo.f_publishTime01_op" value="&lt;=">
			</li>	  
			<li>
				<label><hi:text key="起点" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.origin.f_r_name_path" value="${pageInfo.origin.f_r_name_path}"/>
			</li>	  
			<li>
				<label><hi:text key="货车车型" entity="Goods_info"/>:</label>
				<input type="text" name="pageInfo.truck_type.f_truck_type_name" value="${pageInfo.truck_type.f_truck_type_name}"/>
			</li>	  

		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">

	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="id" class="${pageInfo.sorterName eq 'id' ? pageInfo.sorterDirection : ''}"><hi:text key="货号" entity="Goods_info"/></th>
				<th orderField="logistic_corp.orgName" class="${pageInfo.sorterName eq 'logistic_corp.orgName' ? pageInfo.sorterDirection : ''}"><hi:text key="物流公司" entity="Goods_info"/></th>
				<th orderField="publishTime" class="${pageInfo.sorterName eq 'publishTime' ? pageInfo.sorterDirection : ''}"><hi:text key="发布时间" entity="Goods_info"/></th>
				<th orderField="origin.r_name" class="${pageInfo.sorterName eq 'origin.r_name' ? pageInfo.sorterDirection : ''}"><hi:text key="起点" entity="Goods_info"/></th>
				<th orderField="destination.r_name" class="${pageInfo.sorterName eq 'destination.r_name' ? pageInfo.sorterDirection : ''}"><hi:text key="终点" entity="Goods_info"/></th>
				<th orderField="goodsType.goodsTypeName" class="${pageInfo.sorterName eq 'goodsType.goodsTypeName' ? pageInfo.sorterDirection : ''}"><hi:text key="货物类型名称" entity="Goods_info"/></th>
				<th orderField="truckNumber" class="${pageInfo.sorterName eq 'truckNumber' ? pageInfo.sorterDirection : ''}"><hi:text key="求车（还差）辆" entity="Goods_info"/></th>
				<th orderField="truck_type.truck_type_name" class="${pageInfo.sorterName eq 'truck_type.truck_type_name' ? pageInfo.sorterDirection : ''}"><hi:text key="车型" entity="Goods_info"/></th>
				<th orderField="freightRate" class="${pageInfo.sorterName eq 'freightRate' ? pageInfo.sorterDirection : ''}"><hi:text key="运价（元/辆）" entity="Goods_info"/></th>
				<th orderField="loadTime" class="${pageInfo.sorterName eq 'loadTime' ? pageInfo.sorterDirection : ''}"><hi:text key="装货时间" entity="Goods_info"/></th>
				<th orderField="loadPlace" class="${pageInfo.sorterName eq 'loadPlace' ? pageInfo.sorterDirection : ''}"><hi:text key="装货地点" entity="Goods_info"/></th>
				<th orderField="driver.fullName" class="${pageInfo.sorterName eq 'driver.fullName' ? pageInfo.sorterDirection : ''}"><hi:text key="承运人" entity="Goods_info"/></th>
				<th orderField="orderTime" class="${pageInfo.sorterName eq 'orderTime' ? pageInfo.sorterDirection : ''}"><hi:text key="承运时间" entity="Goods_info"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${goods_infos}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td>${item.id}
					</td>
				    <td>${item.logistic_corp.orgName}
					</td>
				    <td><fmt:formatDate value="${item.publishTime}" pattern="yyyy-MM-dd HH:mm"/></td>
				    <td><authz:authorize ifAnyGranted="REGIONGB_VIEW"><a href="<hi:url>regionGBView.action?regionGB.id=${item.origin.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.origin.r_name}
					<authz:authorize ifAnyGranted="REGIONGB_VIEW"></a></authz:authorize>
					</td>
				    <td><authz:authorize ifAnyGranted="REGIONGB_VIEW"><a href="<hi:url>regionGBView.action?regionGB.id=${item.destination.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.destination.r_name}
					<authz:authorize ifAnyGranted="REGIONGB_VIEW"></a></authz:authorize>
					</td>
				    <td><authz:authorize ifAnyGranted="GOODS_TYPE_VIEW"><a href="<hi:url>goods_typeView.action?goods_type.id=${item.goodsType.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.goodsType.goodsTypeName}
					<authz:authorize ifAnyGranted="GOODS_TYPE_VIEW"></a></authz:authorize>
					</td>
				    <td>${item.truckNumber}(${item.needNum})</td>
				    <td><authz:authorize ifAnyGranted="TRUCK_TYPE_VIEW"><a href="<hi:url>truck_typeView.action?truck_type.id=${item.truck_type.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.truck_type.truck_type_name}
					<authz:authorize ifAnyGranted="TRUCK_TYPE_VIEW"></a></authz:authorize>
					</td>
				
				    <td>${item.freightRate}</td>

				    <td><fmt:formatDate value="${item.loadTime}" pattern="yyyy-MM-dd HH:mm"/></td>
				    <td>${item.loadPlace}</td>
				    <td><authz:authorize ifAnyGranted="DRIVER_INFO_VIEW"><a href="<hi:url>logistic_driver_infoView.action?driver_info.id=${item.driver.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
				    ${item.driver.fullName}
					<authz:authorize ifAnyGranted="DRIVER_INFO_VIEW"></a></authz:authorize>
					</td>
				    <td><fmt:formatDate value="${item.orderTime}" pattern="yyyy-MM-dd HH:mm"/></td>

				<td>
				<c:choose>
					<c:when test="${empty lookup}">
				    <authz:authorize ifAnyGranted="GOODS_INFO_SAVE">
				      <a class="btnEdit" href="<hi:url>logistic_goods_info_orderEdit.action?goods_info.id=${item.id}</hi:url>" target="navTab" rel="goods_info_order" title="<hi:text key="编辑" parameterLanguageKeys="货物承运信息"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
