<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="货物类型"/></h2>
<form action="goods_typeSave.action?navTabId=goods_typeList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="货物类型名称" entity="Goods_type"/>：</dt><dd><input type="text" name="goods_type.goodsTypeName" class="textInput" value="${goods_type.goodsTypeName}" maxlength="30"/></dd>
		</dl>
				<input type="hidden" name="goods_type.id" value="${goods_type.id}"/>
				<input type="hidden" name="goods_type.creator.id" value="${goods_type.creator.id}"/>
				<input type="hidden" name="goods_type.deleted" value="${goods_type.deleted}"/>
				<input type="hidden" name="goods_type.version" value="${goods_type.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
