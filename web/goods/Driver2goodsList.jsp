<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="logistic_driver2goodsList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="logistic_driver2goodsList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">
			<li>
				<label><hi:text key="司机姓名" entity="Driver2goods"/>:</label>
				<input type="text" name="pageInfo.driver.f_fullName" value="${pageInfo.driver.f_fullName}"/>
			</li>			
			<li>
				<label><hi:text key="注册手机" entity="Driver2goods"/>:</label>
				<input type="text" name="pageInfo.driver.f_userName" value="${pageInfo.driver.f_userName}"/>
			</li>			
			<li>
				<label><hi:text key="状态" entity="Driver2goods"/>:</label>
				<hi:search name="pageInfo.f_d2gType" emu="driver2goodsType"/>
			</li>				
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="DRIVER2GOODS_SAVE"><li><a class="add" href="<hi:url>logistic_driver2goodsEdit.action?driver2goods.id=-1</hi:url>" target="navTab" rel="driver2goods"><span><hi:text key="新建" parameterLanguageKeys="司机与货的关系"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="DRIVER2GOODS_DEL"><li><a class="delete" href="<hi:url>driver2goodsRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', info_state:'',d2gType:'',opTime:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
							<tr>
								<th><hi:text key="货号" entity="Driver2goods"/></th>
								<th><hi:text key="运送货物" entity="Driver2goods"/></th>
								<th><hi:text key="起点" entity="Driver2goods"/></th>
								<th><hi:text key="终点" entity="Driver2goods"/></th>
								<th><hi:text key="司机名称" entity="Driver2goods"/></th>
								<th><hi:text key="运单号" entity="Driver2goods"/></th>
								<th><hi:text key="状态" entity="Driver2goods"/></th>
								<th><hi:text key="承运时间" entity="Driver2goods"/></th>
								<th><hi:text key="送达时间" entity="Driver2goods"/></th>
								
								<th><hi:text key="注册手机" entity="Driver2goods"/></th>
								<th><hi:text key="对应车牌" entity="Driver2goods"/></th>

								<th type="del"><hi:text key="操作" entity="Driver2goods"/></th>
							</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${driver2goodss}" varStatus="s">
							<tr>						
								<td>${item.goods_info.id}</td>
								<td>${item.goods_info.goodsType.goodsTypeName} </td>
								<td>${item.goods_info.origin.r_name} </td>
								<td>${item.goods_info.destination.r_name} </td>
								<td>${item.driver.fullName}</td>
								<td>${item.id}</td>
				        		<td><hi:select emu="driver2goodsType" name="item.d2gType" isLabel="true"/></td>
								<td><fmt:formatDate value="${item.goods_info.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td><fmt:formatDate value="${item.goods_info.unloadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td>
								<a target="dialog" rel="loc" href="baiduMaplbs_locateMobile.action?mdn=${item.driver.userName}" width=610 height=480 resizable=false maxable=false>
								${item.driver.userName}
								</a>
								</td>				        		
								<td>${item.driver.truck_info.truck_card}</td>
								<td>
							      <a class="btnEdit" href="<hi:url>logistic_driver2goodsEdit.action?driver2goods.id=${item.id}</hi:url>" target="navTab" rel="driver2goods" title="<hi:text key="编辑" parameterLanguageKeys="司机与货的关系"/>"><hi:text key="编辑"/></a>
								</td>
							</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
