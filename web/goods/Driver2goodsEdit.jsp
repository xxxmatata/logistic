<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="送货评价" parameterLanguageKeys=""/></h2>
<form action="logistic_saveEvaluateDriver2goods.action?navTabId=driver2goodsList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="司机名称" entity="Driver2goods"/>：</dt><dd>${driver2goods.driver.fullName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="运送货物" entity="Goods_info"/>：</dt><dd>${driver2goods.goods_info.goodsType.goodsTypeName}</dd>
		</dl>

		<dl>
			<dt><hi:text key="起点" entity="Goods_info"/>：</dt><dd>${driver2goods.goods_info.origin.r_name}</dd>
		</dl>
		<dl>
			<dt><hi:text key="终点" entity="Goods_info"/>：</dt><dd>${driver2goods.goods_info.destination.r_name}</dd>
		</dl>

		<dl>
			<dt><hi:text key="要求装货时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${driver2goods.goods_info.loadTime}" pattern="yyyy-MM-dd HH:mm"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="要求装货地点" entity="Goods_info"/>：</dt><dd>${driver2goods.goods_info.loadPlace}</dd>
		</dl>
		<dl>
			<dt><hi:text key="成交时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${driver2goods.goods_info.orderTime}" pattern="yyyy-MM-dd HH:mm"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="装货完成时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${driver2goods.goods_info.realLoadTime}" pattern="yyyy-MM-dd HH:mm"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="卸货时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${driver2goods.goods_info.unloadTime}" pattern="yyyy-MM-dd HH:mm"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="当前状态" entity="Driver2goods"/>：</dt><dd><hi:select emu="driver2goodsType" name="driver2goods.d2gType" isLabel="true"/></dd>			
		</dl>		
		<dl>
			<dt><hi:text key="评价" entity="Driver2goods"/>：</dt><dd><hi:select emu="taskRank" name="driver2goods.taskRank"/></dd>			
		</dl>
		
		<dl  class="nowrap">
			<dt><hi:text key="备注" entity="Driver2goods"/>：</dt>
			<dd>
				<textarea class="editor" name="driver2goods.remark" rows="5" cols="95"
					upLinkUrl="xhEditorUpload.action" upLinkExt="zip,rar,txt" 
					upImgUrl="xhEditorUpload.action" upImgExt="jpg,jpeg,gif,png" 
					upFlashUrl="xhEditorUpload.action" upFlashExt="swf"
					upMediaUrl="xhEditorUpload.action" upMediaExt:"avi" html5Upload="false">
				</textarea>
			</dd>
		</dl>		
				<input type="hidden" name="driver2goods.id" value="${driver2goods.id}"/>
				<input type="hidden" name="driver2goods.goods_info.id" value="${driver2goods.goods_info.id}"/>
				<input type="hidden" name="driver2goods.driver.id" value="${driver2goods.driver.id}"/>
				<input type="hidden" name="driver2goods.creator.id" value="${driver2goods.creator.id}"/>
				<input type="hidden" name="driver2goods.version" value="${driver2goods.version}"/>

		<div class="divider"></div>

			
		<div class="tabs">
			<div class="tabsHeader">
				<div class="tabsHeaderContent">
					<ul>
						<li><a href="javascript:void(0)"><span><hi:text key="历史记录"/></span></a></li>
					</ul>
				</div>
			</div>
			<div class="tabsContent" style="height:320px;">
				<div style="overflow: auto">
					<table class="list nowrap noadd" width="100%" itemdetail="goods_info.driver2goodss_log">
						<thead>
							<tr>
								<th><hi:text key="司机名称" entity="Driver2goods_log"/></th>
								<th><hi:text key="注册手机" entity="Driver2goods_log"/></th>
								<th><hi:text key="评价" entity="Driver2goods_log"/></th>
								<th><hi:text key="对应车牌" entity="Driver2goods_log"/></th>
								<th><hi:text key="状态" entity="Driver2goods_log"/></th>
								<th><hi:text key="发生时间" entity="Driver2goods_log"/></th>
								<th><hi:text key="记录人" entity="Driver2goods_log"/></th>
								<th><hi:text key="详细" entity="Driver2goods_log"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${driver2goodss_log}">
							<tr>						
								<td>${item.driver.fullName} </td>
								<td>
								${item.driver.userName}
								</td>				        		
								<td><hi:select emu="taskRank" name="${item.taskRank}" isLabel="true"/> </td>
								<td>${item.driver.truck_info.truck_card}</td>
				        		<td><hi:select emu="driver2goodsType" name="item.d2gType" isLabel="true"/></td>
								<td><fmt:formatDate value="${item.opTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td>${item.creator.fullName} </td>
								<td>
				      				<a class="btnView" href="<hi:url>driver2goods_logView.action?driver2goods_log.id=${item.id}</hi:url>" target="navTab" rel="driver2goodsLog" title="<hi:text key="查看" parameterLanguageKeys="历史记录详细"/>"><hi:text key="查看"/></a>
								</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
			</div>
			<div class="tabsFooter">
				<div class="tabsFooterContent"></div>
			</div>
		</div>					
					</div>	
			
				<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
