<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="货物信息"/></h2>
<form action="logistic_goods_infoPubSave.action?navTabId=logistic_goods_infoList&callbackType=closeCurrent&ajax=1" method="post"  onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">

		<dl>
			<dt><hi:text key="起点" entity="Goods_info"/>：</dt>
			<dd>
				<input type="hidden" name="goods_info.origin.id" value="${goods_info.origin.id}"/>
				<input type="text" readonly="readonly" class="textInput required" name="goods_info.hi_origin.r_shortName" value="${goods_info.origin.r_shortName}"/>
				<a class="btnLook" href="<hi:url>ExRegionGBLookup.action?lookup=1</hi:url>&pCode=${goods_info.origin.r_code}" lookupGroup="goods_info" lookupName="origin"><hi:text key="查找带回"/></a>
			</dd>
			
		</dl>
		<dl>
			<dt><hi:text key="终点" entity="Goods_info"/>：</dt>
			<dd>
				<input type="hidden" name="goods_info.destination.id" value="${goods_info.destination.id}"/>
				<input type="text" readonly="readonly" class="textInput required" name="goods_info.hi_destination.r_shortName" value="${goods_info.destination.r_shortName}"/>
				<a class="btnLook" href="<hi:url>ExRegionGBLookup.action?lookup=1</hi:url>&pCode=${goods_info.destination.r_code}" lookupGroup="goods_info" lookupName="destination"><hi:text key="查找带回"/></a>

			</dd>
		</dl>

		<dl>
			<dt><hi:text key="运价（元/车）" entity="Goods_info"/>：</dt><dd><input type="text" name="goods_info.freightRate" class="textInput float" value="${goods_info.freightRate}" alt="<hi:text key="请输浮点数"/>"/></dd>
		</dl>

		<dl>
			<dt><hi:text key="货物类型" entity="Goods_info"/>：</dt>
			<dd>
				<input type="hidden" name="goods_info.goodsType.id" value="${goods_info.goodsType.id}"/>
				<input type="text" class="textInput required" name="goods_info.hi_goodsType.goodsTypeName" value="${goods_info.goodsType.goodsTypeName}"
					autocomplete="off" lookupGroup="goods_info" lookupName="goodsType" suggestClass="org.logistic.basicinfo.model.Goods_type" searchFields="goodsTypeName"/>
				<a class="btnLook" href="<hi:url>goods_typeLookup.action?lookup=1</hi:url>" lookupGroup="goods_info" lookupName="goodsType"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>		
		<dl>
			<dt><hi:text key="需要车辆数量" entity="Goods_info"/>：</dt><dd><input type="text" name="goods_info.truckNumber" class="textInput integer" value="${goods_info.truckNumber}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>

		
		<dl>
			<dt><hi:text key="货车车型" entity="Goods_info"/>：</dt>
			<dd>
				<input type="hidden" name="goods_info.truck_type.id" value="${goods_info.truck_type.id}"/>
				<input type="text" class="textInput" name="goods_info.hi_truck_type.truck_type_name" value="${goods_info.truck_type.truck_type_name}"
					autocomplete="off" lookupGroup="goods_info" lookupName="truck_type" suggestClass="org.logistic.basicinfo.model.Truck_type" searchFields="truck_type_name"/>
				<a class="btnLook" href="<hi:url>truck_typeLookup.action?lookup=1</hi:url>" lookupGroup="goods_info" lookupName="truck_type"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="要求车长（米）" entity="Goods_info"/>：</dt><dd><input type="text" name="goods_info.truck_length" class="textInput float" value="${goods_info.truck_length}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="要求车宽（米）" entity="Goods_info"/>：</dt><dd><input type="text" name="goods_info.truck_width" class="textInput float" value="${goods_info.truck_width}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="要求吨位" entity="Goods_info"/>：</dt><dd><input type="text" name="goods_info.truck_load" class="textInput float" value="${goods_info.truck_load}" alt="<hi:text key="请输入整数"/>"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="装货时间" entity="Goods_info"/>：</dt>
			<dd>
				<input type="text" name="goods_info.loadTime" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${goods_info.loadTime}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="装货地点" entity="Goods_info"/>：</dt><dd><input type="text" name="goods_info.loadPlace" class="textInput" value="${goods_info.loadPlace}" maxlength="200"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="备注" entity="Goods_info"/>：</dt><dd><input type="text" name="goods_info.remark" class="textInput" value="${goods_info.remark}" maxlength="400"/></dd>
		</dl>
				<input type="hidden" name="goods_info.id" value="${goods_info.id}"/>
				<input type="hidden" name="goods_info.creator.id" value="${goods_info.creator.id}"/>
				<input type="hidden" name="goods_info.deleted" value="${goods_info.deleted}"/>
				<input type="hidden" name="goods_info.version" value="${goods_info.version}"/>
		<div class="divider"></div>
		
		<div class="tabs">
			<div class="tabsHeader">
				<div class="tabsHeaderContent">
					<ul>
						<li><a href="javascript:void(0)"><span><hi:text key="相关货主"/></span></a></li>
					</ul>
				</div>
			</div>
			<div class="tabsContent" style="height:150px;">
				<div>
					<table class="list nowrap" width="100%" itemDetail="goods_info.goods2owners">
						<thead>
							<tr>
								<th type="lookup" class="" name="ownerName" lookupName="owner" lookupUrl="<hi:url>goods_ownerLookup.action?lookup=1</hi:url>" size="12"><hi:text key="货主名称" entity="Goods2owner"/></th>
								<th type="del" width="60"><hi:text key="操作"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${goods_info.goods2owners}"  varStatus="s">
							<tr>
							<input type="hidden" name="goods_info.goods2owners[${s.index}].id" value="${item.id}"/>
							<input type="hidden" name="goods_info.goods2owners[${s.index}].version" value="${item.version}"/>
								<td>
								
									<input type="hidden" name="goods_info.goods2owners[${s.index}].owner.id" value="${item.owner.id}"/>
									<input type="text" class="" name="goods_info.goods2owners[${s.index}].hi_owner.ownerName" value="${item.owner.ownerName}" size="12" />
									<a class="btnLook" href="<hi:url>goods_ownerLookup.action?lookup=1</hi:url>" lookupGroup="goods_info.goods2owners" lookupName="owner" index="${s.index}" title="<hi:text key="查找带回"/>"><hi:text key="查找带回"/></a>
								</td>
								<td><a href="<hi:url>goods2ownerRemove.action?ajax=1&goods2owner.id=${item.id}</hi:url>" class="btnDel" title="<hi:text key="确定要删除吗?"/>">删除</a></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>								
				
			</div>
			<div class="tabsFooter">
				<div class="tabsFooterContent"></div>
			</div>
		</div>				
	</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="发布"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
