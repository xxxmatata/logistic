<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="货物信息"/></h2>
<form action="logistic_goods_info_loadSave.action?navTabId=logistic_goods_info_loadList&callbackType=closeCurrent&ajax=1" method="post"  onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">

		<dl>
			<dt><hi:text key="装货完成时间" entity="Goods_info"/>：</dt>
			<dd>
				<input type="text" name="goods_info.realLoadTime" class="textInput date required" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
					value="<fmt:formatDate value='${goods_info.realLoadTime}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
				<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="备注" entity="Goods_info"/>：</dt><dd><input type="text" name="goods_info.remark" class="textInput" value="${goods_info.remark}" maxlength="400"/></dd>
		</dl>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="确认"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
		
				<input type="hidden" name="goods_info.id" value="${goods_info.id}"/>
				<input type="hidden" name="goods_info.creator.id" value="${goods_info.creator.id}"/>
				<input type="hidden" name="goods_info.deleted" value="${goods_info.deleted}"/>
				<input type="hidden" name="goods_info.version" value="${goods_info.version}"/>



		<div class="divider"></div>
		
		<div class="tabs">
			<div class="tabsHeader">
				<div class="tabsHeaderContent">
					<ul>
						<li><a href="javascript:void(0)"><span><hi:text key="货源详情"/></span></a></li>
						<li><a href="javascript:void(0)"><span><hi:text key="相关货主"/></span></a></li>
						<li><a href="javascript:void(0)"><span><hi:text key="相关司机"/></span></a></li>
					</ul>
				</div>
			</div>
			<div class="tabsContent" style="height:320px;">
				<div style="overflow: auto">
					<dl>
						<dt><hi:text key="物流公司" entity="Goods_info"/>：</dt><dd>${goods_info.logistic_corp.orgName}</dd>
					</dl>				
					<dl>
						<dt><hi:text key="发布时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.publishTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
					</dl>
					<dl>
						<dt><hi:text key="起点" entity="Goods_info"/>：</dt><dd>${goods_info.origin.r_name}</dd>
					</dl>
					<dl>
						<dt><hi:text key="终点" entity="Goods_info"/>：</dt><dd>${goods_info.destination.r_name}</dd>
					</dl>
			
					<dl>
						<dt><hi:text key="运价（元）" entity="Goods_info"/>：</dt><dd>${goods_info.freightRate}</dd>
					</dl>
					<dl>
						<dt><hi:text key="运价单位" entity="Goods_info"/>：</dt><dd><hi:select emu="units" name="goods_info.freightUnits" isLabel="true"/></dd>
					</dl>
					<dl>
						<dt><hi:text key="装货时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.loadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
					</dl>
					<dl>
						<dt><hi:text key="装货地点" entity="Goods_info"/>：</dt><dd>${goods_info.loadPlace}</dd>
					</dl>
					<dl>
						<dt><hi:text key="求车数量" entity="Goods_info"/>：</dt><dd>${goods_info.truckNumber}</dd>
					</dl>
					<dl>
						<dt><hi:text key="货车车型" entity="Goods_info"/>：</dt><dd>${goods_info.truck_type.truck_type_name}</dd>
					</dl>
					<dl>
						<dt><hi:text key="车长（米）" entity="Goods_info"/>：</dt><dd>${goods_info.truck_length}</dd>
					</dl>
					<dl>
						<dt><hi:text key="车宽（米）" entity="Goods_info"/>：</dt><dd>${goods_info.truck_width}</dd>
					</dl>
					<dl>
						<dt><hi:text key="载重（吨）" entity="Goods_info"/>：</dt><dd>${goods_info.truck_load}</dd>
					</dl>
					<dl>
						<dt><hi:text key="状态" entity="Goods_info"/>：</dt><dd><hi:select emu="goods_state" name="goods_info.info_state" isLabel="true"/></dd>
					</dl>
					<dl>
						<dt><hi:text key="承运人" entity="Goods_info"/>：</dt><dd>${goods_info.driver.fullName}</dd>
					</dl>
					<dl>
						<dt><hi:text key="成交时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
					</dl>
					<dl>
						<dt><hi:text key="装货完成时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.realLoadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
					</dl>
					<dl>
						<dt><hi:text key="卸货时间" entity="Goods_info"/>：</dt><dd><fmt:formatDate value="${goods_info.unloadTime}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
					</dl>
				
				</div>
				<div style="overflow: auto">
					<table class="list" width="100%">
						<thead>
							<tr>
								<th><hi:text key="货主名称" entity="Goods2owner"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${goods_info.goods2owners}">
							<tr>						
								<td>${item.owner.ownerName}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div style="overflow: auto">
					<table class="list" width="100%">
						<thead>
							<tr>
								<th><hi:text key="司机名称" entity="Driver2goods"/></th>
								<th><hi:text key="注册手机" entity="Driver2goods"/></th>
								<th><hi:text key="对应车牌" entity="Driver2goods"/></th>
								<th><hi:text key="关系类型" entity="Driver2goods"/></th>
								<th><hi:text key="发生时间" entity="Driver2goods"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${goods_info.driver2goodss}">
							<tr>						
								<td>${item.driver.fullName}</td>
								<td>
								<a target="dialog" rel="loc" href="baiduMaplbs_locateMobile.action?mdn=${item.driver.userName}" width=610 height=480 resizable=false maxable=false>
								${item.driver.userName}
								</a>
								</td>				        		
								<td>${item.driver.truck_info.truck_card}</td>
								<td><hi:select emu="driver2goodsType" name="item.d2gType" isLabel="true"/></td>
								<td><fmt:formatDate value="${item.opTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
			</div>
			<div class="tabsFooter">
				<div class="tabsFooterContent"></div>
			</div>
		</div>				
	</div>

</div>
</form>
