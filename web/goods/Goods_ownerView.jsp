<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="货主信息"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="货主名称" entity="Goods_owner"/>：</dt><dd>${goods_owner.ownerName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="手机号码" entity="Goods_owner"/>：</dt><dd>${goods_owner.mobileNumber}</dd>
		</dl>
		<dl>
			<dt><hi:text key="备注" entity="Goods_owner"/>：</dt><dd>${goods_owner.description}</dd>
		</dl>

		<div class="divider"></div>
			</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
