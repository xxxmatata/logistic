<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="货主信息"/></h2>
<form action="logistic_goods_ownerSave.action?navTabId=logistic_goods_ownerList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="货主名称" entity="Goods_owner"/>：</dt><dd><input type="text" name="goods_owner.ownerName" class="textInput" value="${goods_owner.ownerName}" maxlength="50"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="手机号码" entity="Goods_owner"/>：</dt><dd><input type="text" name="goods_owner.mobileNumber" class="textInput" value="${goods_owner.mobileNumber}" maxlength="20"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="备注" entity="Goods_owner"/>：</dt><dd><input type="text" name="goods_owner.description" class="textInput" value="${goods_owner.description}" maxlength="500"/></dd>
		</dl>
				<input type="hidden" name="goods_owner.id" value="${goods_owner.id}"/>
				<input type="hidden" name="goods_owner.creator.id" value="${goods_owner.creator.id}"/>
				<input type="hidden" name="goods_owner.deleted" value="${goods_owner.deleted}"/>
				<input type="hidden" name="goods_owner.version" value="${goods_owner.version}"/>

		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
