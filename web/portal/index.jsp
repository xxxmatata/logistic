<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main_driver.jsp"%>
<!DOCTYPE html>
<style>
<!--
	.ad1{
		margin : 10px
	}
	
	.ad_bar_img1{
		margin : 10px;
		height:150px;
	}
	
	.tab_data{
		height:450px;
		overflow:auto;
		font-size: 8.5px;
	}
	
	@media (max-width: 980px) {
		#left_ad_bar{
			display:none;
		}
		#right_ad_bar{
			display:none;
		}
		.span6 {
		  width: 45%;
		}				
		.span6 {
		  width: 90%;
		}				
	}
-->
</style>
<html>
  <head>
    <meta charset="utf-8">
    <title>物流信息</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
  </head>

  <body>
	<%@ include file="/includes/styles_portal.jsp"%>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
				<div class="span12">
					<div class="carousel slide" id="carousel-678">
						<ol class="carousel-indicators">
							<li data-slide-to="0" data-target="#carousel-678">
							</li>
							<li data-slide-to="1" data-target="#carousel-678" class="active">
							</li>
							<li data-slide-to="2" data-target="#carousel-678">
							</li>
						</ol>
						<div class="carousel-inner">
							<div class="item  ad_bar_img1">
								<img alt="" src="portal/img/1.jpg" />
							</div>
							<div class="item active ad_bar_img1">
								<img alt="" src="portal/img/2.jpg" />

							</div>
							<div class="item  ad_bar_img1">
								<img alt="" src="portal/img/3.jpg" />

							</div>
						</div> <a data-slide="prev" href="#carousel-678" class="left carousel-control">‹</a> <a data-slide="next" href="#carousel-678" class="right carousel-control">›</a>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span2" id="left_ad_bar">
					<img class="ad1" alt="140x140" src="portal/img/a.jpg" />
					<img class="ad1" alt="140x140" src="portal/img/a.jpg" />
					<img class="ad1" alt="140x140" src="portal/img/a.jpg" />
				</div>
				<div class="span8_1">
					<div class="row-fluid">
			<div class="span7">
				<div class="row-fluid">
					<div class="span12">
						<div style="line-height: 40px;background-color:#d9edf7;">
							<div style="font-family: inherit;font-weight: bold;line-height: 20px;font-size: 17.5px; display:inline;">货源信息</div>
							<div style="float:right;">更多</div>
						</div>
						<div class="tab_data">
						<table class="table">
							<thead>
								<tr>
									<th>
										来源
									</th>
									<th>
										货物
									</th>
									<th>
										线路
									</th>
									<th>
										需车
									</th>
									<th>
										发布时间
									</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="item" items="${goods_infos}" varStatus="s">
								<tr >
									<td>
										${item.logistic_corp.orgName}
									</td>
									<td>
										${item.goodsType.goodsTypeName}
									</td>
									<td>
										${item.origin.r_name}-${item.destination.r_name}
									</td>
									<td>
										${item.truck_type.truck_type_name}(${item.truckNumber}辆)
									</td>
									<td>
										<fmt:formatDate value="${item.publishTime}" pattern="yyyy-MM-dd"/>
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
			<div class="span5">
				<div class="row-fluid">
					<div class="span12">
						<div style="line-height: 40px;background-color:#d9edf7;">
							<div style="font-family: inherit;font-weight: bold;line-height: 20px;font-size: 17.5px; display:inline;">车源信息</div>
							<div style="float:right;">更多</div>
						</div>
						<div class="tab_data">
						<table class="table  tab_data">
							<thead>
								<tr>
									<th>
										司机
									</th>
									<th>
										车牌号
									</th>
									<th>
										车型
									</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="item" items="${driver_infos}" varStatus="s">
								<tr class="warning">
									<td>
										${item.fullName}
									</td>
									<td>
										${item.truck_card}
									</td>
									<td>
										${item.truck_type_name}(长：${item.truck_length},宽：${item.truck_width},重：${item.truck_load})
									</td>
								</tr>
								</c:forEach>
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
					</div>
					<ul class="thumbnails ad1">
						<li class="span4">
							<div class="thumbnail">
								<img alt="300x200" src="portal/img/people.jpg" />
								<div class="caption">
									<h3>
										冯诺尔曼结构
									</h3>
									<p>
										也称普林斯顿结构，是一种将程序指令存储器和数据存储器合并在一起的存储器结构。程序指令存储地址和数据存储地址指向同一个存储器的不同物理位置。
									</p>
									<p>
										<a class="btn btn-primary" href="#">浏览</a> <a class="btn" href="#">分享</a>
									</p>
								</div>
							</div>
						</li>
						<li class="span4">
							<div class="thumbnail">
								<img alt="300x200" src="portal/img/city.jpg" />
								<div class="caption">
									<h3>
										哈佛结构
									</h3>
									<p>
										哈佛结构是一种将程序指令存储和数据存储分开的存储器结构，它的主要特点是将程序和数据存储在不同的存储空间中，进行独立编址。
									</p>
									<p>
										<a class="btn btn-primary" href="#">浏览</a> <a class="btn" href="#">分享</a>
									</p>
								</div>
							</div>
						</li>
						<li class="span4">
							<div class="thumbnail">
								<img alt="300x200" src="portal/img/sports.jpg" />
								<div class="caption">
									<h3>
										改进型哈佛结构
									</h3>
									<p>
										改进型的哈佛结构具有一条独立的地址总线和一条独立的数据总线，两条总线由程序存储器和数据存储器分时复用，使结构更紧凑。
									</p>
									<p>
										<a class="btn btn-primary" href="#">浏览</a> <a class="btn" href="#">分享</a>
									</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="span2" id="right_ad_bar">
					<img class="ad1" alt="140x140" src="portal/img/a.jpg" />
					<img class="ad1" alt="140x140" src="portal/img/a.jpg" />
					<img class="ad1" alt="140x140" src="portal/img/a.jpg" />
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
				</div>
			</div>
		</div>
	</div>
</div>

  </body>
‹
›
</html>
