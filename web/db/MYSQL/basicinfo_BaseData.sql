
delete from HiMenu where ID = 100000;
--
insert into HiMenu(ID, version, menuName, displayRef, description, parentMenu, sequence, creator) values(100000, 0, 'basicinfo', '基本信息', '基本信息', 0, 9999, 0);
--



delete from MenuLink where ID = 102300;
--
insert into MenuLink(ID, version, linkUrl, displayRef, description, authority, sequence, menu, menuLinkType, creator) values(102300, 0, '/driver_mobile_pointList.action', '手机最新位置', '手机最新位置', 102300, 9999, 100000, 0, 0);
--



delete from HI_Authority where ID = 102300;
--
delete from HI_Authority where ID = 102301;
--
delete from HI_Authority where ID = 102302;
--
delete from HI_Authority where ID = 102303;
--
delete from HI_Authority where ID = 102304;
--
insert into HI_Authority(ID, version, authorityName, displayRef, propertedResource, description, authorityType, menuLink) values(102300, 0, 'DRIVER_MOBILE_POINT_LIST', 'basicinfo.Driver_mobile_pointList', '', '手机最新位置查询', 1, 102300);
--
insert into HI_Authority(ID, version, authorityName, displayRef, propertedResource, description, authorityType, menuLink) values(102301, 0, 'DRIVER_MOBILE_POINT_VIEW', 'basicinfo.Driver_mobile_pointView', '', '手机最新位置查看', 2, 102300);
--
insert into HI_Authority(ID, version, authorityName, displayRef, propertedResource, description, authorityType, menuLink) values(102302, 0, 'DRIVER_MOBILE_POINT_SAVE', 'basicinfo.Driver_mobile_pointSave', '', '手机最新位置保存', 3, 102300);
--
insert into HI_Authority(ID, version, authorityName, displayRef, propertedResource, description, authorityType, menuLink) values(102303, 0, 'DRIVER_MOBILE_POINT_DEL', 'basicinfo.Driver_mobile_pointDel', '', '手机最新位置删除', 4, 102300);
--
insert into HI_Authority(ID, version, authorityName, displayRef, propertedResource, description, authorityType, menuLink) values(102304, 0, 'DRIVER_MOBILE_POINT_LOOKUP', 'basicinfo.Driver_mobile_pointLookup', '', '手机最新位置带回', 1, 102300);
--

delete from HI_PrivilegeResource where ID = 102300;
--
delete from HI_PrivilegeResource where ID = 102301;
--
delete from HI_PrivilegeResource where ID = 102302;
--
delete from HI_PrivilegeResource where ID = 102303;
--
delete from HI_PrivilegeResource where ID = 102304;
--
insert into HI_PrivilegeResource(ID, version, authorityName, viewLayer, businessLayer, bizExtAuthNames) values(102300, 0, 'DRIVER_MOBILE_POINT_LIST', '/driver_mobile_pointList.action', 'org.logistic.basicinfo.service.Driver_mobile_pointManager.getSecurityDriver_mobile_pointList', 'DRIVER_MOBILE_POINT_LOOKUP');
--
insert into HI_PrivilegeResource(ID, version, authorityName, viewLayer, businessLayer) values(102301, 0, 'DRIVER_MOBILE_POINT_VIEW', '/driver_mobile_pointView.action', 'org.logistic.basicinfo.service.Driver_mobile_pointManager.getSecurityDriver_mobile_pointById');
--
insert into HI_PrivilegeResource(ID, version, authorityName, viewLayer, businessLayer) values(102302, 0, 'DRIVER_MOBILE_POINT_SAVE', '/driver_mobile_pointSave.action', 'org.logistic.basicinfo.service.Driver_mobile_pointManager.saveSecurityDriver_mobile_point');
--
insert into HI_PrivilegeResource(ID, version, authorityName, viewLayer, businessLayer) values(102303, 0, 'DRIVER_MOBILE_POINT_DEL', '/driver_mobile_pointRemove.action', 'org.logistic.basicinfo.service.Driver_mobile_pointManager.removeSecurityDriver_mobile_pointById');
--
insert into HI_PrivilegeResource(ID, version, authorityName, viewLayer) values(102304, 0, 'DRIVER_MOBILE_POINT_LOOKUP', '/driver_mobile_pointLookup.action');
--







--
delete from HI_Language where ID = 102300;
--
insert into HI_Language(ID, version, keyStr, creator, isSystem) values(102300, 0, '手机最新位置', 1, 0);
--
delete from HI_Language where ID = 102301;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102301, 0, '国际移动用户识别码', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102302;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102302, 0, '移动用户号码簿号码', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102303;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102303, 0, '设备操作系统版本', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102304;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102304, 0, '经度', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102305;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102305, 0, '纬度', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102306;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102306, 0, '高度', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102307;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102307, 0, '速度', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102308;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102308, 0, '上报时间', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102309;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102309, 0, '定位类型', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102310;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102310, 0, '客户端IP地址', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102311;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102311, 0, '位置描述', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102312;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102312, 0, '手机串号', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102313;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102313, 0, 'sid', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102314;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102314, 0, 'nid', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102315;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102315, 0, 'bid', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102316;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102316, 0, 'radius', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102317;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102317, 0, 'error_code', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102318;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102318, 0, '定位时间', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102319;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102319, 0, '卫星数量', 'Driver_mobile_point', 1, 0);
--
delete from HI_Language where ID = 102320;
--
insert into HI_Language(ID, version, keyStr, entity, creator, isSystem) values(102320, 0, 'uuid', 'Driver_mobile_point', 1, 0);
