DROP TABLE IF EXISTS Driver_mobile_point;
--
CREATE TABLE Driver_mobile_point (
    id int auto_increment NOT NULL ,
    version int NOT NULL ,
    imsi nvarchar (20)  NULL,
    mdn nvarchar (20)  NULL,
    os_version nvarchar (10)  NULL,
    lng decimal (12,8)  NULL,
    lat decimal (12,8)  NULL,
    alt decimal (8,2)  NULL,
    speed decimal (6,2)  NULL,
    upd_time datetime   NULL,
    loc_type nvarchar (30)  NULL,
    client_ip nvarchar (30)  NULL,
    loc_desc nvarchar (200)  NULL,
    imei nvarchar (30)  NULL,
    mcc nvarchar (10)  NULL,
    mnc nvarchar (10)  NULL,
    lac nvarchar (10)  NULL,
    sid nvarchar (8)  NULL,
    nid nvarchar (8)  NULL,
    bid nvarchar (8)  NULL,
    radius decimal (10,2)  NULL,
    error_code nvarchar (30)  NULL,
    loc_time datetime   NULL,
    satellite_num int   NULL,
    uuid nvarchar (30)  NULL,
    creator int  NULL,
    primary key (id));
--

