<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>
<script type="text/javascript">
var list = baidumap_idx.getOverlays();

for (i = 0 ; i < list.length ; i++){
	baidumap_idx.removeOverlay(list[i]);
}

/***
 * 点中之后跳一下
 */
function showMarker(idx){
	var m = eval("marker"+idx);
	m.setAnimation(BMAP_ANIMATION_BOUNCE);
	m.setAnimation(null);
}

function showTrace(mdn){
	var url = "baiduMaplbs_driverTrace.action?mdn="+mdn;
	navTab.openTab("driver_trace",url,{title:'查询轨迹'});
}
</script>
								<ul>
									<c:forEach var="item" items="${drivers}" varStatus="s">
											<li style="margin:10px"><a onclick="showMarker(${s.count})">${item.fullName}-${item.userName}-${item.truck_card}</a></li>
											<script type="text/javascript">
												
												var point${s.count} = new BMap.Point('${item.lng}','${item.lat}');  
												var marker${s.count} = new BMap.Marker(point${s.count});
												//创建信息窗口
												var info = "<p style='margin:10px'>${item.fullName}-${item.userName}-${item.truck_card}</p>";
												info = info + "<p style='margin:10px'>${item.loc_desc}</p>";
												info = info + "<p style='margin:10px'> <a onclick='showTrace(${item.userName});'>轨迹</a> <a>消息</a> </p>";
												var infoWindow${s.count} = new BMap.InfoWindow(info);
												marker${s.count}.addEventListener("click", function(){this.openInfoWindow(infoWindow${s.count});});												 
												baidumap_idx.addOverlay(marker${s.count}); 
											</script>
									</c:forEach>
								</ul>
