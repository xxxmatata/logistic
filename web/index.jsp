<%@ taglib uri="/WEB-INF/tld/struts-menu.tld" prefix="menu" %>
<%@ taglib uri="/WEB-INF/tld/hi.tld" prefix="hi" %>
<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=7" />
<title>物流管理平台</title>
<%@ include file="/includes/styles.jsp"%>
<script type="text/javascript">
$(function(){
	DWZ.init("styles/dwz/dwz.frag.xml", {
//		loginUrl:"loginsub.jsp", loginTitle:"登录",	// 弹出登录对话框
		loginUrl:"login.jsp",	// 跳到登录页面
		pageInfo:{pageNum:"pageInfo.currentPage", numPerPage:"pageInfo.pageSize", orderField:"pageInfo.sorterName", orderDirection:"pageInfo.sorterDirection"}, //【可选】
		debug:false,	// 调试模式 【true|false】
		callback:function(){
			initEnv();
			$("#themeList").theme({themeBase:"styles/dwz/themes"});
			
		}
	});
});



function loadMap(baidumap_idx){
	var bs = baidumap_idx.getBounds();  
	var bssw = bs.getSouthWest();   //可视区域左下角
	var bsne = bs.getNorthEast();   //可视区域右上角
	var url = "loadDriverOnMap.action?bssw_lng="+bssw.lng+"&bssw_lat="+bssw.lat+"&bsne_lng="+bsne.lng+"&bsne_lat="+bsne.lat;
	
	url = url + "&recentTime="+recentTime;

	$("#load_driver").load(url,function(){
		
		initUI($("#map"));
	});									
	
	$("#hint").html("地图缩放至：" + baidumap_idx.getZoom() + "级."+"当前地图可视范围是：" + bssw.lng + "," + bssw.lat + "到" + bsne.lng + "," + bsne.lat);

}
</script>
</head>

<body scroll="no">
	<div id="layout">
		<div id="header">
			<div class="headerNav">
				<a class="logo" href="javascript:void(0)">标志</a>
				<ul class="nav">
					<li><a href="personalSettingView.action" target="dialog">个人设置</a></li>
					<li><a href="j_acegi_logout">退出</a></li>
				</ul>

			</div>
		</div>
		
		<div id="leftside">
			<div id="sidebar_s">
				<div class="collapse">
					<div class="toggleCollapse"><div></div></div>
				</div>
			</div>
			<div id="sidebar">
				<div class="toggleCollapse"><h2>主菜单</h2><div>收缩</div></div>
			
				<menu:useMenuDisplayer name="Velocity" config="menu/sysmenu.html"  bundle="">
 					<hi:displayMenu name="com.hi.tree.menu" menuName="himenu" type="sys"/>
				</menu:useMenuDisplayer>
			</div>
			
		</div>
		<div id="container">
			<div id="navTab" class="tabsPage">
				<div class="tabsPageHeader">
					<div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
						<ul class="navTab-tab">
							<li tabid="main" class="main"><a href="javascript:void(0)"><span><span class="home_icon">我的主页</span></span></a></li>
						</ul>
					</div>
					<div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
					<div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
					<div class="tabsMore">more</div>
				</div>
				<ul class="tabsMoreList">
					<li><a href="javascript:void(0)">我的主页</a></li>
				</ul>
				<div class="navTab-panel tabsPageContent">
				
					<div>

						<div style="width:1280px;height:480px;" id="map">
							<div style="float:left;width:900px;height:480px; margin:10px; border:1px solid gray" id="baidumap_idx">
								<script type="text/javascript">
								var baidumap_idx = new BMap.Map("baidumap_idx");
								var recentTime = null;
								baidumap_idx.addControl(new BMap.NavigationControl());
								baidumap_idx.enableScrollWheelZoom(); 
								var point = new BMap.Point(116.4035,39.915);  
								baidumap_idx.centerAndZoom(point,2);
		
		
								function myFun(result){
								    var cityName = result.name;
								    baidumap_idx.centerAndZoom(cityName,11);
								}
								var myCity = new BMap.LocalCity();
								myCity.get(myFun);
								


								baidumap_idx.addEventListener("zoomend", function(){

									loadMap(baidumap_idx);
									});	

								baidumap_idx.addEventListener("dragend", function(){

									loadMap(baidumap_idx);
									
									});	
												
								</script>
							
							</div>
							<div class="accordion dwz-accordion" fillspace="sideBar" style="float:left;width:230px;height:480px;margin:10px;border:1px solid gray;">
									<div class="accordionHeader">
										<h2 class="collapsable"><span>Folder</span>司机列表
										<a href="load_driver_config.jsp" target="dialog" rel="dlg_page11" mask="true">设置</a>
										</h2>
									</div>
							
							
									<div class="accordionContent" style="height: 450px;border:0px;" id="load_driver"><!-- 设置高度出现滚动条 -->

									</div>							
							</div>

						</div>						
						<div style="width:1080px;height:5px;text-align:center;margin:1px;border:1px;" id="hint">
							----
						</div>
					</div>				

				</div>
			</div>
		</div>

		<div id="taskbar" style="left:0px; display:none;">
			<div class="taskbarContent">
				<ul></ul>
			</div>
			<div class="taskbarLeft taskbarLeftDisabled" style="display:none;">taskbarLeft</div>
			<div class="taskbarRight" style="display:none;">taskbarRight</div>
		</div>
		<div id="splitBar"></div>
		<div id="splitBarProxy"></div>
	</div>
	
	<div id="footer">Copyright &copy; 2013 <a href="#" target="dialog">研发团队</a></div>

<!--拖动效果-->
	<div class="resizable"></div>
<!--阴影-->
	<div class="shadow" style="width:508px; top:148px; left:296px;">
		<div class="shadow_h">
			<div class="shadow_h_l"></div>
			<div class="shadow_h_r"></div>
			<div class="shadow_h_c"></div>
		</div>
		<div class="shadow_c">
			<div class="shadow_c_l" style="height:296px;"></div>
			<div class="shadow_c_r" style="height:296px;"></div>
			<div class="shadow_c_c" style="height:296px;"></div>
		</div>
		<div class="shadow_f">
			<div class="shadow_f_l"></div>
			<div class="shadow_f_r"></div>
			<div class="shadow_f_c"></div>
		</div>
	</div>
	<!--遮盖屏幕-->
	<div id="alertBackground" class="alertBackground"></div>
	<div id="dialogBackground" class="dialogBackground"></div>

	<div id='background' class='background'></div>
	<div id='progressBar' class='progressBar'>数据加载中，请稍等...</div>

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16716654-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? ' https://ssl' : ' http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

</body>
</html>