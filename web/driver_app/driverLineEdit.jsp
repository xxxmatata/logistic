<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="index_up.jsp"%>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<form class="form-horizontal" name="form1" action="driverLineSave.action" method="post">
				<input type="hidden" name="driver_line.id" value="${driver_line.id}">
				<div class="control-group">
					 <label class="control-label" for="driver_line_origin_r_name">起点</label>
					<div class="controls">
							<div class="input-append">
								<input readonly type="text" class="span4" id="driver_line_origin_r_name" placeholder="选位置"
								name="driver_line.origin.r_name" value="${driver_line.origin.r_name}"
								/>
								<input type="hidden" id="driver_line_origin_id" name="driver_line.origin.id" value="${driver_line.origin.id}">
								<span class="add-on"><i class="icon-th" data-target="#fromModal" data-toggle="modal" onClick="javascript:a('driver_line_origin_id','driver_line_origin_r_name','${driver_line.origin.r_code}');"></i></span>
							</div>
					</div>
				</div>
				<div class="control-group">
					 <label class="control-label" for="driver_line_origin_r_name">终点</label>
					<div class="controls">
							<div class="input-append">
								<input readonly type="text" class="span4" id="driver_line_destination_r_name" placeholder="选位置"
								name="driver_line.destination.r_name" value="${driver_line.destination.r_name}"/>
								<input type="hidden" id="driver_line_destination_id" name="driver_line.destination.id" value="${driver_line.destination.id}">
								<span class="add-on"><i class="icon-th" data-target="#fromModal" data-toggle="modal" onClick="javascript:a('driver_line_destination_id','driver_line_destination_r_name','${driver_line.destination.r_code}');"></i></span>
							</div>
					</div>
				</div>
				<div class="control-group">
					 <label class="control-label" >运价</label>
					<div class="controls">
						<input style="height:30px" type="number" placeholder="0" name="driver_line.price" value="${driver_line.price}"/>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						 <button type="submit" class="btn">提交</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


<%@ include file="index_down.jsp"%>