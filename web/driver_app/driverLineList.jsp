<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="index_up.jsp"%>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
			<table class="table">
				<thead>
					<tr>
						<th>
							起点
						</th>
						<th>
							终点
						</th>
						<th>
							运价
						</th>
						<th>
							操作
						</th>
					</tr>
				</thead>
				<tbody>
				
				<c:forEach var="item" items="${driver_lines}" varStatus="s">
					<tr>
						<td>
							${item.origin.r_name}
						</td>
						<td>
							${item.destination.r_name}
						</td>
						<td>
							${item.price}
						</td>
						<td>
							<a href="driverLineEdit.action?driver_line.id=${item.id}">
								<button class="btn btn-primary" type="button">改 </button>
							</a>
							<a href="driverLineDelete.action?driver_line.id=${item.id}">
								<button class="btn btn-primary" type="button">删 </button>
							</a>
						</td>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<a href="driverLineEdit.action?driver_line.id=-1">
				<button class="btn btn-primary btn-block" type="button">增加</button>
			</a>
		</div>
	</div>
</div>

<%@ include file="index_down.jsp"%>