<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>

<%@ include file="index_up.jsp"%>

    <div class="container">

      <form name="form1" class="form-signin" method="post" action="driverModifyPassword.action">
        <h5 class="form-signin-heading">请输入新密码：</h5>
        <input name="driver_info.newPassword" id="mima" type="password" required class="input-block-level" placeholder="请输入新密码：">
        <h5 class="form-signin-heading">请再次输入新密码：</h5>
        <input name="mima1" id="mima1" type="password" required class="input-block-level" placeholder="请再次输入新密码：">

        <button class="btn btn-large btn-primary" type="button" onClick="modify();">修改</button>

      </form>

    </div> <!-- /container -->
	<script type="text/javascript">
		function modify(){
			if ($("#mima").val() != $("#mima1").val()) {
				alert("两次密码输入不一样，请确认！");
				
			}else{
				form1.submit();
			}
		}
	</script>    

<%@ include file="index_down.jsp"%>