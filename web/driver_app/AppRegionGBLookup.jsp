<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main_driver.jsp"%>

<script type="text/javascript">
	function bringBack(id,name){

		$('#'+lookupId).val(id);
		$('#'+lookupName).val(name);

	}

	function c(url){
		$.post(url , function(data){
			
			$("#aaa").html(data);
		});
	}		
</script>

<div class="searchBar">
	<table>
	<tr>
	<td>
		<a href="javascript:c('AppExRegionGBLookup.action?lookup=1&pCode=${regionGB.parent_r_code}')">返回</a>
		->${regionGB.r_name}
	</td>
	<td>
		<a data-dismiss="modal" onClick="javascript:bringBack('${regionGB.id}', '${regionGB.r_shortName}')" >
		<button class="btn btn-small btn-primary" type="button">选择</button>
		</a>
	</td>
	</tr>
	</table>
</div>

	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
				
		<tbody>
			<% int i = 0; %>
			<tr>
			<c:forEach var="item" items="${regionGBs}" varStatus="s">
			<%i++; %>
				    <td><a href="javascript:c('AppExRegionGBLookup.action?lookup=1&pCode=${item.r_code}')">${item.r_shortName}</a></td>
			<%if (i%3==0){ %>
			</tr>
			<tr>
			<%}%>
			</c:forEach>
		</tbody>
	</table>	

</div>
