<%@ include file="../includes/main_driver.jsp"%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<title>货源列表</title>

<script type="text/javascript" src="iscroll/iscroll.js"></script>
<script src="assets/js/jquery.js"></script>
<link href="iscroll/iscroll.css" rel="stylesheet">

<script type="text/javascript">

var myScroll,
	pullDownEl, pullDownOffset,
	pullUpEl, pullUpOffset,currentPage = 1,
	generatedCount = 0;

function pullDownAction () {
	setTimeout(function () {	// <-- Simulate network congestion, remove setTimeout from production!
		var el, li, i;
		el = document.getElementById('thelist');

		for (i=0; i<3; i++) {
			li = document.createElement('li');
			li.innerText = 'Generated row ' + (++generatedCount);
			el.insertBefore(li, el.childNodes[0]);
		}
		
		myScroll.refresh();		// Remember to refresh when contents are loaded (ie: on ajax completion)
	}, 1000);	// <-- Simulate network congestion, remove setTimeout from production!
}

function pullUpAction () {
	currentPage++;

	
	$.post("goodsList3.action",
			{"goodsPageInfo.currentPage":currentPage,"goodsPageInfo.origin.f_r_name_path" :"${goodsPageInfo.origin.f_r_name_path}","goodsPageInfo.destination.f_r_name_path":"${goodsPageInfo.destination.f_r_name_path}"	},
			function(result){
				$("#thelist").append(result);
			});
		
	myScroll.refresh();	
}

function loaded() {
	pullDownEl = document.getElementById('pullDown');
	pullDownOffset = pullDownEl.offsetHeight;
	pullUpEl = document.getElementById('pullUp');	
	pullUpOffset = pullUpEl.offsetHeight;
	
	myScroll = new iScroll('wrapper', {
		useTransition: true,
		topOffset: pullDownOffset,
		onRefresh: function () {
			if (pullDownEl.className.match('loading')) {
				pullDownEl.className = '';
				pullDownEl.querySelector('.pullDownLabel').innerHTML = '下拉刷新...';
			} else if (pullUpEl.className.match('loading')) {
				pullUpEl.className = '';
				pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
			}
		},
		onScrollMove: function () {
			if (this.y > 5 && !pullDownEl.className.match('flip')) {
				pullDownEl.className = 'flip';
				pullDownEl.querySelector('.pullDownLabel').innerHTML = '松手刷新...';
				this.minScrollY = 0;
			} else if (this.y < 5 && pullDownEl.className.match('flip')) {
				pullDownEl.className = '';
				pullDownEl.querySelector('.pullDownLabel').innerHTML = '下拉刷新...';
				this.minScrollY = -pullDownOffset;
			} else if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
				pullUpEl.className = 'flip';
				pullUpEl.querySelector('.pullUpLabel').innerHTML = '松手刷新...';
				this.maxScrollY = this.maxScrollY;
			} else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
				pullUpEl.className = '';
				pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多...';
				this.maxScrollY = pullUpOffset;
			}
		},
		onScrollEnd: function () {
			if (pullDownEl.className.match('flip')) {
				pullDownEl.className = 'loading';
				pullDownEl.querySelector('.pullDownLabel').innerHTML = '加载中...';				
				pullDownAction();	// Execute custom function (ajax call?)
			} else if (pullUpEl.className.match('flip')) {
				pullUpEl.className = 'loading';
				pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中...';				
				pullUpAction();	// Execute custom function (ajax call?)
			}
		}
	});
	
	setTimeout(function () { document.getElementById('wrapper').style.left = '0'; }, 800);
}

document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);

document.addEventListener('DOMContentLoaded', function () { setTimeout(loaded, 200); }, false);
</script>


</head>
<body>

	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />

<div id="header"><a href="javascript:history.back(-1)">返回</a></div>
<div id="wrapper">
	<div id="scroller">
	 
		<div id="pullDown">
			<!--
			<span class="pullDownIcon"></span><span class="pullDownLabel">下拉刷新...</span>
			 -->
		</div>

		<ul id="thelist">
			<c:forEach var="item" items="${goods_infos}" varStatus="s">		
			<li>
              <strong>
              ${item.logistic_corp.orgName}：${item.origin.r_name}-->${item.destination.r_name} 
              </strong>

           	有  ${item.goodsType.goodsTypeName} 
           	需 ${item.truck_type.truck_type_name} 车${item.truckNumber} 辆，还差${item.needNum}辆。
           	${item.freightRate}（元）(/车)
           	请于 ${item.loadTime} 到 ${item.loadPlace} 装货。
           	联系人 ${item.logistic_corp.corp_name}
       		联系电话 
           	<a href="tel:${item.logistic_corp.corp_mobile}" >${item.logistic_corp.corp_mobile}</a>,
           	<a href="tel:${item.logistic_corp.corp_phone}" >${item.logistic_corp.corp_phone}</a>

			</li>

			</c:forEach>  
		</ul>
		<div id="pullUp">
			<span class="pullUpIcon"></span><span class="pullUpLabel">上拉加载更多...</span>
		</div>
	</div>
</div>
<div id="footer"><a href="javascript:history.back(-1)">返回</a></div>

</body>
</html>