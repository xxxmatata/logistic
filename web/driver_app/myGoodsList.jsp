<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="index_up.jsp"%>

    <div class="container-fluid">
      <div class="row-fluid">

        <div class="span9">

			<c:if test="${not empty currDriver2goods}">
			
				<!-- Modal -->
				<form name="fromModal_unload" action="driverUnload.action" method="post">
				<input type="hidden" name="currDriver2goods.id" value="${currDriver2goods.id}">
				<div id="fromModal_unload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="myModalLabel">确认完成送货</h3>
				  </div>
				  <div class="modal-body">
				    <p>请不要虚假填报，否则将被禁止使用，确认完成送货？</p>
				  </div>
				  <div class="modal-footer">
				    <button class="btn btn-primary" type="submit">确认</button>
				    <button class="btn" data-dismiss="modal" aria-hidden="true" >关闭</button>
				  </div>
				</div>
				</form>
			
				<div>
		            <h4><a name="myGoods">已接货</a></h4>
		            <p>
		            	${currDriver2goods.goods_info.logistic_corp.orgName} 有 货物 ：${currDriver2goods.goods_info.goodsType.goodsTypeName} 
		            </p>
		            <p>
		            	${currDriver2goods.goods_info.origin.r_name}-->${currDriver2goods.goods_info.destination.r_name}
		            </p>
		            <p>
		            	请按时送达目的地。
		            </p>
		            <p>联系人 ${currDriver2goods.goods_info.logistic_corp.corp_name}</p>
		            <p>联系电话
		            	<a href="tel:${currDriver2goods.goods_info.logistic_corp.corp_mobile}" >${currDriver2goods.goods_info.logistic_corp.corp_mobile}</a>,
		            	<a href="tel:${currDriver2goods.goods_info.logistic_corp.corp_phone}" >${currDriver2goods.goods_info.logistic_corp.corp_phone}</a>
		             </p>
		            <button data-target="#fromModal_unload" data-toggle="modal" class="btn btn-small btn-primary" type="submit">送到了</button>
				</div>
		    </c:if>     
		    <hr>   
			<c:if test="${not empty orderDriver2goods}">
			
				<!-- Modal -->
				<form name="fromModal_load" action="driverLoad.action" method="post">
				<input type="hidden" name="orderDriver2goods.id" value="${orderDriver2goods.id}">
				<div id="fromModal_load" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="myModalLabel">确认完成装货</h3>
				  </div>
				  <div class="modal-body">
				    <p>请不要虚假填报，否则将被禁止使用，确认完成装货？</p>
				  </div>
				  <div class="modal-footer">
				    <button class="btn btn-primary" type="submit"> 确认</button>
				    <button class="btn" data-dismiss="modal" aria-hidden="true" >关闭</button>
				  </div>
				</div>
				</form>
				
			
				<div>
		            <h4><a name="myGoods">已约定</a></h4>
		            <p>
		            	${orderDriver2goods.goods_info.logistic_corp.orgName} 有 货物 ：${orderDriver2goods.goods_info.goodsType.goodsTypeName} 
		            </p>
		            <p>
		            	${orderDriver2goods.goods_info.origin.r_name}-->${orderDriver2goods.goods_info.destination.r_name} 
		            </p>		            <p>
		            	请 ${orderDriver2goods.goods_info.loadTime} 到 ${orderDriver2goods.goods_info.loadPlace} 装货。
		            </p>
		            <p>联系人 ${orderDriver2goods.goods_info.logistic_corp.corp_name}</p>
		            <p>联系电话 
		            	<a href="tel:${orderDriver2goods.goods_info.logistic_corp.corp_mobile}" >${orderDriver2goods.goods_info.logistic_corp.corp_mobile}</a>,
		            	<a href="tel:${orderDriver2goods.goods_info.logistic_corp.corp_phone}" >${orderDriver2goods.goods_info.logistic_corp.corp_phone}</a>
		            </p>
		            <button data-target="#fromModal_load" data-toggle="modal" class="btn btn-small btn-primary" type="submit">装好了</button>
				</div>
				<hr>
 		    </c:if>        
    
        </div><!--/span-->
      </div><!--/row-->
      <hr>
    </div><!--/.fluid-container-->


<%@ include file="index_down.jsp"%>