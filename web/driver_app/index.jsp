<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="index_up.jsp"%>

    <div class="container-fluid">
      <div class="row-fluid">

        <div class="span9">

			<c:choose>
			<c:when test="${not empty currDriver2goods}">
			
				<!-- Modal -->
				<form name="fromModal_unload" action="driverUnload.action" method="post">
				<input type="hidden" name="currDriver2goods.id" value="${currDriver2goods.id}">
				<div id="fromModal_unload" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="myModalLabel">确认完成送货</h3>
				  </div>
				  <div class="modal-body">
				    <p>请不要虚假填报，否则将被禁止使用，确认完成送货？</p>
				  </div>
				  <div class="modal-footer">
				    <button class="btn btn-primary" type="submit">确认</button>
				    <button class="btn" data-dismiss="modal" aria-hidden="true" >关闭</button>
				  </div>
				</div>
				</form>
			
				<div>
		            <legend>已装货</legend>
		            <h4>
		            	${currDriver2goods.goods_info.logistic_corp.orgName} 有 货物 ：${currDriver2goods.goods_info.goodsType.goodsTypeName} 
		            </h4>
		            <h4>
		            	${currDriver2goods.goods_info.origin.r_name}-->${currDriver2goods.goods_info.destination.r_name}
		            </h4>
		            <h4>
		            	请按时送达目的地。
		            </h4>
		            <h4>
		            	联系人 ${currDriver2goods.goods_info.logistic_corp.corp_name}
		            </h4>
		            <h4>
		            	联系电话
		            	<a href="tel:${currDriver2goods.goods_info.logistic_corp.corp_mobile}" >${currDriver2goods.goods_info.logistic_corp.corp_mobile}</a>,
		            	<a href="tel:${currDriver2goods.goods_info.logistic_corp.corp_phone}" >${currDriver2goods.goods_info.logistic_corp.corp_phone}</a>
		            </h4>
		            <button data-target="#fromModal_unload" data-toggle="modal" class="btn btn-primary" type="submit">送到了</button>
				</div>
		    </c:when> 
		    <c:otherwise>
				<div>
		            <legend>尚未装货</legend>
		        </div>		    
		    </c:otherwise>
		    </c:choose>    
		    <hr> 
		    <c:choose>  
			<c:when test="${not empty orderDriver2goods}">
			
				<!-- Modal -->
				<form name="fromModal_load" action="driverLoad.action" method="post">
				<input type="hidden" name="orderDriver2goods.id" value="${orderDriver2goods.id}">
				<div id="fromModal_load" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="myModalLabel">确认完成装货</h3>
				  </div>
				  <div class="modal-body">
				    <p>请不要虚假填报，否则将被禁止使用，确认完成装货？</p>
				  </div>
				  <div class="modal-footer">
				    <button class="btn btn-primary" type="submit"> 确认</button>
				    <button class="btn" data-dismiss="modal" aria-hidden="true" >关闭</button>
				  </div>
				</div>
				</form>
				
			
				<div>
		            <legend>已约定</legend>
		            <h4>
		            	${orderDriver2goods.goods_info.logistic_corp.orgName} 有 货物 ：${orderDriver2goods.goods_info.goodsType.goodsTypeName} 
		            </h4>
		            <h4>
		            	${orderDriver2goods.goods_info.origin.r_name}-->${orderDriver2goods.goods_info.destination.r_name} 
		            </h4>		            
		            <h4>
		            	请 ${orderDriver2goods.goods_info.loadTime} 到 ${orderDriver2goods.goods_info.loadPlace} 装货。
		            </h4>
		            <h4>联系人 ${orderDriver2goods.goods_info.logistic_corp.corp_name}</h4>
		            <h4>联系电话 
		            	<a href="tel:${orderDriver2goods.goods_info.logistic_corp.corp_mobile}" >${orderDriver2goods.goods_info.logistic_corp.corp_mobile}</a>,
		            	<a href="tel:${orderDriver2goods.goods_info.logistic_corp.corp_phone}" >${orderDriver2goods.goods_info.logistic_corp.corp_phone}</a>
		            </h4>
		            <button data-target="#fromModal_load" data-toggle="modal" class="btn btn-primary" type="submit">装好了</button>
				</div>
		    </c:when> 
 		    <c:otherwise>
				<div>
		            <h4>还没接生意，快去找找货源</h4>
		            <a href="goodsList.action">
		            	<button class="btn btn-primary" type="button">找货源 </button>
		            </a>
		        </div>		    
				<hr>
  		    </c:otherwise>       
    		</c:choose>
    		<hr>
			<div>
			<legend>发布行程</legend>
			
			<c:choose>
			
			   <c:when test="${not empty driver_point}">
			   
				<!-- Modal -->
				<form name="fromModal_deleteTrip" action="driver_driverTripDelete.action" method="post">
				<input type="hidden" name="driver_point.id" value="${driver_point.id}">
				<div id="fromModal_deleteTrip" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="myModalLabel">确认</h3>
				  </div>
				  <div class="modal-body">
				    <p>是否删除？</p>
				  </div>
				  <div class="modal-footer">
				    <button class="btn btn-primary" type="submit"> 确认</button>
				    <button class="btn" data-dismiss="modal" aria-hidden="true" >关闭</button>
				  </div>
				</div>
				</form>			   
				<h4>
			   <ul class="breadcrumb">
			   	<li>
				本人<b> <fmt:formatDate value="${driver_point.trip_date}" pattern="yyyy-MM-dd"/></b>
				 在 ：<b>${driver_point.trip_region.r_name_path} </b>
				 </li>
				</ul>
				</h4>
				<button data-target="#fromModal_deleteTrip" data-toggle="modal" class="btn btn-primary" type="submit">删除</button>

				<a href="driver_driverTripEdit.action?driver_point.id=${driver_point.id}">
					<button class="btn btn-primary" type="submit" href="driver_driverTripEdit.action"> 修改</button>
				</a>
			   </c:when>
			   
			   <c:otherwise>
			   <a href="driver_driverTripEdit.action?driver_point.id=-1">
			   		<button class="btn btn-primary" type="button">新增 </button>
			   </a>
			   </c:otherwise>
			
			</c:choose>
			</div>

        </div><!--/span-->
      </div><!--/row-->
      <hr>
    </div><!--/.fluid-container-->


<%@ include file="index_down.jsp"%>