<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="index_up.jsp"%>

    <div class="container-fluid">
      <div class="row-fluid">

        <div class="span9">

    
			<div>
			<h4><a name="trip">发布行程</a></h4>
			
			<c:choose>
			
			   <c:when test="${not empty driver_trip}">
			   
				<!-- Modal -->
				<form name="fromModal_deleteTrip" action="driver_driverTripDelete.action" method="post">
				<input type="hidden" name="driver_trip.id" value="${driver_trip.id}">
				<div id="fromModal_deleteTrip" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				    <h3 id="myModalLabel">确认</h3>
				  </div>
				  <div class="modal-body">
				    <p>是否删除？</p>
				  </div>
				  <div class="modal-footer">
				    <button class="btn btn-primary" type="submit"> 确认</button>
				    <button class="btn" data-dismiss="modal" aria-hidden="true" >关闭</button>
				  </div>
				</div>
				</form>			   
			   
				<p>
				本人<b> <fmt:formatDate value="${driver_trip.trip_date}" pattern="yyyy-MM-dd"/></b>
				 在 ：<b>${driver_trip.trip_region.r_name_path} </b>
				</p>
				<button data-target="#fromModal_deleteTrip" data-toggle="modal" class="btn btn-small btn-primary" type="submit">删除</button>

				<a href="driver_driverTripEdit.action?driver_trip.id=${driver_trip.id}">
					<button class="btn btn-small btn-primary" type="submit" href="driver_driverTripEdit.action"> 修改</button>
				</a>
			   </c:when>
			   
			   <c:otherwise>
			   <a href="driver_driverTripEdit.action?driver_trip.id=-1">
			   		<button class="btn btn-small btn-primary" type="button">新增 </button>
			   </a>
			   </c:otherwise>
			
			</c:choose>
			</div>


        </div><!--/span-->
      </div><!--/row-->
      <hr>
    </div><!--/.fluid-container-->


<%@ include file="index_down.jsp"%>