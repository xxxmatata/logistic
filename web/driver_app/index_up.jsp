<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="../includes/main_driver.jsp"%>
<%@ include file="../includes/styles_driver.jsp"%>
<!DOCTYPE html>
<html lang="en" onkeydown="preventBSK();">
  <head>
    <meta charset="utf-8">
    <title>找活-首页</title>
    <meta name="description" content="">
    <meta name="author" content="">
    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">    
    
    <!-- Le styles -->

    <style type="text/css">
      body {
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    
	<script type="text/javascript">
	var lookupId;
	var lookupName;
	
	function a(lkId,lkName,pCode){
		lookupId = lkId;
		lookupName = lkName;

		if (pCode==null){
			pCode = '000000';
		}
		$.post("AppExRegionGBLookup.action?pCode="+pCode , function(data){
			
			$("#aaa").html(data);
		});
	}

	function doQuery(){
		form1.submit();
	}


	function preventBSK(){  
        var bskEventCancel = false;  
        var _EVENT = window.event;  
        bskEventCancel = _EVENT && _EVENT.altKey && (_EVENT.keyCode == 8 || _EVENT.keyCode == 37 || _EVENT.keyCode == 39);  
        if(_EVENT.keyCode == 8){  
            var tagName = _EVENT.srcElement.tagName.toUpperCase();  
            if(tagName == "TEXTAREA" || tagName == "INPUT")//文本操作不受影响  
                bskEventCancel = _EVENT.srcElement.readOnly;  
            else  
                bskEventCancel = true;  
        }  
        _EVENT.cancelBubble = bskEventCancel;  
        _EVENT.returnValue = !bskEventCancel;  
      //  return !bskEventCancel;  
    }	
	</script>
  </head>

  <body onload="writeUserInfo('${driver_info.userName}');">

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">

        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" href="indexView.action">首页（${driver_info.fullName}）</a>
          <div class="nav-collapse collapse">

            <p class="navbar-text pull-right">
              	<a href="d_logout.action">退出登录</a>
            </p>
            <ul class="nav">
              <li><a href="goodsList.action">找货源</a></li>
              <li><a href="driverLineList.action">设置长跑线路</a></li>
              <!-- 
              <li><a href="myGoodsList.action">我的活</a></li>
              <li><a href="driverMyTruck.action">我的车</a></li>
              <li><a href="myTrip.action">发布行程</a></li>
              <li><a href="driverMyRecord.action">拉货记录</a></li>
               -->
              <li><a href="driverModifyPasswordView.action">修改密码</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

	<!-- Modal -->
	
	<div id="fromModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel">地区</h3>
	  </div>
	  <div class="modal-body" id="aaa">
	    <p>…</p>
	  </div>
	  <div class="modal-footer">
	    <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
	  </div>
	</div>




 	
