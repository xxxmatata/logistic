<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="index_up.jsp"%>
<form class="form-horizontal" action="driver_driverTripSave.action">
	<input type="hidden" name="driver_point.id" value="${driver_point.id}"/>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span9">
				<div>
		            <h4>发布行程</h4>
		            <p>
		            	填写本人什么时候，在什么位置。
		            </p>
		            
					<dl>
						<dt>
							时间：
						</dt>
						<dd>
		                <div class="input-append date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="driver_point_trip_date" data-link-format="yyyy-mm-dd">
		                    <input class="span9" size="16" type="text" placeholder="填写日期" readonly value="<fmt:formatDate value="${driver_point.trip_date}" pattern="yyyy-MM-dd"/>">
		                    <span class="add-on"><i class="icon-remove"></i></span>
							<span class="add-on"><i class="icon-th"></i></span>
		                </div>
						<input type="hidden" id="driver_point_trip_date" name="driver_point.trip_date" value="${driver_point.trip_date}" /><br/>
						</dd>
						<dt>
							位置：
						</dt>
						<dd>
							<div class="input-append">
								<input readonly type="text" class="span4" id="driver_point_trip_region_r_name" placeholder="选位置"
								name="driver_point.trip_region.r_name" value="${driver_point.trip_region.r_name}"
								/>
								<input type="hidden" id="driver_point_trip_region_id" name="driver_point.trip_region.id" value="${driver_point.trip_region.id}">
								<span class="add-on"><i class="icon-th" data-target="#fromModal" data-toggle="modal" onClick="javascript:a('driver_point_trip_region_id','driver_point_trip_region_r_name','${driver_point.trip_region.r_code}');"></i></span>
							</div>
						</dd>
					</dl>



		            <button class="btn btn-primary" type="submit">保存</button>
				</div>
        </div><!--/span-->
      </div><!--/row-->
      <hr>
    </div><!--/.fluid-container-->
</form>
	<script type="text/javascript">
	$('.form_date').datetimepicker({
			language:  'zh-CN',
	        weekStart: 1,
	        todayBtn:  1,
			autoclose: 1,
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0
	    });
	</script>	
<%@ include file="index_down.jsp"%>