<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main_driver.jsp"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>找活-登录</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->


    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>


  </head>

  <body>
	<br>
    <div class="container">

      <form class="form-signin" action="driverLogin.action" method="post">
        <h2 class="form-signin-heading">找 活</h2>
        <h5 class="form-signin-heading">请登录：</h5>
        <input name="driverPageInfo.f_userName" type="number" class="input-block-level" required placeholder="请输入注册手机号码" />
        <input name="mima" type="password" class="input-block-level" placeholder="密  码" />
		<!-- 
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> 记住密码
        </label>
         -->
        <button class="btn btn-large btn-primary" type="submit">登录</button>
     </form>

    </div> <!-- /container -->
    <div class="tishi">
		<c:if test="${not empty errorMsg}">
		        你没有登陆成功，请再试一次。<br><br>
		        原因: ${errorMsg}<br>
	    </c:if>
    </div>
	<%@ include file="/includes/styles_driver.jsp"%>

  </body>

</html>
