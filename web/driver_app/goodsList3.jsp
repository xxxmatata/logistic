<%@ include file="../includes/main_driver.jsp"%>
<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>

			<c:forEach var="item" items="${goods_infos}" varStatus="s">		
			<li>
              <strong>
              ${item.logistic_corp.orgName}：${item.origin.r_name}-->${item.destination.r_name} 
              </strong>

           	有  ${item.goodsType.goodsTypeName} 
           	需 ${item.truck_type.truck_type_name} 车${item.truckNumber} 辆，还差${item.needNum}辆。
           	${item.freightRate}（元）(/车)
           	请于 ${item.loadTime} 到 ${item.loadPlace} 装货。
           	联系人 ${item.logistic_corp.corp_name}
       		联系电话 
           	<a href="tel:${item.logistic_corp.corp_mobile}" >${item.logistic_corp.corp_mobile}</a>,
           	<a href="tel:${item.logistic_corp.corp_phone}" >${item.logistic_corp.corp_phone}</a>

			</li>

			</c:forEach>  
