<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="regionGBList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="searchBar">
	<table>
	<tr>
	<td>
		<a target="dialog" href="ExRegionGBLookup.action?lookup=1&pCode=${regionGB.parent_r_code}">返回</a>
		->${regionGB.r_name}
	</td>
	<td>
		<a class="btnSelect" href="javascript:$.bringBack({id:'${regionGB.id}', r_shortName:'${regionGB.r_shortName}'})" title="<hi:text key="选择地区"/>"><hi:text key="选择"/></a>
	</td>
	</tr>
	</table>
</div>
	<li>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
				
		<tbody>
			<% int i = 0; %>
			<tr>
			<c:forEach var="item" items="${regionGBs}" varStatus="s">
			<%i++; %>
				    <td><a target="dialog" href="ExRegionGBLookup.action?lookup=1&pCode=${item.r_code}">${item.r_shortName}</a></td>
			<%if (i%3==0){ %>
			</tr>
			<tr>
			<%}%>
			</c:forEach>
		</tbody>
	</table>	

</div>
