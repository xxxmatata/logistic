<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="编辑页面" parameterLanguageKeys="司机信息"/></h2>
<form action="logistic_driver_infoSave.action?navTabId=logistic_driver_infoList&callbackType=closeCurrent&ajax=1" method="post" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone)">
<div class="pageContent">
	<div class="pageFormContent" layoutH="97">
		<dl>
			<dt><hi:text key="注册手机号码" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.userName" class="mobile textInput required" value="${driver_info.userName}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="姓名" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.fullName" class="textInput required" value="${driver_info.fullName}" maxlength="30"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="身份证" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.SSN" class="textInput" value="${driver_info.SSN}" maxlength="50"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="驾驶证附件" entity="Driver_info"/>：</dt>
			<dd>
				<input type="hidden" name="driver_info.drive_lic_file_attachment.id" value="${driver_info.drive_lic_file_attachment.id}"/>
				<input type="text" class="textInput" name="driver_info.hi_drive_lic_file_attachment.fileName" value="${driver_info.drive_lic_file_attachment.fileName}" readonly="readonly"/>
				<a class="btnAttach" href="<hi:url>attachmentLookup.action?lookup=1&from=attachment&saveType=1</hi:url>" lookupGroup="driver_info" lookupName="drive_lic_file_attachment" width="560" height="300" title="<hi:text key="附件"/>"><hi:text key="附件"/></a>
				<c:if test="${not empty driver_info.drive_lic_file_attachment}">
				<a class="btnView" href="attachmentView.action?attachment.id=${driver_info.drive_lic_file_attachment.id}" target="_blank">
					<hi:text key="查看"/>
				</a>
				</c:if>			
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="性别" entity="Driver_info"/>：</dt><dd><hi:select emu="gender" name="driver_info.gender"/></dd>			
		</dl>
		<dl>
			<dt><hi:text key="地址" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.address" class="textInput" value="${driver_info.address}" maxlength="200"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="其他联系电话" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.phone" class="textInput" value="${driver_info.phone}" maxlength="50"/></dd>
		</dl>
		
		<dl>
			<dt><hi:text key="备注" entity="Driver_info"/>：</dt><dd><input type="text" name="driver_info.description" class="textInput" value="${driver_info.description}" maxlength="500"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="对应车辆" entity="Driver_info"/>：</dt>
			<dd>
				<input type="hidden" name="driver_info.truck_info.id" value="${driver_info.truck_info.id}"/>
				<input type="text" readonly="readonly" class="textInput required" name="driver_info.hi_truck_info.truck_card" value="${driver_info.truck_info.truck_card}"/>
				<a class="btnLook" href="<hi:url>noDriverTruck_infoLookup.action?lookup=1</hi:url>" lookupGroup="driver_info" lookupName="truck_info"><hi:text key="查找带回"/></a>		
			</dd>
		</dl>
		<dl>
			<dt><hi:text key="当前地区" entity="Driver_info"/>：</dt>
			<dd>
				<input type="hidden" name="driver_info.region_id.id" value="${driver_info.region_id.id}"/>
				<input type="text" readonly="readonly" class="textInput required" name="driver_info.hi_region_id.r_shortName" value="${driver_info.region_id.r_shortName}"/>
				<a class="btnLook" href="<hi:url>ExRegionGBLookup.action?lookup=1</hi:url>&pCode=${driver_info.region_id.r_code}" lookupGroup="driver_info" lookupName="region_id"><hi:text key="查找带回"/></a>
			</dd>
		</dl>
		
				<input type="hidden" name="driver_info.id" value="${driver_info.id}"/>
				<input type="hidden" name="driver_info.drive_lic_file_attachment" value="${driver_info.drive_lic_file_attachment}"/>
				<input type="hidden" name="driver_info.truck_info.id" value="${driver_info.truck_info.id}"/>
				<input type="hidden" name="driver_info.password" value="${driver_info.password}"/>
				<input type="hidden" name="driver_info.creator.id" value="${driver_info.creator.id}"/>
				<input type="hidden" name="driver_info.deleted" value="${driver_info.deleted}"/>
				<input type="hidden" name="driver_info.version" value="${driver_info.version}"/>
				
			<input type="hidden" name="driver_info.info_state" value="100600"/>
			<input type="hidden" name="driver_info.accountEnabled" value="0"/>		
			<input type="hidden" name="driver_info.accountLocked" value="0"/>		
			<input type="hidden" name="driver_info.credentialsExpired" value="0"/>
			<input type="hidden" name="driver_info.userMgrType" value="1403"/>	
		<div class="divider"></div>
			</div>
	<div class="formBar">
		<ul>
				<c:choose>
					<c:when test="${editable}">		
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="保存"/></button></div></div></li>
					</c:when>
					<c:otherwise>
					</c:otherwise>
				</c:choose>
			
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>  
</div>
</form>
