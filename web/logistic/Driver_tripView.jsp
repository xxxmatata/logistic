<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="司机行程"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
				
		<dl>
			<dt><hi:text key="信息状态" entity="Driver_trip"/>：</dt><dd><hi:select emu="info_state" name="driver_trip.driver_id.info_state" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="时间" entity="Driver_trip"/>：</dt><dd><fmt:formatDate value="${driver_trip.trip_date}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>
		<dl>
			<dt><hi:text key="地区简称" entity="Driver_trip"/>：</dt><dd>${driver_trip.trip_region.r_shortName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="发布时间" entity="Driver_trip"/>：</dt><dd><fmt:formatDate value="${driver_trip.publish_time}" pattern="yyyy-MM-dd HH:mm:ss"/></dd>				  	 
		</dl>

		<div class="divider"></div>
			</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
