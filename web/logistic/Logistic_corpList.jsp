<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="logistic_logistic_corpList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="logistic_logistic_corpList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
			<li>
				<label><hi:text key="公司名称" entity="Logistic_corp"/>:</label>
				<input type="text" name="pageInfo.f_orgName" value="${pageInfo.f_orgName}"/>
			</li>	  
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="LOGISTIC_CORP_SAVE"><li><a class="add" href="<hi:url>logistic_logistic_corpEdit.action?logistic_corp.id=-1</hi:url>" target="navTab" rel="logistic_corp"><span><hi:text key="新建" parameterLanguageKeys="物流公司"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="LOGISTIC_CORP_DEL"><li><a class="delete" href="<hi:url>logistic_corpRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', address:'',op_lic:'',op_lic_file:'',tax_lic:'',tax_lic_file:'',corp_name:'',corp_phone:'',corp_mobile:'',info_state:'',orgName:'',orgNum:'',managerName:'',parentOrgName:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="orgName" class="${pageInfo.sorterName eq 'orgName' ? pageInfo.sorterDirection : ''}"><hi:text key="公司名称" entity="Logistic_corp"/></th>
				<th orderField="address" class="${pageInfo.sorterName eq 'address' ? pageInfo.sorterDirection : ''}"><hi:text key="公司地址" entity="Logistic_corp"/></th>
				<th orderField="op_lic" class="${pageInfo.sorterName eq 'op_lic' ? pageInfo.sorterDirection : ''}"><hi:text key="营业执照编号" entity="Logistic_corp"/></th>
				<th orderField="op_lic_file_attachment.fileName" class="${pageInfo.sorterName eq 'op_lic_file_attachment.fileName' ? pageInfo.sorterDirection : ''}"><hi:text key="营业执照附件" entity="Logistic_corp"/></th>
				<th orderField="tax_lic" class="${pageInfo.sorterName eq 'tax_lic' ? pageInfo.sorterDirection : ''}"><hi:text key="税务登记号" entity="Logistic_corp"/></th>
				<th orderField="tax_lic_file_attachment.fileName" class="${pageInfo.sorterName eq 'tax_lic_file_attachment.fileName' ? pageInfo.sorterDirection : ''}"><hi:text key="税务登记证附件" entity="Logistic_corp"/></th>
				<th orderField="corp_name" class="${pageInfo.sorterName eq 'corp_name' ? pageInfo.sorterDirection : ''}"><hi:text key="法人" entity="Logistic_corp"/></th>
				<th orderField="corp_phone" class="${pageInfo.sorterName eq 'corp_phone' ? pageInfo.sorterDirection : ''}"><hi:text key="公司电话" entity="Logistic_corp"/></th>
				<th orderField="corp_mobile" class="${pageInfo.sorterName eq 'corp_mobile' ? pageInfo.sorterDirection : ''}"><hi:text key="手机" entity="Logistic_corp"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${logistic_corps}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td>${item.orgName}</td>
				    <td>${item.address}</td>
				    <td>${item.op_lic}</td>
				    <td>
						<c:if test="${not empty item.op_lic_file_attachment}">
				   		<a href="<hi:url>attachmentView.action?attachment.id=${item.op_lic_file_attachment.id}</hi:url>" target="_blank">${item.op_lic_file_attachment.fileNameImage}</a>
				     	</c:if>
				   </td>		    
				    <td>${item.tax_lic}</td>
				    <td>
						<c:if test="${not empty item.tax_lic_file_attachment}">
				   		<a href="<hi:url>attachmentView.action?attachment.id=${item.tax_lic_file_attachment.id}</hi:url>" target="_blank">${item.tax_lic_file_attachment.fileNameImage}</a>
				     	</c:if>
				   </td>		    
				    <td>${item.corp_name}</td>
				    <td>${item.corp_phone}</td>
				    <td>${item.corp_mobile}</td>

				<td>
				<c:choose>
					<c:when test="${empty lookup}">
				    <authz:authorize ifAnyGranted="LOGISTIC_CORP_DEL">
				      <a class="btnDel" href="<hi:url>logistic_corpRemove.action?ajax=1&logistic_corp.id=${item.id}</hi:url>" target="navTabTodo" title="<hi:text key="删除" parameterLanguageKeys="物流公司"/>"><hi:text key="删除"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="LOGISTIC_CORP_VIEW">
				      <a class="btnView" href="<hi:url>logistic_logistic_corpView.action?logistic_corp.id=${item.id}</hi:url>" target="navTab" rel="logistic_corp" title="<hi:text key="查看" parameterLanguageKeys="物流公司"/>"><hi:text key="查看"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="LOGISTIC_CORP_SAVE">
				      <a class="btnEdit" href="<hi:url>logistic_logistic_corpEdit.action?logistic_corp.id=${item.id}</hi:url>" target="navTab" rel="logistic_corp" title="<hi:text key="编辑" parameterLanguageKeys="物流公司"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
					<c:otherwise>
						<a class="btnSelect" href="javascript:$.bringBack({id:'${item.id}', address:'${item.address}',op_lic:'${item.op_lic}',op_lic_file:'${item.op_lic_file_attachment.fileName}',tax_lic:'${item.tax_lic}',tax_lic_file:'${item.tax_lic_file_attachment.fileName}',corp_name:'${item.corp_name}',corp_phone:'${item.corp_phone}',corp_mobile:'${item.corp_mobile}',info_state:'<hi:select emu="info_state" name="logistic_corps[${s.index}].info_state" isLabel="true"/>',orgName:'${item.orgName}',orgNum:'${item.orgNum}',managerName:'${item.manager.fullName}',parentOrgName:'${item.parentOrg.orgName}'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
