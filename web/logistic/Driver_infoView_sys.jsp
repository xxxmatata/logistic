<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="司机信息"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
		<dl>
			<dt><hi:text key="注册手机" entity="Driver_info"/>：</dt><dd>${driver_info.userName}</dd>
		</dl>				
		<dl>
			<dt><hi:text key="姓名" entity="Driver_info"/>：</dt><dd>${driver_info.fullName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="身份证" entity="Driver_info"/>：</dt><dd>${driver_info.SSN}</dd>
		</dl>
		<dl>
			<dt><hi:text key="所属公司" entity="Driver_info"/>：</dt><dd>${driver_info.org.orgName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="驾驶证附件" entity="Driver_info"/>：</dt>
			<dd>
				<c:if test="${not empty driver_info.drive_lic_file_attachment}">
				<a href="<hi:url>attachmentView.action?attachment.id=${driver_info.drive_lic_file_attachment.id}</hi:url>" target="_blank">
					${driver_info.drive_lic_file_attachment.fileNameImage}
				</a>
				</c:if>
			</dd>				  
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Driver_info"/>：</dt><dd><hi:select emu="info_state" name="driver_info.info_state" isLabel="true"/></dd>
		</dl>


		<dl>
			<dt><hi:text key="性别" entity="Driver_info"/>：</dt><dd><hi:select emu="gender" name="driver_info.gender" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="地址" entity="Driver_info"/>：</dt><dd>${driver_info.address}</dd>
		</dl>
		<dl>
			<dt><hi:text key="其他联系电话" entity="Driver_info"/>：</dt><dd>${driver_info.phone}</dd>
		</dl>
		<dl>
			<dt><hi:text key="描述" entity="Driver_info"/>：</dt><dd>${driver_info.description}</dd>
		</dl>

		<div class="divider"></div>
			</div>

	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
</div>
