<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<script type="text/javascript" src="http://api.map.baidu.com/api?v=1.3"></script>
    
<script type="text/javascript">
var linesPoints = new Array();
var linesPointsInfo = new Array();
var timeout ;

var isRun = true;

function p(){
	isRun = false;
}

function c(){
	isRun = true;
}

function queryTrace(){
	clearTimeout(timeout); 
	linesPoints = new Array();
	linesPointsInfo = new Array();
	var url = "baiduMaplbs_driverTrace.action?mdn="+$("#mdn").val()+"&begin="+$("#begin").val()+"&end="+$("#end").val();
	navTab.openTab("driver_trace",url,{title:'查询轨迹'});	
}
function showClick(e){

}

function run(baidumap_trace,linesPoints){
        var pts = linesPoints[0];
        var len = linesPoints.length;
        var carMk = new BMap.Marker(pts);
        baidumap_trace.addOverlay(carMk);

        resetMkPoint(0,len,linesPoints,carMk);
    
    function resetMkPoint(i,len,pts,carMk){
        carMk.setPosition(pts[i]);
    	$("#hint_trace").html(${mdn}+"-<p><b>" + linesPointsInfo[i].upd_time + "</b></p>-" + linesPointsInfo[i].loc_desc);
        if(i < len){
        	timeout = setTimeout(function(){
                if (isRun){
	                i++;
	                resetMkPoint(i,len,pts,carMk);
                }else{
                	resetMkPoint(i,len,pts,carMk);
                }
            },1000);
        }
    }
    
}
</script>

				<div class="navTab-panel tabsPageContent" id="div_trace">
				
					<div>

						<div style="width:1280px;height:480px;" >
							<div style="float:left;width:800px;height:480px; margin:10px; border:1px solid gray" id="baidumap_trace">

							<script type="text/javascript">

							var baidumap_trace = new BMap.Map("baidumap_trace");
							baidumap_trace.addControl(new BMap.NavigationControl());
							baidumap_trace.enableScrollWheelZoom(); 

							//设置最新位置点
							var point = new BMap.Point(${lng},${lat});  
							baidumap_trace.centerAndZoom(point,15);

							var marker = new BMap.Marker(point); 
							baidumap_trace.addOverlay(marker); 

							//绘制轨迹
							<c:if test="${!empty trace}">

							<c:forEach var="item" items="${trace}" varStatus="status">
								linesPoints.push(new BMap.Point(${item.lng},${item.lat}));	
								linesPointsInfo.push({"upd_time":"${item.upd_time}","loc_desc":"${item.loc_desc}"});
							</c:forEach>							
							
							var polyline = new BMap.Polyline(linesPoints, {strokeColor:"blue", strokeWeight:6, strokeOpacity:0.5});
								baidumap_trace.addOverlay(polyline);	
								polyline.addEventListener("click",showClick);
								
								baidumap_trace.setViewport(polyline.getPath());

								run(baidumap_trace,linesPoints,linesPointsInfo);
								
							</c:if>
							
							</script>
							
							</div>
							<div class="accordion dwz-accordion" fillspace="sideBar" style="float:left;width:230px;height:480px;margin:10px;border:1px solid gray;">
									<div class="accordionHeader">
										<h2 class="collapsable"><span>Folder</span>查询条件
										</h2>
									</div>
							
							<form rel="driver_trace" method="post" action="baiduMaplbs_driverTrace.action?mdn=${mdn}">
									<input type="hidden" id="mdn" name="mdn" value="${mdn}">
									<div class="accordionContent" style="height: 450px;border:0px;" id="load_driver"><!-- 设置高度出现滚动条 -->
										<div style="margin:10px">
											<hi:text key="开始时间" entity=""/>：
											<input type="text" id="begin" name="begin" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
												value="<fmt:formatDate value='${begin}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
											<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
										</div>
										<div style="margin:10px">
											<hi:text key="结束时间" entity=""/>：
											<input type="text" id="end" name="end" class="textInput date" readonly="readonly" pattern="yyyy-MM-dd HH:mm:ss"
												value="<fmt:formatDate value='${end}' pattern='yyyy-MM-dd HH:mm:ss'/>"/>
											<a href="javascript:void(0)" class="inputDateButton"><hi:text key="选择"/></a>
										</div>

										<div style="margin:10px;text-align:center">
											<div class="buttonActive"><div class="buttonContent"><button type="button" onclick="queryTrace();"><hi:text key="查询"/></button></div></div>
											<div class="buttonActive"><div class="buttonContent"><button type="button" onclick="p();"><hi:text key="暂停"/></button></div></div>
											<div class="buttonActive"><div class="buttonContent"><button type="button" onclick="c();"><hi:text key="继续"/></button></div></div>
										</div>
										<br>
											<div style="height:180px;margin-top:100px" id="hint_trace">
											
											</div>	
									</div>
							</form>	
					
							</div>

						</div>

					</div>				

				</div>
