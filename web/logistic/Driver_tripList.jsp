<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="driver_tripList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="logistic_driver_tripList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
			<li>
				<label><hi:text key="行程地点" entity="Driver_info"/>:</label>
				<input type="text" name="pageInfo.trip_region.f_r_name_path" value="${pageInfo.trip_region.f_r_name_path}"/>
			</li>	  
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
				<authz:authorize ifAnyGranted="DRIVER_TRIP_SAVE"><li><a class="add" href="<hi:url>driver_tripEdit.action?driver_trip.id=-1</hi:url>" target="navTab" rel="driver_trip"><span><hi:text key="新建" parameterLanguageKeys="司机行程"/></span></a></li></authz:authorize>
				<authz:authorize ifAnyGranted="DRIVER_TRIP_DEL"><li><a class="delete" href="<hi:url>driver_tripRemoveAll.action?ajax=1</hi:url>" target="removeSelected" title="<hi:text key="确实要删除这些记录吗?"/>"><span><hi:text key="批量删除"/></span></a></li></authz:authorize>
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', info_state:'',trip_date:'',r_shortName:'',publish_time:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>
				<c:if test="${empty lookup}">
				<th width="22"><input type="checkbox" group="orderIndexs" class="checkboxCtrl"></th>
				</c:if>
				<th orderField="driver_id.fullName" class="${pageInfo.sorterName eq 'driver_id.fullName' ? pageInfo.sorterDirection : ''}"><hi:text key="司机姓名" entity="Driver_trip"/></th>
				<th orderField="driver_id.userName" class="${pageInfo.sorterName eq 'driver_id.userName' ? pageInfo.sorterDirection : ''}"><hi:text key="手机" entity="Driver_trip"/></th>
				<th orderField="trip_date" class="${pageInfo.sorterName eq 'trip_date' ? pageInfo.sorterDirection : ''}"><hi:text key="行程时间" entity="Driver_trip"/></th>
				<th orderField="trip_region.r_shortName" class="${pageInfo.sorterName eq 'trip_region.r_shortName' ? pageInfo.sorterDirection : ''}"><hi:text key="行程地点" entity="Driver_trip"/></th>
				<th orderField="org.orgName" class="${pageInfo.sorterName eq 'org.orgName' ? pageInfo.sorterDirection : ''}"><hi:text key="所属公司" entity="Driver_info"/></th>
				<th orderField="truck_info.truck_card" class="${pageInfo.sorterName eq 'truck_info.truck_card' ? pageInfo.sorterDirection : ''}"><hi:text key="对应车辆" entity="Driver_info"/></th>
				<th orderField="driverRank" class="${pageInfo.sorterName eq 'driverRank' ? pageInfo.sorterDirection : ''}"><hi:text key="累计服务评价" entity="Driver_info"/></th>
				<th orderField="publish_time" class="${pageInfo.sorterName eq 'publish_time' ? pageInfo.sorterDirection : ''}"><hi:text key="更新时间" entity="Driver_trip"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${driver_trips}" varStatus="s">
			<tr>
				<c:if test="${empty lookup}">
				<td><input name="orderIndexs" value="${item.id}" type="checkbox"></td>
				</c:if>			
				    <td>
				    	${item.driver_id.fullName}
					</td>
				    <td>
				    	${item.driver_id.userName}
					</td>
				    <td>
				    	<fmt:formatDate value="${item.trip_date}" pattern="yyyy-MM-dd"/></td>
				    <td>
				    	${item.trip_region.r_name_path}
					</td>
				    <td>${item.driver_id.org.orgName}
					</td>
				    <td><a href="<hi:url>logistic_truck_infoView.action?truck_info.id=${item.driver_id.truck_info.id}</hi:url>" target="dialog">
					${item.driver_id.truck_info.truck_card}</a>
					（<hi:select emu="truck_state" name="${item.driver_id.truck_info.truck_state}" isLabel="true"/>）
					
					</td>
				    <td>${item.driver_id.driverRank}</td>
					
				    <td><fmt:formatDate value="${item.publish_time}" pattern="yyyy-MM-dd"/></td>
				<td>

					<a class="btnSelect" href="javascript:$.bringBack({id:'${item.driver_id.id}',fullName:'${item.driver_id.fullName}',trip_date:'${item.trip_date}',r_shortName:'${item.trip_region.r_shortName}',publish_time:'${item.publish_time}'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
