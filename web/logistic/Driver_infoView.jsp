<%@ page language="java"  pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<h2 class="contentTitle"><hi:text key="查看页面"  parameterLanguageKeys="司机信息"/></h2>
<div class="pageContent">
	
	<div class="viewInfo" layoutH="97">
		<div class="tabs">
			<div class="tabsHeader">
				<div class="tabsHeaderContent">
					<ul>
						<li><a href="javascript:void(0)"><span><hi:text key="司机基本信息"/></span></a></li>
						<li><a href="javascript:void(0)"><span><hi:text key="司机长跑线路"/></span></a></li>
						<li><a href="javascript:void(0)"><span><hi:text key="司机位置发布"/></span></a></li>
					</ul>
				</div>
			</div>
			<div class="tabsContent" style="height:400px;">
				<div style="overflow: auto">

		<dl>
			<dt><hi:text key="注册手机" entity="Driver_info"/>：</dt>
			<dd>${driver_info.userName}</dd>
			<a target="dialog" rel="loc" href="baiduMaplbs_locateMobile.action?mdn=${driver_info.userName}" width=610 height=480 resizable=false maxable=false>定位</a>
		</dl>				
		<dl>
			<dt><hi:text key="姓名" entity="Driver_info"/>：</dt><dd>${driver_info.fullName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="身份证" entity="Driver_info"/>：</dt><dd>${driver_info.SSN}</dd>
		</dl>
		<dl>
			<dt><hi:text key="所属公司" entity="Driver_info"/>：</dt><dd>${driver_info.org.orgName}</dd>
		</dl>
		<dl>
			<dt><hi:text key="相关车辆" entity="Driver_info"/>：</dt>
				<dd>
				<a href="<hi:url>logistic_truck_infoView.action?truck_info.id=${driver_info.truck_info.id}</hi:url>" target="dialog">
				${driver_info.truck_info.truck_card}</a></dd>
		</dl>
		<dl>
			<dt><hi:text key="驾驶证附件" entity="Driver_info"/>：</dt>
			<dd>
				<c:if test="${not empty driver_info.drive_lic_file_attachment}">
				<a href="<hi:url>attachmentView.action?attachment.id=${driver_info.drive_lic_file_attachment.id}</hi:url>" target="_blank">
					${driver_info.drive_lic_file_attachment.fileNameImage}
				</a>
				</c:if>
			</dd>				  
		</dl>
		<dl>
			<dt><hi:text key="信息状态" entity="Driver_info"/>：</dt><dd><hi:select emu="info_state" name="driver_info.info_state" isLabel="true"/></dd>
		</dl>


		<dl>
			<dt><hi:text key="性别" entity="Driver_info"/>：</dt><dd><hi:select emu="gender" name="driver_info.gender" isLabel="true"/></dd>
		</dl>
		<dl>
			<dt><hi:text key="地址" entity="Driver_info"/>：</dt><dd>${driver_info.address}</dd>
		</dl>
		<dl>
			<dt><hi:text key="其他联系电话" entity="Driver_info"/>：</dt><dd>${driver_info.phone}</dd>
		</dl>
		<dl>
			<dt><hi:text key="描述" entity="Driver_info"/>：</dt><dd>${driver_info.description}</dd>
		</dl>
		<dl>
			<dt><hi:text key="服务评价" entity="Driver_info"/>：</dt><dd>${driver_info.driverRank}</dd>
		</dl>
		<dl>
			<dt><hi:text key="所在地区" entity="Driver_info"/>：</dt><dd>${driver_info.region_id.r_shortName}</dd>
		</dl>


				</div>
				<div style="overflow: auto">
					<table class="list" width="100%">
						<thead>
							<tr>
								<th><hi:text key="起点" entity="Driver_line"/></th>
								<th><hi:text key="终点" entity="Driver_line"/></th>
								<th><hi:text key="报价" entity="Driver_line"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${driver_info.driver_lines}">
							<tr>						
								<td>${item.origin.r_shortName}</td>
								<td>${item.destination.r_shortName}</td>
								<td>${item.price}</td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div style="overflow: auto">
					<table class="list" width="100%">
						<thead>
							<tr>
								<th><hi:text key="时间" entity="Driver_point"/></th>
								<th><hi:text key="地区简称" entity="Driver_point"/></th>
								<th><hi:text key="发布时间" entity="Driver_point"/></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${driver_info.driver_points}">
							<tr>						
								<td><fmt:formatDate value="${item.trip_date}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td>${item.trip_region.r_shortName}</td>
								<td><fmt:formatDate value="${item.publish_time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
							</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				
			</div>
			<div class="tabsFooter">
				<div class="tabsFooterContent"></div>
			</div>
		</div>				
	</div>
		
		
	<div class="formBar">
		<ul>
			<li><div class="button"><div class="buttonContent"><button class="close" type="button"><hi:text key="关闭"/></button></div></div></li>
		</ul>
	</div>
				</div>
		
		
		



