<%@ page language="java" pageEncoding="UTF-8" contentType="text/html;charset=utf-8"%>
<%@ include file="/includes/main.jsp"%>

<form id="pagerForm" action="sys_driver_infoList.action">
	<input type="hidden" name="pageInfo.currentPage" value="${pageInfo.currentPage}" />
	<input type="hidden" name="pageInfo.sorterName" value="${pageInfo.sorterName}" />
	<input type="hidden" name="pageInfo.sorterDirection" value="${pageInfo.sorterDirection}" />
	<input type="hidden" name="lookup" value="${lookup}" />
</form>

<div class="pageHeader">
	<form rel="pagerForm" method="post" action="sys_driver_infoList.action?lookup=${lookup}" onsubmit="return dwzSearch(this, '${targetType}');">
	<input type="hidden" name="pageInfo.pageSize" value="${pageInfo.pageSize}" />
	<div class="searchBar">
		<ul class="searchContent">	
			<li>
				<label><hi:text key="姓名" entity="Driver_info"/>:</label>
				<input type="text" name="pageInfo.f_fullName" value="${pageInfo.f_fullName}"/>
			</li>	  
			<li>
				<label><hi:text key="注册手机" entity="Driver_info"/>:</label>
				<input type="text" name="pageInfo.f_userName" value="${pageInfo.f_userName}"/>
			</li>	  
			<li>
				<label><hi:text key="所属公司" entity="Driver_info"/>:</label>
				<input type="text" name="pageInfo.org.f_orgName" value="${pageInfo.org.f_orgName}"/>
			</li>	  
	  
			<li>

			</li>	  
		</ul>
		<div class="subBar">
			<div class="buttonActive"><div class="buttonContent"><button type="submit"><hi:text key="查询"/></button></div></div>
		</div>
	</div>
	</form>
</div>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		<c:choose>
			<c:when test="${empty lookup}">
			</c:when>
			<c:otherwise>
				<li><a class="icon" href="javascript:$.bringBack({id:'-1', drive_lic_file:'',info_state:'',userName:'',fullName:'',orgName:'',gender:''})"><span><hi:text key="重置"/></span></a></li>
			</c:otherwise>
		</c:choose>			
		</ul>
	</div>
	<table class="table" width="100%" layoutH="138" targetType="${targetType}">
		<thead>
			<tr>

				<th orderField="fullName" class="${pageInfo.sorterName eq 'fullName' ? pageInfo.sorterDirection : ''}"><hi:text key="姓名" entity="Driver_info"/></th>
				<th orderField="userName" class="${pageInfo.sorterName eq 'userName' ? pageInfo.sorterDirection : ''}"><hi:text key="注册手机" entity="Driver_info"/></th>
				<th orderField="SSN" class="${pageInfo.sorterName eq 'SSN' ? pageInfo.sorterDirection : ''}"><hi:text key="身份证" entity="Driver_info"/></th>
				<th orderField="drive_lic_file_attachment.fileName" class="${pageInfo.sorterName eq 'drive_lic_file_attachment.fileName' ? pageInfo.sorterDirection : ''}"><hi:text key="驾驶证附件" entity="Driver_info"/></th>
				<th orderField="info_state" class="${pageInfo.sorterName eq 'info_state' ? pageInfo.sorterDirection : ''}"><hi:text key="信息状态" entity="Driver_info"/></th>
				<th orderField="org.orgName" class="${pageInfo.sorterName eq 'org.orgName' ? pageInfo.sorterDirection : ''}"><hi:text key="所属公司" entity="Driver_info"/></th>
				<th orderField="truck_info.truck_card" class="${pageInfo.sorterName eq 'truck_info.truck_card' ? pageInfo.sorterDirection : ''}"><hi:text key="对应车辆" entity="Driver_info"/></th>
				<th width="90">
					<c:choose>
						<c:when test="${empty lookup}"><hi:text key="操作"/></c:when>
						<c:otherwise><hi:text key="查找带回"/></c:otherwise>
					</c:choose>
				</th>
			</tr>
		</thead>				
		<tbody>
			<c:forEach var="item" items="${driver_infos}" varStatus="s">
			<tr>
	
				    <td>${item.fullName}</td>
				    <td>${item.userName}</td>
				    <td>${item.SSN}</td>
				    <td>
						<c:if test="${not empty item.drive_lic_file_attachment}">
				   		<a href="<hi:url>attachmentView.action?attachment.id=${item.drive_lic_file_attachment.id}</hi:url>" target="_blank">${item.drive_lic_file_attachment.fileNameImage}</a>
				     	</c:if>
				   </td>		    
				    <td><hi:select emu="info_state" name="driver_infos[${s.index}].info_state" isLabel="true"/></td>
				    <td><authz:authorize ifAnyGranted="HIORG_VIEW"><a href="<hi:url>hiOrgView.action?hiOrg.id=${item.org.id}&workflow=-1</hi:url>" target="dialog"></authz:authorize>
					${item.org.orgName}
					<authz:authorize ifAnyGranted="HIORG_VIEW"></a></authz:authorize>
					</td>
				    <td><a href="<hi:url>truck_infoView.action?truck_info.id=${item.truck_info.id}</hi:url>" target="dialog">
					${item.truck_info.truck_card}
					</a>
					</td>

				<td>
				<c:choose>
					<c:when test="${empty lookup}">

				    <authz:authorize ifAnyGranted="DRIVER_INFO_VIEW">
				      <a class="btnView" href="<hi:url>sys_driver_infoView.action?driver_info.id=${item.id}</hi:url>" target="navTab" rel="driver_info" title="<hi:text key="查看" parameterLanguageKeys="司机信息"/>"><hi:text key="查看"/></a>
				    </authz:authorize>
				    <authz:authorize ifAnyGranted="DRIVER_INFO_SAVE">
				      <a class="btnEdit" href="<hi:url>sys_driver_infoEdit.action?driver_info.id=${item.id}</hi:url>" target="navTab" rel="driver_info" title="<hi:text key="编辑" parameterLanguageKeys="司机信息"/>"><hi:text key="编辑"/></a>
				    </authz:authorize>
					</c:when>
					<c:otherwise>
						<a class="btnSelect" href="javascript:$.bringBack({id:'${item.id}', drive_lic_file:'${item.drive_lic_file_attachment.fileName}',info_state:'<hi:select emu="info_state" name="driver_infos[${s.index}].info_state" isLabel="true"/>',userName:'${item.userName}',fullName:'${item.fullName}',orgName:'${item.org.orgName}',gender:'<hi:select emu="gender" name="driver_infos[${s.index}].gender" isLabel="true"/>'})" title="<hi:text key="查找带回"/>"><hi:text key="选择"/></a>
					</c:otherwise>
				</c:choose>
				</td>
			</tr>
			</c:forEach>
		</tbody>
	</table>	
	<div class="panelBar">
		<div class="pages">
			<span><hi:text key="每页"/></span>
			<c:set var="pageSizeList" value="${fn:split('10|20|50|100', '|')}"/>  
			<select name="pageInfo.pageSize" onchange="dwzPageBreak({targetType:'${targetType}', numPerPage:this.value})">
				<c:forEach var="item" items="${pageSizeList}">
				<option value="${item}" ${item eq pageInfo.pageSize ? 'selected="selected"' : ''}>${item}</option>
				</c:forEach>
			</select>
			<span><hi:text key="条"/>，<hi:text key="共"/>${pageInfo.totalRecords}<hi:text key="条"/></span>
		</div>
		<div class="pagination" targetType="${targetType}" totalCount="${pageInfo.totalRecords}" numPerPage="${pageInfo.pageSize}" pageNumShown="${pageInfo.middlePageNum*2}" currentPage="${pageInfo.currentPage}"></div>
	</div>
</div>
