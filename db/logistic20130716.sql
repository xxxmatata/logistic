﻿# SQL Manager 2011 Lite for MySQL 5.0.0.1
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : logistic


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `logistic`
    CHARACTER SET 'gb2312'
    COLLATE 'gb2312_chinese_ci';

USE `logistic`;

#
# Structure for the `driver_info` table : 
#

CREATE TABLE `driver_info` (
  `id` int(11) NOT NULL,
  `drive_lic_file_attachment` int(11) DEFAULT NULL,
  `truck_info` int(11) DEFAULT NULL,
  `info_state` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `enumeration` table : 
#

CREATE TABLE `enumeration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `enName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `displayRef` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `enumerationType` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100601 DEFAULT CHARSET=gb2312;

#
# Structure for the `enumerationvalue` table : 
#

CREATE TABLE `enumerationvalue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `valueName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `displayRef` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `valueCode` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `enumeration` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100602 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_appsetting` table : 
#

CREATE TABLE `hi_appsetting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `appGroup` varchar(30) CHARACTER SET utf8 NOT NULL,
  `appName` varchar(30) CHARACTER SET utf8 NOT NULL,
  `appValue` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_attachment` table : 
#

CREATE TABLE `hi_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `fileName` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `fileType` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `fileSize` decimal(18,2) DEFAULT NULL,
  `attachmentType` int(11) DEFAULT NULL,
  `attachmentPath` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `attachDesc` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_authority` table : 
#

CREATE TABLE `hi_authority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `authorityName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `displayRef` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `propertedResource` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `authorityType` int(11) DEFAULT NULL,
  `menuLink` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100507 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_excelcell` table : 
#

CREATE TABLE `hi_excelcell` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `excelSheet` int(11) DEFAULT NULL,
  `cellColumn` varchar(10) CHARACTER SET utf8 NOT NULL,
  `cellRow` int(11) NOT NULL,
  `variableName` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `constant` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `isEnumeration` int(11) DEFAULT NULL,
  `reportDataType` int(11) DEFAULT NULL,
  `stretchingType` int(11) DEFAULT NULL,
  `conditionCell` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_excelreportdesign` table : 
#

CREATE TABLE `hi_excelreportdesign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `reportName` varchar(20) CHARACTER SET utf8 NOT NULL,
  `reportNum` varchar(10) CHARACTER SET utf8 NOT NULL,
  `template` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `enabled` int(11) DEFAULT NULL,
  `actionName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_excelsheet` table : 
#

CREATE TABLE `hi_excelsheet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `excelReportDesign` int(11) DEFAULT NULL,
  `sheetName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `sequence` decimal(18,2) DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_grouprole` table : 
#

CREATE TABLE `hi_grouprole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `securityGroup` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_jobdetaildef` table : 
#

CREATE TABLE `hi_jobdetaildef` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `jobName` varchar(80) CHARACTER SET utf8 NOT NULL,
  `jobGroup` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `jobClassName` varchar(128) CHARACTER SET utf8 NOT NULL,
  `durable` int(11) DEFAULT NULL,
  `volatiled` int(11) DEFAULT NULL,
  `shouldRecover` int(11) DEFAULT NULL,
  `description` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_language` table : 
#

CREATE TABLE `hi_language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `keyStr` varchar(200) CHARACTER SET utf8 NOT NULL,
  `service` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `entity` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `isSystem` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100602 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_languagecode` table : 
#

CREATE TABLE `hi_languagecode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `languageCode` varchar(50) CHARACTER SET utf8 NOT NULL,
  `description` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_languagestr` table : 
#

CREATE TABLE `hi_languagestr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `language` int(11) DEFAULT NULL,
  `languageCode` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_log` table : 
#

CREATE TABLE `hi_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `operator` int(11) DEFAULT NULL,
  `operateDate` datetime DEFAULT NULL,
  `action` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `actionContext` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=489 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_message` table : 
#

CREATE TABLE `hi_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `receivers` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `receiverNames` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  `sender` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `messageType` int(11) DEFAULT NULL,
  `messageText` varchar(3000) CHARACTER SET utf8 DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `sendDate` datetime DEFAULT NULL,
  `isSent` int(11) DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_messageparameter` table : 
#

CREATE TABLE `hi_messageparameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `parameterKey` varchar(50) CHARACTER SET utf8 NOT NULL,
  `parameterValue` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `message` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_org` table : 
#

CREATE TABLE `hi_org` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `orgName` varchar(50) CHARACTER SET utf8 NOT NULL,
  `orgNum` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `manager` int(11) DEFAULT NULL,
  `parentOrg` int(11) DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_privilegeresource` table : 
#

CREATE TABLE `hi_privilegeresource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `authorityName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `viewLayer` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `veiwExtAuthNames` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `businessLayer` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `bizExtAuthNames` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100505 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_role` table : 
#

CREATE TABLE `hi_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `roleName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `displayRef` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_roleauthority` table : 
#

CREATE TABLE `hi_roleauthority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `authority` int(11) DEFAULT NULL,
  `org` int(11) DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_securitygroup` table : 
#

CREATE TABLE `hi_securitygroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `groupName` varchar(200) CHARACTER SET utf8 NOT NULL,
  `displayRef` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_timezone` table : 
#

CREATE TABLE `hi_timezone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `timezone` int(11) NOT NULL,
  `description` varchar(3000) CHARACTER SET utf8 NOT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_triggerdef` table : 
#

CREATE TABLE `hi_triggerdef` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `triggerName` varchar(80) CHARACTER SET utf8 NOT NULL,
  `triggerGroup` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `jobDetail` int(11) DEFAULT NULL,
  `volatiled` int(11) DEFAULT NULL,
  `nextFireTime` datetime DEFAULT NULL,
  `prevFireTime` datetime DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `triggerStats` int(11) DEFAULT NULL,
  `startTime` datetime DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `misfireInstr` int(11) DEFAULT NULL,
  `cronExpression` varchar(80) CHARACTER SET utf8 NOT NULL,
  `enabled` int(11) DEFAULT NULL,
  `timeZone` int(11) DEFAULT NULL,
  `description` varchar(300) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_user` table : 
#

CREATE TABLE `hi_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `userName` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `timeZone` int(11) DEFAULT NULL,
  `accountEnabled` int(11) DEFAULT NULL,
  `accountLocked` int(11) DEFAULT NULL,
  `expiredDate` datetime DEFAULT NULL,
  `credentialsExpired` int(11) DEFAULT NULL,
  `fullName` varchar(30) CHARACTER SET utf8 NOT NULL,
  `org` int(11) DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `mobile` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `zip` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `SSN` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `mail` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `userMgrType` int(11) DEFAULT NULL,
  `notifyMode` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userName` (`userName`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_userauthority` table : 
#

CREATE TABLE `hi_userauthority` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `securityUser` int(11) DEFAULT NULL,
  `authority` int(11) DEFAULT NULL,
  `org` int(11) DEFAULT NULL,
  `scope` int(11) DEFAULT NULL,
  `roleAuthority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_usergroup` table : 
#

CREATE TABLE `hi_usergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `securityUser` int(11) DEFAULT NULL,
  `securityGroup` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `hi_userrole` table : 
#

CREATE TABLE `hi_userrole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `securityUser` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=gb2312;

#
# Structure for the `himenu` table : 
#

CREATE TABLE `himenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `menuName` varchar(100) CHARACTER SET utf8 NOT NULL,
  `displayRef` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `parentMenu` int(11) DEFAULT NULL,
  `sequence` decimal(18,2) DEFAULT NULL,
  `menuType` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100004 DEFAULT CHARSET=gb2312;

#
# Structure for the `logistic_corp` table : 
#

CREATE TABLE `logistic_corp` (
  `id` int(11) NOT NULL,
  `address` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `op_lic` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `op_lic_file_attachment` int(11) DEFAULT NULL,
  `tax_lic` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `tax_lic_file_attachment` int(11) DEFAULT NULL,
  `corp_name` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `corp_phone` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `corp_mobile` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `info_state` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=gb2312;

#
# Structure for the `menulink` table : 
#

CREATE TABLE `menulink` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `linkUrl` varchar(200) CHARACTER SET utf8 NOT NULL,
  `displayRef` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `description` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `authority` int(11) DEFAULT NULL,
  `sequence` decimal(18,2) DEFAULT NULL,
  `menu` int(11) DEFAULT NULL,
  `menuLinkType` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100519 DEFAULT CHARSET=gb2312;

#
# Structure for the `truck_info` table : 
#

CREATE TABLE `truck_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `truck_card` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `truck_type` int(11) DEFAULT NULL,
  `truck_length` decimal(18,2) DEFAULT NULL,
  `truck_width` decimal(18,2) DEFAULT NULL,
  `truck_load` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `insure_id` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `insure_file_attachment` int(11) DEFAULT NULL,
  `truck_state` int(11) DEFAULT NULL,
  `info_state` int(11) DEFAULT NULL,
  `creator` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=gb2312;

#
# Structure for the `truck_type` table : 
#

CREATE TABLE `truck_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) NOT NULL,
  `truck_type_name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `creator` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=gb2312;

#
# Data for the `driver_info` table  (LIMIT -493,500)
#

INSERT INTO `driver_info` (`id`, `drive_lic_file_attachment`, `truck_info`, `info_state`) VALUES 
  (5,NULL,2,100600),
  (11,NULL,1,100600),
  (12,NULL,2,100600),
  (13,8,2,100600),
  (14,NULL,1,100600),
  (15,NULL,6,100600);
COMMIT;

#
# Data for the `enumeration` table  (LIMIT -489,500)
#

INSERT INTO `enumeration` (`id`, `version`, `enName`, `displayRef`, `description`, `enumerationType`, `creator`) VALUES 
  (1200,0,'gender','性别','性别',1,0),
  (1300,0,'job','职位','职位',0,0),
  (1400,0,'userType','用户类型','用户类型',1,0),
  (2900,0,'securityScope','权限范围','权限范围',1,0),
  (3200,0,'yesNo','是否','是否',1,0),
  (5300,0,'messageType','消息类型','消息类型',0,0),
  (12300,0,'reportDataType','数据类型','数据类型',0,0),
  (12400,0,'stretchingType','伸展方式','伸展方式',0,0),
  (100400,0,'truck_state','货车状态','货车状态',0,0),
  (100600,0,'info_state','信息状态','信息状态',0,0);
COMMIT;

#
# Data for the `enumerationvalue` table  (LIMIT -468,500)
#

INSERT INTO `enumerationvalue` (`id`, `version`, `valueName`, `displayRef`, `description`, `valueCode`, `enumeration`, `creator`) VALUES 
  (1200,0,'Male','男','男',NULL,1200,0),
  (1201,0,'Woman','女','女',NULL,1200,0),
  (1300,0,'Accountant','会计','会计',NULL,1300,0),
  (1301,0,'Cashier','出纳','出纳',NULL,1300,0),
  (1302,0,'ProjectManager','项目经理','项目经理',NULL,1300,0),
  (1400,0,'Administrator','超级管理员','超级管理员',NULL,1400,0),
  (1401,0,'Manager','平台管理员','平台管理员',NULL,1400,0),
  (1402,0,'Menual','物流公司管理员','物流公司管理员',NULL,1400,0),
  (1403,0,'Driver','司机','司机',NULL,1400,0),
  (2900,0,'User_Level','用户级','用户级',NULL,2900,0),
  (2901,0,'CurrentOrg_Level','当前部门级','当前部门级',NULL,2900,0),
  (2902,0,'Org_Level','部门级','部门级',NULL,2900,0),
  (2903,0,'OrgAndSuborg_Level','部门及子部门级','部门及子部门级',NULL,2900,0),
  (2904,0,'System_Level','系统级','系统级',NULL,2900,0),
  (3200,0,'Yes','是','是',NULL,3200,0),
  (3201,0,'No','否','否',NULL,3200,0),
  (5300,0,'SMS','手机短信','手机短信',NULL,5300,0),
  (5301,0,'InternalMail','内部邮件','内部邮件',NULL,5300,0),
  (5302,0,'ExternalMail','外部邮件','外部邮件',NULL,5300,0),
  (5303,0,'InteralMessage','短消息','短消息',NULL,5300,0),
  (5304,0,'QQ','QQ','QQ',NULL,5300,0),
  (5305,0,'MSN','MSN','MSN',NULL,5300,0),
  (12300,0,'Element','元素','元素',NULL,12300,0),
  (12301,0,'Collection','集合','集合',NULL,12300,0),
  (12400,0,'Vertical','纵向','纵向',NULL,12400,0),
  (12401,0,'Horizontal','横向','横向',NULL,12400,0),
  (100400,0,'nul','空车','空车','nul',100400,0),
  (100401,0,'cof','已接货','已接货','cof',100400,0),
  (100402,0,'lod','已上货','已上货','lod',100400,0),
  (100600,0,'c0','未审核','未审核','c0',100600,0),
  (100601,0,'c1','已审核','已审核','c1',100600,0);
COMMIT;

#
# Data for the `hi_appsetting` table  (LIMIT -498,500)
#

INSERT INTO `hi_appsetting` (`id`, `version`, `appGroup`, `appName`, `appValue`, `description`, `creator`) VALUES 
  (1,0,'HOSTING','WEB_HOSTING','http://localhost:8080','应用服务器地址',0);
COMMIT;

#
# Data for the `hi_attachment` table  (LIMIT -491,500)
#

INSERT INTO `hi_attachment` (`id`, `version`, `fileName`, `fileType`, `fileSize`, `attachmentType`, `attachmentPath`, `attachDesc`) VALUES 
  (1,0,'dwz-user-guide.pdf','application/pdf',1984.00,1,'upload/attachment/20130607/dwz-user-guide.pdf','attachment'),
  (2,0,'dwz-user-guide.zip','application/octet-stream',340.99,1,'upload/attachment/20130607/dwz-user-guide.zip','attachment'),
  (3,0,'Socket_Demo.pdf','application/pdf',213.18,1,'upload/attachment/20130607/Socket_Demo.pdf','attachment'),
  (4,0,'dwz-user-guide.pdf','application/pdf',1984.00,1,'upload/attachment/20130608/dwz-user-guide.pdf','attachment'),
  (5,0,'物流.xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',12.14,1,'upload/attachment/20130702/物流.xlsx','attachment'),
  (6,0,'Socket_Demo.pdf','application/pdf',213.18,1,'upload/attachment/20130702/Socket_Demo.pdf','attachment'),
  (7,0,'写卡模块开发及技术支撑合同.doc','application/msword',38.00,1,'upload/attachment/20130702/写卡模块开发及技术支撑合同.doc','attachment'),
  (8,0,'201220184245-环保云计算平台-实用新型.pdf','application/pdf',290.02,1,'upload/attachment/20130709/201220184245-环保云计算平台-实用新型.pdf','attachment');
COMMIT;

#
# Data for the `hi_authority` table  (LIMIT -388,500)
#

INSERT INTO `hi_authority` (`id`, `version`, `authorityName`, `displayRef`, `propertedResource`, `description`, `authorityType`, `menuLink`) VALUES 
  (1000,0,'HIORG_LIST','organization.HiOrgList','','部门查询',1,1000),
  (1001,0,'HIORG_VIEW','organization.HiOrgView','','部门查看',2,1000),
  (1002,0,'HIORG_SAVE','organization.HiOrgSave','','部门保存',3,1000),
  (1003,0,'HIORG_DEL','organization.HiOrgDel','','部门删除',4,1000),
  (1004,0,'HIORG_LOOKUP','organization.HiOrgLookup','','部门带回',1,1000),
  (1100,0,'HIUSER_LIST','organization.HiUserList','','人员查询',1,1100),
  (1101,0,'HIUSER_VIEW','organization.HiUserView','','人员查看',2,1100),
  (1102,0,'HIUSER_SAVE','organization.HiUserSave','','人员保存',3,1100),
  (1103,0,'HIUSER_DEL','organization.HiUserDel','','人员删除',4,1100),
  (1104,0,'HIUSER_LOOKUP','organization.HiUserLookup','','人员带回',1,1100),
  (2100,0,'ROLE_LIST','security.RoleList','','角色查询',1,2100),
  (2101,0,'ROLE_VIEW','security.RoleView','','角色查看',2,2100),
  (2102,0,'ROLE_SAVE','security.RoleSave','','角色保存',3,2100),
  (2103,0,'ROLE_DEL','security.RoleDel','','角色删除',4,2100),
  (2104,0,'ROLE_LOOKUP','security.RoleLookup','','角色带回',1,2100),
  (2300,0,'USERAUTHORITY_LIST','security.UserAuthorityList','','用户权限查询',1,2300),
  (2301,0,'USERAUTHORITY_VIEW','security.UserAuthorityView','','用户权限查看',2,2300),
  (2302,0,'USERAUTHORITY_SAVE','security.UserAuthoritySave','','用户权限保存',3,2300),
  (2303,0,'USERAUTHORITY_DEL','security.UserAuthorityDel','','用户权限删除',4,2300),
  (2304,0,'USERAUTHORITY_LOOKUP','security.UserAuthorityLookup','','用户权限带回',1,2300),
  (2800,0,'PRIVILEGERESOURCE_LIST','security.PrivilegeResourceList','','权限资源查询',1,2800),
  (2801,0,'PRIVILEGERESOURCE_VIEW','security.PrivilegeResourceView','','权限资源查看',2,2800),
  (2802,0,'PRIVILEGERESOURCE_SAVE','security.PrivilegeResourceSave','','权限资源保存',3,2800),
  (2803,0,'PRIVILEGERESOURCE_DEL','security.PrivilegeResourceDel','','权限资源删除',4,2800),
  (2804,0,'PRIVILEGERESOURCE_LOOKUP','security.PrivilegeResourceLookup','','权限资源带回',1,2800),
  (2999,0,'SYSTEM_USER_MANAGE','用户管理','','用户管理',0,2701),
  (3000,0,'ENUMERATION_LIST','enumeration.EnumerationList','','枚举实体查询',1,3000),
  (3001,0,'ENUMERATION_VIEW','enumeration.EnumerationView','','枚举实体查看',2,3000),
  (3002,0,'ENUMERATION_SAVE','enumeration.EnumerationSave','','枚举实体保存',3,3000),
  (3003,0,'ENUMERATION_DEL','enumeration.EnumerationDel','','枚举实体删除',4,3000),
  (3004,0,'ENUMERATION_LOOKUP','enumeration.EnumerationLookup','','枚举实体带回',1,3000),
  (3100,0,'ENUMERATIONVALUE_LIST','enumeration.EnumerationValueList','','枚举值查询',1,3100),
  (3101,0,'ENUMERATIONVALUE_VIEW','enumeration.EnumerationValueView','','枚举值查看',2,3100),
  (3102,0,'ENUMERATIONVALUE_SAVE','enumeration.EnumerationValueSave','','枚举值保存',3,3100),
  (3103,0,'ENUMERATIONVALUE_DEL','enumeration.EnumerationValueDel','','枚举值删除',4,3100),
  (3104,0,'ENUMERATIONVALUE_LOOKUP','enumeration.EnumerationValueLookup','','枚举值带回',1,3100),
  (4000,0,'JOBDETAILDEF_LIST','schedule.JobDetailDefList','','工作项定义查询',1,4000),
  (4001,0,'JOBDETAILDEF_VIEW','schedule.JobDetailDefView','','工作项定义查看',2,4000),
  (4002,0,'JOBDETAILDEF_SAVE','schedule.JobDetailDefSave','','工作项定义保存',3,4000),
  (4003,0,'JOBDETAILDEF_DEL','schedule.JobDetailDefDel','','工作项定义删除',4,4000),
  (4004,0,'JOBDETAILDEF_LOOKUP','schedule.JobDetailDefLookup','','工作项定义带回',1,4000),
  (4100,0,'TRIGGERDEF_LIST','schedule.TriggerDefList','','触发器查询',1,4100),
  (4101,0,'TRIGGERDEF_VIEW','schedule.TriggerDefView','','触发器查看',2,4100),
  (4102,0,'TRIGGERDEF_SAVE','schedule.TriggerDefSave','','触发器保存',3,4100),
  (4103,0,'TRIGGERDEF_DEL','schedule.TriggerDefDel','','触发器删除',4,4100),
  (4104,0,'TRIGGERDEF_LOOKUP','schedule.TriggerDefLookup','','触发器带回',1,4100),
  (5000,0,'APPSETTING_LIST','sysapp.AppSettingList','','应用配置查询',1,5000),
  (5001,0,'APPSETTING_VIEW','sysapp.AppSettingView','','应用配置查看',2,5000),
  (5002,0,'APPSETTING_SAVE','sysapp.AppSettingSave','','应用配置保存',3,5000),
  (5003,0,'APPSETTING_DEL','sysapp.AppSettingDel','','应用配置删除',4,5000),
  (5004,0,'APPSETTING_LOOKUP','sysapp.AppSettingLookup','','应用配置带回',1,5000),
  (5100,0,'MESSAGE_LIST','sysapp.MessageList','','消息查询',1,5100),
  (5101,0,'MESSAGE_VIEW','sysapp.MessageView','','消息查看',2,5100),
  (5102,0,'MESSAGE_SAVE','sysapp.MessageSave','','消息保存',3,5100),
  (5103,0,'MESSAGE_DEL','sysapp.MessageDel','','消息删除',4,5100),
  (5104,0,'MESSAGE_LOOKUP','sysapp.MessageLookup','','消息带回',1,5100),
  (5200,0,'MESSAGEPARAMETER_LIST','sysapp.MessageParameterList','','消息参数查询',1,5200),
  (5201,0,'MESSAGEPARAMETER_VIEW','sysapp.MessageParameterView','','消息参数查看',2,5200),
  (5202,0,'MESSAGEPARAMETER_SAVE','sysapp.MessageParameterSave','','消息参数保存',3,5200),
  (5203,0,'MESSAGEPARAMETER_DEL','sysapp.MessageParameterDel','','消息参数删除',4,5200),
  (5204,0,'MESSAGEPARAMETER_LOOKUP','sysapp.MessageParameterLookup','','消息参数带回',1,5200),
  (5400,0,'HILOG_LIST','sysapp.HiLogList','','系统日志查询',1,5400),
  (5401,0,'HILOG_VIEW','sysapp.HiLogView','','系统日志查看',2,5400),
  (5403,0,'HILOG_DEL','sysapp.HiLogDel','','系统日志删除',4,5400),
  (10000,0,'MENU_LIST','menu.MenuList','','菜单项查询',1,10000),
  (10001,0,'MENU_VIEW','menu.MenuView','','菜单项查看',2,10000),
  (10002,0,'MENU_SAVE','menu.MenuSave','','菜单项保存',3,10000),
  (10003,0,'MENU_DEL','menu.MenuDel','','菜单项删除',4,10000),
  (10004,0,'MENU_LOOKUP','menu.MenuLookup','','菜单项带回',1,10000),
  (10100,0,'MENULINK_LIST','menu.MenuLinkList','','菜单链接查询',1,10100),
  (10101,0,'MENULINK_VIEW','menu.MenuLinkView','','菜单链接查看',2,10100),
  (10102,0,'MENULINK_SAVE','menu.MenuLinkSave','','菜单链接保存',3,10100),
  (10103,0,'MENULINK_DEL','menu.MenuLinkDel','','菜单链接删除',4,10100),
  (10104,0,'MENULINK_LOOKUP','menu.MenuLinkLookup','','菜单链接带回',1,10100),
  (12000,0,'EXCELREPORTDESIGN_LIST','report.ExcelReportDesignList','','Excel报表设计查询',1,12000),
  (12001,0,'EXCELREPORTDESIGN_VIEW','report.ExcelReportDesignView','','Excel报表设计查看',2,12000),
  (12002,0,'EXCELREPORTDESIGN_SAVE','report.ExcelReportDesignSave','','Excel报表设计保存',3,12000),
  (12003,0,'EXCELREPORTDESIGN_DEL','report.ExcelReportDesignDel','','Excel报表设计删除',4,12000),
  (12004,0,'EXCELREPORTDESIGN_LOOKUP','report.ExcelReportDesignLookup','','Excel报表设计带回',1,12000),
  (12100,0,'EXCELSHEET_LIST','report.ExcelSheetList','','工作表查询',1,12100),
  (12101,0,'EXCELSHEET_VIEW','report.ExcelSheetView','','工作表查看',2,12100),
  (12102,0,'EXCELSHEET_SAVE','report.ExcelSheetSave','','工作表保存',3,12100),
  (12103,0,'EXCELSHEET_DEL','report.ExcelSheetDel','','工作表删除',4,12100),
  (12104,0,'EXCELSHEET_LOOKUP','report.ExcelSheetLookup','','工作表带回',1,12100),
  (12200,0,'EXCELCELL_LIST','report.ExcelCellList','','单元格查询',1,12200),
  (12201,0,'EXCELCELL_VIEW','report.ExcelCellView','','单元格查看',2,12200),
  (12202,0,'EXCELCELL_SAVE','report.ExcelCellSave','','单元格保存',3,12200),
  (12203,0,'EXCELCELL_DEL','report.ExcelCellDel','','单元格删除',4,12200),
  (12204,0,'EXCELCELL_LOOKUP','report.ExcelCellLookup','','单元格带回',1,12200),
  (100100,0,'TRUCK_TYPE_LIST','basicinfo.Truck_typeList','','货车类型查询',1,100100),
  (100101,0,'TRUCK_TYPE_VIEW','basicinfo.Truck_typeView','','货车类型查看',2,100100),
  (100102,0,'TRUCK_TYPE_SAVE','basicinfo.Truck_typeSave','','货车类型保存',3,100100),
  (100103,0,'TRUCK_TYPE_DEL','basicinfo.Truck_typeDel','','货车类型删除',4,100100),
  (100104,0,'TRUCK_TYPE_LOOKUP','basicinfo.Truck_typeLookup','','货车类型带回',1,100100),
  (100200,0,'DRIVER_INFO_LIST','basicinfo.Driver_infoList','','司机信息查询',1,100200),
  (100201,0,'DRIVER_INFO_VIEW','basicinfo.Driver_infoView','','司机信息查看',2,100200),
  (100202,0,'DRIVER_INFO_SAVE','basicinfo.Driver_infoSave','','司机信息保存',3,100200),
  (100203,0,'DRIVER_INFO_DEL','basicinfo.Driver_infoDel','','司机信息删除',4,100200),
  (100204,0,'DRIVER_INFO_LOOKUP','basicinfo.Driver_infoLookup','','司机信息带回',1,100200),
  (100300,0,'TRUCK_INFO_LIST','basicinfo.Truck_infoList','','货车信息表查询',1,100300),
  (100301,0,'TRUCK_INFO_VIEW','basicinfo.Truck_infoView','','货车信息表查看',2,100300),
  (100302,0,'TRUCK_INFO_SAVE','basicinfo.Truck_infoSave','','货车信息表保存',3,100300),
  (100303,0,'TRUCK_INFO_DEL','basicinfo.Truck_infoDel','','货车信息表删除',4,100300),
  (100304,0,'TRUCK_INFO_LOOKUP','basicinfo.Truck_infoLookup','','货车信息表带回',1,100300),
  (100500,0,'LOGISTIC_CORP_LIST','basicinfo.Logistic_corpList','','物流公司查询',1,100500),
  (100501,0,'LOGISTIC_CORP_VIEW','basicinfo.Logistic_corpView','','物流公司查看',2,100500),
  (100502,0,'LOGISTIC_CORP_SAVE','basicinfo.Logistic_corpSave','','物流公司保存',3,100500),
  (100503,0,'LOGISTIC_CORP_DEL','basicinfo.Logistic_corpDel','','物流公司删除',4,100500),
  (100504,0,'LOGISTIC_CORP_LOOKUP','basicinfo.Logistic_corpLookup','','物流公司带回',1,100500),
  (100505,0,'LOGISTIC_CORP_M_ALL','basicinfo.Logistic_corp_m_all',NULL,'物流公司管理权限',1,100501),
  (100506,0,'INFO_M_ALL','basicinfo.Info_m_all',NULL,'平台管理权限',1,100510);
COMMIT;

#
# Data for the `hi_language` table  (LIMIT -238,500)
#

INSERT INTO `hi_language` (`id`, `version`, `keyStr`, `service`, `entity`, `isSystem`, `creator`) VALUES 
  (3,5,'查询条件',NULL,'',1,1),
  (4,0,'新建',NULL,'',1,1),
  (5,0,'查询',NULL,'',1,1),
  (6,0,'重置',NULL,'',1,1),
  (8,6,'列表',NULL,'',1,1),
  (9,0,'操作',NULL,'',1,1),
  (10,0,'查找带回',NULL,'',1,1),
  (11,0,'删除',NULL,'',1,1),
  (12,0,'查看',NULL,'',1,1),
  (13,0,'编辑',NULL,'',1,1),
  (14,3,'取消全选',NULL,'',1,1),
  (15,0,'全选',NULL,'',1,1),
  (16,0,'批量删除',NULL,'',1,1),
  (17,2,'编辑页面',NULL,'',1,1),
  (18,0,'保存',NULL,'',1,1),
  (19,0,'关闭',NULL,'',1,1),
  (20,1,'查看页面',NULL,'',1,1),
  (22,0,'总条数',NULL,'',1,1),
  (23,0,'总页数',NULL,'',1,1),
  (24,0,'尾页',NULL,'',1,1),
  (25,0,'下一页',NULL,'',1,1),
  (26,0,'上一页',NULL,'',1,1),
  (27,0,'首页',NULL,'',1,1),
  (28,0,'到',NULL,'',1,1),
  (29,0,'页',NULL,'',1,1),
  (30,0,'跳转',NULL,'',1,1),
  (31,0,'全部',NULL,'',1,1),
  (32,0,'操作符',NULL,'',1,1),
  (33,0,'小于',NULL,'',1,1),
  (34,0,'大于',NULL,'',1,1),
  (35,0,'等于',NULL,'',1,1),
  (36,0,'小于等于',NULL,'',1,1),
  (37,0,'大于等于',NULL,'',1,1),
  (38,1,'请选择需要上传的附件',NULL,'Attachment',1,1),
  (39,1,'上传',NULL,'Attachment',1,1),
  (40,3,'请先选择需要上传的文件!',NULL,'Attachment',1,1),
  (41,0,'权限管理',NULL,'',1,1),
  (42,0,'错误详细信息',NULL,'',1,1),
  (43,0,'您没有操作此功能的权限',NULL,'',1,1),
  (44,0,'返回',NULL,'',1,1),
  (45,0,'未找到Action',NULL,'ExcelReportDesign',1,1),
  (46,0,'上传文件过大',NULL,'Attachment',1,1),
  (47,0,'Action加载失败',NULL,'',1,1),
  (48,0,'您是一般用户,不能分派角色',NULL,'Role',1,1),
  (49,1,'您的用户类型是管理员,必须是您自己创建的角色才可以为该角色分派用户',NULL,'Role',1,1),
  (50,0,'超级管理员无需授权',NULL,'Role',1,1),
  (51,0,'您是一般用户,不能删除建角色',NULL,'Role',1,1),
  (52,0,'您的用户类型为管理员,只能删除您自己所创建的角色',NULL,'Role',1,1),
  (53,0,'角色名重复',NULL,'Role',1,1),
  (54,0,'系统无法识别您的用户类型',NULL,'',1,1),
  (55,0,'您是一般用户,不能修改与创建角色',NULL,'Role',1,1),
  (56,1,'您的用户是管理员,所以只能编辑您自己创建的角色!',NULL,'Role',1,1),
  (57,0,'该页面中的数据已经被改写，请重新刷新页面后重新编辑该记录',NULL,'',1,1),
  (58,0,'Action对象赋值时出错',NULL,'',1,1),
  (59,0,'发送邮件失败',NULL,'',1,1),
  (61,0,'请选择要授权的用户',NULL,'Role',1,1),
  (62,0,'HI平台',NULL,'',1,1),
  (63,0,'用户名',NULL,'',1,1),
  (64,0,'密码',NULL,'',1,1),
  (65,0,'验证码',NULL,'',1,1),
  (66,0,'你没有登陆成功',NULL,'',1,1),
  (67,0,'可能的原因',NULL,'',1,1),
  (68,0,'欢迎',NULL,'',1,1),
  (69,0,'登陆',NULL,'',1,1),
  (70,0,'退出',NULL,'',1,1),
  (1000,0,'部门',NULL,NULL,0,1),
  (1001,0,'部门名称',NULL,'HiOrg',0,1),
  (1002,0,'部门编号',NULL,'HiOrg',0,1),
  (1003,0,'部门经理',NULL,'HiOrg',0,1),
  (1004,0,'上级部门',NULL,'HiOrg',0,1),
  (1005,0,'创建人',NULL,'HiOrg',0,1),
  (1100,0,'人员',NULL,NULL,0,1),
  (1101,0,'帐号',NULL,'HiUser',0,1),
  (1102,0,'姓名',NULL,'HiUser',0,1),
  (1103,0,'部门',NULL,'HiUser',0,1),
  (1104,0,'性别',NULL,'HiUser',0,1),
  (1200,0,'性别',NULL,NULL,0,1),
  (1201,0,'男',NULL,'gender',0,1),
  (1202,0,'女',NULL,'gender',0,1),
  (1300,0,'职位',NULL,NULL,0,1),
  (1301,0,'会计',NULL,'job',0,1),
  (1302,0,'出纳',NULL,'job',0,1),
  (1303,0,'项目经理',NULL,'job',0,1),
  (1400,0,'用户类型',NULL,NULL,0,1),
  (1401,0,'超级管理员',NULL,'userType',0,1),
  (1402,0,'管理员',NULL,'userType',0,1),
  (2000,0,'权限',NULL,NULL,0,1),
  (2001,0,'权限名称',NULL,'Authority',0,1),
  (2002,0,'属性资源',NULL,'Authority',0,1),
  (2003,0,'描述',NULL,'Authority',0,1),
  (2004,0,'菜单项',NULL,'Authority',0,1),
  (2100,0,'角色',NULL,NULL,0,1),
  (2101,0,'角色名称',NULL,'Role',0,1),
  (2102,0,'显示信息',NULL,'Role',0,1),
  (2103,0,'描述',NULL,'Role',0,1),
  (2104,0,'创建人',NULL,'Role',0,1),
  (2200,0,'用户组',NULL,NULL,0,1),
  (2201,0,'用户组名',NULL,'SecurityGroup',0,1),
  (2202,0,'显示信息',NULL,'SecurityGroup',0,1),
  (2203,0,'描述',NULL,'SecurityGroup',0,1),
  (2300,0,'用户权限',NULL,NULL,0,1),
  (2301,0,'用户',NULL,'UserAuthority',0,1),
  (2302,0,'权限',NULL,'UserAuthority',0,1),
  (2303,0,'部门',NULL,'UserAuthority',0,1),
  (2304,0,'范围',NULL,'UserAuthority',0,1),
  (2400,0,'用户角色',NULL,NULL,0,1),
  (2401,0,'用户',NULL,'UserRole',0,1),
  (2402,0,'角色名称',NULL,'UserRole',0,1),
  (2500,0,'用户与组',NULL,NULL,0,1),
  (2501,0,'用户',NULL,'UserGroup',0,1),
  (2502,0,'角色名称',NULL,'UserGroup',0,1),
  (2600,0,'角色权限',NULL,NULL,0,1),
  (2601,0,'角色名称',NULL,'RoleAuthority',0,1),
  (2602,0,'用户',NULL,'RoleAuthority',0,1),
  (2603,0,'部门',NULL,'RoleAuthority',0,1),
  (2604,0,'范围',NULL,'RoleAuthority',0,1),
  (2700,0,'组与角色',NULL,NULL,0,1),
  (2701,0,'用户组',NULL,'GroupRole',0,1),
  (2702,0,'角色名称',NULL,'GroupRole',0,1),
  (2800,0,'权限资源',NULL,NULL,0,1),
  (2801,0,'权限名称',NULL,'PrivilegeResource',0,1),
  (2802,0,'业务层',NULL,'PrivilegeResource',0,1),
  (2900,0,'权限范围',NULL,NULL,0,1),
  (2901,0,'用户级',NULL,'securityScope',0,1),
  (2902,0,'当前部门级',NULL,'securityScope',0,1),
  (2903,0,'部门级',NULL,'securityScope',0,1),
  (2904,0,'部门及子部门级',NULL,'securityScope',0,1),
  (3000,0,'枚举实体',NULL,NULL,0,1),
  (3001,0,'枚举名称',NULL,'Enumeration',0,1),
  (3002,0,'显示信息',NULL,'Enumeration',0,1),
  (3003,0,'描述',NULL,'Enumeration',0,1),
  (3100,0,'枚举值',NULL,NULL,0,1),
  (3101,0,'枚举值名称',NULL,'EnumerationValue',0,1),
  (3102,0,'显示信息',NULL,'EnumerationValue',0,1),
  (3103,0,'描述',NULL,'EnumerationValue',0,1),
  (3104,0,'枚举值编号',NULL,'EnumerationValue',0,1),
  (3200,0,'是否',NULL,NULL,0,1),
  (3201,0,'是',NULL,'yesNo',0,1),
  (4000,0,'工作项定义',NULL,NULL,0,1),
  (4001,0,'工作名称',NULL,'JobDetailDef',0,1),
  (4002,0,'类全名',NULL,'JobDetailDef',0,1),
  (4003,0,'工作描述',NULL,'JobDetailDef',0,1),
  (4100,0,'触发器',NULL,NULL,0,1),
  (4101,0,'触发名称',NULL,'TriggerDef',0,1),
  (4102,0,'工作项',NULL,'TriggerDef',0,1),
  (4103,0,'优先级',NULL,'TriggerDef',0,1),
  (4104,0,'开始时间',NULL,'TriggerDef',0,1),
  (4105,0,'截止时间',NULL,'TriggerDef',0,1),
  (4106,0,'表达式',NULL,'TriggerDef',0,1),
  (4107,0,'激活',NULL,'TriggerDef',0,1),
  (5000,0,'应用配置',NULL,NULL,0,1),
  (5001,0,'组名',NULL,'AppSetting',0,1),
  (5002,0,'配置名',NULL,'AppSetting',0,1),
  (5003,0,'配置值',NULL,'AppSetting',0,1),
  (5100,0,'消息',NULL,NULL,0,1),
  (5101,0,'收信人',NULL,'Message',0,1),
  (5102,0,'发信人',NULL,'Message',0,1),
  (5103,0,'消息类型',NULL,'Message',0,1),
  (5104,0,'创建时间',NULL,'Message',0,1),
  (5105,0,'发送时间',NULL,'Message',0,1),
  (5106,0,'已发送',NULL,'Message',0,1),
  (5107,0,'描述',NULL,'Message',0,1),
  (5200,0,'消息参数',NULL,NULL,0,1),
  (5201,0,'参数键',NULL,'MessageParameter',0,1),
  (5202,0,'参数值',NULL,'MessageParameter',0,1),
  (5300,0,'消息类型',NULL,NULL,0,1),
  (5301,0,'手机短信',NULL,'messageType',0,1),
  (5302,0,'内部邮件',NULL,'messageType',0,1),
  (5303,0,'外部邮件',NULL,'messageType',0,1),
  (5304,0,'短消息',NULL,'messageType',0,1),
  (5305,0,'QQ',NULL,'messageType',0,1),
  (5306,0,'MSN',NULL,'messageType',0,1),
  (5400,0,'系统日志',NULL,NULL,0,1),
  (5401,0,'操作人',NULL,'HiLog',0,1),
  (5402,0,'操作时间',NULL,'HiLog',0,1),
  (5403,0,'动作',NULL,'HiLog',0,1),
  (6000,0,'附件',NULL,NULL,0,1),
  (6001,0,'文件名',NULL,'Attachment',0,1),
  (6002,0,'文件类型',NULL,'Attachment',0,1),
  (6003,0,'文件大小',NULL,'Attachment',0,1),
  (6004,0,'附件类型',NULL,'Attachment',0,1),
  (6005,0,'附件',NULL,'Attachment',0,1),
  (10000,0,'菜单项',NULL,NULL,0,1),
  (10001,0,'菜单名称',NULL,'Menu',0,1),
  (10002,0,'描述',NULL,'Menu',0,1),
  (10003,0,'父菜单项',NULL,'Menu',0,1),
  (10004,0,'序列',NULL,'Menu',0,1),
  (10100,0,'菜单链接',NULL,NULL,0,1),
  (10101,0,'菜单URL',NULL,'MenuLink',0,1),
  (10102,0,'描述',NULL,'MenuLink',0,1),
  (10103,0,'权限名称',NULL,'MenuLink',0,1),
  (10104,0,'序列',NULL,'MenuLink',0,1),
  (10105,0,'菜单项名称',NULL,'MenuLink',0,1),
  (12000,0,'Excel报表设计',NULL,NULL,0,1),
  (12001,0,'报表名称',NULL,'ExcelReportDesign',0,1),
  (12002,0,'报表编号',NULL,'ExcelReportDesign',0,1),
  (12003,0,'创建时间',NULL,'ExcelReportDesign',0,1),
  (12004,0,'激活',NULL,'ExcelReportDesign',0,1),
  (12005,0,'描述',NULL,'ExcelReportDesign',0,1),
  (12100,0,'工作表',NULL,NULL,0,1),
  (12101,0,'工作表名称',NULL,'ExcelSheet',0,1),
  (12102,0,'序列',NULL,'ExcelSheet',0,1),
  (12103,0,'描述',NULL,'ExcelSheet',0,1),
  (12200,0,'单元格',NULL,NULL,0,1),
  (12201,0,'列',NULL,'ExcelCell',0,1),
  (12202,0,'行',NULL,'ExcelCell',0,1),
  (12203,0,'变量名',NULL,'ExcelCell',0,1),
  (12204,0,'常量',NULL,'ExcelCell',0,1),
  (12205,0,'枚举类型',NULL,'ExcelCell',0,1),
  (12206,0,'数据类型',NULL,'ExcelCell',0,1),
  (12207,0,'伸展方式',NULL,'ExcelCell',0,1),
  (12208,0,'条件',NULL,'ExcelCell',0,1),
  (12209,0,'描述',NULL,'ExcelCell',0,1),
  (12300,0,'数据类型',NULL,NULL,0,1),
  (12301,0,'元素',NULL,'reportDataType',0,1),
  (12302,0,'集合',NULL,'reportDataType',0,1),
  (12400,0,'伸展方式',NULL,NULL,0,1),
  (12401,0,'纵向',NULL,'stretchingType',0,1),
  (42100,0,'多语言信息',NULL,NULL,0,1),
  (42101,0,'Key值',NULL,'Language',0,1),
  (42102,0,'服务',NULL,'Language',0,1),
  (42103,0,'实体',NULL,'Language',0,1),
  (42200,0,'语言编码',NULL,NULL,0,1),
  (42201,0,'语言编码',NULL,'LanguageCode',0,1),
  (42202,0,'描述',NULL,'LanguageCode',0,1),
  (42300,0,'多语言值',NULL,NULL,0,1),
  (42301,0,'语言代码',NULL,'LanguageStr',0,1),
  (42302,0,'值',NULL,'LanguageStr',0,1),
  (42400,0,'时区',NULL,NULL,0,1),
  (42401,0,'时区值',NULL,'Timezone',0,1),
  (100100,0,'货车类型',NULL,NULL,0,1),
  (100200,0,'司机信息',NULL,NULL,0,1),
  (100201,0,'驾驶证附件',NULL,'Driver_info',0,1),
  (100202,0,'信息状态',NULL,'Driver_info',0,1),
  (100203,0,'司机备注',NULL,'Driver_info',0,1),
  (100204,0,'公司名称',NULL,'Driver_info',0,1),
  (100300,0,'货车信息表',NULL,NULL,0,1),
  (100301,0,'车牌',NULL,'Truck_info',0,1),
  (100302,0,'货车类型',NULL,'Truck_info',0,1),
  (100303,0,'车长',NULL,'Truck_info',0,1),
  (100304,0,'车宽',NULL,'Truck_info',0,1),
  (100305,0,'载重',NULL,'Truck_info',0,1),
  (100306,0,'保单号',NULL,'Truck_info',0,1),
  (100307,0,'保单附件',NULL,'Truck_info',0,1),
  (100308,0,'货车状态',NULL,'Truck_info',0,1),
  (100400,0,'货车状态',NULL,NULL,0,1),
  (100401,0,'空车',NULL,'truck_state',0,1),
  (100402,0,'已接货',NULL,'truck_state',0,1),
  (100403,0,'已上货',NULL,'truck_state',0,1),
  (100500,0,'物流公司',NULL,NULL,0,1),
  (100501,0,'公司地址',NULL,'Logistic_corp',0,1),
  (100502,0,'营业执照编号',NULL,'Logistic_corp',0,1),
  (100503,0,'营业执照附件',NULL,'Logistic_corp',0,1),
  (100504,0,'税务登记号',NULL,'Logistic_corp',0,1),
  (100505,0,'税务登记证附件',NULL,'Logistic_corp',0,1),
  (100506,0,'法人',NULL,'Logistic_corp',0,1),
  (100507,0,'公司电话',NULL,'Logistic_corp',0,1),
  (100508,0,'手机',NULL,'Logistic_corp',0,1),
  (100509,0,'信息状态',NULL,'Logistic_corp',0,1),
  (100600,0,'信息状态',NULL,NULL,0,1),
  (100601,0,'草稿',NULL,'info_state',0,1);
COMMIT;

#
# Data for the `hi_languagecode` table  (LIMIT -497,500)
#

INSERT INTO `hi_languagecode` (`id`, `version`, `languageCode`, `description`, `creator`) VALUES 
  (1,2,'zh_CN','中国汉语',1),
  (2,0,'en_US','美国英语',1);
COMMIT;

#
# Data for the `hi_languagestr` table  (LIMIT -435,500)
#

INSERT INTO `hi_languagestr` (`id`, `version`, `language`, `languageCode`, `value`, `creator`) VALUES 
  (2,8,3,'zh_CN','查询条件',1),
  (3,0,4,'zh_CN','新建',1),
  (4,0,5,'zh_CN','查询',1),
  (5,0,6,'zh_CN','重置',1),
  (7,1,8,'zh_CN','{1}列表',1),
  (8,1,9,'zh_CN','操作',1),
  (9,0,10,'zh_CN','查找带回',1),
  (10,0,11,'zh_CN','删除{1}',1),
  (11,0,12,'zh_CN','查看{1}',1),
  (12,0,13,'zh_CN','编辑{1}',1),
  (13,0,14,'zh_CN','取消全选',1),
  (14,0,15,'zh_CN','全选',1),
  (15,0,16,'zh_CN','批量删除',1),
  (16,1,17,'zh_CN','{1}编辑页面',1),
  (17,0,18,'zh_CN','保存',1),
  (18,0,19,'zh_CN','关闭',1),
  (19,1,20,'zh_CN','{1}查看页面',1),
  (27,0,22,'zh_CN','共{1}条',1),
  (29,0,23,'zh_CN','共{1}页',1),
  (31,0,24,'zh_CN','尾页',1),
  (33,0,25,'zh_CN','下一页',1),
  (35,0,26,'zh_CN','上一页',1),
  (37,0,27,'zh_CN','首页',1),
  (39,0,28,'zh_CN','到',1),
  (41,0,29,'zh_CN','页',1),
  (43,0,30,'zh_CN','跳转',1),
  (45,0,31,'zh_CN','全部',1),
  (47,0,32,'zh_CN','操作符',1),
  (49,0,33,'zh_CN','小于',1),
  (51,0,34,'zh_CN','大于',1),
  (53,0,35,'zh_CN','等于',1),
  (55,0,36,'zh_CN','小于等于',1),
  (57,0,37,'zh_CN','大于等于',1),
  (59,0,38,'zh_CN','请选择需要上传的附件',1),
  (60,0,39,'zh_CN','上传',1),
  (61,0,40,'zh_CN','请先选择需要上传的文件！',1),
  (62,0,41,'zh_CN','权限管理',1),
  (63,0,42,'zh_CN','错误详细信息',1),
  (64,0,43,'zh_CN','您没有操作此功能的权限',1),
  (65,0,44,'zh_CN','返回',1),
  (66,0,45,'zh_CN','在系统中没有与action:{1}  对应的报表设计,如果存在请确认是否处于激活状态',1),
  (67,0,46,'zh_CN','上传文件过大！最大能上传{1}MB 的附件.',1),
  (68,0,47,'zh_CN','{1}加载失败!',1),
  (69,0,48,'zh_CN','您是一般用户,不能分派角色',1),
  (70,0,49,'zh_CN','您的用户类型是管理员,必须是您自己创建的角色才可以为该角色分派用户',1),
  (71,0,50,'zh_CN','您选择的用户：{1}是超级管理员,不需要为超级管理员授权',1),
  (72,0,51,'zh_CN','您是一般用户,不能删除建角色',1),
  (73,0,52,'zh_CN','您的用户类型为管理员,只能删除您自己所创建的角色',1),
  (74,0,53,'zh_CN','角色名：{1}已经存在，不能有重复的角色名！',1),
  (75,1,54,'zh_CN','系统无法识别您的用户类型!',1),
  (76,0,55,'zh_CN','您是一般用户,不能修改与创建角色!',1),
  (77,0,56,'zh_CN','您的用户是管理员,所以只能编辑您自己创建的角色!',1),
  (78,0,57,'zh_CN','该页面中的数据已经被改写，请重新刷新页面后重新编辑该记录!',1),
  (79,0,58,'zh_CN','在给action对象赋值时出错：{1}属性！',1),
  (80,0,59,'zh_CN','发送邮件失败',1),
  (82,0,61,'zh_CN','请选择要授权的用户',1),
  (83,0,62,'zh_CN','HI平台',1),
  (84,0,63,'zh_CN','用户名',1),
  (85,0,64,'zh_CN','密　码',1),
  (86,0,65,'zh_CN','验证码',1),
  (87,0,66,'zh_CN','你没有登陆成功，请再试一次。',1),
  (88,0,67,'zh_CN','可能的原因: 您输入的用户名或密码错误！',1),
  (89,0,68,'zh_CN','欢迎',1),
  (90,0,69,'zh_CN','登陆',1);
COMMIT;

#
# Data for the `hi_log` table  (LIMIT -351,500)
#

INSERT INTO `hi_log` (`id`, `version`, `operator`, `operateDate`, `action`, `actionContext`) VALUES 
  (341,0,6,'2013-07-10 15:45:45','保存货车信息表','数据信息:更新[车牌:b->b4;]'),
  (342,0,6,'2013-07-11 00:52:31','保存货车信息表','数据信息:插入[id:null;车牌:qw;货车类型:前八轮;车长:1.0;车宽:1.0;载重:2;保单号:null;附件:null;保单附件:null;货车状态:空车;信息状态:未审核;'),
  (343,0,6,'2013-07-11 00:56:20','保存货车信息表','数据信息:插入[id:null;车牌:qwe;货车类型:前八轮;车长:null;车宽:null;载重:null;保单号:null;附件:null;保单附件:null;货车状态:空车;信息状态:未审核;'),
  (344,0,6,'2013-07-11 00:56:33','保存货车信息表','数据信息:插入[id:null;车牌:tee;货车类型:null;车长:null;车宽:null;载重:null;保单号:null;附件:null;保单附件:null;货车状态:空车;信息状态:未审核;'),
  (345,0,6,'2013-07-11 01:12:17','保存货车信息表','数据信息:插入[id:null;车牌:as;货车类型:null;车长:null;车宽:null;载重:null;保单号:null;附件:null;保单附件:null;货车状态:空车;信息状态:未审核;'),
  (346,0,6,'2013-07-11 01:19:43','保存货车信息表','数据信息:插入[id:null;车牌:ewewr;货车类型:前八轮;车长:null;车宽:null;载重:null;保单号:null;附件:null;保单附件:null;货车状态:空车;信息状态:未审核;'),
  (347,0,1,'2013-07-11 13:17:26','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:LOGISTIC_CORP_M_ALL;部门:null;范围:当前部门级;'),
  (348,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:司机信息保存;部门:null;范围:null;'),
  (349,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:物流公司管理权限;部门:null;范围:当前部门级;'),
  (350,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:司机信息带回;部门:null;范围:当前部门级;'),
  (351,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:司机信息保存;部门:null;范围:null;'),
  (352,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:货车信息表保存;部门:null;范围:null;'),
  (353,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:物流公司管理权限;部门:null;范围:当前部门级;'),
  (354,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:LOGISTIC_CORP_VIEW;部门:null;范围:null;'),
  (355,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:司机信息保存;部门:null;范围:null;'),
  (356,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:物流公司管理权限;部门:null;范围:当前部门级;'),
  (357,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:司机信息带回;部门:null;范围:当前部门级;'),
  (358,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:TRUCK_INFO_DEL;部门:null;范围:null;'),
  (359,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:物流公司查看;部门:null;范围:null;'),
  (360,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:DRIVER_INFO_VIEW;部门:null;范围:null;'),
  (361,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:TRUCK_INFO_VIEW;部门:null;范围:null;'),
  (362,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:物流公司查看;部门:null;范围:null;'),
  (363,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:DRIVER_INFO_DEL;部门:null;范围:null;'),
  (364,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:货车信息表删除;部门:null;范围:null;'),
  (365,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:司机信息查看;部门:null;范围:null;'),
  (366,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:物流公司查看;部门:null;范围:null;'),
  (367,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:司机信息删除;部门:null;范围:null;'),
  (368,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:货车信息表查看;部门:null;范围:null;'),
  (369,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:货车信息表删除;部门:null;范围:null;'),
  (370,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:司机信息查看;部门:null;范围:null;'),
  (371,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:LOGISTIC_CORP_LOOKUP;部门:null;范围:当前部门级;'),
  (372,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:货车信息表查看;部门:null;范围:null;'),
  (373,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:货车信息表删除;部门:null;范围:null;'),
  (374,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:物流公司带回;部门:null;范围:当前部门级;'),
  (375,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:TRUCK_INFO_LOOKUP;部门:null;范围:当前部门级;'),
  (376,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:货车信息表查看;部门:null;范围:null;'),
  (377,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:司机信息查看;部门:null;范围:null;'),
  (378,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:司机信息删除;部门:null;范围:null;'),
  (379,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:物流公司带回;部门:null;范围:当前部门级;'),
  (380,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:货车信息表带回;部门:null;范围:当前部门级;'),
  (381,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:物流公司带回;部门:null;范围:当前部门级;'),
  (382,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:DRIVER_INFO_SAVE;部门:null;范围:null;'),
  (383,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:TRUCK_INFO_SAVE;部门:null;范围:null;'),
  (384,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:货车信息表带回;部门:null;范围:当前部门级;'),
  (385,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:司机信息删除;部门:null;范围:null;'),
  (386,0,1,'2013-07-11 13:17:26','保存角色','数据信息:更新[id:null->2;角色名称:null->物流公司管理;显示信息:null->物流公司管理;描述:null->物流公司管理;]'),
  (387,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:3;权限:货车信息表带回;部门:null;范围:当前部门级;'),
  (388,0,1,'2013-07-11 13:17:27','保存角色权限','数据信息:插入[id:null;角色名称:物流公司管理;用户:DRIVER_INFO_LOOKUP;部门:null;范围:当前部门级;'),
  (389,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:货车信息表保存;部门:null;范围:null;'),
  (390,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:1;权限:司机信息带回;部门:null;范围:当前部门级;'),
  (391,0,1,'2013-07-11 13:17:27','保存用户权限','数据信息:插入[id:null;用户:2;权限:货车信息表保存;部门:null;范围:null;'),
  (392,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:INFO_M_ALL;部门:null;范围:系统级;'),
  (393,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:人员保存;部门:null;范围:null;'),
  (394,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:平台管理权限;部门:null;范围:系统级;'),
  (395,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:HIUSER_DEL;部门:null;范围:null;'),
  (396,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:LOGISTIC_CORP_DEL;部门:null;范围:null;'),
  (397,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:人员删除;部门:null;范围:null;'),
  (398,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:物流公司删除;部门:null;范围:null;'),
  (399,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:HIUSER_LOOKUP;部门:null;范围:系统级;'),
  (400,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:LOGISTIC_CORP_LOOKUP;部门:null;范围:系统级;'),
  (401,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:人员带回;部门:null;范围:系统级;'),
  (402,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:物流公司带回;部门:null;范围:系统级;'),
  (403,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:LOGISTIC_CORP_VIEW;部门:null;范围:null;'),
  (404,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:HIUSER_VIEW;部门:null;范围:null;'),
  (405,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:物流公司查看;部门:null;范围:null;'),
  (406,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:LOGISTIC_CORP_SAVE;部门:null;范围:null;'),
  (407,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:人员查看;部门:null;范围:null;'),
  (408,0,1,'2013-07-16 13:48:43','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:物流公司保存;部门:null;范围:null;'),
  (409,0,1,'2013-07-16 13:48:43','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:HIUSER_SAVE;部门:null;范围:null;'),
  (410,0,1,'2013-07-16 13:58:12','保存人员','数据信息:更新<3>[密码:eccbc87e4b5ce2fe28308fd9f2a7baf3->c16a5320fa475530d9583c34fd356ef5;]'),
  (411,0,10,'2013-07-16 13:58:38','保存人员','数据信息:更新<3>[密码:c16a5320fa475530d9583c34fd356ef5->eccbc87e4b5ce2fe28308fd9f2a7baf3;]'),
  (412,0,9,'2013-07-16 15:26:07','保存人员','数据信息:更新<wwqe>[姓名:4->wwqe;]'),
  (413,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:人员保存;部门:null;范围:null;'),
  (414,0,9,'2013-07-16 15:26:08','保存用户角色','数据信息:插入[id:null;用户:wwqe;角色名称:平台管理;'),
  (415,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:人员删除;部门:null;范围:null;'),
  (416,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:平台管理权限;部门:null;范围:系统级;'),
  (417,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:人员带回;部门:null;范围:系统级;'),
  (418,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:物流公司删除;部门:null;范围:null;'),
  (419,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:物流公司查看;部门:null;范围:null;'),
  (420,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:物流公司带回;部门:null;范围:系统级;'),
  (421,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:物流公司保存;部门:null;范围:null;'),
  (422,0,9,'2013-07-16 15:26:08','保存用户权限','数据信息:插入[id:null;用户:wwqe;权限:人员查看;部门:null;范围:null;'),
  (423,0,9,'2013-07-16 15:26:07','保存角色','数据信息:更新[id:null->1;角色名称:null->平台管理;显示信息:null->平台管理;描述:null->平台管理;创建人:平台管理员->administrator;]更新<wwqe>[]'),
  (424,0,1,'2013-07-16 15:41:45','保存人员','数据信息:更新<wwqee>[姓名:wwqe->wwqee;]'),
  (425,0,1,'2013-07-16 15:41:45','保存用户角色','数据信息:插入[id:null;用户:wwqee;角色名称:物流公司管理;'),
  (426,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:物流公司管理权限;部门:null;范围:当前部门级;'),
  (427,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:司机信息查看;部门:null;范围:null;'),
  (428,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:司机信息保存;部门:null;范围:null;'),
  (429,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:司机信息删除;部门:null;范围:null;'),
  (430,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:司机信息带回;部门:null;范围:当前部门级;'),
  (431,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:货车信息表查看;部门:null;范围:null;'),
  (432,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:货车信息表保存;部门:null;范围:null;'),
  (433,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:货车信息表删除;部门:null;范围:null;'),
  (434,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:货车信息表带回;部门:null;范围:当前部门级;'),
  (435,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:物流公司查看;部门:null;范围:null;'),
  (436,0,16,'2013-07-16 15:48:07','保存用户角色','数据信息:插入[id:null;用户:5;角色名称:物流公司管理;'),
  (437,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:司机信息保存;部门:null;范围:null;'),
  (438,0,1,'2013-07-16 15:41:45','保存用户权限','数据信息:插入[id:null;用户:wwqee;权限:物流公司带回;部门:null;范围:当前部门级;'),
  (439,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:货车信息表查看;部门:null;范围:null;'),
  (440,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:货车信息表带回;部门:null;范围:当前部门级;'),
  (441,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:物流公司管理权限;部门:null;范围:当前部门级;'),
  (442,0,1,'2013-07-16 15:41:45','保存角色','数据信息:更新[id:null->2;角色名称:null->物流公司管理;显示信息:null->物流公司管理;描述:null->物流公司管理;]更新<wwqee>[]'),
  (443,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:物流公司查看;部门:null;范围:null;'),
  (444,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:货车信息表保存;部门:null;范围:null;'),
  (445,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:司机信息查看;部门:null;范围:null;'),
  (446,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:司机信息删除;部门:null;范围:null;'),
  (447,0,1,'2013-07-16 15:46:13','保存人员','数据信息:更新<wwqee1>[姓名:wwqee->wwqee1;]'),
  (448,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:货车信息表删除;部门:null;范围:null;'),
  (449,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:物流公司带回;部门:null;范围:当前部门级;'),
  (450,0,16,'2013-07-16 15:48:07','保存用户权限','数据信息:插入[id:null;用户:5;权限:司机信息带回;部门:null;范围:当前部门级;'),
  (451,0,1,'2013-07-16 15:46:14','保存角色','数据信息:更新[id:null->2;角色名称:null->物流公司管理;显示信息:null->物流公司管理;描述:null->物流公司管理;]更新<wwqee1>[]'),
  (452,0,16,'2013-07-16 15:48:07','保存人员','数据信息:插入[id:null;帐号:5;密码:e4da3b7fbbce2345d7772b0674a318d5;国家:null;时区:null;帐号可用:null;加锁:null;用效期至:null;是否过期:null;姓名:5;部门:蓝焰物流;性别:男;地址:null;电话:null;手机:null;邮编:null;身份证:null;E-Mail:null;用户类型:物流公司管理员;提醒方式:null;描述:null;'),
  (453,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:INFO_M_ALL;部门:null;范围:系统级;'),
  (454,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:司机信息删除;部门:null;范围:null;'),
  (455,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:平台管理权限;部门:null;范围:系统级;'),
  (456,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:DRIVER_INFO_LOOKUP;部门:null;范围:系统级;'),
  (457,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:TRUCK_TYPE_LOOKUP;部门:null;范围:系统级;'),
  (458,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:TRUCK_INFO_LOOKUP;部门:null;范围:系统级;'),
  (459,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:司机信息带回;部门:null;范围:系统级;'),
  (460,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:货车类型带回;部门:null;范围:系统级;'),
  (461,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:物流公司带回;部门:null;范围:系统级;'),
  (462,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:TRUCK_INFO_VIEW;部门:null;范围:null;'),
  (463,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:货车信息表带回;部门:null;范围:系统级;'),
  (464,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:DRIVER_INFO_VIEW;部门:null;范围:null;'),
  (465,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:HIUSER_VIEW;部门:null;范围:null;'),
  (466,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:货车信息表查看;部门:null;范围:null;'),
  (467,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:LOGISTIC_CORP_VIEW;部门:null;范围:null;'),
  (468,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:司机信息查看;部门:null;范围:null;'),
  (469,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:人员查看;部门:null;范围:null;'),
  (470,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:TRUCK_INFO_SAVE;部门:null;范围:null;'),
  (471,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:DRIVER_INFO_SAVE;部门:null;范围:null;'),
  (472,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:物流公司查看;部门:null;范围:null;'),
  (473,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:HIUSER_SAVE;部门:null;范围:null;'),
  (474,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:司机信息保存;部门:null;范围:null;'),
  (475,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:人员保存;部门:null;范围:null;'),
  (476,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:货车信息表保存;部门:null;范围:null;'),
  (477,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:LOGISTIC_CORP_SAVE;部门:null;范围:null;'),
  (478,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:DRIVER_INFO_DEL;部门:null;范围:null;'),
  (479,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:物流公司保存;部门:null;范围:null;'),
  (480,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:TRUCK_INFO_DEL;部门:null;范围:null;'),
  (481,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:HIUSER_DEL;部门:null;范围:null;'),
  (482,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:人员删除;部门:null;范围:null;'),
  (483,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:货车信息表删除;部门:null;范围:null;'),
  (484,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:LOGISTIC_CORP_DEL;部门:null;范围:null;'),
  (485,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:HIUSER_LOOKUP;部门:null;范围:系统级;'),
  (486,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:物流公司删除;部门:null;范围:null;'),
  (487,0,1,'2013-07-16 16:26:23','保存用户权限','数据信息:插入[id:null;用户:平台管理员;权限:人员带回;部门:null;范围:系统级;'),
  (488,0,1,'2013-07-16 16:26:23','保存角色权限','数据信息:插入[id:null;角色名称:平台管理;用户:LOGISTIC_CORP_LOOKUP;部门:null;范围:系统级;');
COMMIT;

#
# Data for the `hi_org` table  (LIMIT -492,500)
#

INSERT INTO `hi_org` (`id`, `version`, `orgName`, `orgNum`, `manager`, `parentOrg`, `address`, `description`, `creator`, `deleted`) VALUES 
  (3,0,'运营部','',NULL,NULL,'','',1,0),
  (6,1,'物流公司','',NULL,3,'','',1,0),
  (7,6,'蓝焰物流','',NULL,6,'无论是长途汽车，还是城市内的公交车，都是人流密集的地方。如何加强司乘人员的管理、提升车辆运','一种面向3G网络车辆监控的云计算系统',1,0),
  (8,0,'大道物流','',NULL,6,'','',1,0),
  (9,0,'公众物流',NULL,NULL,6,'Copyright © 2011 J-Hi研发团队','1',9,0),
  (10,0,'物流公司2',NULL,NULL,6,'1','',9,0),
  (11,2,'一个物流',NULL,NULL,6,'','',9,1);
COMMIT;

#
# Data for the `hi_privilegeresource` table  (LIMIT -391,500)
#

INSERT INTO `hi_privilegeresource` (`id`, `version`, `authorityName`, `viewLayer`, `veiwExtAuthNames`, `businessLayer`, `bizExtAuthNames`) VALUES 
  (1000,0,'HIORG_LIST','/hiOrgList.action',NULL,'org.hi.base.organization.service.HiOrgManager.getSecurityHiOrgList','HIORG_LOOKUP'),
  (1001,0,'HIORG_VIEW','/hiOrgView.action',NULL,'org.hi.base.organization.service.HiOrgManager.getSecurityHiOrgById',NULL),
  (1002,0,'HIORG_SAVE','/hiOrgSave.action',NULL,'org.hi.base.organization.service.HiOrgManager.saveSecurityHiOrg',NULL),
  (1003,0,'HIORG_DEL','/hiOrgRemove.action',NULL,'org.hi.base.organization.service.HiOrgManager.removeSecurityHiOrgById',NULL),
  (1004,0,'HIORG_LOOKUP','/hiOrgLookup.action',NULL,NULL,NULL),
  (1100,0,'HIUSER_LIST','/hiUserList.action',NULL,'org.hi.base.organization.service.HiUserManager.getSecurityHiUserList','HIUSER_LOOKUP'),
  (1101,0,'HIUSER_VIEW','/hiUserView.action',NULL,'org.hi.base.organization.service.HiUserManager.getSecurityHiUserById',NULL),
  (1102,0,'HIUSER_SAVE','/hiUserSave.action',NULL,'org.hi.base.organization.service.HiUserManager.saveSecurityHiUser',NULL),
  (1103,0,'HIUSER_DEL','/hiUserRemove.action',NULL,'org.hi.base.organization.service.HiUserManager.removeSecurityHiUserById',NULL),
  (1104,0,'HIUSER_LOOKUP','/hiUserLookup.action',NULL,NULL,NULL),
  (2100,0,'ROLE_LIST','/roleList.action',NULL,'org.hi.framework.security.service.RoleManager.getSecurityRoleList','ROLE_LOOKUP'),
  (2101,0,'ROLE_VIEW','/roleView.action',NULL,'org.hi.framework.security.service.RoleManager.getSecurityRoleById',NULL),
  (2102,0,'ROLE_SAVE','/roleSave.action',NULL,'org.hi.framework.security.service.RoleManager.saveSecurityRole',NULL),
  (2103,0,'ROLE_DEL','/roleRemove.action',NULL,'org.hi.framework.security.service.RoleManager.removeSecurityRoleById',NULL),
  (2104,0,'ROLE_LOOKUP','/roleLookup.action',NULL,NULL,NULL),
  (2300,0,'USERAUTHORITY_LIST','/userAuthorityList.action',NULL,'org.hi.framework.security.service.UserAuthorityManager.getSecurityUserAuthorityList','USERAUTHORITY_LOOKUP'),
  (2301,0,'USERAUTHORITY_VIEW','/userAuthorityView.action',NULL,'org.hi.framework.security.service.UserAuthorityManager.getSecurityUserAuthorityById',NULL),
  (2302,0,'USERAUTHORITY_SAVE','/userAuthoritySave.action',NULL,'org.hi.framework.security.service.UserAuthorityManager.saveSecurityUserAuthority',NULL),
  (2303,0,'USERAUTHORITY_DEL','/userAuthorityRemove.action',NULL,'org.hi.framework.security.service.UserAuthorityManager.removeSecurityUserAuthorityById',NULL),
  (2304,0,'USERAUTHORITY_LOOKUP','/userAuthorityLookup.action',NULL,NULL,NULL),
  (2800,0,'PRIVILEGERESOURCE_LIST','/privilegeResourceList.action',NULL,'org.hi.framework.security.service.PrivilegeResourceManager.getSecurityPrivilegeResourceList','PRIVILEGERESOURCE_LOOKUP'),
  (2801,0,'PRIVILEGERESOURCE_VIEW','/privilegeResourceView.action',NULL,'org.hi.framework.security.service.PrivilegeResourceManager.getSecurityPrivilegeResourceById',NULL),
  (2802,0,'PRIVILEGERESOURCE_SAVE','/privilegeResourceSave.action',NULL,'org.hi.framework.security.service.PrivilegeResourceManager.saveSecurityPrivilegeResource',NULL),
  (2803,0,'PRIVILEGERESOURCE_DEL','/privilegeResourceRemove.action',NULL,'org.hi.framework.security.service.PrivilegeResourceManager.removeSecurityPrivilegeResourceById',NULL),
  (2804,0,'PRIVILEGERESOURCE_LOOKUP','/privilegeResourceLookup.action',NULL,NULL,NULL),
  (3000,0,'ENUMERATION_LIST','/enumerationList.action',NULL,'org.hi.base.enumeration.service.EnumerationManager.getSecurityEnumerationList','ENUMERATION_LOOKUP'),
  (3001,0,'ENUMERATION_VIEW','/enumerationView.action',NULL,'org.hi.base.enumeration.service.EnumerationManager.getSecurityEnumerationById',NULL),
  (3002,0,'ENUMERATION_SAVE','/enumerationSave.action',NULL,'org.hi.base.enumeration.service.EnumerationManager.saveSecurityEnumeration',NULL),
  (3003,0,'ENUMERATION_DEL','/enumerationRemove.action',NULL,'org.hi.base.enumeration.service.EnumerationManager.removeSecurityEnumerationById',NULL),
  (3004,0,'ENUMERATION_LOOKUP','/enumerationLookup.action',NULL,NULL,NULL),
  (3100,0,'ENUMERATIONVALUE_LIST','/enumerationValueList.action',NULL,'org.hi.base.enumeration.service.EnumerationValueManager.getSecurityEnumerationValueList','ENUMERATIONVALUE_LOOKUP'),
  (3101,0,'ENUMERATIONVALUE_VIEW','/enumerationValueView.action',NULL,'org.hi.base.enumeration.service.EnumerationValueManager.getSecurityEnumerationValueById',NULL),
  (3102,0,'ENUMERATIONVALUE_SAVE','/enumerationValueSave.action',NULL,'org.hi.base.enumeration.service.EnumerationValueManager.saveSecurityEnumerationValue',NULL),
  (3103,0,'ENUMERATIONVALUE_DEL','/enumerationValueRemove.action',NULL,'org.hi.base.enumeration.service.EnumerationValueManager.removeSecurityEnumerationValueById',NULL),
  (3104,0,'ENUMERATIONVALUE_LOOKUP','/enumerationValueLookup.action',NULL,NULL,NULL),
  (4000,0,'JOBDETAILDEF_LIST','/jobDetailDefList.action',NULL,'org.hi.base.schedule.service.JobDetailDefManager.getSecurityJobDetailDefList','JOBDETAILDEF_LOOKUP'),
  (4001,0,'JOBDETAILDEF_VIEW','/jobDetailDefView.action',NULL,'org.hi.base.schedule.service.JobDetailDefManager.getSecurityJobDetailDefById',NULL),
  (4002,0,'JOBDETAILDEF_SAVE','/jobDetailDefSave.action',NULL,'org.hi.base.schedule.service.JobDetailDefManager.saveSecurityJobDetailDef',NULL),
  (4003,0,'JOBDETAILDEF_DEL','/jobDetailDefRemove.action',NULL,'org.hi.base.schedule.service.JobDetailDefManager.removeSecurityJobDetailDefById',NULL),
  (4004,0,'JOBDETAILDEF_LOOKUP','/jobDetailDefLookup.action',NULL,NULL,NULL),
  (4100,0,'TRIGGERDEF_LIST','/triggerDefList.action',NULL,'org.hi.base.schedule.service.TriggerDefManager.getSecurityTriggerDefList','TRIGGERDEF_LOOKUP'),
  (4101,0,'TRIGGERDEF_VIEW','/triggerDefView.action',NULL,'org.hi.base.schedule.service.TriggerDefManager.getSecurityTriggerDefById',NULL),
  (4102,0,'TRIGGERDEF_SAVE','/triggerDefSave.action',NULL,'org.hi.base.schedule.service.TriggerDefManager.saveSecurityTriggerDef',NULL),
  (4103,0,'TRIGGERDEF_DEL','/triggerDefRemove.action',NULL,'org.hi.base.schedule.service.TriggerDefManager.removeSecurityTriggerDefById',NULL),
  (4104,0,'TRIGGERDEF_LOOKUP','/triggerDefLookup.action',NULL,NULL,NULL),
  (5000,0,'APPSETTING_LIST','/appSettingList.action',NULL,'org.hi.base.sysapp.service.AppSettingManager.getSecurityAppSettingList','APPSETTING_LOOKUP'),
  (5001,0,'APPSETTING_VIEW','/appSettingView.action',NULL,'org.hi.base.sysapp.service.AppSettingManager.getSecurityAppSettingById',NULL),
  (5002,0,'APPSETTING_SAVE','/appSettingSave.action',NULL,'org.hi.base.sysapp.service.AppSettingManager.saveSecurityAppSetting',NULL),
  (5003,0,'APPSETTING_DEL','/appSettingRemove.action',NULL,'org.hi.base.sysapp.service.AppSettingManager.removeSecurityAppSettingById',NULL),
  (5004,0,'APPSETTING_LOOKUP','/appSettingLookup.action',NULL,NULL,NULL),
  (5100,0,'MESSAGE_LIST','/messageList.action',NULL,'org.hi.base.sysapp.service.MessageManager.getSecurityMessageList','MESSAGE_LOOKUP'),
  (5101,0,'MESSAGE_VIEW','/messageView.action',NULL,'org.hi.base.sysapp.service.MessageManager.getSecurityMessageById',NULL),
  (5102,0,'MESSAGE_SAVE','/messageSave.action',NULL,'org.hi.base.sysapp.service.MessageManager.saveSecurityMessage',NULL),
  (5103,0,'MESSAGE_DEL','/messageRemove.action',NULL,'org.hi.base.sysapp.service.MessageManager.removeSecurityMessageById',NULL),
  (5104,0,'MESSAGE_LOOKUP','/messageLookup.action',NULL,NULL,NULL),
  (5200,0,'MESSAGEPARAMETER_LIST','/messageParameterList.action',NULL,'org.hi.base.sysapp.service.MessageParameterManager.getSecurityMessageParameterList','MESSAGEPARAMETER_LOOKUP'),
  (5201,0,'MESSAGEPARAMETER_VIEW','/messageParameterView.action',NULL,'org.hi.base.sysapp.service.MessageParameterManager.getSecurityMessageParameterById',NULL),
  (5202,0,'MESSAGEPARAMETER_SAVE','/messageParameterSave.action',NULL,'org.hi.base.sysapp.service.MessageParameterManager.saveSecurityMessageParameter',NULL),
  (5203,0,'MESSAGEPARAMETER_DEL','/messageParameterRemove.action',NULL,'org.hi.base.sysapp.service.MessageParameterManager.removeSecurityMessageParameterById',NULL),
  (5204,0,'MESSAGEPARAMETER_LOOKUP','/messageParameterLookup.action',NULL,NULL,NULL),
  (5400,0,'HILOG_LIST','/hiLogList.action',NULL,'org.hi.base.sysapp.service.HiLogManager.getSecurityHiLogList','HILOG_LOOKUP'),
  (5401,0,'HILOG_VIEW','/hiLogView.action',NULL,'org.hi.base.sysapp.service.HiLogManager.getSecurityHiLogById',NULL),
  (5403,0,'HILOG_DEL','/hiLogRemove.action',NULL,'org.hi.base.sysapp.service.HiLogManager.removeSecurityHiLogById',NULL),
  (10000,0,'MENU_LIST','/menuList.action',NULL,'org.hi.base.menu.service.MenuManager.getSecurityMenuList','MENU_LOOKUP'),
  (10001,0,'MENU_VIEW','/menuView.action',NULL,'org.hi.base.menu.service.MenuManager.getSecurityMenuById',NULL),
  (10002,0,'MENU_SAVE','/menuSave.action',NULL,'org.hi.base.menu.service.MenuManager.saveSecurityMenu',NULL),
  (10003,0,'MENU_DEL','/menuRemove.action',NULL,'org.hi.base.menu.service.MenuManager.removeSecurityMenuById',NULL),
  (10004,0,'MENU_LOOKUP','/menuLookup.action',NULL,NULL,NULL),
  (10100,0,'MENULINK_LIST','/menuLinkList.action',NULL,'org.hi.base.menu.service.MenuLinkManager.getSecurityMenuLinkList','MENULINK_LOOKUP'),
  (10101,0,'MENULINK_VIEW','/menuLinkView.action',NULL,'org.hi.base.menu.service.MenuLinkManager.getSecurityMenuLinkById',NULL),
  (10102,0,'MENULINK_SAVE','/menuLinkSave.action',NULL,'org.hi.base.menu.service.MenuLinkManager.saveSecurityMenuLink',NULL),
  (10103,0,'MENULINK_DEL','/menuLinkRemove.action',NULL,'org.hi.base.menu.service.MenuLinkManager.removeSecurityMenuLinkById',NULL),
  (10104,0,'MENULINK_LOOKUP','/menuLinkLookup.action',NULL,NULL,NULL),
  (12000,0,'EXCELREPORTDESIGN_LIST','/excelReportDesignList.action',NULL,'org.hi.base.report.excel.service.ExcelReportDesignManager.getSecurityExcelReportDesignList','EXCELREPORTDESIGN_LOOKUP'),
  (12001,0,'EXCELREPORTDESIGN_VIEW','/excelReportDesignView.action',NULL,'org.hi.base.report.excel.service.ExcelReportDesignManager.getSecurityExcelReportDesignById',NULL),
  (12002,0,'EXCELREPORTDESIGN_SAVE','/excelReportDesignSave.action',NULL,'org.hi.base.report.excel.service.ExcelReportDesignManager.saveSecurityExcelReportDesign',NULL),
  (12003,0,'EXCELREPORTDESIGN_DEL','/excelReportDesignRemove.action',NULL,'org.hi.base.report.excel.service.ExcelReportDesignManager.removeSecurityExcelReportDesignById',NULL),
  (12004,0,'EXCELREPORTDESIGN_LOOKUP','/excelReportDesignLookup.action',NULL,NULL,NULL),
  (12100,0,'EXCELSHEET_LIST','/excelSheetList.action',NULL,'org.hi.base.report.excel.service.ExcelSheetManager.getSecurityExcelSheetList','EXCELSHEET_LOOKUP'),
  (12101,0,'EXCELSHEET_VIEW','/excelSheetView.action',NULL,'org.hi.base.report.excel.service.ExcelSheetManager.getSecurityExcelSheetById',NULL),
  (12102,0,'EXCELSHEET_SAVE','/excelSheetSave.action',NULL,'org.hi.base.report.excel.service.ExcelSheetManager.saveSecurityExcelSheet',NULL),
  (12103,0,'EXCELSHEET_DEL','/excelSheetRemove.action',NULL,'org.hi.base.report.excel.service.ExcelSheetManager.removeSecurityExcelSheetById',NULL),
  (12104,0,'EXCELSHEET_LOOKUP','/excelSheetLookup.action',NULL,NULL,NULL),
  (12200,0,'EXCELCELL_LIST','/excelCellList.action',NULL,'org.hi.base.report.excel.service.ExcelCellManager.getSecurityExcelCellList','EXCELCELL_LOOKUP'),
  (12201,0,'EXCELCELL_VIEW','/excelCellView.action',NULL,'org.hi.base.report.excel.service.ExcelCellManager.getSecurityExcelCellById',NULL),
  (12202,0,'EXCELCELL_SAVE','/excelCellSave.action',NULL,'org.hi.base.report.excel.service.ExcelCellManager.saveSecurityExcelCell',NULL),
  (12203,0,'EXCELCELL_DEL','/excelCellRemove.action',NULL,'org.hi.base.report.excel.service.ExcelCellManager.removeSecurityExcelCellById',NULL),
  (12204,0,'EXCELCELL_LOOKUP','/excelCellLookup.action',NULL,NULL,NULL),
  (100100,0,'TRUCK_TYPE_LIST','/truck_typeList.action',NULL,'org.logistic.basicinfo.service.Truck_typeManager.getSecurityTruck_typeList','TRUCK_TYPE_LOOKUP'),
  (100101,0,'TRUCK_TYPE_VIEW','/truck_typeView.action',NULL,'org.logistic.basicinfo.service.Truck_typeManager.getSecurityTruck_typeById',NULL),
  (100102,0,'TRUCK_TYPE_SAVE','/truck_typeSave.action',NULL,'org.logistic.basicinfo.service.Truck_typeManager.saveSecurityTruck_type',NULL),
  (100103,0,'TRUCK_TYPE_DEL','/truck_typeRemove.action',NULL,'org.logistic.basicinfo.service.Truck_typeManager.removeSecurityTruck_typeById',NULL),
  (100104,0,'TRUCK_TYPE_LOOKUP','/truck_typeLookup.action',NULL,NULL,NULL),
  (100200,0,'DRIVER_INFO_LIST','/driver_infoList.action',NULL,'org.logistic.basicinfo.service.Driver_infoManager.getSecurityDriver_infoList','DRIVER_INFO_LOOKUP'),
  (100201,0,'DRIVER_INFO_VIEW','/driver_infoView.action',NULL,'org.logistic.basicinfo.service.Driver_infoManager.getSecurityDriver_infoById',NULL),
  (100202,0,'DRIVER_INFO_SAVE','/driver_infoSave.action',NULL,'org.logistic.basicinfo.service.Driver_infoManager.saveSecurityDriver_info',NULL),
  (100203,0,'DRIVER_INFO_DEL','/driver_infoRemove.action',NULL,'org.logistic.basicinfo.service.Driver_infoManager.removeSecurityDriver_infoById',NULL),
  (100204,0,'DRIVER_INFO_LOOKUP','/driver_infoLookup.action',NULL,NULL,NULL),
  (100300,0,'TRUCK_INFO_LIST','/truck_infoList.action',NULL,'org.logistic.basicinfo.service.Truck_infoManager.getSecurityTruck_infoList','TRUCK_INFO_LOOKUP'),
  (100301,0,'TRUCK_INFO_VIEW','/truck_infoView.action',NULL,'org.logistic.basicinfo.service.Truck_infoManager.getSecurityTruck_infoById',NULL),
  (100302,0,'TRUCK_INFO_SAVE','/truck_infoSave.action',NULL,'org.logistic.basicinfo.service.Truck_infoManager.saveSecurityTruck_info',NULL),
  (100303,0,'TRUCK_INFO_DEL','/truck_infoRemove.action',NULL,'org.logistic.basicinfo.service.Truck_infoManager.removeSecurityTruck_infoById',NULL),
  (100304,0,'TRUCK_INFO_LOOKUP','/truck_infoLookup.action',NULL,NULL,NULL),
  (100500,0,'LOGISTIC_CORP_LIST','/logistic_corpList.action',NULL,'org.logistic.basicinfo.service.Logistic_corpManager.getSecurityLogistic_corpList','LOGISTIC_CORP_LOOKUP'),
  (100501,0,'LOGISTIC_CORP_VIEW','/logistic_corpView.action',NULL,'org.logistic.basicinfo.service.Logistic_corpManager.getSecurityLogistic_corpById',NULL),
  (100502,0,'LOGISTIC_CORP_SAVE','/logistic_corpSave.action',NULL,'org.logistic.basicinfo.service.Logistic_corpManager.saveSecurityLogistic_corp',NULL),
  (100503,0,'LOGISTIC_CORP_DEL','/logistic_corpRemove.action',NULL,'org.logistic.basicinfo.service.Logistic_corpManager.removeSecurityLogistic_corpById',NULL),
  (100504,0,'LOGISTIC_CORP_LOOKUP','/logistic_corpLookup.action',NULL,NULL,NULL);
COMMIT;

#
# Data for the `hi_role` table  (LIMIT -497,500)
#

INSERT INTO `hi_role` (`id`, `version`, `roleName`, `displayRef`, `description`, `creator`) VALUES 
  (1,0,'平台管理','平台管理','平台管理',1),
  (2,0,'物流公司管理','物流公司管理','物流公司管理',1);
COMMIT;

#
# Data for the `hi_roleauthority` table  (LIMIT -470,500)
#

INSERT INTO `hi_roleauthority` (`id`, `version`, `role`, `authority`, `org`, `scope`) VALUES 
  (115,0,2,100505,NULL,2901),
  (116,0,2,100201,NULL,NULL),
  (117,0,2,100202,NULL,NULL),
  (118,0,2,100203,NULL,NULL),
  (119,0,2,100204,NULL,2901),
  (120,0,2,100301,NULL,NULL),
  (121,0,2,100302,NULL,NULL),
  (122,0,2,100303,NULL,NULL),
  (123,0,2,100304,NULL,2901),
  (124,0,2,100501,NULL,NULL),
  (125,0,2,100504,NULL,2901),
  (135,0,1,100506,NULL,2904),
  (136,0,1,100104,NULL,2904),
  (137,0,1,100201,NULL,NULL),
  (138,0,1,100202,NULL,NULL),
  (139,0,1,100203,NULL,NULL),
  (140,0,1,100204,NULL,2904),
  (141,0,1,100301,NULL,NULL),
  (142,0,1,100302,NULL,NULL),
  (143,0,1,100303,NULL,NULL),
  (144,0,1,100304,NULL,2904),
  (145,0,1,100501,NULL,NULL),
  (146,0,1,100502,NULL,NULL),
  (147,0,1,100503,NULL,NULL),
  (148,0,1,100504,NULL,2904),
  (149,0,1,1101,NULL,NULL),
  (150,0,1,1102,NULL,NULL),
  (151,0,1,1103,NULL,NULL),
  (152,0,1,1104,NULL,2904);
COMMIT;

#
# Data for the `hi_user` table  (LIMIT -488,500)
#

INSERT INTO `hi_user` (`id`, `version`, `userName`, `password`, `country`, `timeZone`, `accountEnabled`, `accountLocked`, `expiredDate`, `credentialsExpired`, `fullName`, `org`, `gender`, `address`, `phone`, `mobile`, `zip`, `SSN`, `mail`, `userMgrType`, `notifyMode`, `description`, `creator`, `deleted`) VALUES 
  (1,52,'sa','c12e01f2a13ff5587e1e9e4aedb8242d',NULL,NULL,NULL,NULL,NULL,NULL,'administrator',1,NULL,'ss12','12','','',NULL,'',1400,NULL,NULL,NULL,0),
  (6,1,'1','c4ca4238a0b923820dcc509a6f75849b',1,NULL,3200,3201,NULL,NULL,'1',7,1200,'','','','','','',1402,'','',1,0),
  (7,1,'2','c81e728d9d4c2f636f067f89cc14862c',1,NULL,3200,3201,NULL,NULL,'2',8,1200,'','','','','','',1402,'','',1,0),
  (9,1,'admin','21232f297a57a5a743894a0e4a801fc3',1,NULL,3200,3201,NULL,NULL,'平台管理员',3,1200,'','','','','','',1401,'','',1,0),
  (10,3,'3','eccbc87e4b5ce2fe28308fd9f2a7baf3',1,NULL,3200,3201,NULL,NULL,'3',7,1200,'','','','','','',1402,'','',1,0),
  (11,2,'18988888888',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'zhangsan',7,NULL,NULL,NULL,NULL,NULL,'887',NULL,1403,NULL,NULL,6,0),
  (13,2,'89106',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6',7,NULL,NULL,NULL,NULL,NULL,'999',NULL,1403,NULL,NULL,6,0),
  (14,0,'18988888889',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'二二热',7,NULL,NULL,NULL,NULL,NULL,'热天',NULL,1403,NULL,NULL,6,0),
  (15,3,'11234','',NULL,NULL,0,0,NULL,0,'多发点',NULL,1200,'','',NULL,NULL,'2',NULL,1403,NULL,'',1,0),
  (16,4,'4','a87ff679a2f3e71d9181a67b7542122c',NULL,NULL,NULL,NULL,NULL,NULL,'wwqee1',8,1200,'','','','','','',1402,NULL,'',9,1),
  (17,0,'5','e4da3b7fbbce2345d7772b0674a318d5',NULL,NULL,NULL,NULL,NULL,NULL,'5',7,1200,'','','','','','',1402,NULL,'',16,0);
COMMIT;

#
# Data for the `hi_userauthority` table  (LIMIT -437,500)
#

INSERT INTO `hi_userauthority` (`id`, `version`, `securityUser`, `authority`, `org`, `scope`, `roleAuthority`) VALUES 
  (181,0,6,100505,NULL,2901,115),
  (182,0,7,100505,NULL,2901,115),
  (183,0,10,100505,NULL,2901,115),
  (184,0,6,100201,NULL,NULL,116),
  (185,0,7,100201,NULL,NULL,116),
  (186,0,10,100201,NULL,NULL,116),
  (187,0,6,100202,NULL,NULL,117),
  (188,0,7,100202,NULL,NULL,117),
  (189,0,10,100202,NULL,NULL,117),
  (190,0,6,100203,NULL,NULL,118),
  (191,0,7,100203,NULL,NULL,118),
  (192,0,10,100203,NULL,NULL,118),
  (193,0,6,100204,NULL,2901,119),
  (194,0,7,100204,NULL,2901,119),
  (195,0,10,100204,NULL,2901,119),
  (196,0,6,100301,NULL,NULL,120),
  (197,0,7,100301,NULL,NULL,120),
  (198,0,10,100301,NULL,NULL,120),
  (199,0,6,100302,NULL,NULL,121),
  (200,0,7,100302,NULL,NULL,121),
  (201,0,10,100302,NULL,NULL,121),
  (202,0,6,100303,NULL,NULL,122),
  (203,0,7,100303,NULL,NULL,122),
  (204,0,10,100303,NULL,NULL,122),
  (205,0,6,100304,NULL,2901,123),
  (206,0,7,100304,NULL,2901,123),
  (207,0,10,100304,NULL,2901,123),
  (208,0,6,100501,NULL,NULL,124),
  (209,0,7,100501,NULL,NULL,124),
  (210,0,10,100501,NULL,NULL,124),
  (211,0,6,100504,NULL,2901,125),
  (212,0,7,100504,NULL,2901,125),
  (213,0,10,100504,NULL,2901,125),
  (243,0,17,100505,NULL,2901,115),
  (244,0,17,100201,NULL,NULL,116),
  (245,0,17,100202,NULL,NULL,117),
  (246,0,17,100203,NULL,NULL,118),
  (247,0,17,100204,NULL,2901,119),
  (248,0,17,100301,NULL,NULL,120),
  (249,0,17,100302,NULL,NULL,121),
  (250,0,17,100303,NULL,NULL,122),
  (251,0,17,100304,NULL,2901,123),
  (252,0,17,100501,NULL,NULL,124),
  (253,0,17,100504,NULL,2901,125),
  (254,0,9,100506,NULL,2904,135),
  (255,0,9,100104,NULL,2904,136),
  (256,0,9,100201,NULL,NULL,137),
  (257,0,9,100202,NULL,NULL,138),
  (258,0,9,100203,NULL,NULL,139),
  (259,0,9,100204,NULL,2904,140),
  (260,0,9,100301,NULL,NULL,141),
  (261,0,9,100302,NULL,NULL,142),
  (262,0,9,100303,NULL,NULL,143),
  (263,0,9,100304,NULL,2904,144),
  (264,0,9,100501,NULL,NULL,145),
  (265,0,9,100502,NULL,NULL,146),
  (266,0,9,100503,NULL,NULL,147),
  (267,0,9,100504,NULL,2904,148),
  (268,0,9,1101,NULL,NULL,149),
  (269,0,9,1102,NULL,NULL,150),
  (270,0,9,1103,NULL,NULL,151),
  (271,0,9,1104,NULL,2904,152);
COMMIT;

#
# Data for the `hi_userrole` table  (LIMIT -494,500)
#

INSERT INTO `hi_userrole` (`id`, `version`, `securityUser`, `role`) VALUES 
  (1,0,9,1),
  (2,0,6,2),
  (3,0,7,2),
  (4,0,10,2),
  (7,0,17,2);
COMMIT;

#
# Data for the `himenu` table  (LIMIT -488,500)
#

INSERT INTO `himenu` (`id`, `version`, `menuName`, `displayRef`, `description`, `parentMenu`, `sequence`, `menuType`, `creator`) VALUES 
  (1000,0,'organization','部门管理','部门管理',0,9999.00,NULL,0),
  (2000,0,'security','安全管理','安全管理',0,9999.00,NULL,0),
  (3000,0,'enumeration','枚举管理','枚举管理',0,9999.00,NULL,0),
  (5000,0,'sysapp','系统管理','系统管理',0,9999.00,NULL,0),
  (10000,0,'menu','菜单管理','菜单管理',0,9999.00,NULL,0),
  (12000,0,'report','报表管理','报表管理',0,9999.00,NULL,0),
  (42000,0,'i18n','国际化','国际化',0,9999.00,NULL,0),
  (100000,0,'basicinfo','基本信息','基本信息',0,9999.00,NULL,0),
  (100001,3,'logistic_corp_m','物流公司','物流公司',NULL,2.00,0,1),
  (100002,0,'info_m','信息管理','信息管理',NULL,1.00,0,1),
  (100003,0,'person','个人设置','个人设置',0,1.00,0,1);
COMMIT;

#
# Data for the `logistic_corp` table  (LIMIT -494,500)
#

INSERT INTO `logistic_corp` (`id`, `address`, `op_lic`, `op_lic_file_attachment`, `tax_lic`, `tax_lic_file_attachment`, `corp_name`, `corp_phone`, `corp_mobile`, `info_state`, `creator`) VALUES 
  (7,'无论是长途汽车，还是城市内的公交车，都是人流密集的地方。如何加强司乘人员的管理、提升车辆运','无论是长途汽',5,'123331',6,'adf','51255555','18907282564',100601,1),
  (8,'','',NULL,'',NULL,'','','',100600,1),
  (9,'Copyright © 2011 J-Hi研发团队','Copyright © 2011 J-Hi研发团队',NULL,'1',NULL,'1','88888888','18999999999',100601,9),
  (10,'1','1',NULL,'1',NULL,'1','88888888','13999999999',100601,9),
  (11,'','',NULL,'',NULL,'撒旦法而然','','',100601,9);
COMMIT;

#
# Data for the `menulink` table  (LIMIT -458,500)
#

INSERT INTO `menulink` (`id`, `version`, `linkUrl`, `displayRef`, `description`, `authority`, `sequence`, `menu`, `menuLinkType`, `creator`) VALUES 
  (1000,0,'/hiOrgList.action','部门','部门',1000,9999.00,1000,0,0),
  (1100,0,'/hiUserList.action','人员','人员',1100,9999.00,1000,0,0),
  (1200,1,'/personalSettingView.action','个人设置','个人设置',1101,9999.00,100003,0,1),
  (2000,0,'/authorityList.action','权限','权限',2000,9999.00,2000,1,0),
  (2100,0,'/roleList.action','角色','角色',2100,9999.00,2000,0,0),
  (2300,0,'/userAuthorityList.action','用户权限','用户权限',2300,9999.00,2000,0,0),
  (2701,0,'/securityUserList.action','用户管理','用户管理',2999,9999.00,2000,0,0),
  (2800,0,'/privilegeResourceList.action','权限资源','权限资源',2800,9999.00,2000,1,0),
  (3000,0,'/enumerationList.action','枚举实体','枚举实体',3000,9999.00,3000,0,0),
  (3100,0,'/enumerationValueList.action','枚举值','枚举值',3100,9999.00,3000,0,0),
  (4100,0,'/triggerDefList.action','触发器','触发器',4100,9999.00,5000,1,0),
  (5000,0,'/appSettingList.action','应用配置','应用配置',5000,9999.00,5000,0,0),
  (5100,0,'/messageList.action','消息','消息',5100,9999.00,5000,1,0),
  (5400,0,'/hiLogList.action','系统日志','系统日志',5400,9999.00,5000,0,0),
  (10000,0,'/menuList.action','菜单项','菜单项',10000,9999.00,10000,0,0),
  (10100,0,'/menuLinkList.action','菜单链接','菜单链接',10100,9999.00,10000,0,0),
  (12000,0,'/excelReportDesignList.action','Excel报表设计','Excel报表设计',12000,9999.00,12000,0,0),
  (42100,0,'/languageList.action','多语言信息','多语言信息',42100,9999.00,42000,1,0),
  (42200,0,'/languageCodeList.action','语言编码','语言编码',42200,9999.00,42000,1,0),
  (42400,0,'/timezoneList.action','时区','时区',42400,9999.00,42000,1,0),
  (100100,0,'/truck_typeList.action','货车类型','货车类型',100100,9999.00,100000,0,0),
  (100200,0,'/driver_infoList.action','司机信息','司机信息',100200,9999.00,100000,0,0),
  (100300,0,'/truck_infoList.action','货车信息表','货车信息表',100300,9999.00,100000,0,0),
  (100500,0,'/logistic_corpList.action','物流公司','物流公司',100500,9999.00,100000,0,0),
  (100501,2,'/logistic_logistic_corpView.action','公司信息','公司信息',100505,9999.00,100001,0,1),
  (100503,0,'/logistic_truck_infoList.action','货车信息','货车信息',100505,9999.00,100001,0,1),
  (100504,0,'/logistic_driver_infoList.action','司机信息','司机信息',100505,9999.00,100001,0,1),
  (100505,0,'/1','求车信息','求车信息',100505,9999.00,100001,0,1),
  (100506,0,'/1','求货信息','求货信息',100505,9999.00,100001,0,1),
  (100507,0,'/1','承运信息','承运信息',100505,9999.00,100001,0,1),
  (100508,0,'/1','装货记录','装货记录',100505,9999.00,100001,0,1),
  (100509,0,'/1','卸货记录','卸货记录',100505,9999.00,100001,0,1),
  (100510,3,'/sys_logistic_corpList.action','物流公司管理','物流公司管理',100506,9999.00,100002,0,1),
  (100511,0,'/sys_hiUserList.action','管理员信息','管理员信息',100506,9999.00,100002,0,1),
  (100512,0,'/sys_truck_infoList.action','货车信息','货车信息',100506,9999.00,100002,0,1),
  (100513,0,'/2','司机信息','司机信息',100506,9999.00,100002,0,1),
  (100514,0,'/2','求车信息','求车信息',100506,9999.00,100002,0,1),
  (100515,0,'/2','求货信息','求货信息',100506,9999.00,100002,0,1),
  (100516,0,'/2','承运信息','承运信息',100506,9999.00,100002,0,1),
  (100517,0,'/2','装货记录','装货记录',100506,9999.00,100002,0,1),
  (100518,0,'/2','卸货记录','卸货记录',100506,9999.00,100002,0,1);
COMMIT;

#
# Data for the `truck_info` table  (LIMIT -492,500)
#

INSERT INTO `truck_info` (`id`, `version`, `truck_card`, `truck_type`, `truck_length`, `truck_width`, `truck_load`, `insure_id`, `insure_file_attachment`, `truck_state`, `info_state`, `creator`, `deleted`) VALUES 
  (1,6,'a',1,1.00,2.00,'3123','1',NULL,100400,100601,6,1),
  (2,6,'b4',1,4.00,5.00,'6','1',NULL,100400,100600,10,1),
  (3,1,'qw',1,1.00,1.00,'2','',NULL,100400,100601,6,1),
  (4,1,'qwe',1,NULL,NULL,'','',NULL,100400,100601,6,1),
  (5,1,'tee',NULL,NULL,NULL,'','',NULL,100400,100601,6,1),
  (6,1,'as',NULL,NULL,NULL,'','',NULL,100400,NULL,6,0),
  (7,2,'ewewr',1,1.00,1.00,'2','3',NULL,100400,NULL,6,0);
COMMIT;

#
# Data for the `truck_type` table  (LIMIT -498,500)
#

INSERT INTO `truck_type` (`id`, `version`, `truck_type_name`, `creator`, `deleted`) VALUES 
  (1,0,'前八轮',1,0);
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;