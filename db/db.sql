insert into regiongb select * from regiongb_copy;
insert into truck_type select * from truck_type_copy;
insert into goods_type select * from goods_type_copy;

ALTER TABLE `goods_info`
CHANGE COLUMN `publishTime` `publishTime`  timestamp Not NULL DEFAULT CURRENT_TIMESTAMP AFTER `version`;

ALTER TABLE `hi_roleauthority`
ADD COLUMN `privilegeProcessor`  varchar(200) NULL AFTER `scope`;

ALTER TABLE `hi_userauthority`
ADD COLUMN `privilegeProcessor`  varchar(200) NULL AFTER `roleAuthority`;

ALTER TABLE `driver2goods` ADD CONSTRAINT `fk_dg_driver` FOREIGN KEY (`driver`) REFERENCES `driver_info` (`id`);

ALTER TABLE `driver2goods` ADD CONSTRAINT `fk_dg_goods` FOREIGN KEY (`goods_info`) REFERENCES `goods_info` (`id`);

ALTER TABLE `driver2goods_log` ADD CONSTRAINT `fk_dglog_driver` FOREIGN KEY (`driver`) REFERENCES `driver_info` (`id`);

ALTER TABLE `driver2goods_log` ADD CONSTRAINT `fk_dglog_goods` FOREIGN KEY (`goods_info`) REFERENCES `goods_info` (`id`);

ALTER TABLE `driver_info` ADD CONSTRAINT `fk_dv_truck` FOREIGN KEY (`truck_info`) REFERENCES `goods_info` (`id`);

ALTER TABLE `driver_point` ADD CONSTRAINT `fk_dvpt_driver` FOREIGN KEY (`driver_info`) REFERENCES `driver_info` (`id`);

ALTER TABLE `driver_line` ADD CONSTRAINT `fk_dvln_driver` FOREIGN KEY (`driver_info`) REFERENCES `driver_info` (`id`);

ALTER TABLE `goods2owner` ADD CONSTRAINT `fk_gdow_goods` FOREIGN KEY (`goods_info`) REFERENCES `goods_info` (`id`);

ALTER TABLE `goods2owner` ADD CONSTRAINT `fk_gdow_owner` FOREIGN KEY (`owner`) REFERENCES `goods_owner` (`id`);

ALTER TABLE `truck_info` ADD CONSTRAINT `fk_truck_cr` FOREIGN KEY (`creator`) REFERENCES `hi_user` (`id`);

ALTER TABLE `goods_info` ADD CONSTRAINT `fk_goods_cr` FOREIGN KEY (`creator`) REFERENCES `hi_user` (`id`);